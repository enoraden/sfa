package myskysfa.com.sfa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFailedAbsen;
import myskysfa.com.sfa.database.TableMasterMS;
import myskysfa.com.sfa.database.db_adapter.TableMSAdapter;

/**
 * Created by Eno on 6/27/2016.
 */
public class AbsenFailedAutoSenderAdapter extends RecyclerView.Adapter<AbsenFailedAutoSenderAdapter.ViewHolder> {
    private List<TableFailedAbsen> list;
    View itemLayoutView;
    ViewGroup mParent;
    List<TableMasterMS> listStore = new ArrayList<>();
    TableMSAdapter dbModerStore;
    Context context;
    String nameStore;

    public AbsenFailedAutoSenderAdapter(Context context, List<TableFailedAbsen> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mParent = parent;
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.outbox_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableFailedAbsen itemPackage = list.get(position);
        if (itemPackage != null) {
            dbModerStore = new TableMSAdapter(context);
            listStore = dbModerStore.getDatabyCondition(TableMasterMS.FLD_ROWID, itemPackage.getEntity());
            holder.title.setText(listStore.get(0).getName().substring(0, 1).toUpperCase());
            holder.id.setText(listStore.get(0).getName());
            if (itemPackage.getAttendance_type().equals("1")){
                holder.attendenceType.setText("Clock in");
            }else{
                holder.attendenceType.setText("Clock out");
            }
            holder.date.setText(itemPackage.getAttendance_time());
            if (itemPackage.getStatus().equalsIgnoreCase("0")) {
                holder.status.setText("Pending");
            } else {
                holder.status.setText("Success");
            }
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title, id, date, attendenceType;
        private Button status;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemLayoutView.findViewById(R.id.draf_title);
            attendenceType = (TextView) itemLayoutView.findViewById(R.id.type);
            id = (TextView) itemLayoutView.findViewById(R.id.draf_id);
            date = (TextView) itemLayoutView.findViewById(R.id.draf_date);
            status = (Button) itemLayoutView.findViewById(R.id.draf_send);
        }
    }
}
