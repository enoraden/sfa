package myskysfa.com.sfa.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.main.menu.dtd.DTDEmergency;
import myskysfa.com.sfa.main.menu.dtd.DTDPaket;
import myskysfa.com.sfa.main.menu.dtd.DTDPayment;
import myskysfa.com.sfa.main.menu.dtd.DTDProfile;
import myskysfa.com.sfa.main.menu.dtd.DTDStatus;

/**
 * Created by admin on 6/22/2016.
 */
public class DtdTabPagerAdapter extends FragmentPagerAdapter {
    private Context context;
    private String[] titles;

    public DtdTabPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
        titles        = context.getResources().getStringArray(R.array.ms_menu_ft_title);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return DTDProfile.newInstance(context);
            case 1:
                return DTDEmergency.newInstance(context);
            case 2:
                return DTDPaket.newInstance(context);
            case 3:
                return DTDPayment.newInstance(context);
            case 4:
                return DTDStatus.newInstance(context);
        }
        return null;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return titles.length;
    }
}

