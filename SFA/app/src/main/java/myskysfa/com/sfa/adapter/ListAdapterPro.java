package myskysfa.com.sfa.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFreeTrial;

/**
 * Created by Hari Hendryan on 20/08/2015.
 */
public class ListAdapterPro extends BaseAdapter {

    private Activity context;
    private List<TableFreeTrial> list;
    private List<TableFreeTrial> filterd = new ArrayList<TableFreeTrial>();
    private TableFreeTrial item;
    private String ftId, planDate, statusDesc, vc, alamat, value, productName, header, isMulty;
    private TextView _ftId, _planDate, _statusDesc, _productName, _area, icon, _title, _countDay;
    private ImageView imgIcon;

    public ListAdapterPro(Activity context, List<TableFreeTrial> list) {
        this.context = context;
        this.list = list;
        this.filterd = this.list;

    }


    @Override
    public int getCount() {
        return filterd.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return filterd.get(position);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(this.context);
        item = filterd.get(position);
        ftId = item.getFt_id();
        planDate = item.getCreated_date();
        statusDesc = item.getStatus();
        vc = item.getSfl_code();
        //alamat      = item.getProspectName();
        value = item.getValue();
        header = item.getHeader();

        convertView = inflater.inflate(R.layout.free_trial_list_item, parent, false);
        ViewExecute(convertView);

        return convertView;
    }


    private void ViewExecute(View convertView) {
        _ftId = (TextView) convertView.findViewById(R.id.ft_id);
        _planDate = (TextView) convertView.findViewById(R.id.ft_date);
        _statusDesc = (TextView) convertView.findViewById(R.id.ft_status);
        _productName = (TextView) convertView.findViewById(R.id.ft_product);
        _area = (TextView) convertView.findViewById(R.id.ft_address);
        imgIcon = (ImageView) convertView.findViewById(R.id.imageView1);
        icon = (TextView) convertView.findViewById(R.id.txt_icon);
        try {
            JSONObject profObj = new JSONObject(value);
            alamat = profObj.getString("alamat");
            productName = profObj.getString("product_name");
            isMulty = profObj.getString("is_multi");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        _ftId.setText(ftId.toString());
        if (statusDesc.equalsIgnoreCase("1")) {
            _statusDesc.setText("New");
        } else if (statusDesc.equalsIgnoreCase("2")) {
            _statusDesc.setText("PA");
        } else {
            _statusDesc.setText("");
        }

        if (ftId.substring(0, 1).equalsIgnoreCase("p")) {
            imgIcon.setVisibility(View.GONE);
            icon.setVisibility(View.VISIBLE);
            icon.setText("JP");
        } else if (ftId.substring(0, 1).equalsIgnoreCase("T")) {
            imgIcon.setVisibility(View.GONE);
            icon.setVisibility(View.VISIBLE);
            icon.setText("PP");
        } else {
            imgIcon.setVisibility(View.VISIBLE);
            icon.setVisibility(View.GONE);
        }

        if (statusDesc.equalsIgnoreCase("null"))
            statusDesc = "";
        if (statusDesc.equalsIgnoreCase("1")) {
            _statusDesc.setTextColor(Color.parseColor("#01579b"));
        } else if (statusDesc.equalsIgnoreCase("2")
                || statusDesc.equalsIgnoreCase("ep")) {
            _statusDesc.setTextColor(Color.parseColor("#1b5e20"));
        } else {
            _statusDesc.setTextColor(Color.parseColor("#b71c1c"));
        }

        if (isMulty.equals("1")) {
            _productName.setText(productName + " - Multy");
        } else {
            _productName.setText(productName + " - Single");
        }

        _area.setText(alamat);
        _planDate.setText(planDate);

    }
}
