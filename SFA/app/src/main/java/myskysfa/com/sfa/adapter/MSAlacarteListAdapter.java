package myskysfa.com.sfa.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableMasterPackage;
import myskysfa.com.sfa.utils.listItemObject.AlacarteStore;

/**
 * Created by admin on 11/19/2015.
 */
public class MSAlacarteListAdapter extends RecyclerView.Adapter<MSAlacarteListAdapter.ViewHolder> {
    private List<TableMasterPackage> itemsData;
    private ViewHolder viewHolder;
    public AlacarteStore itemAlacarte = new AlacarteStore();

    public MSAlacarteListAdapter(List<TableMasterPackage> itemsData) {
        this.itemsData = itemsData;

    }


    @Override
    public MSAlacarteListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ms_package_alacarte_item, parent,false);
        viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        TableMasterPackage cn = itemsData.get(position);
        viewHolder.txtTitle.setText(cn.getProduct_name());
        viewHolder.txtIcon.setText(cn.getProduct_name().substring(0, 1).toUpperCase());
        viewHolder.chkCarte.setTag(cn.getProduct_id() + "#" + cn.getProduct_name());

        Set<String> hs = new HashSet<>();
        hs.addAll(itemAlacarte.getList());
        itemAlacarte.getList().clear();
        itemAlacarte.getList().addAll(hs);
        viewHolder.chkCarte.setOnCheckedChangeListener(chgAlacarte);

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle;
        public TextView txtIcon;
        public CheckBox chkCarte;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtTitle = (TextView) itemLayoutView.findViewById(R.id.title_alacarte);
            txtIcon = (TextView) itemLayoutView.findViewById(R.id.title_icon_alacarte);
            chkCarte = (CheckBox) itemLayoutView.findViewById(R.id.check_alacarte);
        }

    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }


    private CheckBox.OnCheckedChangeListener chgAlacarte = new CheckBox.OnCheckedChangeListener(){

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            System.out.println(itemAlacarte.getList());
            if(buttonView.isChecked()){
                itemAlacarte.setList(buttonView.getTag().toString());
            }else{
                itemAlacarte.removeAlacarte(buttonView.getTag().toString());
            }
        }
    };

}
