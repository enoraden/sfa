package myskysfa.com.sfa.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableDataMSDTDFailed;

/**
 * Created by Eno on 6/27/2016.
 */
public class MSDTDAutoSender extends RecyclerView.Adapter<MSDTDAutoSender.ViewHolder> {
    private List<TableDataMSDTDFailed> list;
    View itemLayoutView;
    ViewGroup mParent;

    public MSDTDAutoSender(List<TableDataMSDTDFailed> list) {
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mParent = parent;
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.outbox_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableDataMSDTDFailed itemPackage = list.get(position);
        if (itemPackage != null) {
            holder.title.setText(itemPackage.getApp_name().substring(0, 1).toUpperCase());
            holder.id.setText(itemPackage.getForm_no());
            holder.date.setText(itemPackage.getDate());
            holder.appType.setText(itemPackage.getApp_name().toUpperCase());
            if (itemPackage.getStatus().equalsIgnoreCase("0")){
                holder.status.setText("Pending");
            }else {
                holder.status.setText("Success");
            }
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView title, id, date, appType;
        private Button status;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemLayoutView.findViewById(R.id.draf_title);
            id = (TextView) itemLayoutView.findViewById(R.id.draf_id);
            date = (TextView) itemLayoutView.findViewById(R.id.draf_date);
            status = (Button) itemLayoutView.findViewById(R.id.draf_send);
            appType = (TextView) itemLayoutView.findViewById(R.id.type);
        }
    }
}
