package myskysfa.com.sfa.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFreeTrial;

/**
 * Created by admin on 6/27/2016.
 */
public class MSTaskAdapter extends RecyclerView.Adapter<MSTaskAdapter.ViewHolder> {
    private List<TableFreeTrial> list;
    View itemLayoutView;
    ViewGroup mParent;
    private String value;

    public MSTaskAdapter(List<TableFreeTrial> list) {
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mParent = parent;
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ms_task_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableFreeTrial itemPackage = list.get(position);
        if (itemPackage != null) {
            try {
                holder.title.setText(itemPackage.getFt_id().substring(0, 1));
                holder.id.setText(itemPackage.getFt_id());
                value = itemPackage.getValue();
                JSONObject jsonObject = new JSONObject(value);
                holder.address.setText(jsonObject.getString("alamat"));
                //holder.product.setText(jsonObject.getString("product_name"));
                if (jsonObject.getString("is_multi").equals("1")) {
                    holder.product.setText(jsonObject.getString("product_name") + " - " + " - Multy");
                } else {
                    holder.product.setText(jsonObject.getString("product_name") + " - " + " - Single");
                }
                holder.date.setText(itemPackage.getCreated_date());
                if (itemPackage.getStatus().equalsIgnoreCase("1")) {
                    holder.status.setText("EP");
                } else if (itemPackage.getStatus().equalsIgnoreCase("2")) {
                    holder.status.setText("PA");
                } else {
                    holder.status.setText("");
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title, id, address, product, date, status;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemLayoutView.findViewById(R.id.ms_title);
            id = (TextView) itemLayoutView.findViewById(R.id.ms_id);
            address = (TextView) itemLayoutView.findViewById(R.id.ms_address);
            product = (TextView) itemLayoutView.findViewById(R.id.ms_product);
            date = (TextView) itemLayoutView.findViewById(R.id.ms_date);
            status = (TextView) itemLayoutView.findViewById(R.id.ms_status);
        }
    }
}
