package myskysfa.com.sfa.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;

import myskysfa.com.sfa.utils.listItemObject.MainMenuListItem;
import myskysfa.com.sfa.R;

/**
 * Created by admin on 11/10/2015.
 */
public class MainMenuListviewAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<MainMenuListItem> mainMenuItems;

    //Imageload imageLoader = AppController.getInstance().getImageLoader();

    public MainMenuListviewAdapter(Activity activity, ArrayList<MainMenuListItem> mainMenuItems) {
        this.activity = activity;
        this.mainMenuItems = mainMenuItems;
    }

    @Override
    public int getCount() {
        return mainMenuItems.size();
    }

    @Override
    public Object getItem(int i) {
        return mainMenuItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.main_menu_listview,  parent, false);

        TextView title = (TextView)convertView.findViewById(R.id.textView);
        ImageView img = (ImageView)convertView.findViewById(R.id.imageView);
        LinearLayout l = (LinearLayout) convertView.findViewById(R.id.main_menu_listview_lineralayout1);
        ViewGroup.LayoutParams params = l.getLayoutParams();

        // getting movie data for the row
        MainMenuListItem m = mainMenuItems.get(position);

        title.setText(m.getMenuTitle());
        img.setImageResource(m.getMenuImage());

        String fontPath = "fonts/RobotoMedium.ttf";
        Typeface tf = Typeface.createFromAsset(this.activity.getAssets(), fontPath);
        title.setTypeface(tf);

        if( m.getId()==0 ){
            title.setVisibility(View.GONE);
            img.setVisibility(View.GONE);
            l.setBackgroundColor(Color.parseColor("#eeeeee"));
            params.height = 20;
            l.setLayoutParams(params);
            //title.setPadding(0,50,0,50);
            //l.setPadding(0,10,0,10);
        }
        else{
            title.setVisibility(View.VISIBLE);
            img.setVisibility(View.VISIBLE);
            l.setBackgroundColor(Color.WHITE);
            //l.setBackgroundColor(convertView.getResources().getColor(R.color.white));
        }
        return convertView;
    }

}
