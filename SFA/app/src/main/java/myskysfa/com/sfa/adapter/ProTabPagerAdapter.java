package myskysfa.com.sfa.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.main.menu.dtdproject.ProEmergency;
import myskysfa.com.sfa.main.menu.dtdproject.ProPaket;
import myskysfa.com.sfa.main.menu.dtdproject.ProProfile;
import myskysfa.com.sfa.main.menu.dtdproject.ProSign;
import myskysfa.com.sfa.main.menu.dtdproject.ProStatus;

/**
 * Created by Hari Hendryan on 12/17/2015.
 */
public class ProTabPagerAdapter extends FragmentPagerAdapter {
    private Context context;
    private String[] titles;

    public ProTabPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
        titles        = context.getResources().getStringArray(R.array.ms_menu_ft_title);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ProProfile();
                //return ProProfile.newInstance(context);
            case 1:
                return new ProEmergency();
                //return ProEmergency.newInstance(context);
            case 2:
                return new ProPaket();
                //return ProPaket.newInstance(context);
            case 3:
                return new ProSign();
                //return ProSign.newInstance(context);
            case 4:
                return new ProStatus();
                //return ProStatus.newInstance(context);
        }
        return null;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return titles.length;
    }
}
