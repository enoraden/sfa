package myskysfa.com.sfa.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import myskysfa.com.sfa.database.TableMBrand;


/**
 * Created by admin on 12/14/2015.
 */
public class SpinnerBrandAdapter extends ArrayAdapter<TableMBrand> {
    private List<TableMBrand> itemsStatus;
    private Context context;
    private static String fontPath = "fonts/steelfis.TTF";
    Typeface tf;

    public SpinnerBrandAdapter(Context context, int textViewResourceId,
                              List<TableMBrand> itemsStatus) {
        super(context, textViewResourceId, itemsStatus);
        this.context = context;
        this.itemsStatus = itemsStatus;
        tf = Typeface.createFromAsset(context.getAssets(), fontPath);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = new TextView(getContext());
        label.setTextColor(Color.WHITE);
        label.setPadding(10,10,10,10);
        label.setTextSize(50);
        label.setText(itemsStatus.get(position).getBrand_name());
        label.setTypeface(tf);
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.DKGRAY);
        label.setPadding(10,10,10,10);
        label.setText(itemsStatus.get(position).getBrand_name());

        return label;
    }
}
