package myskysfa.com.sfa.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Hari h on 8/1/2016.
 */
public class SpinnerHWAdapter extends ArrayAdapter<String> {

    private List<String> itemsStatus;
    private Context context;

    public SpinnerHWAdapter(Context context, int textViewResourceId,
                            List<String> itemsStatus) {
        super(context, textViewResourceId, itemsStatus);
        this.context = context;
        this.itemsStatus = itemsStatus;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = new TextView(getContext());
        label.setTextColor(Color.DKGRAY);
        label.setPadding(10, 10, 10, 10);
        label.setTextSize(16);
        label.setText(itemsStatus.get(position));
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.DKGRAY);
        label.setPadding(10,10,10,10);
        label.setTextSize(16);
        label.setText(itemsStatus.get(position));

        return label;
    }

}
