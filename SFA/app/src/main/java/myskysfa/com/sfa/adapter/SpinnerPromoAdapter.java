package myskysfa.com.sfa.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import myskysfa.com.sfa.database.TableMasterPromo;

/**
 * Created by admin on 12/11/2015.
 */
public class SpinnerPromoAdapter extends ArrayAdapter<String> {
    private List<String> itemsStatus;
    private Context context;

    public SpinnerPromoAdapter(Context context, int textViewResourceId,
                               List<String> itemsStatus) {
        super(context, textViewResourceId, itemsStatus);
        this.context = context;
        this.itemsStatus = itemsStatus;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = new TextView(getContext());
        label.setTextColor(Color.DKGRAY);
        label.setPadding(10,10,10,10);
        label.setText(itemsStatus.get(position));

        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.DKGRAY);
        label.setPadding(10,10,10,10);
        label.setText(itemsStatus.get(position));

        return label;
    }
}
