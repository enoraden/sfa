package myskysfa.com.sfa.adapter;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.main.menu.dtdproject.ProStatus;
import myskysfa.com.sfa.utils.Utils;
import myskysfa.com.sfa.utils.listItemObject.StatusInfo;

/**
 * Created by Eno on 10/30/2015.
 */
public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.MainViewHolder> {

    private List<StatusInfo> statusinfo;
    private ArrayList<String> list;
    private static final int TYPE_CATEGORY = 1;
    private static final int TYPE_CONTENT = 2;
    private Context _context;
    private Utils utils;
    private Double latitude, longitude;
    private String addressLine;

    public StatusAdapter(Context context, List<StatusInfo> statusinfo, ArrayList<String> list) {
        this.statusinfo     = statusinfo;
        this.list           = list;
        _context            = context;
        utils               = new Utils(context);
        utils.setGeoLocation();
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0) {
            return  TYPE_CATEGORY;
        } else {
            return TYPE_CONTENT;
        }
    }


    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        System.out.println("view: " + viewType);
        switch (viewType){
            case TYPE_CATEGORY:
                return new CategoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.ft_status_tagging, parent,false));
            case TYPE_CONTENT:
                return new ContentViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_status, parent,false));
        }
        return null;
    }

    /*
    @Override
    public StatusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.cardview_status, parent, false);

        return new StatusViewHolder(itemView);
    }*/


    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        if (position==0) {
            String b= list.get(position);
            String[] data    = b.split(";");
            String longitude = data[0];
            String lat       = data[1];
            // -- begin of -- ari
            String address = "-";
            try {
                address = data[2];
            }catch (Exception ex){
            }
            // -- end of -- ari

            final CategoryViewHolder mholder = (CategoryViewHolder) holder;
            mholder.longitude.setText(longitude);
            mholder.lat.setText(lat);
            mholder.address.setText(address);
            mholder.btnLoc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setLocation(mholder.longitude, mholder.lat, mholder.address);
                }
            });
        } else {
            StatusInfo si = statusinfo.get(position-1);
            ContentViewHolder mHolder = (ContentViewHolder) holder;
            if (list.size() == 0
                    ||list.get(position) == null
                    || list.get(position).length() < 1
                    || list.get(position).equalsIgnoreCase("")) {
                mHolder.vFlag.setImageResource(R.drawable.ic_kali);
                mHolder.vBorder.setBackgroundResource(R.drawable.border_item_red);
                mHolder.vKeterangan.setText(si.Keterangan);
                mHolder.vStatus.setText(si.Status);
            } else {
                mHolder.vFlag.setImageResource(R.drawable.ic_centang);
                mHolder.vBorder.setBackgroundResource(R.drawable.border_item_master);
                mHolder.vKeterangan.setText(si.Keterangan);
                mHolder.vStatus.setText("Data Sudah Diambil");
            }
            mHolder.vTitle.setText(si.Title);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class CategoryViewHolder extends MainViewHolder{
        TextView longitude, lat, address, vTitle;
        ImageButton btnLoc;

        public CategoryViewHolder(View v){
            super(v);
            this.longitude  = (TextView) v.findViewById(R.id.longitude);
            this.lat        = (TextView) v.findViewById(R.id.latitude);
            this.address    = (TextView) v.findViewById(R.id.address);
            this.vTitle     = (TextView) v.findViewById(R.id.ststitle);
            this.btnLoc     = (ImageButton) v.findViewById(R.id.ib_location);
        }
    }

    public class ContentViewHolder extends MainViewHolder{
        public TextView vStatus;
        public TextView vKeterangan;
        public ImageView vFlag;
        public TextView vTitle;
        public LinearLayout vBorder;

        public ContentViewHolder(View v){
            super(v);
            vStatus     = (TextView) v.findViewById(R.id.txtStatus);
            vKeterangan = (TextView)  v.findViewById(R.id.vKeterangan);
            vFlag       = (ImageView) v.findViewById(R.id.imageFlag);
            vTitle      = (TextView) v.findViewById(R.id.ststitle);
            vBorder     = (LinearLayout) v.findViewById(R.id.border);
        }
    }

    public class MainViewHolder extends  RecyclerView.ViewHolder {
        public MainViewHolder(View v) {
            super(v);
        }
    }

    private void setLocation(TextView longit, TextView lat, TextView address) {
        String stringLatitude = utils.getLatitude();
        String stringLongitude = utils.getLongitude();
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(_context, Locale.getDefault());
        if (stringLatitude!=null && stringLatitude.length()> 1) {
            latitude  = Double.valueOf(stringLatitude);
            longitude = Double.valueOf(stringLongitude);
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (addresses != null && addresses.size() != 0) {
                addressLine = addresses.get(0).getAddressLine(0);
            } else {
                addressLine = "";
            }
            ProStatus.latitude = latitude;
            ProStatus.longitude  = longitude;
            ProStatus.addressLine = addressLine;
            longit.setText(String.valueOf(longitude));
            lat.setText(String.valueOf(latitude));
            address.setText(String.valueOf(addressLine));
        } else {
            Toast.makeText(_context, "Longitude dan latitude kosong", Toast.LENGTH_SHORT).show();
        }
    }

}
