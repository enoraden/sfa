package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/20/2016.
 */
@DatabaseTable(tableName = "form_app")
public class TableFormApp {

    public static final String TABLE_NAME = "form_app";

    public static final String fFORM_NO = "FORM_NO";
    public static final String fTRANS_NO = "TRANS_NO";
    public static final String fDATE_REG = "DATE_REG";
    public static final String fIDENTITY_TYPE = "IDENTITY_TYPE";
    public static final String fIDENTITY_NO = "IDENTITY_NO";
    public static final String fFIRST_NAME = "FIRST_NAME";
    public static final String fLAST_NAME = "LAST_NAME";
    public static final String fBIRTH_DATE = "BIRTH_DATE";
    public static final String fFORM_STATUS = "FORM_STATUS";
    public static final String fFORM_TYPE = "FORM_TYPE";
    public static final String fUSER_ID = "USER_ID";
    public static final String fVALUES_PROFILE = "VALUES_PROFILE";
    public static final String fVALUES_EMERGENCY = "VALUES_EMERGENCY";
    public static final String fVALUES_PAYMENT = "VALUES_PAYMENT";
    public static final String fVALUES_PACKAGE = "VALUES_PACKAGE";
    public static final String fVALUES_SIGNATURE = "VALUES_SIGNATURE";
    public static final String fVALUES_SIGNBEDA = "VALUES_SIGNBEDA";
    public static final String fVALUES_SIGNMULTY= "VALUES_SIGNMULTY";
    public static final String fVALUES_SIGNACCOUNT= "VALUES_SIGNACCOUNT";
    public static final String fVALUES_SIGNINDOSAT= "VALUES_SIGNINDOSAT";
    public static final String fVALUES_CHECKLIST = "VALUES_CHECKLIST";

    public static final String fCUST_ID = "CUST_ID";

    @DatabaseField(id = true)
    private String FORM_NO;

    @DatabaseField
    private String FORM_TYPE;
    @DatabaseField
    private String USER_ID;
    @DatabaseField
    private String TRANS_NO;
    @DatabaseField
    private String DATE_REG;
    @DatabaseField
    private String IDENTITY_TYPE;
    @DatabaseField
    private String IDENTITY_NO;
    @DatabaseField
    private String FIRST_NAME;
    @DatabaseField
    private String LAST_NAME;
    @DatabaseField
    private String BIRTH_DATE;
    @DatabaseField
    private String VALUES_PROFILE;
    @DatabaseField
    private String VALUES_EMERGENCY;
    @DatabaseField
    private String VALUES_PAYMENT;
    @DatabaseField
    private String VALUES_PACKAGE;
    @DatabaseField
    private String VALUES_SIGNATURE;
    @DatabaseField
    private String VALUES_SIGNBEDA;
    @DatabaseField
    private String VALUES_SIGNMULTY;
    @DatabaseField
    private String VALUES_SIGNACCOUNT;
    @DatabaseField
    private String VALUES_SIGNINDOSAT;
    @DatabaseField
    private String VALUES_CHECKLIST;
    @DatabaseField
    private String FORM_STATUS;
    @DatabaseField
    private String CUST_ID;

    public String getVALUES_SIGNBEDA() {
        return VALUES_SIGNBEDA;
    }

    public void setVALUES_SIGNBEDA(String VALUES_SIGNBEDA) {
        this.VALUES_SIGNBEDA = VALUES_SIGNBEDA;
    }

    public String getVALUES_SIGNMULTY() {
        return VALUES_SIGNMULTY;
    }

    public void setVALUES_SIGNMULTY(String VALUES_SIGNMULTY) {
        this.VALUES_SIGNMULTY = VALUES_SIGNMULTY;
    }

    public void setBIRTH_DATE(String BIRTH_DATE) {
        this.BIRTH_DATE = BIRTH_DATE;
    }

    public String getBIRTH_DATE() {
        return BIRTH_DATE;
    }

    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    public String getCUST_ID() {
        return CUST_ID;
    }

    public void setDATE_REG(String DATE_REG) {
        this.DATE_REG = DATE_REG;
    }

    public String getDATE_REG() {
        return DATE_REG;
    }

    public void setFIRST_NAME(String FIRST_NAME) {
        this.FIRST_NAME = FIRST_NAME;
    }

    public String getFIRST_NAME() {
        return FIRST_NAME;
    }

    public void setFORM_NO(String FORM_NO) {
        this.FORM_NO = FORM_NO;
    }

    public String getFORM_NO() {
        return FORM_NO;
    }

    public void setFORM_STATUS(String FORM_STATUS) {
        this.FORM_STATUS = FORM_STATUS;
    }

    public String getFORM_STATUS() {
        return FORM_STATUS;
    }

    public void setFORM_TYPE(String FORM_TYPE) {
        this.FORM_TYPE = FORM_TYPE;
    }

    public String getFORM_TYPE() {
        return FORM_TYPE;
    }

    public void setIDENTITY_NO(String IDENTITY_NO) {
        this.IDENTITY_NO = IDENTITY_NO;
    }

    public String getIDENTITY_NO() {
        return IDENTITY_NO;
    }

    public void setIDENTITY_TYPE(String IDENTITY_TYPE) {
        this.IDENTITY_TYPE = IDENTITY_TYPE;
    }

    public String getIDENTITY_TYPE() {
        return IDENTITY_TYPE;
    }

    public void setLAST_NAME(String LAST_NAME) {
        this.LAST_NAME = LAST_NAME;
    }

    public String getLAST_NAME() {
        return LAST_NAME;
    }

    public void setTRANS_NO(String TRANS_NO) {
        this.TRANS_NO = TRANS_NO;
    }

    public String getTRANS_NO() {
        return TRANS_NO;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setVALUES_EMERGENCY(String VALUES_EMERGENCY) {
        this.VALUES_EMERGENCY = VALUES_EMERGENCY;
    }

    public String getVALUES_EMERGENCY() {
        return VALUES_EMERGENCY;
    }

    public void setVALUES_PACKAGE(String VALUES_PACKAGE) {
        this.VALUES_PACKAGE = VALUES_PACKAGE;
    }

    public String getVALUES_PACKAGE() {
        return VALUES_PACKAGE;
    }

    public void setVALUES_PAYMENT(String VALUES_PAYMENT) {
        this.VALUES_PAYMENT = VALUES_PAYMENT;
    }

    public String getVALUES_PAYMENT() {
        return VALUES_PAYMENT;
    }

    public void setVALUES_PROFILE(String VALUES_PROFILE) {
        this.VALUES_PROFILE = VALUES_PROFILE;
    }

    public String getVALUES_PROFILE() {
        return VALUES_PROFILE;
    }

    public void setVALUES_SIGNATURE(String VALUES_SIGNATURE) {
        this.VALUES_SIGNATURE = VALUES_SIGNATURE;
    }

    public String getVALUES_SIGNATURE() {
        return VALUES_SIGNATURE;
    }

    public void setVALUES_CHECKLIST(String VALUES_CHECKLIST) {
        this.VALUES_CHECKLIST = VALUES_CHECKLIST;
    }

    public String getVALUES_CHECKLIST() {
        return VALUES_CHECKLIST;
    }

    public String getVALUES_SIGNACCOUNT() {
        return VALUES_SIGNACCOUNT;
    }

    public void setVALUES_SIGNACCOUNT(String VALUES_SIGNACCOUNT) {
        this.VALUES_SIGNACCOUNT = VALUES_SIGNACCOUNT;
    }

    public String getVALUES_SIGNINDOSAT() {
        return VALUES_SIGNINDOSAT;
    }

    public void setVALUES_SIGNINDOSAT(String VALUES_SIGNINDOSAT) {
        this.VALUES_SIGNINDOSAT = VALUES_SIGNINDOSAT;
    }

}
