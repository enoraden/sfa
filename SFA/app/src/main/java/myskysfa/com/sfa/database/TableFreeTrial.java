package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/13/2016.
 */

@DatabaseTable(tableName = "free_trial")
public class TableFreeTrial {

    public static final String KEY_FT_ID = "ft_id";
    public static final String KEY_PLAN_DATE = "plan_date";
    public static final String KEY_STATUS = "status";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_PRODUCT_NAME = "product_name";
    public static final String KEY_REG_DATE = "register_date";
    public static final String KEY_BRAND_CODE = "brand_code";
    public static final String KEY_SFL_CODE = "sfl_code";
    public static final String KEY_CREATED = "created_date";
    public static final String KEY_VALUE = "value";
    public static final String KEY_PLAN_ID = "plan_id";
    public static final String KEY_HEADER = "header";

    @DatabaseField(id = true)
    private String ft_id;

    @DatabaseField
    private String register_date, brand_code, sfl_code, created_date, status, value, plan_id, address,
            product_name, header;

    public TableFreeTrial() {}

    public void setFt_id(String ft_id) {
        this.ft_id = ft_id;
    }

    public String getFt_id() {
        return ft_id;
    }

    public void setBrand_code(String brand_code) {
        this.brand_code = brand_code;
    }

    public String getBrand_code() {
        return brand_code;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHeader() {
        return header;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public String getPlan_id() {
        return plan_id;
    }

    public void setRegister_date(String register_date) {
        this.register_date = register_date;
    }

    public String getRegister_date() {
        return register_date;
    }

    public void setSfl_code(String sfl_code) {
        this.sfl_code = sfl_code;
    }

    public String getSfl_code() {
        return sfl_code;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getAddress() {
        return address;
    }

    public String getProduct_name() {
        return product_name;
    }
}
