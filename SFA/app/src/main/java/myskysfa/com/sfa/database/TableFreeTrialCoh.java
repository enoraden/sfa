package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/14/2016.
 */
@DatabaseTable(tableName = "free_trial_coh")
public class TableFreeTrialCoh {

    public static final String TABLE_NAME = "free_trial_coh";
    public static final String KEY_HW_SN = "sn";
    public static final String KEY_HW_TYPE = "type";
    public static final String KEY_HW_NAME = "name";
    public static final String KEY_HW_STOCK_DATE = "stock_date";
    public static final String KEY_HW_PACKAGE = "package_id";
    public static final String KEY_HW_STATUS = "status";
    public static final String KEY_IS_SERIALIZE = "serialized";
    public static final String KEY_HW_ID = "hw_id";
    public static final String KEY_HW_RATE = "rate";

    public TableFreeTrialCoh() {
    }

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String sn;

    @DatabaseField
    private String type;
    @DatabaseField
    private String name;
    @DatabaseField
    private String stock_date;
    @DatabaseField
    private String package_id;
    @DatabaseField
    private String status;
    @DatabaseField
    private String serialized;
    @DatabaseField
    private String hw_id;
    @DatabaseField
    private String rate;


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public String getPackage_id() {
        return package_id;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getSn() {
        return sn;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStock_date(String stock_date) {
        this.stock_date = stock_date;
    }

    public String getStock_date() {
        return stock_date;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getSerialized() {
        return serialized;
    }

    public void setSerialized(String serialized) {
        this.serialized = serialized;
    }

    public String getHw_id() {
        return hw_id;
    }

    public void setHw_id(String hw_id) {
        this.hw_id = hw_id;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }
}
