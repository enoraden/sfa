package myskysfa.com.sfa.database;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by admin on 6/14/2016.
 */
@DatabaseTable(tableName = "free_trial_survay_list")
public class TableFreeTrialSurvayList {

    public static final String TABLE_NAME       = "free_trial_survay_list";
    public static final String KEY_PRO_ID        = "prospect_nbr";
    public static final String KEY_SV_ID        = "sv_id";
    public static final String KEY_SV_NAME      = "sv_name";
    public static final String KEY_STATUS       = "sv_status";
    public static final String KEY_STATUS_DESC  = "sv_status_desc";
    public static final String KEY_VALUE        = "sv_value";
    public static final String KEY_VISIT_DATE   = "sv_visit_date";

    @DatabaseField(id = true)
    private String sv_id;

    @DatabaseField
    private String prospect_nbr, sv_name, sv_visit_date, sv_status_desc, sv_status, sv_value;

    public TableFreeTrialSurvayList() {}

    public void setProspect_nbr(String prospect_nbr) {
        this.prospect_nbr = prospect_nbr;
    }

    public String getProspect_nbr() {
        return prospect_nbr;
    }

    public void setSv_id(String sv_id) {
        this.sv_id = sv_id;
    }

    public String getSv_id() {
        return sv_id;
    }

    public void setSv_name(String sv_name) {
        this.sv_name = sv_name;
    }

    public String getSv_name() {
        return sv_name;
    }

    public void setSv_status(String sv_status) {
        this.sv_status = sv_status;
    }

    public String getSv_status() {
        return sv_status;
    }

    public void setSv_status_desc(String sv_status_desc) {
        this.sv_status_desc = sv_status_desc;
    }

    public String getSv_status_desc() {
        return sv_status_desc;
    }

    public void setSv_value(String sv_value) {
        this.sv_value = sv_value;
    }

    public String getSv_value() {
        return sv_value;
    }

    public void setSv_visit_date(String sv_visit_date) {
        this.sv_visit_date = sv_visit_date;
    }

    public String getSv_visit_date() {
        return sv_visit_date;
    }
}
