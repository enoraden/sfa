package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/20/2016.
 */
@DatabaseTable(tableName = "m_brand")
public class TableMBrand {

    public static final String TABLE_NAME = "m_brand";
    public static final String fROWID = "brand_id";
    public static final String fBRAND_NAME = "brand_name";
    public static final String fBRAND_CODE = "brand_code";

    @DatabaseField(id = true)
    private String brand_id;

    @DatabaseField
    private String brand_name, brand_code;

    public void setBrand_code(String brand_code) {
        this.brand_code = brand_code;
    }

    public String getBrand_code() {
        return brand_code;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getBrand_name() {
        return brand_name;
    }
}
