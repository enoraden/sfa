package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableFreeTrialCoh;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by admin on 6/14/2016.
 */
public class TableFTSOHAdapter {

    static private TableFTSOHAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableFTSOHAdapter(ctx);
        }
    }
    static public TableFTSOHAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableFTSOHAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableFreeTrialCoh> getAllData() {
        List<TableFreeTrialCoh> tblsatu = null;
        try {
            tblsatu = getHelper().getTableFTSOHDAO()
                    .queryBuilder().orderBy(TableFreeTrialCoh.KEY_HW_STOCK_DATE,
                    false)
                    .orderBy(TableFreeTrialCoh.KEY_HW_STOCK_DATE, true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableFreeTrialCoh> getDatabyCondition(String condition, Object param) {
        List<TableFreeTrialCoh> tblsatu = null;

        int count = 0;
        try {
            dao = helper.getDao(TableFreeTrialCoh.class);
            QueryBuilder<TableFreeTrialCoh, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFreeTrialCoh, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy(TableFreeTrialCoh.KEY_HW_TYPE, false);
            queryBuilder.orderBy(TableFreeTrialCoh.KEY_HW_STOCK_DATE, true);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableFreeTrialCoh> getDatabyConditionMultyCondition(String condition, Object param,
                                                       String condition2, Object param2) {
        List<TableFreeTrialCoh> tblsatu = null;

        int count = 0;
        try {
            dao = helper.getDao(TableFreeTrialCoh.class);
            QueryBuilder<TableFreeTrialCoh, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFreeTrialCoh, Integer> where = queryBuilder.where();
            where.eq(condition, param).and().like(condition2, param2+"%");
            queryBuilder.orderBy(TableFreeTrialCoh.KEY_HW_TYPE, false);
            queryBuilder.orderBy(TableFreeTrialCoh.KEY_HW_STOCK_DATE, true);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }


    /**
     * Get Data By ID
     * @return
     */
    public  List<TableFreeTrialCoh> _fetchDesc(String condition1, String condition2,  Object param1,
                                               Object param2) {
        List<TableFreeTrialCoh> tblsatu = null;

        int count = 0;
        try {
            dao = helper.getDao(TableFreeTrialCoh.class);
            QueryBuilder<TableFreeTrialCoh, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFreeTrialCoh, Integer> where = queryBuilder.where();
            where.eq(condition1, param1).and().eq(condition2, param2);
            queryBuilder.orderBy(TableFreeTrialCoh.KEY_HW_STOCK_DATE, true).limit((long) 1);
            count++;
            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }


    public  List<TableFreeTrialCoh> _fetchDescSingleParam(String condition, Object param) {
        List<TableFreeTrialCoh> tblsatu = null;

        int count = 0;
        try {
            dao = helper.getDao(TableFreeTrialCoh.class);
            QueryBuilder<TableFreeTrialCoh, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFreeTrialCoh, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy(TableFreeTrialCoh.KEY_HW_TYPE, false);
            queryBuilder.orderBy(TableFreeTrialCoh.KEY_HW_STOCK_DATE, true).limit((long) 1);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }


    /**
     * Insert Data
     */
    public void insertData(TableFreeTrialCoh tbl, String sn, String  type, String name,
                           String stock_date, String package_id, String status,
                           String isSerial, String hw_id, String rate) {
        try {
            tbl.setSn(sn);
            tbl.setType(type);
            tbl.setName(name);
            tbl.setStock_date(stock_date);
            tbl.setPackage_id(package_id);
            tbl.setStatus(status);
            tbl.setSerialized(isSerial);
            tbl.setHw_id(hw_id);
            tbl.setRate(rate);
            getHelper().getTableFTSOHDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */

    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFreeTrialCoh.class);
            UpdateBuilder<TableFreeTrialCoh, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update All
     */
    public void updateAll(Context context, String  type, String name,
                          String stock_date, String package_id, String status,
                          String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFreeTrialCoh.class);
            UpdateBuilder<TableFreeTrialCoh, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableFreeTrialCoh.KEY_HW_TYPE, type);
            updateBuilder.updateColumnValue(TableFreeTrialCoh.KEY_HW_NAME, name);
            updateBuilder.updateColumnValue(TableFreeTrialCoh.KEY_HW_STOCK_DATE, stock_date);
            updateBuilder.updateColumnValue(TableFreeTrialCoh.KEY_HW_PACKAGE, package_id);
            updateBuilder.updateColumnValue(TableFreeTrialCoh.KEY_HW_STATUS, status);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFreeTrialCoh.class);
            DeleteBuilder<TableFreeTrialCoh, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(Context context) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFreeTrialCoh.class);
            DeleteBuilder<TableFreeTrialCoh, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    /*public void deleteAll() {
        List<TableFreeTrialCoh> tblsatu = null;
        try {
            tblsatu = getHelper().getTableFTSOHDAO().queryForAll();
            getHelper().getTableFTSOHDAO().delete(tblsatu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
