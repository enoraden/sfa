package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableFreeTrialSurvay;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by admin on 6/14/2016.
 */
public class TableFTSurvayAdapter {

    static private TableFTSurvayAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableFTSurvayAdapter(ctx);
        }
    }
    static public TableFTSurvayAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableFTSurvayAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableFreeTrialSurvay> getAllData() {
        List<TableFreeTrialSurvay> tblsatu = null;
        try {
            tblsatu = getHelper().getTableFTSurvaysDAO()
                    .queryBuilder().orderBy(TableFreeTrialSurvay.KEY_PROSPECT_ID,
                            false)
                    .orderBy(TableFreeTrialSurvay.KEY_SUBMIT_DATE, false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableFreeTrialSurvay> getDatabyCondition(String condition, Object param) {
        List<TableFreeTrialSurvay> tblsatu = null;

        int count = 0;
        try {
            dao = helper.getDao(TableFreeTrialSurvay.class);
            QueryBuilder<TableFreeTrialSurvay, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFreeTrialSurvay, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy(TableFreeTrialSurvay.KEY_PROSPECT_ID, false);
            queryBuilder.orderBy(TableFreeTrialSurvay.KEY_SUBMIT_DATE, false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableFreeTrialSurvay tbl, String sv_id, String  prospect_id,
                           String sv_profile, String sv_address, String sv_tlp, String sv_longitude,
                           String sv_latitude, String sv_interested, String sv_meet,
                           String sv_visit_date, String sv_submit_date, String sv_note,
                           String sv_photo, String sv_status, String sv_value) {
        try {
            tbl.setSv_id(sv_id);
            tbl.setProspect_id(prospect_id);
            tbl.setSv_profile(sv_profile);
            tbl.setSv_address(sv_address);
            tbl.setSv_tlp(sv_tlp);
            tbl.setSv_longitude(sv_longitude);
            tbl.setSv_latitude(sv_latitude);
            tbl.setSv_interested(sv_interested);
            tbl.setSv_meet(sv_meet);
            tbl.setSv_visit_date(sv_visit_date);
            tbl.setSv_submit_date(sv_submit_date);
            tbl.setSv_note(sv_note);
            tbl.setSv_photo(sv_photo);
            tbl.setSv_status(sv_status);
            tbl.setSv_value(sv_value);
            getHelper().getTableFTSurvaysDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */

    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFreeTrialSurvay.class);
            UpdateBuilder<TableFreeTrialSurvay, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update All
     */
    public void updateAll(Context context, String id, String  prospect_id, String sv_profile, String sv_address,
                          String sv_tlp, String sv_longitude, String sv_latitude,
                          String sv_interested, String sv_meet, String sv_visit_date,
                          String sv_submit_date, String sv_note, String sv_photo, String sv_status,
                          String sv_value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFreeTrialSurvay.class);
            UpdateBuilder<TableFreeTrialSurvay, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_SV_ID, id);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_PROSPECT_ID, prospect_id);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_SV_PROFILE, sv_profile);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_SV_ALAMAT, sv_address);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_SV_TLP, sv_tlp);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_LONGITUDE, sv_longitude);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_LANGITUDE, sv_latitude);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_INTERESTED, sv_interested);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_MEET, sv_meet);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_VISIT_DATE, sv_visit_date);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_SUBMIT_DATE, sv_submit_date);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_NOTE, sv_note);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_SV_PHOTO, sv_photo);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_STATUS, sv_status);
            updateBuilder.updateColumnValue(TableFreeTrialSurvay.KEY_VALUE, sv_value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFreeTrialSurvay.class);
            DeleteBuilder<TableFreeTrialSurvay, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableFreeTrialSurvay> tblsatu = null;
        try {
            tblsatu = getHelper().getTableFTSurvaysDAO().queryForAll();
            getHelper().getTableFTSurvaysDAO().delete(tblsatu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
