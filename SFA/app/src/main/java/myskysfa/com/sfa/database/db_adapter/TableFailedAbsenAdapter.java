package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableFailedAbsen;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by Eno on 10/26/2016.
 */

public class TableFailedAbsenAdapter {
    static private TableFailedAbsenAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableFailedAbsenAdapter(ctx);
        }
    }

    static public TableFailedAbsenAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableFailedAbsenAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableFailedAbsen> getAllData() {
        List<TableFailedAbsen> tblsatu = null;
        try {
            tblsatu = getHelper().getFailedAbsensDAO().queryBuilder().query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     *
     * @return
     */
    public List<TableFailedAbsen> getDatabyCondition(String condition, Object param) {
        List<TableFailedAbsen> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableFailedAbsen.class);
            QueryBuilder<TableFailedAbsen, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFailedAbsen, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableFailedAbsen tbl, String entity, String employeeid,
                           String attendance_type, String latitude, String longitude,
                           String attendance_time, String status, String jarak) {
        try {
            tbl.setEntity(entity);
            tbl.setEmployeeid(employeeid);
            tbl.setAttendance_type(attendance_type);
            tbl.setLatitude(latitude);
            tbl.setLongitude(longitude);
            tbl.setAttendance_time(attendance_time);
            tbl.setStatus(status);
            tbl.setJarak(jarak);
            getHelper().getFailedAbsensDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    * Update by Condition
    */
    public void updatePartial(Context context, String column, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFailedAbsen.class);
            UpdateBuilder<TableFailedAbsen, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFailedAbsen.class);
            DeleteBuilder<TableFailedAbsen, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }
}
