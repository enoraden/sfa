package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableFreeTrial;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by admin on 6/13/2016.
 */
public class TableFreeTrialAdapter {

    static private TableFreeTrialAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableFreeTrialAdapter(ctx);
        }
    }
    static public TableFreeTrialAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableFreeTrialAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableFreeTrial> getAllData() {
        List<TableFreeTrial> tblsatu = null;
        try {
            tblsatu = getHelper()
                    .getTableFreeTrialDAO()
                    .queryBuilder()
                    .orderBy(TableFreeTrial.KEY_CREATED, false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableFreeTrial> getDatabyCondition(String condition, Object param) {
        List<TableFreeTrial> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = helper.getDao(TableFreeTrial.class);
            QueryBuilder<TableFreeTrial, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFreeTrial, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy(TableFreeTrial.KEY_CREATED, false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    public  List<TableFreeTrial> getDatabyPrefix(String condition, Object param) {
        List<TableFreeTrial> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = helper.getDao(TableFreeTrial.class);
            QueryBuilder<TableFreeTrial, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFreeTrial, Integer> where = queryBuilder.where();
            where.like(condition, "%"+param+"%");
            queryBuilder.orderBy(TableFreeTrial.KEY_FT_ID, false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    public List<TableFreeTrial> getDistincHeader(){
        List<TableFreeTrial> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableFreeTrial.class);
            QueryBuilder<TableFreeTrial, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.distinct().selectColumns(TableFreeTrial.KEY_HEADER);
            count++;
            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableFreeTrial tbl, String ftid, String  register_date, String brand_code,
                           String sfl_code, String created_date, String status, String value,
                           String plan_id, String header) {
        try {
            tbl.setFt_id(ftid);
            tbl.setRegister_date(register_date);
            tbl.setBrand_code(brand_code);
            tbl.setSfl_code(sfl_code);
            tbl.setCreated_date(created_date);
            tbl.setStatus(status);
            tbl.setValue(value);
            tbl.setPlan_id(plan_id);
            tbl.setHeader(header);
            getHelper().getTableFreeTrialDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */

    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFreeTrial.class);
            UpdateBuilder<TableFreeTrial, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update All
     */
    public void updateAll(Context context, String  register_date, String brand_code,
                          String sfl_code, String created_date, String status, String value,
                          String plan_id, String address, String product_name,
                          String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFreeTrial.class);
            UpdateBuilder<TableFreeTrial, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableFreeTrial.KEY_REG_DATE, register_date);
            updateBuilder.updateColumnValue(TableFreeTrial.KEY_BRAND_CODE, brand_code);
            updateBuilder.updateColumnValue(TableFreeTrial.KEY_SFL_CODE, sfl_code);
            updateBuilder.updateColumnValue(TableFreeTrial.KEY_CREATED, created_date);
            updateBuilder.updateColumnValue(TableFreeTrial.KEY_STATUS, status);
            updateBuilder.updateColumnValue(TableFreeTrial.KEY_VALUE, value);
            updateBuilder.updateColumnValue(TableFreeTrial.KEY_PLAN_ID, plan_id);
            updateBuilder.updateColumnValue(TableFreeTrial.KEY_ADDRESS, address);
            updateBuilder.updateColumnValue(TableFreeTrial.KEY_PRODUCT_NAME, product_name);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFreeTrial.class);
            DeleteBuilder<TableFreeTrial, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableFreeTrial> tblsatu = null;
        try {
            tblsatu = getHelper().getTableFreeTrialDAO().queryForAll();
            getHelper().getTableFreeTrialDAO().delete(tblsatu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
