package myskysfa.com.sfa.database.db_adapter;


import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.utils.DatabaseManager;


/**
 * Created by Hari Hendryan on 10/23/2015.
 */
public class TableLogLoginAdapter {

    static private TableLogLoginAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableLogLoginAdapter(ctx);
        }
    }

    static public TableLogLoginAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableLogLoginAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableLogLogin> getAllData() {
        List<TableLogLogin> tblsatu = null;
        try {
            /*QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.orderBy(TableLogLogin.C_ROW_ID,false);
            tblsatu = queryBuilder.query();*/
            tblsatu = getHelper().getTableLogLoginDAO()
                    .queryBuilder()
                    .orderBy(TableLogLogin.C_ROW_ID, false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableLogLogin> getDatabyCondition(String condition, Object param) {
        List<TableLogLogin> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = helper.getDao(TableLogLogin.class);
            QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();
            Where<TableLogLogin, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy(TableLogLogin.C_ROW_ID, false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    public List<TableLogLogin> getLastData() {
        List<TableLogLogin> tblsatu = null;
        try {
            QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.orderBy(TableLogLogin.C_ROW_ID,true).limit((long) 1);
            tblsatu = queryBuilder.query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    private int getLastRowId(){
        int lastRow = 0;
        List<TableLogLogin> tblsatu = null;


        int count = 0;
        try {
            dao = helper.getDao(TableLogLogin.class);
            QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.orderBy(TableLogLogin.C_ROW_ID,false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(tblsatu.size()>0){
            lastRow = tblsatu.get(0).getcRowId() + 1;
        }else{
            lastRow = 1;
        }

        return lastRow;
    }

    /**
     * Insert Data
     */
    public void insertData(TableLogLogin tbl, String uid, String user_name, String employee_id,
                           String full_name, String role_code, String role_name, String is_supervisor,
                           String token, String sfl_code, String sfl_name, String log_date,String is_login,
                           String password, String brand, String branch, String branch_id,
                           String user_type, String region_code, String region_name, String nik) {
        //int lastRowid = getLastRowId();
        try {
            tbl.setcUid(uid);
            tbl.setcBranch(branch);
            tbl.setcBranchId(branch_id);
            tbl.setcBrand(brand);
            tbl.setcEmployeeId(employee_id);
            tbl.setcFullName(full_name);
            tbl.setcIsLogin(is_login);
            tbl.setcIsSupervisor(is_supervisor);
            tbl.setcLogDate(log_date);
            tbl.setcPassword(password);
            tbl.setcRegioncode(region_code);
            tbl.setcRegionname(region_name);
            tbl.setcSflCode(sfl_code);
            tbl.setcSflName(sfl_name);
            tbl.setcToken(token);
            tbl.setcUserName(user_name);
            tbl.setcUsertype(user_type);
            tbl.setcRoleCode(role_code);
            tbl.setcRoleName(role_name);
            tbl.setNik(nik);
            getHelper().getTableLogLoginDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */

    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableLogLogin.class);
            UpdateBuilder<TableLogLogin, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update All
     */
    public void updateAll(Context context, String uid, String user_name, String employee_id, String full_name, String role_code, String role_name,
                          String is_supervisor,String token, String sfl_code, String sfl_name, String log_date,String is_login,
                          String password, String brand, String branch, String branch_id, String user_type, String region_code, String region_name,
                          String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableLogLogin.class);
            UpdateBuilder<TableLogLogin, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableLogLogin.C_FULL_NAME, full_name);
            updateBuilder.updateColumnValue(TableLogLogin.C_ROLE_NAME, role_name);
            updateBuilder.updateColumnValue(TableLogLogin.C_SFL_NAME, sfl_name);
            updateBuilder.updateColumnValue(TableLogLogin.C_USER_NAME, user_name);
            updateBuilder.updateColumnValue(TableLogLogin.C_BRANCH, branch);
            updateBuilder.updateColumnValue(TableLogLogin.C_BRANCH_ID, branch_id);
            updateBuilder.updateColumnValue(TableLogLogin.C_BRAND, brand);
            updateBuilder.updateColumnValue(TableLogLogin.C_EMPLOYEE_ID, employee_id);
            updateBuilder.updateColumnValue(TableLogLogin.C_IS_LOGIN, is_login);
            updateBuilder.updateColumnValue(TableLogLogin.C_IS_SUPERVISOR, is_supervisor);
            updateBuilder.updateColumnValue(TableLogLogin.C_LOG_DATE, log_date);
            updateBuilder.updateColumnValue(TableLogLogin.C_PASSWORD, password);
            updateBuilder.updateColumnValue(TableLogLogin.C_REGIONCODE, region_code);
            updateBuilder.updateColumnValue(TableLogLogin.C_REGIONNAME, region_name);
            updateBuilder.updateColumnValue(TableLogLogin.C_ROLE_CODE, role_code);
            updateBuilder.updateColumnValue(TableLogLogin.C_SFL_CODE, sfl_code);
            updateBuilder.updateColumnValue(TableLogLogin.C_TOKEN, token);
            updateBuilder.updateColumnValue(TableLogLogin.C_USERTYPE, user_type);
            updateBuilder.updateColumnValue(TableLogLogin.C_UID, uid);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableLogLogin.class);
            DeleteBuilder<TableLogLogin, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableLogLogin> tblsatu = null;
        try {
            tblsatu = getHelper().getTableLogLoginDAO().queryForAll();
            getHelper().getTableLogLoginDAO().delete(tblsatu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

