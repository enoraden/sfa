package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableMBrand;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by admin on 6/20/2016.
 */
public class TableMBrandAdapter {

    static private TableMBrandAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableMBrandAdapter(ctx);
        }
    }
    static public TableMBrandAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableMBrandAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableMBrand> getAllData() {
        List<TableMBrand> tblsatu = null;
        try {
            tblsatu = getHelper().getTableBrandDAO().queryBuilder().query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableMBrand> getDatabyCondition(String condition, Object param) {
        List<TableMBrand> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableMBrand.class);
            QueryBuilder<TableMBrand, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMBrand, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableMBrand tbl, String brand_id, String brand_name, String brand_code) {
        try {
            tbl.setBrand_id(brand_id);
            tbl.setBrand_name(brand_name);
            tbl.setBrand_code(brand_code);
            getHelper().getTableBrandDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */

    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableMBrand.class);
            UpdateBuilder<TableMBrand, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update All
     */
    public void updateAll(Context context, String brand_id, String brand_name, String brand_code,
                          String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableMBrand.class);
            UpdateBuilder<TableMBrand, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableMBrand.fROWID, brand_id);
            updateBuilder.updateColumnValue(TableMBrand.fBRAND_NAME, brand_name);
            updateBuilder.updateColumnValue(TableMBrand.fBRAND_CODE, brand_code);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
    * Delete By Conditition
    */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableMBrand.class);
            DeleteBuilder<TableMBrand, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableMBrand> tblsatu = null;
        try {
            tblsatu = getHelper().getTableBrandDAO().queryForAll();
            getHelper().getTableBrandDAO().delete(tblsatu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
