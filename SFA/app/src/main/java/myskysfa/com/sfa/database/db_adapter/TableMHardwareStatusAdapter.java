package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableMHardwareStatus;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by admin on 6/20/2016.
 */
public class TableMHardwareStatusAdapter {

    static private TableMHardwareStatusAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableMHardwareStatusAdapter(ctx);
        }
    }

    static public TableMHardwareStatusAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableMHardwareStatusAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableMHardwareStatus> getAllData() {
        List<TableMHardwareStatus> tblsatu = null;
        try {
            tblsatu = getHelper().getTableHWStatusDAO().queryBuilder().query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     *
     * @return
     */
    public List<TableMHardwareStatus> getDatabyCondition(String condition, Object param) {
        List<TableMHardwareStatus> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableMHardwareStatus.class);
            QueryBuilder<TableMHardwareStatus, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMHardwareStatus, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableMHardwareStatus tbl, String hw_status_id, String hw_status_name, String status) {
        try {
            tbl.setHw_status_id(hw_status_id);
            tbl.setHw_status_name(hw_status_name);
            tbl.setStatus(status);
            getHelper().getTableHWStatusDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */

    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableMHardwareStatus.class);
            UpdateBuilder<TableMHardwareStatus, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    /**
     * Update All
     */
    public void updateAll(Context context, String hw_status_id, String hw_status_name, String status,
                          String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableMHardwareStatus.class);
            UpdateBuilder<TableMHardwareStatus, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableMHardwareStatus.fROWID, hw_status_id);
            updateBuilder.updateColumnValue(TableMHardwareStatus.fHW_STATUS_NAME, hw_status_name);
            updateBuilder.updateColumnValue(TableMHardwareStatus.fSTATUS, status);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableMHardwareStatus.class);
            DeleteBuilder<TableMHardwareStatus, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableMHardwareStatus> tblsatu = null;
        try {
            tblsatu = getHelper().getTableHWStatusDAO().queryForAll();
            getHelper().getTableHWStatusDAO().delete(tblsatu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
