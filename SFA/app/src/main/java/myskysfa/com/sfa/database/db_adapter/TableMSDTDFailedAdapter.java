package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableDataMSDTDFailed;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by Eno on 11/1/2016.
 */

public class TableMSDTDFailedAdapter {
    static private TableMSDTDFailedAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableMSDTDFailedAdapter(ctx);
        }
    }

    static public TableMSDTDFailedAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableMSDTDFailedAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableDataMSDTDFailed> getAllData() {
        List<TableDataMSDTDFailed> tblsatu = null;
        try {
            tblsatu = getHelper().getFailedAdaptersMsDtdDAO().queryBuilder().query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     *
     * @return
     */
    public List<TableDataMSDTDFailed> getDatabyCondition(String condition, Object param) {
        List<TableDataMSDTDFailed> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableDataMSDTDFailed.class);
            QueryBuilder<TableDataMSDTDFailed, Integer> queryBuilder = dao.queryBuilder();
            Where<TableDataMSDTDFailed, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableDataMSDTDFailed tbl, String form_no, String data,
                           String app_name, String date, String status, String plan_id) {
        try {
            tbl.setForm_no(form_no);
            tbl.setData(data);
            tbl.setApp_name(app_name);
            tbl.setDate(date);
            tbl.setStatus(status);
            tbl.setPlanId(plan_id);
            getHelper().getFailedAdaptersMsDtdDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableDataMSDTDFailed.class);
            DeleteBuilder<TableDataMSDTDFailed, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }


}
