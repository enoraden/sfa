package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableMasterPackage;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by Eno on 6/20/2016.
 */
public class TableMasterPackageAdapter {
    static private TableMasterPackageAdapter instance;
    private DatabaseManager helper;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableMasterPackageAdapter(ctx);
        }
    }

    static public TableMasterPackageAdapter getInstance() {
        return instance;
    }

    public TableMasterPackageAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }


    /**
     * Get All Data
     *
     * @return
     */
    public List<TableMasterPackage> getAllData() {
        List<TableMasterPackage> tblData = null;
        try {
            tblData = getHelper().getTablePackageDAO()
                    .queryBuilder()
                    .orderBy(TableMasterPackage.fPRODUCT_NAME, true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblData;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableMasterPackage> getDatabyCondition(String condition, Object param) {
        List<TableMasterPackage> tblsatu = null;
        int count = 0;
        try {
            Dao dao = helper.getDao(TableMasterPackage.class);
            QueryBuilder<TableMasterPackage, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMasterPackage, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableMasterPackage> fetchByProfile(Object param1, Object param, Object basicID) {
        List<TableMasterPackage> tblsatu = null;
        int count = 0;
        try {
            Dao dao = helper.getDao(TableMasterPackage.class);
            QueryBuilder<TableMasterPackage, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMasterPackage, Integer> where = queryBuilder.where();
            where.eq("alacarte", param1)
                    .and().eq("brand", param)
                    .and().like("key_id", basicID + "%");
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableMasterPackage> fetchBasic(Object param) {
        List<TableMasterPackage> tblsatu = null;
        int count = 0;
        try {
            Dao dao = helper.getDao(TableMasterPackage.class);
            QueryBuilder<TableMasterPackage, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMasterPackage, Integer> where = queryBuilder.where();
            where.eq("alacarte", "0").and().eq("brand", param);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
=======
>>>>>>> Master Data
     * Insert Data
     */
    public void insertData(TableMasterPackage tbl, String product_id, String product_type,
                           String product_name, int price, int alacarte, int flag_product,
                           String brand, String key_id, String basicId) {
        try {
            tbl.setProduct_id(product_id);
            tbl.setProduct_type(product_type);
            tbl.setProduct_name(product_name);
            tbl.setPrice(String.valueOf(price));
            tbl.setAlacarte(String.valueOf(alacarte));
            tbl.setFlag_product(String.valueOf(flag_product));
            tbl.setBrand(brand);
            tbl.setKey_id(key_id);
            tbl.setBasic_id(basicId);
            getHelper().getTablePackageDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void delete() {
        try {
            Dao dao = helper.getDao(TableMasterPackage.class);
            DeleteBuilder<TableMasterPackage, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
