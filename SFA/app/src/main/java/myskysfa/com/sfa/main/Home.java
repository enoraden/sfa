package myskysfa.com.sfa.main;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;
import java.util.List;

import myskysfa.com.sfa.R;

/**
 * Created by Eno on 10/11/2016.
 */

public class Home extends Fragment {
    private ViewGroup root;
    private BarChart combinedChart;
    private PieChart pieChart;
    private BarData barChartData;
    private PieData pieChartData;
    private Menu menu;
    private MenuItem alert;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.home, container, false);
        combinedChart = (BarChart) root.findViewById(R.id.combineCart);
        pieChart = (PieChart) root.findViewById(R.id.pieChart);

        barChartData = new BarData(getXAxisValues(), getDataSet());
        pieChartData = new PieData(getXAxisValues(), getDataSetPie());


        combinedChart.setData(barChartData);
        pieChart.setData(pieChartData);
        pieChart.animateY(5000);
        combinedChart.animateXY(2000, 2000);
        pieChart.setDescription("");
        combinedChart.setDescription("");
        combinedChart.invalidate();
        return root;
    }

    private ArrayList<IBarDataSet> getDataSet() {
        ArrayList<IBarDataSet> dataSets = null;

        ArrayList<BarEntry> setValueIndov = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            setValueIndov.add(new BarEntry(30.000f * i, i));
        }

        ArrayList<BarEntry> setValueTop = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            setValueTop.add(new BarEntry(60.000f * i + 1, i));
        }

        ArrayList<BarEntry> setValueOkev = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            setValueOkev.add(new BarEntry(45.000f * i + 5, i));
        }

        BarDataSet barDataIndov = new BarDataSet(setValueIndov, "Indovision");
        barDataIndov.setColor(Color.rgb(4, 4, 104));

        BarDataSet barDataTop = new BarDataSet(setValueTop, "TopTv");
        barDataTop.setColor(Color.rgb(174, 204, 58));

        BarDataSet barDataOkev = new BarDataSet(setValueOkev, "OkeVision");
        barDataOkev.setColor(Color.rgb(3, 146, 66));

        dataSets = new ArrayList<>();
        dataSets.add(barDataIndov);
        dataSets.add(barDataTop);
        dataSets.add(barDataOkev);
        return dataSets;
    }

    private PieDataSet getDataSetPie() {
        List<Entry> setValueIndov = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            setValueIndov.add(new Entry(3.000f * i + 5, i));
        }

        PieDataSet dataSet = new PieDataSet(setValueIndov, "");
        dataSet.setColors(new int[]{Color.rgb(193, 37, 82), Color.rgb(255, 102, 0), Color.rgb(245, 199, 0), Color.rgb(106, 150, 31), Color.rgb(179, 100, 53), Color.rgb(4, 4, 104)});
        return dataSet;
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("JAN");
        xAxis.add("FEB");
        xAxis.add("MAR");
        xAxis.add("APR");
        xAxis.add("MAY");
        xAxis.add("JUN");
        return xAxis;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.info_menu, menu);
        this.menu = menu;
        alert = this.menu.findItem(R.id.count);
        alert.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }
}
