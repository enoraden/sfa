package myskysfa.com.sfa.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import myskysfa.com.sfa.BuildConfig;
import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.sfa.service.AutoSender;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionDetector;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.UserProfile;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by Hari Hendryan on 10/26/2015.
 */
public class LoginActivity extends AppCompatActivity {
    private static String fontPath = "fonts/steelfis.TTF";
    private EditText _username, _password;
    private TextView _txtusername, _txtpassword, version;
    private Button _login;
    private Boolean isInternetPresent = false, status = false;
    private ConnectionDetector cd;
    private Utils utils;
    private int countLoad = 0;
    private boolean isTaskRunning = false;
    private ProgressDialog _progressDialog;
    private String username, password, url, token, imei, response, message,
            user, sflCode, sflName, userId, sfaId, data = "", employeeId,
            fullName, roleCode, roleName, isSupervisor, pass, brand, branch, branchId,
            isUsernameLogin, userType, regionCode, regionName, nik;
    private Handler handler = new Handler();
    private UserProfile actProfile;
    private SharedPreferences sessPref;
    private boolean isLogin;
    private List<TableLogLogin> loginListItems;
    private static FirebaseAnalytics mFirebaseAnalytics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        utils = new Utils(LoginActivity.this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        actProfile = new UserProfile();
        utils.setIMEI();
        url = ConnectionManager.CM_URL_LOGIN;

        setUpProxy();
        initView();
        setFontType();
    }

    private void setUpProxy() {
        try {
            Properties props = System.getProperties();
            System.out.println("proxy");
            System.out.println("before: " + props.toString());
            System.setProperty("http.proxyHost", "192.168.220.32");
            System.setProperty("http.proxyPort", "3128");
            System.out.println("after: " + props.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Initialization  View
     */
    private void initView() {
        _username = (EditText) findViewById(R.id.username);
        _password = (EditText) findViewById(R.id.password);
        _txtusername = (TextView) findViewById(R.id.txtusername);
        _txtpassword = (TextView) findViewById(R.id.txtpassword);
        version = (TextView) findViewById(R.id.version);
        version.setText("Version " + Config.version);
        checkExistLogin();
        _login = (Button) findViewById(R.id.login);
        _login.setOnClickListener(loginListener);

        if (loginListItems.size() > 0) {
            token = loginListItems.get(loginListItems.size() - 1).getcToken();
            username = loginListItems.get(loginListItems.size() - 1).getcUserName();
            pass = loginListItems.get(loginListItems.size() - 1).getcPassword();
            if (token != null && token.length() > 1) {
                _username.setText(username);
                _password.requestFocus();
                isLogin = true;
            }
        }
        //setLogin();
    }

    /*private void setLogin(){
        actProfile = new UserProfile();
        sessPref = getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        isLogin = Integer.valueOf(sessPref.getString(TableLogLogin.C_IS_LOGIN, "0"));

        if(isLogin == 1){
            Intent i = new Intent(LoginActivity.this, MainMenuActivity.class);
            startActivity(i);
            finish();
        } else {
            username = sessPref.getString(TableLogLogin.C_USER_NAME, "");
            _username.setText(username);
        }

    }*/


    /**
     * Method for change font type
     */
    private void setFontType() {
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        _txtusername.setTypeface(tf);
        _txtpassword.setTypeface(tf);
        _login.setTypeface(tf);
    }

    /*private void checkLogin(){
        if(_username.getText().length()<3){
            Toast.makeText(this,"Username kosong !",Toast.LENGTH_LONG).show();
            return;
        }

        if(_password.getText().length()<3){
            Toast.makeText(this,"Password kosong !",Toast.LENGTH_LONG).show();
            return;
        }
        username = _username.getText().toString();
        password = _password.getText().toString();
        cd = new ConnectionDetector(LoginActivity.this);
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new AttempLoginTask().execute();
        } else {
            utils.showAlertDialog(LoginActivity.this, getResources().getString(R.string.no_internet1),
                    getResources().getString(R.string.no_internet2), false);
        }
    }*/

    /**
     * Listener for button login
     */
    private View.OnClickListener loginListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startService(new Intent(LoginActivity.this, AutoSender.class));

            cd = new ConnectionDetector(LoginActivity.this);
            isInternetPresent = cd.isConnectingToInternet();
            if (_username.getText().toString().equalsIgnoreCase("")
                    || _username.getText().toString().trim().length() < 1) {
                Toast.makeText(LoginActivity.this, "username tidak boleh kosong.!", Toast.LENGTH_SHORT).show();
            } else if (_password.getText().toString().equalsIgnoreCase("")
                    || _password.getText().toString().trim().length() < 1) {
                Toast.makeText(LoginActivity.this, "password tidak boleh kosong.!", Toast.LENGTH_SHORT).show();
            } else if (isUsernameLogin != null && !_username.getText().toString().equalsIgnoreCase(isUsernameLogin)) {
                _username.setText("");
                _password.setText("");
                _username.requestFocus();
                Toast.makeText(LoginActivity.this, "Anda masih login dengan user : " + isUsernameLogin, Toast.LENGTH_SHORT).show();
            } else if (isInternetPresent && !isLogin) {
                username = _username.getText().toString();
                password = _password.getText().toString();
                new AttempLoginTask().execute();
            } else if (isLogin) {
                if (username.equals(_username.getText().toString())) {
                    password = utils.md5(_password.getText().toString());
                    if (password.equals(pass)) {
                        Intent i = new Intent(LoginActivity.this, MainMenuActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.login_error), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    new AttempLoginTask().execute();
                }
            } else {
                utils.showAlertDialog(LoginActivity.this, getResources().getString(R.string.no_internet1),
                        getResources().getString(R.string.no_internet2), false);
            }
        }
    };

    /**
     * Class Asynctask for request login
     */
    private class AttempLoginTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!isTaskRunning) {
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
                isTaskRunning = true;
                _progressDialog = new ProgressDialog(LoginActivity.this);
                _progressDialog.setMessage(getResources().getString(R.string.loading_login));
                _progressDialog.setIndeterminate(false);
                _progressDialog.setCancelable(false);
                _progressDialog.show();
            } else {
                return;
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                JSONObject jsonObject, object;
                imei = utils.getIMEI();
                response = ConnectionManager.requestLogin(url, username, password, imei, Config.version, LoginActivity.this);
                System.out.println("response: " + response);
                if (response.length() > 0) {
                    jsonObject = new JSONObject(response.toString());
                    if (jsonObject != null) {
                        status = jsonObject.getBoolean(Config.KEY_STATUS);
                        data = jsonObject.getString(Config.KEY_DATA);
                        if (status) {
                            token = jsonObject.getString(Config.KEY_ACCESS_TOKEN);
                            data = jsonObject.getString(Config.KEY_DATA);
                            object = new JSONObject(data.toString());
                            userId = object.getString("user_id");
                            username = object.getString("user_name");
                            employeeId = object.getString("employee_id");
                            fullName = object.getString("full_name");
                            roleCode = object.getString("role_code");
                            roleName = object.getString("role_name");
                            sflCode = object.getString("sfl_code");
                            isSupervisor = object.getString("is_supervisor");
                            sflCode = object.getString("sfl_code");
                            branch = object.getString("branch");
                            brand = object.getString("brand");
                            branchId = object.getString("branch_id");
                            userType = object.getString("user_type");
                            regionCode = object.getString("region_code");
                            regionName = object.getString("region_name");
                            if (object.has("nik")){
                                nik = object.getString("nik");
                            }else{
                                nik = "";
                            }

                            if (status && object.getString("user_name").equalsIgnoreCase(username)) {
                                String currentDate = utils.getCurrentDate();
                                String pass = utils.md5(password);
                                TableLogLoginAdapter db = new TableLogLoginAdapter(LoginActivity.this);
                                TableLogLogin tbl = new TableLogLogin();
                                db.insertData(tbl, userId, username, employeeId, fullName, roleCode,
                                        roleName, isSupervisor, token, sflCode, fullName, currentDate,
                                        "1", pass, brand, branch, branchId, userType, regionCode,
                                        regionName, nik);
                            }
                        } else {
                            message = data;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (message != null) {
                return message;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (status) {
                actProfile.setActiveLogin(LoginActivity.this);
                logEventChat(sflCode, "log_login");
                Intent i = new Intent(LoginActivity.this, MainMenuActivity.class);
                startActivity(i);
                finish();
            } else {
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
                isTaskRunning = false;
                if (message != null) {
                    utils.showErrorDlg(handler, message, LoginActivity.this);
                } else {
                    utils.showErrorDlg(handler, getResources().getString(R.string.no_internet1), LoginActivity.this);
                }

            }
        }
    }

    private void checkExistLogin() {
        loginListItems = new ArrayList<>();
        TableLogLoginAdapter tbl = new TableLogLoginAdapter(LoginActivity.this);
        loginListItems = tbl.getDatabyCondition(TableLogLogin.C_IS_LOGIN, "1");
        for (int i = 0; i < loginListItems.size(); i++) {
            isUsernameLogin = loginListItems.get(i).getcUserName();
        }
    }

    public static void logEventChat(String username, String type) {
        //Here Add Firebase LogEvent
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, username);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, type);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }
}
