package myskysfa.com.sfa.main;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.MainMenuListviewAdapter;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableFTSOHAdapter;
import myskysfa.com.sfa.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePlanAdapter;
import myskysfa.com.sfa.main.menu.AbsenMS;
import myskysfa.com.sfa.main.menu.DataMaster;
import myskysfa.com.sfa.main.menu.Outbox;
import myskysfa.com.sfa.main.menu.FreeTrialPro;
import myskysfa.com.sfa.main.menu.dtd.DTD_Task;
import myskysfa.com.sfa.main.menu.ms.Ms_Task;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionDetector;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.UserProfile;
import myskysfa.com.sfa.utils.Utils;
import myskysfa.com.sfa.utils.listItemObject.MainMenuListItem;
import myskysfa.com.sfa.widget.CircleImageView;

/**
 * Created by admin on 6/10/2016.
 */
public class MainMenuActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private CircleImageView user_profile;
    private boolean isMainActivity = false, status = false;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    protected DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private LinearLayout leftLayout;
    private TextView sflName, sflCode, userType, userRegion;
    private ProgressDialog progressDialog;
    ListView mainMenuListview;
    MainMenuListviewAdapter mainMenuListviewAdapter;
    ArrayList<MainMenuListItem> menuItemsList;
    private SharedPreferences sessionPref;
    private String username, token, url, response, message;
    private Utils utils;
    private UserProfile actProfile;
    private FragmentManager fragmentManager;
    private Boolean backPressAgain = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        utils = new Utils(this);
        utils.setIMEI();
        actProfile = new UserProfile();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        leftLayout = (LinearLayout) findViewById(R.id.left_layout);
        mainMenuListview = (ListView) findViewById(R.id.main_menu_listView1);
        setSupportActionBar(toolbar);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        actionBarDrawerToggle.syncState();

        prepareMainMenuListItem();
        mainMenuListviewAdapter = new MainMenuListviewAdapter(MainMenuActivity.this, menuItemsList);

        ViewGroup mainMenuListviewHeader = (ViewGroup) getLayoutInflater().inflate(R.layout.header, mainMenuListview, false);
        sflName = (TextView) mainMenuListviewHeader.findViewById(R.id.user_name);
        sflCode = (TextView) mainMenuListviewHeader.findViewById(R.id.user_code);
        userType = (TextView) mainMenuListviewHeader.findViewById(R.id.user_type);
        userRegion = (TextView) mainMenuListviewHeader.findViewById(R.id.user_region);
        user_profile = (CircleImageView) mainMenuListviewHeader.findViewById(R.id.user_profile);

        mainMenuListview.addHeaderView(mainMenuListviewHeader, null, false);
        mainMenuListview.setAdapter(mainMenuListviewAdapter);
        mainMenuListview.setOnItemClickListener(menuListListener);
        setViewforHeader();
        setHomeFirstView();
    }

    private AdapterView.OnItemClickListener menuListListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            try {
                Fragment detail;
                MainMenuListItem item = (MainMenuListItem) mainMenuListviewAdapter.getItem(position - 1);
                switch (item.getId()) {
                    case -1:
                        detail = new Home();
                        initView(detail, "Raptor");
                        isMainActivity = true;
                        break;
                    case -2:
                        drawerLayout.closeDrawer(leftLayout);
                        cd = new ConnectionDetector(MainMenuActivity.this);
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            set_logout();
                        } else {
                            utils.showAlertDialog(MainMenuActivity.this, getResources().getString(R.string.no_internet1),
                                    getResources().getString(R.string.no_internet2), false);
                        }
                        break;
                    case 1:
                        detail = new DataMaster();
                        initView(detail, "Data Master");
                        isMainActivity = false;
                        break;
                    case 2:
                        detail = new FreeTrialPro();
                        initView(detail, "DTD Bawa Barang");
                        isMainActivity = false;
                        break;
                    case 4:
                        detail = new Ms_Task();
                        initView(detail, "Modern Store");
                        isMainActivity = false;
                        break;
                    case 5:
                        detail = new DTD_Task();
                        initView(detail, "Door To Door");
                        isMainActivity = false;
                        break;
                    case 6:
                        detail = new Outbox();
                        initView(detail, "Outbox");
                        isMainActivity = false;
                        break;
                    case 7:
                        //startActivity(new Intent(MainMenuActivity.this, AbsenMS.class));
                        //drawerLayout.closeDrawer(leftLayout);
                        detail = new AbsenMS();
                        initView(detail, "Presensi");
                        isMainActivity = false;
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void prepareMainMenuListItem() {
        menuItemsList = new ArrayList<MainMenuListItem>();
        menuItemsList.add(new MainMenuListItem(-1, "Home", R.drawable.icm_home));
        menuItemsList.add(new MainMenuListItem(-2, "Logout", R.drawable.icm_logout));
        menuItemsList.add(new MainMenuListItem(0, "", R.drawable.icm_home));
        menuItemsList.add(new MainMenuListItem(1, "Data Master", R.drawable.icm_data_master));
        menuItemsList.add(new MainMenuListItem(2, "DTD Bawa Barang", R.drawable.icm_dtd));
        menuItemsList.add(new MainMenuListItem(4, "Modern Store", R.drawable.icm_ms));
        menuItemsList.add(new MainMenuListItem(5, "Door To Door", R.drawable.icm_dtd));
        menuItemsList.add(new MainMenuListItem(6, "Outbox", R.drawable.icm_ae));
        menuItemsList.add(new MainMenuListItem(7, "Presensi", R.drawable.icm_absen));
    }

    /**
     * Method for parsing json profile
     */
    private void setViewforHeader() {
        sessionPref = getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        sflName.setText(sessionPref.getString(TableLogLogin.C_FULL_NAME, ""));
        sflCode.setText(sessionPref.getString(TableLogLogin.C_EMPLOYEE_ID, ""));
        String userTipe = sessionPref.getString(TableLogLogin.C_USERTYPE, "");
        if (userTipe.equalsIgnoreCase("S")) {
            userType.setText("- Sales,");
        } else if (userTipe.equalsIgnoreCase("T")) {
            userType.setText("- Teknisi,");
        } else if (userTipe.equalsIgnoreCase("V")) {
            userType.setText("- Verifikator,");
        } else {
            userTipe.equalsIgnoreCase("-");
        }
        userRegion.setText(sessionPref.getString(TableLogLogin.C_REGIONNAME, ""));

    }

    private void setHomeFirstView() {
        Fragment fragment = new Home();
        initView(fragment, "Raptor");
        isMainActivity = true;
    }

    /**
     * Method for set fragment view
     */
    private void initView(Fragment fragment, String title) {
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_framemain, fragment).commit();
        setTitle(title);
        drawerLayout.closeDrawer(leftLayout);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void setTitle(CharSequence title) {
        getSupportActionBar().setTitle(title);
    }

    /**
     * Method confirm for logout
     */
    private void set_logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainMenuActivity.this);
        builder.setMessage(getResources().getString(R.string.ask_logout_app));
        builder.setTitle(getResources().getString(R.string.text_logout));
        builder.setPositiveButton(getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        new LogoutTask().execute();
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
        alert.show();
    }

    public void backButtonHandler() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainMenuActivity.this);
        builder.setMessage(getResources().getString(R.string.ask_exit_app));
        builder.setTitle(getResources().getString(R.string.exit_app));
        builder.setPositiveButton(getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MainMenuActivity.this.finish();
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
        alert.show();
    }


    /**
     * Oprek lagi entar, untuk
     */
    /*@Override
    public void onBackPressed() {
        if (isMainActivity) {
            backButtonHandler();
        } else {
            //initView(homeFragment, getResources().getString(R.string.app_name));
            if (fragmentManager.getBackStackEntryCount() == 0) {
                //super.onBackPressed();
                isMainActivity = true;
            } else {
                fragmentManager.popBackStack();
                isMainActivity = false;
            }
        }
    }*/
    @Override
    public void onBackPressed() {
        if (isMainActivity) {
            //backButtonHandler();
            if (backPressAgain) {
                finish();
            } else {
                Toast.makeText(MainMenuActivity.this, "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT).show();
                backPressAgain = true;
            }


            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    backPressAgain = false;
                }
            }, 2000); // 2 detik
        } else {
            Fragment home = new Home();
            initView(home, "Raptor");
            isMainActivity = true;
        }
    }

    /**
     * Class Asynctask for request logout
     */
    private class LogoutTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            progressDialog = new ProgressDialog(MainMenuActivity.this);
            progressDialog.setMessage(getResources().getString(R.string.loading_logout));
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = ConnectionManager.CM_URL_LOGOUT;
                System.out.println("url: " + url);
                response = ConnectionManager.requestLogout(url, utils.getIMEI(), token, username, Config.version, MainMenuActivity.this);
                System.out.println("response: " + response);
                status = false;
                if (response.length() > 0) {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject != null) {
                        status = jsonObject.getBoolean(Config.KEY_STATUS);
                        message = jsonObject.getString(Config.KEY_MESSAGE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return message;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            Toast.makeText(MainMenuActivity.this, s, Toast.LENGTH_SHORT).show();
            clearSharedPreference();
            deleteSOHandPlan();
            DeleteLogLogin();
        }
    }

    /**
     * Method for clear all of data
     */
    private void clearSharedPreference() {
        TableLogLoginAdapter db = new TableLogLoginAdapter(getApplicationContext());
        db.deleteAll();
        actProfile.clearActiveLogin(getApplicationContext());
        Intent exit = new Intent(MainMenuActivity.this, LoginActivity.class);
        exit.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY
                | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(exit);
        finish();
        System.exit(0);
    }

    private void deleteSOHandPlan() {
        TableFTSOHAdapter soh = new TableFTSOHAdapter(getApplicationContext());
        soh.delete(getApplicationContext());
        TablePlanAdapter plan = new TablePlanAdapter(getApplicationContext());
        plan.delete(getApplicationContext());
    }

    private void DeleteLogLogin() {
        TableLogLoginAdapter mLogLoginAdapter = new TableLogLoginAdapter(MainMenuActivity.this);
        mLogLoginAdapter.deleteAll();
    }
}
