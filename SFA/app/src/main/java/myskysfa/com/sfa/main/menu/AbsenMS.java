package myskysfa.com.sfa.main.menu;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableAbsensi;
import myskysfa.com.sfa.database.TableFailedAbsen;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.TableMasterMS;
import myskysfa.com.sfa.database.TablePlanMS;
import myskysfa.com.sfa.database.db_adapter.TableAbsenAdapter;
import myskysfa.com.sfa.database.db_adapter.TableFailedAbsenAdapter;
import myskysfa.com.sfa.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMSAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePlanMSAdapter;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;

/**
 * By Enobyte 11/20/2015
 */

public class AbsenMS extends Fragment implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private Button absMasuk, absKeluar;
    private TextView chooseStore, latitude, longitude, valueKM, txtAbsMasuk, txtAbsKeluar;
    private Utils utils;
    private String storeId;
    private ArrayList<String> storeList;
    private ArrayList<String> storeID;
    private ArrayList<Double> latList1;
    private ArrayList<Double> longList1;
    private List<TablePlanMS> listPlan;
    private String lastid, dateLastMasuk, dateLastKeluar, strButton, url, response,
            employeeId, status, jarak;
    private ArrayAdapter<String> spinnerStore;
    private double lat1, long1, lat2, long2;
    private TableAbsenAdapter tblAbsenAdapter;
    private boolean isTaskRunning = false;

    List<TableMasterMS> listStore;
    List<TableAbsensi> listAbsenMasuk,
            listAbsenKeluar, fetchLogicButton = new ArrayList<>();
    List<TableLogLogin> listLogin = new ArrayList<>();
    private Menu menu;
    private ProgressDialog _progressDialog;
    private SharedPreferences sessionPref;
    private GoogleApiClient Google;
    private LocationRequest mLocationRequest;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.activity_absenms, container, false);
        initView(root);
        return root;
    }

    private void initView(ViewGroup root) {

        absMasuk = (Button) root.findViewById(R.id.absMasuk);
        absKeluar = (Button) root.findViewById(R.id.absKeluar);
        chooseStore = (TextView) root.findViewById(R.id.list_choose_store);
        latitude = (TextView) root.findViewById(R.id.txtLat);
        longitude = (TextView) root.findViewById(R.id.txtLong);
        valueKM = (TextView) root.findViewById(R.id.valueJarak);
        txtAbsMasuk = (TextView) root.findViewById(R.id.valueabsMasuk);
        txtAbsKeluar = (TextView) root.findViewById(R.id.valueabsKelur);

        absMasuk.setOnClickListener(this);
        absKeluar.setOnClickListener(this);
        chooseStore.setOnClickListener(this);

        sessionPref = getContext().getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        employeeId = sessionPref.getString(TableLogLogin.C_EMPLOYEE_ID, "");
        utils = new Utils(getActivity());
        //utils.setGeoLocation();
        prepareShow();
        getDataFromDB();
        logicButton();

    }

    //ON OFF/LOGIC BUTTON
    private void logicButton() {
        TableAbsenAdapter dbAbsen;
        dbAbsen = new TableAbsenAdapter(getActivity());
        try {
            fetchLogicButton = dbAbsen.getDataByCondition(TableAbsensi.UID, employeeId);
            if (fetchLogicButton.size() > 0) {
                strButton = fetchLogicButton.get(0).getAbsen_masuk();

                Long currentDate = Long.parseLong(utils.getCurrentDateMerge());
                Long msuk = Long.parseLong(utils.getCurrentDateMergeReverse(strButton));

                if (strButton == null) {
                    absKeluar.setClickable(false);
                    absKeluar.setBackgroundResource(R.color.cyan_2);
                } else {
                    if (!strButton.equalsIgnoreCase("")
                            && currentDate > msuk) {
                        absKeluar.setClickable(false);
                        absKeluar.setBackgroundResource(R.color.cyan_2);
                    } else {
                        absMasuk.setClickable(false);
                        absMasuk.setBackgroundResource(R.color.cyan_2);
                    }
                }
            } else {
                absKeluar.setClickable(false);
                absKeluar.setBackgroundResource(R.color.cyan_2);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //END ON/OFF LOGIC BUTTON

    private void getDataFromDB() {

        TableMSAdapter dbModerStore;
        TableAbsenAdapter dbAbsenAdapter;
        TablePlanMSAdapter planMSAdapter;

        dbModerStore = new TableMSAdapter(getActivity());
        dbAbsenAdapter = new TableAbsenAdapter(getActivity());
        planMSAdapter = new TablePlanMSAdapter(getActivity());


        storeList = new ArrayList<String>();
        storeID = new ArrayList<String>();
        latList1 = new ArrayList<Double>();
        longList1 = new ArrayList<Double>();
        listPlan = new ArrayList<>();
        listStore = new ArrayList<>();

        if (planMSAdapter.getAllData() != null) {
            String date = utils.getCurrentDate();
            listPlan = planMSAdapter.getDatabyCondition(TablePlanMS.KEY_PLAN_DATE, date);
            if (listPlan.size() > 0 && listPlan != null && !listPlan.isEmpty()) {
                for (int i = 0; i < listPlan.size(); i++) {
                    TablePlanMS item = listPlan.get(i);
                    //START Make show Option on DROP DOWN "PILIH STORE"
                    listStore = dbModerStore.getDatabyCondition(TableMasterMS.FLD_ENTITY, item.getEntity_id());
                    if (listStore.size() > 0) {
                        storeList.add(listStore.get(0).getName());
                        storeID.add(listStore.get(0).getId());
                        latList1.add(Double.valueOf(listStore.get(0).getLatitude()));
                        longList1.add(Double.valueOf(listStore.get(0).getLongitude()));
                    }
                    //END Make show Option on DROP DOWN "PILIH STORE"
                }
            }
        }


        //START Make show History Last Absen Masuk
        try {
            listAbsenMasuk = dbAbsenAdapter.getDataByCondition(TableAbsensi.UID, employeeId);
            for (int i = 0; i < listAbsenMasuk.size(); i++) {
                dateLastMasuk = String.valueOf(listAbsenMasuk.get(i).getAbsen_masuk());
            }

            String dateLastMasuk1 = String.valueOf(utils.formatDateTimeReverse(dateLastMasuk));
            txtAbsMasuk.setText(dateLastMasuk1);
        } catch (Exception e) {
            e.printStackTrace();

        }
        //END Make show History Last Absen Masuk


        //START Make show History Last Absen Keluar
        try {
            listAbsenKeluar = dbAbsenAdapter.getDataByCondition(TableAbsensi.UID, employeeId);
            for (int i = 0; i < listAbsenKeluar.size(); i++) {
                dateLastKeluar = String.valueOf(listAbsenKeluar.get(i).getAbsen_pulang());
            }

            String dateLastKeluar1 = String.valueOf(utils.formatDateTimeReverse(dateLastKeluar));
            txtAbsKeluar.setText(dateLastKeluar1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //END Make show History Last Absen Keluar

        spinnerStore = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, storeList);
    }

    private void setTextList(double lat, double longi) {
        latitude.setText(String.valueOf(lat));
        longitude.setText(String.valueOf(longi));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.absMasuk:
                if (chooseStore.getText().toString().trim().length() < 1) {
                    Toast.makeText(getActivity(), "harap isi Store", Toast.LENGTH_SHORT).show();
                } else if (latitude == null && longitude == null) {
                    Toast.makeText(getActivity(), "belum mendapatkan latitude atau longitude", Toast.LENGTH_SHORT).show();
                } else {
                    postAbsMasuk();
                }
                break;
            case R.id.absKeluar:
                if (chooseStore.getText().toString().trim().length() < 1) {
                    Toast.makeText(getActivity(), "harap isi Store", Toast.LENGTH_SHORT).show();
                } else if (latitude == null && longitude == null) {
                    Toast.makeText(getActivity(), "belum mendapatkan latitude atau longitude", Toast.LENGTH_SHORT).show();
                } else {
                    postAbsKeluar();
                }
                break;
            case R.id.list_choose_store:
                listStore();
                break;
        }
    }

    private void postAbsMasuk() {
        String logUserID = "";
        TableLogLoginAdapter tblLogLogin;
        tblLogLogin = new TableLogLoginAdapter(getActivity());
        listLogin = tblLogLogin.getAllData();
        for (int i = 0; i < listLogin.size(); i++) {
            logUserID = String.valueOf(listLogin.get(i).getcEmployeeId());
        }
        tblAbsenAdapter = new TableAbsenAdapter(getActivity());
        String valueAbsMasuk = String.valueOf(utils.getCurrentDateandTimeSecond());

        tblAbsenAdapter.delete();

        tblAbsenAdapter.insertData(new TableAbsensi(), logUserID, storeId, valueAbsMasuk, "",
                latitude.getText().toString(), longitude.getText().toString(), valueKM.getText().toString());


        //format Date to dd/MM/yyyy
        try {
            txtAbsMasuk.setText(valueAbsMasuk);
        } catch (Exception e) {
            e.printStackTrace();
        }


        absMasuk.setClickable(false);
        absMasuk.setBackgroundResource(R.color.cyan_2);

        absKeluar.setClickable(true);
        absKeluar.setBackgroundResource(R.color.button_absen_keluar);
        txtAbsKeluar.setText("");

        new postAbsMasukToServer().execute();
    }


    class postAbsMasukToServer extends AsyncTask<String, String, String> {
        String lat      = latitude.getText().toString();
        String longi    = longitude.getText().toString();
        String clockIn  = txtAbsMasuk.getText().toString();
        String jarak    = valueKM.getText().toString();

        @Override
        protected void onPreExecute() {
            if (!isTaskRunning) {
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
                isTaskRunning = true;
                _progressDialog = new ProgressDialog(getActivity());
                _progressDialog.setMessage(getResources().getString(R.string.loading_clockin));
                _progressDialog.setIndeterminate(false);
                _progressDialog.setCancelable(false);
                _progressDialog.show();
            } else {
                return;
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                employeeId = listLogin.get(listLogin.size() - 1).getcEmployeeId();
                url = ConnectionManager.CM_URL_ABSEN;
                response = ConnectionManager.requestPushAbsen(url, storeId, employeeId, "1",
                        lat, longi, clockIn, jarak, getActivity());
                JSONObject object = new JSONObject(response);
                if (object != null) {
                    status = object.getString("status");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return status;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null && status.equalsIgnoreCase("200")) {
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Clock IN Success", Toast.LENGTH_SHORT).show();
                }
                isTaskRunning = false;
            } else {

                SharedPreferences preferences = getActivity()
                        .getSharedPreferences(getString(R.string.fn_absen), Context.MODE_PRIVATE);
                SharedPreferences.Editor shareEditor = preferences.edit();
                shareEditor.putBoolean("fabsen", true);
                shareEditor.commit();

                Toast.makeText(getActivity(), "Clock IN Process", Toast.LENGTH_SHORT).show();
                TableFailedAbsenAdapter tableFailedAbsen = new TableFailedAbsenAdapter(getActivity());
                tableFailedAbsen.insertData(new TableFailedAbsen(), storeId, employeeId, "1", lat,
                        longi, clockIn, "0", jarak);
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
                isTaskRunning = false;

            }
        }
    }


    private void postAbsKeluar() {

        TableAbsenAdapter dbAbsenAdapter;
        TableLogLoginAdapter tblLogLogin;
        dbAbsenAdapter = new TableAbsenAdapter(getActivity());
        tblLogLogin = new TableLogLoginAdapter(getActivity());
        listLogin = tblLogLogin.getAllData();
        tblAbsenAdapter = new TableAbsenAdapter(getActivity());
        String valueAbsKeluar = String.valueOf(utils.getCurrentDateandTimeSecond());
        listAbsenKeluar = dbAbsenAdapter.getAllData();
        tblAbsenAdapter.updatePartial(getActivity(), TableAbsensi.ABSPULANG, valueAbsKeluar,
                TableAbsensi.IDSTORE, storeId);

        try {
            txtAbsKeluar.setText(valueAbsKeluar);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new postAbsKeluarToServer().execute();
    }

    class postAbsKeluarToServer extends AsyncTask<String, String, String> {
        String lat      = latitude.getText().toString();
        String longi    = longitude.getText().toString();
        String clockOut = txtAbsKeluar.getText().toString();
        String jarak    = valueKM.getText().toString();

        @Override
        protected void onPreExecute() {
            if (!isTaskRunning) {
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
                isTaskRunning = true;
                _progressDialog = new ProgressDialog(getActivity());
                _progressDialog.setMessage(getResources().getString(R.string.loading_clockout));
                _progressDialog.setIndeterminate(false);
                _progressDialog.setCancelable(false);
                _progressDialog.show();
            } else {
                return;
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                employeeId = listLogin.get(listLogin.size() - 1).getcEmployeeId();
                url = ConnectionManager.CM_URL_ABSEN;
                response = ConnectionManager.requestPushAbsen(url, storeId, employeeId, "2",
                        lat, longi, clockOut, jarak, getActivity());
                JSONObject object = new JSONObject(response);
                if (object != null) {
                    status = object.getString("status");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return status;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null
                    && status.equalsIgnoreCase("200")) {
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Clock Out Success", Toast.LENGTH_SHORT).show();
                }
                isTaskRunning = false;
            } else {
                SharedPreferences preferences = getActivity()
                        .getSharedPreferences(getString(R.string.fn_absen), Context.MODE_PRIVATE);
                SharedPreferences.Editor shareEditor = preferences.edit();
                shareEditor.putBoolean("fabsen", true);
                shareEditor.commit();

                Toast.makeText(getActivity(), "Clock Out Process", Toast.LENGTH_SHORT).show();
                TableFailedAbsenAdapter tableFailedAbsen = new TableFailedAbsenAdapter(getActivity());
                tableFailedAbsen.insertData(new TableFailedAbsen(), storeId, employeeId, "2", lat,
                        longi, clockOut, "0", jarak);
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
                isTaskRunning = false;

            }
        }
    }

    private void listStore() {
        new AlertDialog.Builder(getActivity())
                .setAdapter(spinnerStore,
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                                int which) {
                                chooseStore.setText(storeList.get(which).toString());
                                lat1 = latList1.get(which);
                                long1 = longList1.get(which);
                                storeId = storeID.get(which);
                                calculateKM();
                                dialog.dismiss();
                            }
                        }).create().show();
    }

    private void calculateKM() {
        if (lat2 != 0.0 && long2 != 0.0) {
            valueKM.setText(String.valueOf(distance(lat1, long1, lat2, long2)));
        } else {
            Toast.makeText(getActivity(), "Refresh Kordinat Anda!", Toast.LENGTH_SHORT).show();
            chooseStore.setText("");
        }
    }

    public String distance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        int dist = (int) (earthRadius * c);

        NumberFormat defaultF = NumberFormat.getInstance();
        String result = defaultF.format(dist)
                .replace(",", ".");
        return result;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.info_menu, menu);
        inflater.inflate(R.menu.get_geo, menu);
        this.menu = menu;
        MenuItem alert = menu.findItem(R.id.count);
        alert.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                break;
            case R.id.action_menu_location:
                onConnected(new Bundle());
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void prepareShow() {
        if (Google == null) {
            Google = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1000); // 1 second, in milliseconds
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            if (ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = LocationServices.FusedLocationApi.getLastLocation(Google);
            if (location == null) {
                Toast.makeText(getActivity(), "Pastikan GPS Anda Active", Toast.LENGTH_SHORT).show();
                LocationServices.FusedLocationApi.requestLocationUpdates(Google, mLocationRequest, this);
            } else {
                lat2 = location.getLatitude();
                long2 = location.getLongitude();
                setTextList(lat2, long2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Google.connect();
            }
        }, 1000);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        lat2 = location.getLatitude();
        long2 = location.getLongitude();
        setTextList(lat2, long2);
    }

    @Override
    public void onResume() {
        super.onResume();
        Google.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Google.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(Google, this);
            Google.disconnect();
        }

    }
}
