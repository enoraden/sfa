package myskysfa.com.sfa.main.menu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.main.menu.dtdproject.ProMaster;
import myskysfa.com.sfa.main.menu.master.Katalog;
import myskysfa.com.sfa.main.menu.master.Material;
import myskysfa.com.sfa.main.menu.master.Modern_Store;
import myskysfa.com.sfa.main.menu.master.Package;
import myskysfa.com.sfa.main.menu.master.Promo;

/**
 * Created by admin on 6/20/2016.
 */
public class DataMaster extends Fragment {
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 5;
    private Menu menu;
    private MenuItem alert;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment_master, null);
        tabLayout = (TabLayout) view.findViewById(R.id.tabsMaster);
        viewPager = (ViewPager) view.findViewById(R.id.viewpagermaster);
        viewPager.setOffscreenPageLimit(5);

        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });
        return view;
    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new Material();
                case 1:
                    return new Modern_Store();
                case 2:
                    return new Package();
                /*case 3:
                    Fragment fragOne = new ProMaster();
                    Bundle bundle = new Bundle();
                    bundle.putString("filter", "Stock On Hand");
                    fragOne.setArguments(bundle);
                    return fragOne;*/
                case 3:
                    return new Promo();
                case 4:
                    return new Katalog();
            }
            return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }


        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Material";
                case 1:
                    return "Modern Store";
                case 2:
                    return "Package";
                /*case 3:
                    return "Stock On Hand";*/
                case 3:
                    return "Promo";
                case 4:
                    return "Catalog";
            }
            return null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.info_menu, menu);
        this.menu = menu;
        alert = this.menu.findItem(R.id.count);
        alert.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }
}
