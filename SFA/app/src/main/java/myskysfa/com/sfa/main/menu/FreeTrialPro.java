package myskysfa.com.sfa.main.menu;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFreeTrial;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.TablePlan;
import myskysfa.com.sfa.database.db_adapter.TableFreeTrialAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePlanAdapter;
import myskysfa.com.sfa.main.menu.dtdproject.ProMaster;
import myskysfa.com.sfa.main.menu.dtdproject.ProTaskList;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by Hari Hendryan on 12/16/2015.
 */
public class FreeTrialPro extends Fragment {

    private ArrayAdapter<CharSequence> mAdapterFilter;
    private View view;
    private SharedPreferences sessionPref;
    private Utils utils;
    private Handler handler;
    private static TextView tv, _tsName;
    private Fragment fragProHome;
    private String target, planId, zipCode, tsCode, response, sflCode,token, imei,planUrl,
            username,planDate,value,area, tsName;
    private int signal;
    private JSONArray jsonArray;
    private TableFreeTrialAdapter dbAdapter;
    private List<TableFreeTrial> list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_frag_ftdtd, container, false);
        sessionPref = getContext().getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        initView(view);
        return view;
    }

    private void initView(View v){

        utils           = new Utils(getActivity());
        handler         = new Handler();
        utils       = new Utils(getActivity());
        utils.setIMEI();
        _tsName         = (TextView) v.findViewById(R.id.ts_name);
        Spinner spinner = (Spinner) v.findViewById(R.id.task_filter);
        mAdapterFilter  = ArrayAdapter.createFromResource(getContext(), R.array.spinner_pro, R.layout.status_spinner);
        mAdapterFilter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(mAdapterFilter);
        spinner.setOnItemSelectedListener(filterAct);
        planUrl     = ConnectionManager.CM_PLAN_ALL;
        //planTask    = new PlanTask();
        //StartAsyncTaskInParallel(planTask);
    }

    private AdapterView.OnItemSelectedListener filterAct = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String filterParam = parent.getItemAtPosition(position).toString();

            switch (filterParam){
                case "Free Trial" :
                    addFragTask("F");
                    break;
                case "Stock On Hand" :
                    addFragMaster("Stock On Hand");
                    break;
                case "DTD Jual Putus" :
                    addFragTask("P");
                    break;
                case "DTD Pinjam Pakai" :
                    addFragTask("T");
                    break;
                case "Survey List":
                    //addFragSurvey();
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    private void addFragTask(String filter){
        FragmentManager fm = getFragmentManager();
        fragProHome = fm.findFragmentByTag("ft_home");
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragOne = new ProTaskList();
        Bundle bundle = new Bundle();
        bundle.putString("filter", filter);
        fragOne.setArguments(bundle);

        if (fragProHome == null) {
            ft.add(R.id.frame_spinner_ft, fragOne,"ft_home");
        }else{
            ft.replace(R.id.frame_spinner_ft, fragOne,"ft_home");
        }
        ft.addToBackStack(null).commit();
    }

    /*private void addFragSurvey(){
        FragmentManager fm = getFragmentManager();
        fragProHome = fm.findFragmentByTag("ft_home");
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragOne = new FTSurvayList();

        if (fragProHome == null) {
            ft.add(R.id.frame_spinner_ft, fragOne,"ft_home");
        }else{
            ft.replace(R.id.frame_spinner_ft, fragOne,"ft_home");
        }
        ft.addToBackStack(null).commit();
    }*/

    private void addFragMaster(String mFilter){
        FragmentManager fm = getFragmentManager();
        fragProHome = fm.findFragmentByTag("ft_home");
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragOne = new ProMaster();
        Bundle bundle = new Bundle();
        bundle.putString("filter", mFilter);
        fragOne.setArguments(bundle);

        if (fragProHome == null) {
            ft.add(R.id.frame_spinner_ft, fragOne,"ft_home");
        }else{
            ft.replace(R.id.frame_spinner_ft, fragOne,"ft_home");
        }
        ft.addToBackStack(null).commit();
    }

    private class PlanTask extends AsyncTask<String, String, String> {

        boolean status;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            signal = utils.checkSignal(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                status          = false;
                imei            = utils.getIMEI();
                sflCode         = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
                token           = sessionPref.getString(TableLogLogin.C_TOKEN, "");
                username = sessionPref.getString(TableLogLogin.C_USER_NAME, "");
                /*mTableLogLoginAdapter = new TableLogLoginAdapter(getActivity());
                listLogLogin          = mTableLogLoginAdapter.fetchLastData(getActivity(), TableLogLogin.fLOG_DATE);

                if (listLogLogin != null && listLogLogin.size()>0) {
                    sflCode               = listLogLogin.get(listLogLogin.size()-1).getSflCode();
                    token                 = listLogLogin.get(listLogLogin.size()-1).getToken();
                }*/
                planUrl     = planUrl + sflCode + "/1";
                response    = ConnectionManager.requestPlan(planUrl, imei, token,username,Config.version, String.valueOf(signal), getActivity());
                Log.d("FreeTrial", "response= "+response);
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject!=null) {
                    status = jsonObject.getBoolean(Config.KEY_STATUS);
                    jsonArray = jsonObject.getJSONArray(Config.KEY_DATA);
                    if (status) {
                        TablePlanAdapter planDbAdapter = new TablePlanAdapter(getActivity());
                        /*if (jsonArray.length()>0) {
                            planDbAdapter.deleteAll();
                        }*/

                        planDbAdapter.delete(getActivity());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            sflCode         = obj.getString("sfl_code");
                            planId          = obj.getString("plan_id");
                            planDate        = obj.getString("plan_date");
                            zipCode         = obj.getString("zip_code");
                            area            = obj.getString("area");
                            tsCode          = obj.getString("ts_code");
                            tsName          = obj.getString("ts_name");
                            target          = obj.getString( "target");
                            value           = obj.toString();

                            planDbAdapter.insertData(new TablePlan(),planId, planDate, zipCode, sflCode, area, tsCode,
                                    tsName, target, "0", value, "0");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (status) {
                    getStockTaskValue();
                } else {
                    utils.showErrorDlg(handler, getActivity().getResources().getString(R.string.network_error),
                            getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getStockTaskValue() {
        TablePlanAdapter tablePlanAdapter = new TablePlanAdapter(getActivity());
        dbAdapter = new TableFreeTrialAdapter(getActivity());
        List<TableFreeTrial> listProspect = new ArrayList<TableFreeTrial>();
        List<TablePlan> listPlan = new ArrayList<TablePlan>();
        String date = utils.getCurrentDate();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c.getTime());
        listPlan = tablePlanAdapter.getDatabySfl(getActivity(), TablePlan.KEY_PLAN_DATE, date);
        list = dbAdapter.getDatabyCondition(TableFreeTrial.KEY_CREATED, formattedDate);

        for (int n=0; n<listPlan.size();n++) {
            TablePlan item = listPlan.get(n);
            if (item!=null) {
                _tsName.setText(item.getPlan_id()+" - "+item.getTs_name());
                String target = list.size()+"/" + item.getTarget();
                tv.setText(target);
            } else {
                tv.setText("0");
                _tsName.setText("");
            }
        }
    }

    public void setTarget(String target){
        tv.setText(target);
    }

    public void setTsName(String tsName) {
        _tsName.setText(tsName);
    }

    public String getTsName() {
        return this.tsName;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void StartAsyncTaskInParallel(PlanTask task) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            task.execute();
    }
}
