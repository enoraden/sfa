package myskysfa.com.sfa.main.menu;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.MSAlacarteListAdapter;
import myskysfa.com.sfa.adapter.SpinnerBrandAdapter;
import myskysfa.com.sfa.adapter.SpinnerHWAdapter;
import myskysfa.com.sfa.adapter.SpinnerMaterialAdapter;
import myskysfa.com.sfa.adapter.SpinnerPackAdapter;
import myskysfa.com.sfa.adapter.SpinnerPromoAdapter;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableFreeTrial;
import myskysfa.com.sfa.database.TableFreeTrialCoh;
import myskysfa.com.sfa.database.TableMBrand;
import myskysfa.com.sfa.database.TableMasterMaterial;
import myskysfa.com.sfa.database.TableMasterPackage;
import myskysfa.com.sfa.database.TableMasterPromo;
import myskysfa.com.sfa.database.TablePlan;
import myskysfa.com.sfa.database.db_adapter.TableFTSOHAdapter;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.database.db_adapter.TableFreeTrialAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMBrandAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMasterMaterialAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMasterPackageAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMasterPromoAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePlanAdapter;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by Eno on 11/4/2016.
 */

public class MSDTDPackage {
    private static Activity activity;
    private Utils utils;
    private PopupWindow mpopup;
    private RecyclerView recyclerView;
    private Button btnSave;
    private TableMasterPackageAdapter mTablePackageProductAdapter;
    private TableMasterPromoAdapter mPromoAdapter;
    private TableMBrandAdapter mBrandAdapter;
    private TableFTSOHAdapter mMaterialAdapter;
    private List<TableMasterPackage> mPackageProductListItem;

    private List<TableFormApp> listFormApp = new ArrayList<>();
    private List<TableFreeTrialCoh> listSOH = new ArrayList<>();
    private List<TableMBrand> mBrandListItem = new ArrayList<>();
    private List<TableMasterPromo> mPromoListItems = new ArrayList<>();
    private List<TableFreeTrialCoh> mMaterialListItems = new ArrayList<>();
    private ArrayList<String> listProductPackage = new ArrayList<>();
    private ArrayList<ArrayList<String>> selAlacarteSaveAll = new ArrayList<>();
    private List<TablePlan> listPlan = new ArrayList<TablePlan>();
    private List<TableFreeTrial> listFreetrial = new ArrayList<TableFreeTrial>();
    private List<TableMasterPackage> mPackageProductListItemList;
    private ArrayList<String> listPromo, listBrand, listVc, listDsd,
            listLnb, listDish, listMaterial;
    private ArrayList<String> selAlacarte = new ArrayList<String>();
    private SpinnerPromoAdapter spinPromoAdapter;
    private SpinnerBrandAdapter spinBrandAdapter;
    private SpinnerMaterialAdapter spinnerMaterialAdapter;
    private TableFTSOHAdapter dbCohAdapter;
    private MSAlacarteListAdapter mPackageProductAdapter;
    private SpinnerHWAdapter spinAdapterVC, spinAdapterLNB, spinAdapterDSD, spinAdapterODU,
            spinAdapterType;
    private int totPakacge = 0, isMulty, totAdditional = 0;
    private LinearLayout mLayoutPackage, mLayoutAdd, mLayoutAdditional, packageBundling, LinearBundling;
    private int target, index;
    private static int hrgBasic, hrgAlacarte, hargaAdditional;
    private Spinner spinPackage, spinType, spinBrand, spinVc, spinLnb, spinDsd, spinDish,
            materialAdd, bundling;
    private SearchableSpinner spinPromo;
    private TextView pricePackage, priceInstalasi, pricePromo, priceAlacarte, txtPack;
    private ViewGroup mView;
    private SpinnerPackAdapter spinAdapter;
    private String promoCode, promoPrice, formattednumber, strPaket, strAlacarte, paket, saveBrand, numberForm, appType,
            simcardId = "", routerId = "", priceMaterial = "", product_id = "", basic_id;
    private Double duit;
    private NumberFormat defaultF;
    private TableFormAppAdapter mFormAppAdapter;
    private TablePlanAdapter tablePlanAdapter;
    private TableFreeTrialAdapter dbAdapter;
    //private ArrayAdapter<String> spinnerVc, spinnerDsd, spinnerLnb, spinnerDish;
    private ImageView additional, btnAdd, btnBundling, delete_additional;
    private int count;
    private Button save;
    private Boolean isShowBundling = true, isShowAdditonal = true;
    private EditText countAdd, simcardValue, routerValue;


    public MSDTDPackage(Activity activity, ViewGroup mView, LinearLayout mLayoutAdd,
                        LinearLayout mLayoutPackage, Spinner spinType,
                        Spinner spinBrand, SearchableSpinner spinPromo, TextView priceInstalasi, TextView pricePackage,
                        TextView pricePromo, String numberForm, String appType, Spinner bundling,
                        TextView priceAlacarte, Button save) {
        this.activity = activity;
        this.mView = mView;
        this.mLayoutPackage = mLayoutPackage;
        this.mLayoutAdd = mLayoutAdd;
        this.spinType = spinType;
        this.spinBrand = spinBrand;
        this.spinPromo = spinPromo;
        this.priceInstalasi = priceInstalasi;
        this.pricePackage = pricePackage;
        this.pricePromo = pricePromo;
        this.numberForm = numberForm;
        this.appType = appType;
        this.save = save;
        this.bundling = bundling;
        this.priceAlacarte = priceAlacarte;

    }

    public void InitView() {

        // generate basic list
        utils = new Utils(activity);
        dbCohAdapter = new TableFTSOHAdapter(activity);
        mFormAppAdapter = new TableFormAppAdapter(activity);
        listProductPackage = new ArrayList<String>();
        listPromo = new ArrayList<String>();
        listBrand = new ArrayList<String>();
        listVc = new ArrayList<String>();
        listDsd = new ArrayList<String>();
        listLnb = new ArrayList<String>();
        listDish = new ArrayList<String>();
        listSOH = new ArrayList<TableFreeTrialCoh>();
        listMaterial = new ArrayList<>();
        mTablePackageProductAdapter = new TableMasterPackageAdapter(activity);
        mPromoAdapter = new TableMasterPromoAdapter(activity);
        mBrandAdapter = new TableMBrandAdapter(activity);
        mMaterialAdapter = new TableFTSOHAdapter(activity);
        listFormApp = mFormAppAdapter.getDatabyCondition(TableFormApp.fFORM_NO, numberForm);
        mBrandListItem = mBrandAdapter.getAllData();
        mPromoListItems = mPromoAdapter.getAllData();
        mMaterialListItems = mMaterialAdapter.getDatabyCondition(TableFreeTrialCoh.KEY_IS_SERIALIZE, "N");
        listSOH = dbCohAdapter.getDatabyCondition(TableFreeTrialCoh.KEY_HW_STATUS, "ADD");

        for (int i = 0; i < mPromoListItems.size(); i++) {
            TableMasterPromo item = mPromoListItems.get(i);
            listPromo.add(item.getPromotion_code());
        }

        for (int i = 0; i < mBrandListItem.size(); i++) {
            TableMBrand item = mBrandListItem.get(i);
            listBrand.add(item.getBrand_name());
        }

        for (int i = 0; i < listFormApp.size(); i++) {
            TableFormApp item = listFormApp.get(i);
            if (item != null) {
                paket = item.getVALUES_PACKAGE();
            }
        }

        for (int i = 0; i < mMaterialListItems.size(); i++) {
            TableFreeTrialCoh item = mMaterialListItems.get(i);
            listMaterial.add(item.getName());
        }


        /**
         * Generate Process Package
         */
        listVc.add("");
        listDsd.add("");
        listLnb.add("");
        listDish.add("");
        for (int i = 0; i < listSOH.size(); i++) {
            TableFreeTrialCoh item = listSOH.get(i);
            if (item.getType().equalsIgnoreCase("VC")) {
                listVc.add(item.getSn());
            }
            if (item.getType().equalsIgnoreCase("DEC")) {
                listDsd.add(item.getSn());
            }
            if (item.getType().equalsIgnoreCase("LNB")) {
                listLnb.add(item.getSn());
            }
            if (item.getType().equalsIgnoreCase("ANT")) {
                listDish.add(item.getSn());
            }
        }
        // End Process

        //spinnerPromo = new ArrayAdapter<String>(activity, R.layout.support_simple_spinner_dropdown_item, listPromo);
        //spinnerBrand = new ArrayAdapter<String>(activity, R.layout.support_simple_spinner_dropdown_item, listBrand);

        //spinPromoAdapter = new SpinnerPromoAdapter(activity, R.layout.status_spinner, mPromoListItems);
        //spinPromoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinBrandAdapter = new SpinnerBrandAdapter(activity, R.layout.status_spinner, mBrandListItem);
        spinBrandAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinPromoAdapter = new SpinnerPromoAdapter(activity, R.layout.status_spinner, listPromo);
        spinPromoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerMaterialAdapter = new SpinnerMaterialAdapter(activity, R.layout.status_spinner, mMaterialListItems);
        spinnerMaterialAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayList<String> typeList = new ArrayList<String>();
        typeList.add("Single");
        typeList.add("Multi");

        spinAdapterType = new SpinnerHWAdapter(activity, R.layout.status_spinner, typeList);
        spinAdapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //spinPromo = (Spinner) mView.findViewById(R.id.spinner_promo);
        //spinPromo.setAdapter(spinPromoAdapter);
        //spinPromo.setOnItemSelectedListener(spinPromoListener);

        spinType = (Spinner) mView.findViewById(R.id.spinner_type);
        spinType.setAdapter(spinAdapterType);
        spinType.setSelection(0);
        spinType.setOnItemSelectedListener(spinerTypeListener);

        spinBrand = (Spinner) mView.findViewById(R.id.spinner_brand);
        spinBrand.setAdapter(spinBrandAdapter);
        spinBrand.setSelection(2);
        spinBrand.setOnItemSelectedListener(spinBrandListener);

        spinPromo = (SearchableSpinner) mView.findViewById(R.id.spinner_promo);
        spinPromo.setAdapter(spinPromoAdapter);
        spinPromo.setSelection(0);
        spinPromo.setTitle("Select Item");
        spinPromo.setOnItemSelectedListener(spinerPromoListener);


        additional = (ImageView) mView.findViewById(R.id.btn_add_additional);
        delete_additional = (ImageView) mView.findViewById(R.id.btn_min_additional);
        btnAdd = (ImageView) mView.findViewById(R.id.btn_add_package);

        additional.setOnClickListener(addAdditionalItems);
        delete_additional.setOnClickListener(deleteAdditional);
        btnAdd.setOnClickListener(actAddPackage);

        priceInstalasi.setText("200000");

        packageBundling = (LinearLayout) mView.findViewById(R.id.bundling);
        btnBundling = (ImageView) mView.findViewById(R.id.btn_add_bundling);
        LinearBundling = (LinearLayout) mView.findViewById(R.id.itemBundling);
        simcardValue = (EditText) mView.findViewById(R.id.sValue);
        routerValue = (EditText) mView.findViewById(R.id.rValue);
        bundling.setOnItemSelectedListener(bundlingListener);
        btnBundling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isShowBundling) {
                    packageBundling.setVisibility(View.VISIBLE);
                    isShowBundling = false;
                    LinearBundling.setVisibility(View.VISIBLE);
                    btnBundling.setImageResource(R.drawable.ic_cancel_bundling);
                    simcardId = "882";
                    routerId = "881";
                } else {
                    packageBundling.setVisibility(View.GONE);
                    isShowBundling = true;
                    LinearBundling.setVisibility(View.GONE);
                    btnBundling.setImageResource(R.drawable.ic_plus);
                    simcardId = "";
                    routerId = "";
                    simcardValue.setText("");
                    routerValue.setText("");
                }
            }
        });

        if (paket != null) {
            try {
                JSONObject paketObj = new JSONObject(paket);
                String vc = paketObj.getString("VC");
                String odu = paketObj.getString("ODU");
                String lnb = paketObj.getString("LNB");
                String dsd = paketObj.getString("DSD");
                if (vc != null)
                    setSelectHw("VC", vc);
                if (odu != null)
                    setSelectHw("ANT", odu);
                if (lnb != null)
                    setSelectHw("LNB", lnb);
                if (dsd != null)
                    setSelectHw("DEC", dsd);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        save.setOnClickListener(actSaveAll);
    }

    private AdapterView.OnItemSelectedListener bundlingListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (bundling.getSelectedItem().toString().equalsIgnoreCase("xl") ||
                    position == 0) {
                simcardId = "882";
                routerId = "881";
            } else if (bundling.getSelectedItem().toString().equalsIgnoreCase("indosat") ||
                    position == 1) {
                simcardId = "888";
                routerId = "887";
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private void setSelectHw(String type, String val) {
        if (type.equalsIgnoreCase("VC")) {
            for (int i = 0; i < listVc.size(); i++) {
                if (listVc.get(i).equalsIgnoreCase(val)) {
                    spinVc.setSelection(i);
                }
            }
        }

        if (type.equalsIgnoreCase("DEC")) {
            for (int i = 0; i < listDsd.size(); i++) {
                if (listDsd.get(i).equalsIgnoreCase(val)) {
                    spinDsd.setSelection(i);
                }
            }
        }

        if (type.equalsIgnoreCase("ANT")) {
            for (int i = 0; i < listDish.size(); i++) {
                if (listDish.get(i).equalsIgnoreCase(val)) {
                    spinDish.setSelection(i);
                }
            }
        }

        if (type.equalsIgnoreCase("LNB")) {
            for (int i = 0; i < listLnb.size(); i++) {
                if (listLnb.get(i).equalsIgnoreCase(val)) {
                    spinLnb.setSelection(i);
                }
            }
        }
    }

    /**
     * Spin Listener
     */

    AdapterView.OnItemSelectedListener spinerTypeListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0) {
                for (int i = totPakacge; i >= 1; i--) {
                    delPackage(i);
                }
                isMulty = 0;
                addPackage();
                mLayoutAdd.setVisibility(View.GONE);
            } else {
                if (appType.equals("d")) {
                    getTarget();
                    isMulty = 1;
                    if (target >= 2) {
                        addPackage();
                        mLayoutAdd.setVisibility(View.VISIBLE);
                    } else {
                        spinType.setSelection(0);
                        Toast.makeText(activity, "Sisa plan anda kurang dari 2", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    isMulty = 1;
                    addPackage();
                    mLayoutAdd.setVisibility(View.VISIBLE);
                }


            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private AdapterView.OnItemSelectedListener spinBrandListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            String code = mBrandListItem.get(position).getBrand_code();
            TableMasterPackageAdapter tableMPackageProductAdapter
                    = new TableMasterPackageAdapter(activity);
            List<TableMasterPackage> paketList = new ArrayList<TableMasterPackage>();
            paketList = tableMPackageProductAdapter.fetchByProfile("2", code, product_id);
            if (paketList != null) {
                for (int m = 0; m < paketList.size(); m++) {
                    TableMasterPackage item = paketList.get(m);
                    if (item != null) {
                        item.getPrice();
                        Double install = Double.parseDouble(item.getPrice());
                        NumberFormat defaultF = NumberFormat.getInstance();
                        String formattednumber = defaultF.format(install);
                        String str = formattednumber.replace(",", ".");
                        //instalasi.setText("Rp." + strPaket);
                        //
                    }
                }
            }

            LinearLayout mLayoutBackProduct = (LinearLayout) mView.findViewById(R.id.back_product);
            switch (code) {
                case "0":
                    mLayoutBackProduct.setBackgroundColor(Color.parseColor("#43a047"));
                    break;
                case "1":
                    mLayoutBackProduct.setBackgroundColor(Color.parseColor("#ffd600"));
                    break;
                case "2":
                    mLayoutBackProduct.setBackgroundColor(Color.parseColor("#01579b"));
                    break;
                default:
                    mLayoutBackProduct.setBackgroundColor(Color.parseColor("#01579b"));
                    break;
            }

            mPackageProductListItemList = mTablePackageProductAdapter.fetchBasic(code);
            for (int i = 0; i < mPackageProductListItemList.size(); i++) {
                TableMasterPackage item = mPackageProductListItemList.get(i);
                listProductPackage.add(item.getProduct_name());
            }
            //spinnerProductPackage = new ArrayAdapter<String>(activity, R.layout.support_simple_spinner_dropdown_item, listProductPackage);
            spinAdapter = new SpinnerPackAdapter(activity, R.layout.status_spinner, mPackageProductListItemList);
            spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            /*for (int i=0; i<listSOH.size();i++) {
                if(listSOH.get(i).getType().equalsIgnoreCase("VC")) {
                    listVc.add(listSOH.get(i).getSn());
                } else if (listSOH.get(i).getType().equalsIgnoreCase("LNB")) {
                    listLnb.add(listSOH.get(i).getSn());
                } else if (listSOH.get(i).getType().equalsIgnoreCase("ANT")) {
                    listDish.add(listSOH.get(i).getSn());
                } else if (listSOH.get(i).getType().equalsIgnoreCase("DEC")) {
                    listDsd.add(listSOH.get(i).getSn());
                }
            }*/

            spinAdapterVC = new SpinnerHWAdapter(activity, R.layout.status_spinner, listVc);
            spinAdapterVC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinAdapterLNB = new SpinnerHWAdapter(activity, R.layout.status_spinner, listLnb);
            spinAdapterLNB.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinAdapterODU = new SpinnerHWAdapter(activity, R.layout.status_spinner, listDish);
            spinAdapterODU.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinAdapterDSD = new SpinnerHWAdapter(activity, R.layout.status_spinner, listDsd);
            spinAdapterDSD.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            try {
                mLayoutPackage.removeAllViews();
                selAlacarteSaveAll.clear();
                totPakacge = 0;
                addPackage();
            } catch (NullPointerException e) {

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapter) {
        }
    };


    AdapterView.OnItemSelectedListener spinerPromoListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            promoCode = mPromoListItems.get(position).getPromotion_code();
            //promoPrice = mPromoListItems.get(position).
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    private void delPackage(int tag) {
        try {
            if (mLayoutPackage.findViewWithTag("PACK_" + tag) != null) {
                mLayoutPackage.removeView(mLayoutPackage.findViewWithTag("PACK_" + tag));
                totPakacge = totPakacge - 1;
                hrgBasic = 0;
                for (int i = 0; i < totPakacge; i++) {
                    spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (i + 1));
                    String product_id = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getProduct_id();
                    TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(activity);
                    List<TableMasterPackage> packageProductListItems
                            = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fROWID, product_id);

                    for (int n = 0; n < packageProductListItems.size(); n++) {
                        TableMasterPackage item = packageProductListItems.get(n);
                        hrgBasic = hrgBasic + Integer.parseInt(item.getPrice());
                    }
                }
                Log.d("FTPaket", "Coba= " + hrgBasic);

                for (int j = 0; j < selAlacarteSaveAll.size(); j++) {
                    if (j == (tag - 1)) {
                        selAlacarteSaveAll.remove(j);
                    }
                }

                hrgAlacarte = 0;
                for (int x = 0; x < selAlacarteSaveAll.size(); x++) {

                    for (int y = 0; y < selAlacarteSaveAll.get(x).size(); y++) {
                        if (x == 0) {

                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(activity);
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                            }
                            //alaBuild1.append(selAlacarteSaveAll.get(x).get(y).toString());
                            //alaBuild1.append(",");
                        }
                        if (x == 1) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter
                                    = new TableMasterPackageAdapter(activity);
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                            }
                        }
                        if (x == 2) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter
                                    = new TableMasterPackageAdapter(activity);
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                            }
                        }

                    }
                }

                /*duit = Double.parseDouble(String.valueOf(hrgBasic + hrgAlacarte));
                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(duit);
                strPaket = formattednumber.replace(",", ".");*/
                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(hrgBasic + hargaAdditional);
                strPaket = formattednumber.replace(",", ".");

                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(hrgAlacarte);
                strAlacarte = formattednumber.replace(",", ".");

                pricePackage.setText(strPaket);
                priceAlacarte.setText(strAlacarte);


                LinearLayout layoutTitle = (LinearLayout) mLayoutPackage.findViewWithTag("TITLE_" + totPakacge);
                if (layoutTitle.findViewWithTag(totPakacge) == null) {
                    LinearLayout.LayoutParams pDelPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
                    pDelPackage.gravity = Gravity.END;
                    pDelPackage.setMargins(0, 0, 5, 0);
                    if (totPakacge > 2) {
                        ImageView delPack = new ImageView(activity);
                        delPack.setImageResource(R.drawable.ic_close_package);
                        delPack.setTag(totPakacge);
                        delPack.setLayoutParams(pDelPackage);
                        delPack.setOnClickListener(deletePackage);
                        layoutTitle.addView(delPack);
                    }
                }
            }

        } catch (Exception e) {
        }
    }

    private TextWatcher totalWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String a, b, totB, totA;
            a = pricePackage.getText().toString();
            b = priceInstalasi.getText().toString();
            //totB = b.replace("Rp.", "");
            //totB = totB.replace(".", "");
            totA = a.replace("Rp.", "");
            totA = totA.replace(".", "");
            duit = Double.parseDouble(String.valueOf(Integer.parseInt(totA) + Integer.parseInt(b)));
            defaultF = NumberFormat.getInstance();
            formattednumber = defaultF.format(duit);
            strPaket = formattednumber.replace(",", ".");
            //pricePromo.setText("Rp."+strPaket);
            //pricePromo.setText("Rp.0");
        }
    };

    private View.OnClickListener addAdditionalItems = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AdditionalPackage();
            delete_additional.setVisibility(View.VISIBLE);
        }
    };


    private View.OnClickListener deleteAdditional = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hargaAdditional = 0;
            defaultF = NumberFormat.getInstance();
            formattednumber = defaultF.format(hrgBasic + hargaAdditional);
            strPaket = formattednumber.replace(",", ".");
            pricePackage.setText(strPaket);
            totAdditional = 0;
            mLayoutAdditional.removeAllViews();
            delete_additional.setVisibility(View.GONE);

            /*for (int i = 0; i < mLayoutAdditional.getChildCount(); i++) {
                View childView = mLayoutAdditional.getChildAt(i);
                int b = mLayoutAdditional.indexOfChild(childView);
                materialAdd = (Spinner) childView.findViewWithTag("add_" + (b + 1));
                priceMaterial = mMaterialListItems.get(materialAdd.getSelectedItemPosition()).getRate();
                hargaAdditional = hargaAdditional + Integer.parseInt(priceMaterial);
                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(hrgBasic - hargaAdditional);
                strPaket = formattednumber.replace(",", ".");
                pricePackage.setText(strPaket);
                //mLayoutAdditional.removeView(mLayoutAdditional.findViewWithTag("ADDITIONAL_" + (b)));
            }*/

        }
    };

    private View.OnClickListener actAddPackage = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (appType.equals("d")) {
                if (target > 2 && totPakacge == 3) {
                    Toast.makeText(activity, "Paket tidak boleh lebih dari 3", Toast.LENGTH_SHORT).show();
                } else if (target == 2 && totPakacge == 2) {
                    Toast.makeText(activity, "Paket tidak boleh lebih dari 2", Toast.LENGTH_SHORT).show();
                } else {
                    addPackage();
                }
            } else {
                if (totPakacge == 3) {
                    Toast.makeText(activity, "Paket tidak boleh lebih dari 3", Toast.LENGTH_SHORT).show();
                } else {
                    addPackage();
                }
            }
        }
    };


    private void AdditionalPackage() {
        totAdditional = totAdditional + 1;
        mLayoutAdditional = (LinearLayout) mView.findViewById(R.id.package_additional);

        final LinearLayout linearPackage = new LinearLayout(activity);
        linearPackage.setBackgroundColor(activity.getResources().getColor(R.color.white));
        linearPackage.setOrientation(LinearLayout.VERTICAL);
        linearPackage.setTag("ADDITIONAL_" + totPakacge);

        LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLinearPackage.setMargins(0, 10, 0, 0);
        linearPackage.setLayoutParams(pLinearPackage);
        mLayoutAdditional.addView(linearPackage);

        LinearLayout.LayoutParams pLineTitlePack = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout lineTitlePack = new LinearLayout(activity);
        lineTitlePack.setBackgroundColor(activity.getResources().getColor(R.color.white));
        lineTitlePack.setTag("TITLE_" + totAdditional);
        lineTitlePack.setOrientation(LinearLayout.HORIZONTAL);
        lineTitlePack.setLayoutParams(pLineTitlePack);
        linearPackage.addView(lineTitlePack);

        LinearLayout.LayoutParams pTxtPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtPack = new TextView(activity);
        txtPack.setLayoutParams(pTxtPackage);
        txtPack.setText("Additional Item ");
        txtPack.setPadding(5, 0, 0, 0);
        txtPack.setTypeface(Typeface.DEFAULT_BOLD);
        lineTitlePack.addView(txtPack);
        //Line For Change Items

        LinearLayout.LayoutParams pLineItems = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineItems.setMargins(0, 0, 0, 20);
        LinearLayout linearFormVC = new LinearLayout(activity);
        linearFormVC.setLayoutParams(pLineItems);
        linearFormVC.setOrientation(LinearLayout.HORIZONTAL);
        linearFormVC.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormVC);

        LinearLayout.LayoutParams pTxtItems = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtTitleItem = new TextView(activity);
        txtTitleItem.setLayoutParams(pTxtItems);
        txtTitleItem.setText("Items :");
        txtTitleItem.setPadding(5, 0, 0, 0);
        linearFormVC.addView(txtTitleItem);

        LinearLayout.LayoutParams pTxtAdditionalForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner spinnerAdditional = new Spinner(activity);
        spinnerAdditional.setLayoutParams(pTxtAdditionalForm);
        spinnerAdditional.setPadding(0, 5, 0, 5);
        spinnerAdditional.setTag("add_" + totAdditional);
        //genBasic("VC_" + totPakacge, mLayoutPackage);
        //genVC("item_" + totPakacge, mLayoutPackage);
        genMaterial("add_" + totAdditional, mLayoutAdditional);
        linearFormVC.addView(spinnerAdditional);

        //End Line

        LinearLayout.LayoutParams pLineCounter = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineCounter.setMargins(0, 0, 0, 20);
        LinearLayout linearFormDSD = new LinearLayout(activity);
        linearFormDSD.setLayoutParams(pLineCounter);
        linearFormDSD.setOrientation(LinearLayout.HORIZONTAL);
        linearFormDSD.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormDSD);

        LinearLayout.LayoutParams pTxtCounter = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.30f);
        TextView txtDSD = new TextView(activity);
        txtDSD.setLayoutParams(pTxtCounter);
        txtDSD.setText("Total :");
        txtDSD.setPadding(5, 0, 0, 0);
        linearFormDSD.addView(txtDSD);

        LinearLayout.LayoutParams pTxtCounterForm = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.39f);
        EditText counterEdit = new EditText(activity);
        counterEdit.setLayoutParams(pTxtCounterForm);
        counterEdit.setPadding(5, 0, 0, 0);
        counterEdit.setBackgroundResource(R.drawable.border_item_zipcode);
        counterEdit.setInputType(InputType.TYPE_CLASS_DATETIME);
        counterEdit.setGravity(Gravity.CENTER);
        counterEdit.setTag("addCount_" + totAdditional);
        //genDSD("DSD_" + totPakacge, mLayoutPackage);
        linearFormDSD.addView(counterEdit);

        LinearLayout.LayoutParams pTxtSpinerSatuan = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.2f);
        LinearLayout linearLayout = new LinearLayout(activity);
        linearLayout.setLayoutParams(pTxtSpinerSatuan);
        /*pTxtSpinerSatuan.setMargins(25,0,50,0);
        spinnerSatuan.setPadding(0, 16, 0, 16);*/
        //txtDSDForm.setTag("DSD_" + totPakacge);
        //genDSD("DSD_" + totPakacge, mLayoutPackage);
        linearFormDSD.addView(linearLayout);
    }

    private void genMaterial(final String tagId, final LinearLayout mLayoutAdd) {
        mLayoutAdd.post(new Runnable() {
            @Override
            public void run() {
                materialAdd = (Spinner) mLayoutAdd.findViewWithTag(tagId);
                materialAdd.setAdapter(spinnerMaterialAdapter);
                materialAdd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        //priceMaterial = mMaterialListItems.get(position).getRate();
                        hargaAdditional = 0;
                        for (int i = 0; i < totAdditional; i++) {
                            materialAdd = (Spinner) mLayoutAdditional.findViewWithTag("add_" + (i + 1));
                            countAdd = (EditText) mLayoutAdditional.findViewWithTag("addCount_" + (i + 1));
                            priceMaterial = mMaterialListItems.get(materialAdd.getSelectedItemPosition()).getRate();
                            if (!priceMaterial.equalsIgnoreCase("null")) {
                                hargaAdditional = hargaAdditional + Integer.parseInt(priceMaterial);
                                defaultF = NumberFormat.getInstance();
                                formattednumber = defaultF.format(hrgBasic + hargaAdditional);
                                strPaket = formattednumber.replace(",", ".");
                                pricePackage.setText(strPaket);
                            }
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }
        });
    }

    private void addPackage() {
        totPakacge = totPakacge + 1;
        mLayoutPackage = (LinearLayout) mView.findViewById(R.id.package_list);

        LinearLayout linearPackage = new LinearLayout(activity);
        linearPackage.setBackgroundColor(activity.getResources().getColor(R.color.white));
        linearPackage.setOrientation(LinearLayout.VERTICAL);
        linearPackage.setTag("PACK_" + totPakacge);

        LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLinearPackage.setMargins(0, 10, 0, 0);
        linearPackage.setLayoutParams(pLinearPackage);
        mLayoutPackage.addView(linearPackage);

        LinearLayout.LayoutParams pLineTitlePack = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout lineTitlePack = new LinearLayout(activity);
        lineTitlePack.setBackgroundColor(activity.getResources().getColor(R.color.white));
        lineTitlePack.setTag("TITLE_" + totPakacge);
        lineTitlePack.setOrientation(LinearLayout.HORIZONTAL);
        lineTitlePack.setLayoutParams(pLineTitlePack);
        linearPackage.addView(lineTitlePack);


        //Title Package
        LinearLayout.LayoutParams pTxtPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        txtPack = new TextView(activity);
        txtPack.setLayoutParams(pTxtPackage);
        txtPack.setText("Package " + totPakacge);
        txtPack.setPadding(5, 5, 5, 5);
        txtPack.setTypeface(Typeface.DEFAULT_BOLD);
        lineTitlePack.addView(txtPack);

        LinearLayout.LayoutParams pLineView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 0.5f);
        View lineView = new View(activity);
        lineView.setLayoutParams(pLineView);
        lineView.setBackgroundColor(activity.getResources().getColor(R.color.black_translucent));
        linearPackage.addView(lineView);

        LinearLayout.LayoutParams pLineForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearForm = new LinearLayout(activity);
        linearForm.setLayoutParams(pLineForm);
        linearForm.setOrientation(LinearLayout.HORIZONTAL);
        linearForm.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearForm);

        LinearLayout.LayoutParams pTxtBasic = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtBasic = new TextView(activity);
        txtBasic.setLayoutParams(pTxtBasic);
        txtBasic.setText("Basic");
        txtBasic.setPadding(5, 0, 0, 0);
        linearForm.addView(txtBasic);

        LinearLayout.LayoutParams pTxtBasicForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtBasicForm = new Spinner(activity);
        txtBasicForm.setLayoutParams(pTxtBasicForm);
        txtBasicForm.setPadding(0, 16, 0, 16);
        txtBasicForm.setTag("BASIC_" + totPakacge);
        genBasic("BASIC_" + totPakacge, mLayoutPackage);
        linearForm.addView(txtBasicForm);

        LinearLayout.LayoutParams pLineAlacarte = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearAlacarte = new LinearLayout(activity);
        linearAlacarte.setLayoutParams(pLineAlacarte);
        linearAlacarte.setOrientation(LinearLayout.HORIZONTAL);
        linearAlacarte.setPadding(16, 10, 16, 10);
        linearPackage.addView(linearAlacarte);

        //Title Alacart
        LinearLayout.LayoutParams pTxtAlacarte = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtAlacarte = new TextView(activity);
        txtAlacarte.setLayoutParams(pTxtAlacarte);
        txtAlacarte.setText("Alacarte");
        txtAlacarte.setGravity(TextView.TEXT_ALIGNMENT_CENTER);
        linearAlacarte.addView(txtAlacarte);

        LinearLayout.LayoutParams pListAlacarte = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        pListAlacarte.setMargins(0, 10, 0, 0);
        LinearLayout linearListAlacarte = new LinearLayout(activity);
        linearListAlacarte.setLayoutParams(pListAlacarte);
        linearListAlacarte.setOrientation(LinearLayout.VERTICAL);
        linearListAlacarte.setTag("LIST_" + totPakacge);
        linearAlacarte.addView(linearListAlacarte);

        // Button Add Alacart
        LinearLayout.LayoutParams pContentAla = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pContentAla.setMargins(5, 5, 0, 0);
        TextView txtAdd = new TextView(activity);
        txtAdd.setText("+ Add Alacarte");
        txtAdd.setTag("ADD_" + totPakacge);
        txtAdd.setPadding(10, 10, 10, 10);
        txtAdd.setClickable(true);
        txtAdd.setLayoutParams(pContentAla);
        txtAdd.setTextColor(activity.getResources().getColor(R.color.white));
        txtAdd.setBackgroundResource(R.color.blue_10);
        txtAdd.setOnClickListener(addAlacarteListener);
        linearListAlacarte.addView(txtAdd);

        addListAlacarte();

        LinearLayout.LayoutParams pLineTitleHW = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineTitleHW.setMargins(0, 0, 0, 10);
        LinearLayout lineTitleHW = new LinearLayout(activity);
        lineTitleHW.setBackgroundColor(activity.getResources().getColor(R.color.white));
        lineTitleHW.setTag("TITLE_" + totPakacge);
        lineTitleHW.setOrientation(LinearLayout.HORIZONTAL);
        lineTitleHW.setLayoutParams(pLineTitleHW);
        linearPackage.addView(lineTitleHW);


        //List Hardware
        LinearLayout.LayoutParams pTxtHW = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtHW = new TextView(activity);
        txtHW.setLayoutParams(pTxtHW);
        txtHW.setText("Hardware");
        txtHW.setVisibility(View.GONE);
        txtHW.setPadding(5, 5, 5, 5);
        txtHW.setTypeface(Typeface.DEFAULT_BOLD);
        lineTitleHW.addView(txtHW);

        /**
         * Line For VC
         */
        LinearLayout.LayoutParams pLineFormVC = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineFormVC.setMargins(0, 0, 0, 20);
        LinearLayout linearFormVC = new LinearLayout(activity);
        linearFormVC.setLayoutParams(pLineFormVC);
        linearFormVC.setOrientation(LinearLayout.HORIZONTAL);
        linearFormVC.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormVC);

        LinearLayout.LayoutParams pTxtVC = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtVC = new TextView(activity);
        txtVC.setLayoutParams(pTxtVC);
        txtVC.setText("VC");
        txtVC.setVisibility(View.GONE);
        txtVC.setPadding(5, 0, 0, 0);
        linearFormVC.addView(txtVC);

        LinearLayout.LayoutParams pTxtVCForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtVCForm = new Spinner(activity);
        txtVCForm.setLayoutParams(pTxtVCForm);
        txtVCForm.setPadding(0, 16, 0, 16);
        txtVCForm.setTag("VC_" + totPakacge);
        txtVCForm.setVisibility(View.GONE);
        genVC("VC_" + totPakacge, mLayoutPackage);
        linearFormVC.addView(txtVCForm);

        /**
         * Line For DSD
         */
        LinearLayout.LayoutParams pLineFormDSD = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineFormDSD.setMargins(0, 0, 0, 20);
        LinearLayout linearFormDSD = new LinearLayout(activity);
        linearFormDSD.setLayoutParams(pLineFormDSD);
        linearFormDSD.setOrientation(LinearLayout.HORIZONTAL);
        linearFormDSD.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormDSD);

        LinearLayout.LayoutParams pTxtDSD = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtDSD = new TextView(activity);
        txtDSD.setLayoutParams(pTxtDSD);
        txtDSD.setText("DSD");
        txtDSD.setVisibility(View.GONE);
        txtDSD.setPadding(5, 0, 0, 0);
        linearFormDSD.addView(txtDSD);

        LinearLayout.LayoutParams pTxtDSDForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtDSDForm = new Spinner(activity);
        txtDSDForm.setLayoutParams(pTxtDSDForm);
        txtDSDForm.setPadding(0, 16, 0, 16);
        txtDSDForm.setTag("DSD_" + totPakacge);
        txtDSDForm.setVisibility(View.GONE);
        genDSD("DSD_" + totPakacge, mLayoutPackage);
        linearFormDSD.addView(txtDSDForm);

        /**
         * Line For LNB
         */

        LinearLayout.LayoutParams pLineFormLNB = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineFormLNB.setMargins(0, 0, 0, 20);
        LinearLayout linearFormLNB = new LinearLayout(activity);
        linearFormLNB.setLayoutParams(pLineFormLNB);
        linearFormLNB.setOrientation(LinearLayout.HORIZONTAL);
        linearFormLNB.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormLNB);

        LinearLayout.LayoutParams pTxtLNB = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtLNB = new TextView(activity);
        txtLNB.setLayoutParams(pTxtLNB);
        txtLNB.setText("LNB");
        txtLNB.setVisibility(View.GONE);
        txtLNB.setPadding(5, 0, 0, 0);
        linearFormLNB.addView(txtLNB);

        LinearLayout.LayoutParams pTxtLNBForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtLNBForm = new Spinner(activity);
        txtLNBForm.setLayoutParams(pTxtLNBForm);
        txtLNBForm.setPadding(0, 16, 0, 16);
        txtLNBForm.setTag("LNB_" + totPakacge);
        txtLNBForm.setVisibility(View.GONE);
        genLNB("LNB_" + totPakacge, mLayoutPackage);
        linearFormLNB.addView(txtLNBForm);

        /**
         * Line For ODU
         */

        LinearLayout.LayoutParams pLineFormODU = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineFormODU.setMargins(0, 0, 0, 20);
        LinearLayout linearFormODU = new LinearLayout(activity);
        linearFormODU.setLayoutParams(pLineFormODU);
        linearFormODU.setOrientation(LinearLayout.HORIZONTAL);
        linearFormODU.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormODU);

        LinearLayout.LayoutParams pTxtODU = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtODU = new TextView(activity);
        txtODU.setLayoutParams(pTxtODU);
        txtODU.setText("ODU");
        txtODU.setPadding(5, 0, 0, 0);
        txtODU.setVisibility(View.GONE);
        linearFormODU.addView(txtODU);

        LinearLayout.LayoutParams pTxtODUForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtODUForm = new Spinner(activity);
        txtODUForm.setLayoutParams(pTxtODUForm);
        txtODUForm.setPadding(0, 16, 0, 16);
        txtODUForm.setTag("ODU_" + totPakacge);
        txtODUForm.setVisibility(View.GONE);
        genODU("ODU_" + totPakacge, mLayoutPackage);
        linearFormODU.addView(txtODUForm);


        //Delete
        for (int i = 1; i < totPakacge; i++) {
            spinLnb = (Spinner) mLayoutPackage.findViewWithTag("LNB_" + i);
            spinLnb.setSelection(0);
            spinDish = (Spinner) mLayoutPackage.findViewWithTag("ODU_" + i);
            spinDish.setSelection(0);
        }
    }

    /**
     * Generate Package
     */

    private void genBasic(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                index = mLayoutPackage.indexOfChild(childView);
                spinPackage = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                spinPackage.setAdapter(spinAdapter);
                spinPackage.setOnItemSelectedListener(spinPackageListener);
            }
        });
    }

    private void ControlSpinner(String tag, String preMessaage) {
        count = mLayoutPackage.getChildCount();
        JSONObject obj;
        JSONArray result = new JSONArray();
        Spinner packageSpinner = null;
        int b = 0;

        for (int i = 0; i < count; i++) {
            obj = new JSONObject();
            View childView = mLayoutPackage.getChildAt(i);
            packageSpinner = (Spinner) childView.findViewWithTag(tag + (i + 1));
            Log.d("tagku", tag + (i + 1));
            b = mLayoutPackage.indexOfChild(childView);
            if (!packageSpinner.getSelectedItem().toString().equals("")) {
                try {
                    obj.put("id", b);
                    obj.put("value", packageSpinner.getSelectedItem().toString());
                    result.put(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
        if (result != null || result.length() > 0) {
            int m;
            for (m = 0; m < result.length(); m++) {
                try {
                    JSONObject objres = result.getJSONObject(m);
                    int id = objres.getInt("id");
                    String value = objres.getString("value");
                    executeValidation(id, value, preMessaage, tag);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void executeValidation(int id, String value, String preMessaage, String tag) {
        Spinner spinner = null;
        if (count > 0) {
            for (int i = 1; i < count; i++) {
                View childLnb = mLayoutPackage.getChildAt(i - 1); // Start From 0
                spinner = (Spinner) childLnb.findViewWithTag(tag + i); // Start from 1
                int index = mLayoutPackage.indexOfChild(childLnb);
                if (id != index && value.equals(spinner.getSelectedItem().toString())) {
                    Toast.makeText(activity, preMessaage + " ada yang sama, mohon periksa kembali", Toast.LENGTH_SHORT).show();
                    spinner.setSelection(0);
                }

            }
        }
    }

    private void autoTextLNBandOdu(String tag, int position) {
        int count = mLayoutPackage.getChildCount();
        if (count > 1) {
            Spinner spinner = null;
            for (int n = 1; n < count; n++) {
                switch (tag) {
                    case "LNB_":
                        View childLnb = mLayoutPackage.getChildAt(n); // Start From 0
                        spinner = (Spinner) childLnb.findViewWithTag(tag + (n + 1)); // Start from 1
                        spinner.setSelection(position);
                        //spinner.setClickable(false);
                        break;
                    case "ODU_":
                        View childOdu = mLayoutPackage.getChildAt(n); // Start From 0
                        spinner = (Spinner) childOdu.findViewWithTag(tag + (n + 1)); // Start from 1
                        spinner.setSelection(position);
                        //spinner.setClickable(false);
                        break;
                }

            }
        }

    }

    private void genVC(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                spinVc = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                spinVc.setAdapter(spinAdapterVC);
                spinVc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                        index = mLayoutPackage.indexOfChild(childView);
                        ControlSpinner("VC_", "VC");
                    }

                    @Override

                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
    }

    private void genLNB(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                spinLnb = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                if (totPakacge > 1) {
                    spinLnb.setClickable(false);
                }
                spinLnb.setAdapter(spinAdapterLNB);
                spinLnb.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                        index = mLayoutPackage.indexOfChild(childView);
                        //ControlSpinner("LNB_", "LNB", position, index);
                        autoTextLNBandOdu("LNB_", position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
    }

    private void genODU(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                spinDish = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                if (totPakacge > 1) {
                    spinDish.setClickable(false);
                }
                spinDish.setAdapter(spinAdapterODU);
                spinDish.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                        index = mLayoutPackage.indexOfChild(childView);
                        //ControlSpinner("ODU_", "ODU", position, index);
                        autoTextLNBandOdu("ODU_", position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }
        });
    }

    private void genDSD(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                spinDsd = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                spinDsd.setAdapter(spinAdapterDSD);
                spinDsd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                        index = mLayoutPackage.indexOfChild(childView);
                        ControlSpinner("DSD_", "DSD");
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
    }

    private View.OnClickListener deletePackage = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d("FTPaket", "tag = " + view.getTag().toString());
            //setBasicId();
            delPackage(Integer.parseInt(view.getTag().toString()));
        }
    };

    private int getTarget() {
        tablePlanAdapter = new TablePlanAdapter(activity);
        dbAdapter = new TableFreeTrialAdapter(activity);
        String date = utils.getCurrentDate();
        listPlan = tablePlanAdapter.getDatabySfl(activity, TablePlan.KEY_PLAN_DATE, date);

        if (listPlan != null && listPlan.size() > 0) {
            TablePlan item = listPlan.get(0);
            if (item.getTarget() == null || item.getTarget().equalsIgnoreCase("")) {
                target = 0;
            } else {
                target = Integer.parseInt(item.getTarget());
            }

            listFreetrial = dbAdapter.getDatabyCondition(TableFreeTrial.KEY_PLAN_ID, item.getPlan_id());
            for (int a = 0; a < listFreetrial.size(); a++) {
                Log.d("FTTASKLIST:", "planIdfor= " + listFreetrial.get(a).getPlan_id());
                if (!listFreetrial.get(a).getValue().equalsIgnoreCase("TITLE")) {
                    int result = listFreetrial.size() - 1;
                    Log.d("FTTASKLIST:", "result= " + result);
                    target = target - result;
                }
            }
        }
        return target;
    }

    private View.OnClickListener addAlacarteListener = new View.OnClickListener() {
        private ImageView closeAla;

        @Override
        public void onClick(View view) {
            View popUpView = activity.getLayoutInflater().inflate(R.layout.ms_package_alacarte_list, null);
            mpopup = new PopupWindow(popUpView, LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, true);
            mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
            mpopup.showAtLocation(popUpView, Gravity.START, 0, 0);
            closeAla = (ImageView) popUpView.findViewById(R.id.close_alacarte);
            closeAla.setClickable(true);
            closeAla.setOnClickListener(closeAlaListener);
            loadAlacarte(popUpView, view.getTag().toString());
        }
    };

    private View.OnClickListener closeAlaListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            mpopup.dismiss();
        }
    };

    private void loadAlacarte(View popUpView, String tagId) {
        String[] tag = tagId.split("_");

        Spinner spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (tag[1]));
        String productId = mPackageProductListItemList
                .get(spinPackage.getSelectedItemPosition())
                .getProduct_id();

        basic_id = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getBasic_id();

        TableMasterPackageAdapter db = new TableMasterPackageAdapter(this.activity);
        recyclerView = (RecyclerView) popUpView.findViewById(R.id.package_alacarte_list);
        btnSave = (Button) popUpView.findViewById(R.id.btn_save_alacarte);
        btnSave.setTag("SAVE_" + tag[1]);
        btnSave.setOnClickListener(btnSaveListener);
        final LinearLayoutManager layoutParams = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutParams);
        mPackageProductListItem = db.fetchByProfile("1",
                mBrandListItem.get(spinBrand.getSelectedItemPosition()).getBrand_code(), productId);//IVDGT
        mPackageProductAdapter = new MSAlacarteListAdapter(mPackageProductListItem);
        recyclerView.setAdapter(mPackageProductAdapter);
    }

    private AdapterView.OnItemSelectedListener spinPackageListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
             /*
            for (int n=0;n<packageProductListItems.size();n++){
                PackageProductListItem item = packageProductListItems.get(n);
                try {
                    String price = item.getPrice();
                    if (item.getPrice()==""||item.getPrice()==null) {
                        price = "0";
                    }
                    hrgBasic = Integer.parseInt(price);

                    duit = Double.parseDouble(String.valueOf(hrgBasic + hrgAlacarte));
                    defaultF = NumberFormat.getInstance();
                    formattednumber = defaultF.format(duit);
                    strPaket = formattednumber.replace(",", ".");
                    pricePackage.setText("Rp."+strPaket);
                    //pricePackage.setText(String.valueOf(hrgBasic+hrgAlacarte));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("FTPaket", "Harga basic= "+ hrgBasic);
            }*/

            /*for (int i = 0; i < totPakacge; i++) {
                spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (i + 1));
                product_id = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getProduct_id();
            }*/

            TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(activity);
            List<TableMasterPackage> packageProductListItems;

            hrgBasic = 0;
            for (int i = 0; i < totPakacge; i++) {
                spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (i + 1));
                product_id = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getProduct_id();
                mPackageProductAdapter = new TableMasterPackageAdapter(activity);
                packageProductListItems = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fROWID, product_id);
                for (int n = 0; n < packageProductListItems.size(); n++) {
                    TableMasterPackage item = packageProductListItems.get(n);
                    hrgBasic = hrgBasic + Integer.parseInt(item.getPrice());
                }
            }
            /*duit = Double.parseDouble(String.valueOf(hrgBasic + hrgAlacarte));
                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(duit);
                strPaket = formattednumber.replace(",", ".");*/
            defaultF = NumberFormat.getInstance();
            formattednumber = defaultF.format(hrgBasic + hargaAdditional);
            strPaket = formattednumber.replace(",", ".");

            /*defaultF = NumberFormat.getInstance();
            formattednumber = defaultF.format(hrgAlacarte);
            strAlacarte = formattednumber.replace(",", ".");*/

            pricePackage.setText(strPaket);
            //priceAlacarte.setText(strAlacarte);

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapter) {
        }
    };

    private View.OnClickListener btnSaveListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            try {
                String[] tag = view.getTag().toString().split("_");
                int objArray = Integer.parseInt(tag[1]) - 1;
                int dataAll = selAlacarteSaveAll.size();
                if (objArray + 1 > dataAll) {
                    selAlacarteSaveAll.add(objArray, mPackageProductAdapter.itemAlacarte.getList());
                } else {
                    selAlacarteSaveAll.set(objArray, mPackageProductAdapter.itemAlacarte.getList());
                }
                addListAlacarte();
            } catch (IndexOutOfBoundsException e) {

            }
            mpopup.dismiss();
        }
    };

    private void addListAlacarte() {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < totPakacge; i++) {
                    LinearLayout mLayoutList = (LinearLayout) mLayoutPackage.findViewWithTag("LIST_" + (i + 1));
                    LinearLayout.LayoutParams params;
                    params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(5, 5, 0, 0);
                    selAlacarte.clear();
                    if (selAlacarteSaveAll.size() > 0) {
                        try {
                            for (int j = 0; j < selAlacarteSaveAll.get(i).size(); j++) {
                                selAlacarte.add(selAlacarteSaveAll.get(i).get(j).toString());
                            }
                        } catch (IndexOutOfBoundsException e) {
                        }
                    }

                    try {
                        mLayoutList.removeAllViews();

                        if (mLayoutList.findViewWithTag("ADD_" + (i + 1)) == null) {
                            TextView txtAdd = new TextView(activity);
                            txtAdd.setText("+ Add Alacarte");
                            txtAdd.setTag("ADD_" + (i + 1));
                            txtAdd.setPadding(10, 10, 10, 10);
                            txtAdd.setClickable(true);
                            txtAdd.setLayoutParams(params);
                            txtAdd.setTextColor(activity.getResources().getColor(R.color.white));
                            txtAdd.setBackgroundResource(R.color.blue_10);
                            txtAdd.setOnClickListener(addAlacarteListener);
                            mLayoutList.addView(txtAdd);
                        }

                        if (selAlacarte.size() > 0) {
                            for (int k = 0; k < selAlacarte.size(); k++) {
                                String[] alaData = selAlacarte.get(k).toString().split("#");
                                String alaName = alaData[1].toString();
                                String alaId = alaData[0].toString();
                                TableMasterPackageAdapter mPackageProductAdapter
                                        = new TableMasterPackageAdapter(activity);
                                List<TableMasterPackage> packageProductListItems
                                        = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fROWID,
                                        alaId);/*
                                for (int n=0;n<packageProductListItems.size();n++){
                                    PackageProductListItem item = packageProductListItems.get(n);
                                    try {
                                        String price = item.getPrice();
                                        if (item.getPrice()==""||item.getPrice()==null) {
                                            price = "0";
                                        }

                                        hrgAlacarte = hrgAlacarte+Integer.parseInt(price);
                                        duit = Double.parseDouble(String.valueOf(hrgBasic + hrgAlacarte));
                                        defaultF = NumberFormat.getInstance();
                                        formattednumber = defaultF.format(duit);
                                        strPaket = formattednumber.replace(",", ".");
                                        pricePackage.setText("Rp."+strPaket);
                                        //pricePackage.setText(String.valueOf(hrgBasic+hrgAlacarte));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    Log.d("FTPaket", "Harga basic= "+ hrgBasic);
                                }*/
                                LinearLayout LL = new LinearLayout(activity);
                                LL.setOrientation(LinearLayout.HORIZONTAL);
                                LL.setGravity(Gravity.LEFT | Gravity.START);
                                LL.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                if (LL.findViewWithTag(alaId) == null) {
                                    TextView textView = new TextView(activity);
                                    textView.setText(alaName);
                                    textView.setTag(alaId);
                                    textView.setPadding(10, 10, 10, 10);
                                    textView.setLayoutParams(params);
                                    textView.setTextColor(activity.getResources().getColor(R.color.white));
                                    textView.setBackgroundResource(R.color.blue_grey_11);
                                    LL.addView(textView);

                                    ImageView imgDel = new ImageView(activity);
                                    imgDel.setImageResource(R.drawable.ic_close);
                                    imgDel.setTag(i + "_" + alaId);
                                    LinearLayout.LayoutParams imgParam = new LinearLayout.LayoutParams(60, 60);
                                    imgParam.setMargins(5, 5, 0, 0);
                                    imgDel.setLayoutParams(imgParam);
                                    imgDel.setBackgroundColor(activity.getResources().getColor(R.color.blue_grey_11));
                                    imgDel.setOnClickListener(delAlacarteListener);
                                    LL.addView(imgDel);

                                    mLayoutList.addView(LL);
                                }
                            }
                        }
                        int seq = i + 1;
                        LinearLayout layoutTitle = (LinearLayout) mLayoutPackage.findViewWithTag("TITLE_" + seq);
                        if (seq == totPakacge) {
                            if (layoutTitle.findViewWithTag(seq) == null) {
                                LinearLayout.LayoutParams pDelPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
                                pDelPackage.gravity = Gravity.END;
                                pDelPackage.setMargins(0, 0, 5, 0);
                                ImageView delPack = new ImageView(activity);
                                if (totPakacge > 2) {
                                    delPack.setImageResource(R.drawable.ic_close_package);
                                }

                                delPack.setTag(totPakacge);
                                delPack.setLayoutParams(pDelPackage);
                                delPack.setOnClickListener(deletePackage);
                                layoutTitle.addView(delPack);
                            }
                        } else {
                            layoutTitle.removeView(layoutTitle.findViewWithTag(seq));
                        }
                    } catch (NullPointerException e) {
                    }
                }

                hrgAlacarte = 0;
                for (int x = 0; x < selAlacarteSaveAll.size(); x++) {

                    for (int y = 0; y < selAlacarteSaveAll.get(x).size(); y++) {
                        if (x == 0) {

                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(activity);
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                            }
                            //alaBuild1.append(selAlacarteSaveAll.get(x).get(y).toString());
                            //alaBuild1.append(",");
                        }
                        if (x == 1) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(activity);
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                            }
                        }
                        if (x == 2) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();

                            TableMasterPackageAdapter mPackageProductAdapter
                                    = new TableMasterPackageAdapter(activity);

                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                            }


                        }

                    }
                }
                /*duit = Double.parseDouble(String.valueOf(hrgBasic + hrgAlacarte));
                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(duit);
                strPaket = formattednumber.replace(",", ".");*/
                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(hrgBasic + hargaAdditional);
                strPaket = formattednumber.replace(",", ".");

                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(hrgAlacarte);
                strAlacarte = formattednumber.replace(",", ".");

                pricePackage.setText(strPaket);
                priceAlacarte.setText(strAlacarte);

                /*
                for (int i = 0; i < totPakacge; i++) {
                    spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (i + 1));
                    String product_id = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getProductId();
                    TableMPackageProductAdapter mPackageProductAdapter = new TableMPackageProductAdapter(activity);
                    List<PackageProductListItem> packageProductListItems = mPackageProductAdapter.fetchByCondition(activity, product_id,
                            TableMPackageProduct.fROWID);
                    for (int n=0;n<packageProductListItems.size();n++){
                        PackageProductListItem item = packageProductListItems.get(n);
                        coba = coba +Integer.parseInt(item.getPrice());
                    }
                }
                Log.d("FTPaket","Coba= "+ coba);*/
            }
        });
    }

    private View.OnClickListener delAlacarteListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String[] dataTag = view.getTag().toString().split("_");
            ArrayList<String> dataObject = selAlacarteSaveAll.get(Integer.parseInt(dataTag[0]));
            for (int i = 0; i < dataObject.size(); i++) {
                String[] ala = dataObject.get(i).toString().split("#");
                String idChannel = ala[0];
                if (idChannel.toString().equals(dataTag[1])) {
                    dataObject.remove(i);
                    addListAlacarte();
                }
            }
        }
    };


    private View.OnClickListener actSaveAll = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            try {
                JSONObject objAdd = null;
                String _vc, _lnb, _dsd, _odu, vcList = "", lnbList = "", dsdList = "", oduList = "";
                /*_vc     = vc.getText().toString();
                _lnb    = lnb.getText().toString();
                _dsd    = dsd.getText().toString();
                _odu    = dish.getText().toString();*/
                _vc = spinVc.getSelectedItem().toString();
                _lnb = spinLnb.getSelectedItem().toString();
                _dsd = spinDsd.getSelectedItem().toString();
                _odu = spinDish.getSelectedItem().toString();

                /*if (_vc == null || _vc.equalsIgnoreCase("") || _vc.length() <= 0) {
                    Toast.makeText(activity, "Silahkan masukan no VC.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (_dsd == null || _dsd.equalsIgnoreCase("") || _dsd.length() <= 0) {
                    Toast.makeText(activity, "Silahkan masukan no DSD.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (_lnb == null || _lnb.equalsIgnoreCase("") || _lnb.length() <= 0) {
                    Toast.makeText(activity, "Silahkan masukan no LNB.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (_odu == null || _odu.equalsIgnoreCase("") || _odu.length() <= 0) {
                    Toast.makeText(activity, "Silahkan masukan no ODU.", Toast.LENGTH_SHORT).show();
                    return;
                }*/


                for (int i = 0; i < listSOH.size(); i++) {
                    if (listSOH.get(i).getType().equalsIgnoreCase("VC")
                            && _vc.equalsIgnoreCase(listSOH.get(i).getSn())) {
                        vcList = _vc;
                    }

                    if (listSOH.get(i).getType().equalsIgnoreCase("LNB")
                            && _lnb.equalsIgnoreCase(listSOH.get(i).getSn())) {
                        lnbList = _lnb;
                    }

                    if (listSOH.get(i).getType().equalsIgnoreCase("ANT")
                            && _odu.equalsIgnoreCase(listSOH.get(i).getSn())) {
                        oduList = _odu;
                    }

                    if (listSOH.get(i).getType().equalsIgnoreCase("DEC")
                            && _dsd.equalsIgnoreCase(listSOH.get(i).getSn())) {
                        dsdList = _dsd;
                    }
                }

                //MS DTD tidak input Hardware

                /*if (vcList == null || vcList.equalsIgnoreCase("")|| vcList.length()<=0) {
                    Toast.makeText(activity, "Silahkan cek kembali no VC.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (dsdList == null || dsdList.equalsIgnoreCase("") || dsdList.length()<=0) {
                    Toast.makeText(activity, "Silahkan cek kembali no DSD.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (lnbList == null || lnbList.equalsIgnoreCase("") || lnbList.length()<=0) {
                    Toast.makeText(activity, "Silahkan cek kembali no LNB.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (oduList == null || oduList.equalsIgnoreCase("") || oduList.length()<=0) {
                    Toast.makeText(activity, "Silahkan cek kembali no ODU.", Toast.LENGTH_SHORT).show();
                    return;
                }*/

                spinBrand = (Spinner) mView.findViewById(R.id.spinner_brand);
                //spinPromo = (Spinner) root.findViewById(R.id.spinner_promo);
                EditText biayaLain = (EditText) mView.findViewById(R.id.biaya_lain);
                EditText ketLain = (EditText) mView.findViewById(R.id.ket_lain);

                /*int selectedId = radioStatus.getCheckedRadioButtonId();
                selRadio = (RadioButton) mView.findViewById(selectedId);*/

                //alacarte
                ArrayList alacarte = new ArrayList();
                alacarte.add(selAlacarteSaveAll);

                ArrayList basic = new ArrayList();
                ArrayList arrayVc = new ArrayList();
                ArrayList arrayLNB = new ArrayList();
                ArrayList arrayODU = new ArrayList();
                ArrayList arrayDSD = new ArrayList();

                arrayVc.clear();
                arrayLNB.clear();
                arrayODU.clear();
                arrayDSD.clear();
                basic.clear();

                for (int i = 0; i < totPakacge; i++) {
                    spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (i + 1));
                    spinVc = (Spinner) mLayoutPackage.findViewWithTag("VC_" + (i + 1));
                    spinLnb = (Spinner) mLayoutPackage.findViewWithTag("LNB_" + (i + 1));
                    spinDish = (Spinner) mLayoutPackage.findViewWithTag("ODU_" + (i + 1));
                    spinDsd = (Spinner) mLayoutPackage.findViewWithTag("DSD_" + (i + 1));

                    String vcId = listVc.get(spinVc.getSelectedItemPosition());
                    arrayVc.add(vcId);

                    String lnbId = listLnb.get(spinLnb.getSelectedItemPosition());
                    arrayLNB.add(lnbId);

                    String oduId = listDish.get(spinDish.getSelectedItemPosition());
                    arrayODU.add(oduId);

                    String dsdId = listDsd.get(spinDsd.getSelectedItemPosition());
                    arrayDSD.add(dsdId);

                    String basicId = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getProduct_id();
                    basic.add(basicId);
                }

                JSONArray arrayAdd = new JSONArray();
                JSONObject addResult = new JSONObject();
                for (int i = 0; i < totAdditional; i++) {
                    materialAdd = (Spinner) mLayoutAdditional.findViewWithTag("add_" + (i + 1));
                    countAdd = (EditText) mLayoutAdditional.findViewWithTag("addCount_" + (i + 1));

                    objAdd = new JSONObject();
                    objAdd.put("additional_id", mMaterialListItems.get
                            (materialAdd.getSelectedItemPosition()).getHw_id());
                    objAdd.put("additional_total", countAdd.getText().toString());
                    arrayAdd.put(objAdd);
                    addResult.put("data", arrayAdd);
                }


                String basic1 = "";
                String basic2 = "";
                String basic3 = "";
                String alacarte1 = "";
                String alacarte2 = "";
                String alacarte3 = "";
                String vc1 = "";
                String vc2 = "";
                String vc3 = "";
                String lnb1 = "";
                String lnb2 = "";
                String lnb3 = "";
                String odu1 = "";
                String odu2 = "";
                String odu3 = "";
                String dsd1 = "";
                String dsd2 = "";
                String dsd3 = "";

                StringBuilder alaBuild1 = new StringBuilder();
                StringBuilder alaBuild2 = new StringBuilder();
                StringBuilder alaBuild3 = new StringBuilder();
                for (int x = 0; x < selAlacarteSaveAll.size(); x++) {

                    for (int y = 0; y < selAlacarteSaveAll.get(x).size(); y++) {
                        if (x == 0) {
                            alaBuild1.append(selAlacarteSaveAll.get(x).get(y).toString());
                            alaBuild1.append(",");
                        }
                        if (x == 1) {
                            alaBuild2.append(selAlacarteSaveAll.get(x).get(y).toString());
                            alaBuild2.append(",");
                        }
                        if (x == 2) {
                            alaBuild3.append(selAlacarteSaveAll.get(x).get(y).toString());
                            alaBuild3.append(",");
                        }
                    }

                    if (x == 0) {
                        alacarte1 = alaBuild1.toString().substring(0, alaBuild1.length() - 1);
                    }
                    if (x == 1) {
                        alacarte2 = alaBuild2.toString().substring(0, alaBuild2.length() - 1);
                    }
                    if (x == 2) {
                        alacarte3 = alaBuild3.toString().substring(0, alaBuild3.length() - 1);
                    }
                }

                for (int x = 0; x < basic.size(); x++) {
                    if (x == 0) {
                        basic1 = basic.get(x).toString();
                    }
                    if (x == 1) {
                        basic2 = basic.get(x).toString();
                    }
                    if (x == 2) {
                        basic3 = basic.get(x).toString();
                    }
                }

                for (int x = 0; x < arrayVc.size(); x++) {
                    if (x == 0) {
                        vc1 = arrayVc.get(x).toString();
                    }
                    if (x == 1) {
                        vc2 = arrayVc.get(x).toString();
                    }
                    if (x == 2) {
                        vc3 = arrayVc.get(x).toString();
                    }
                }

                for (int x = 0; x < arrayLNB.size(); x++) {
                    if (x == 0) {
                        lnb1 = arrayLNB.get(x).toString();
                    }
                    if (x == 1) {
                        lnb2 = arrayLNB.get(x).toString();
                    }
                    if (x == 2) {
                        lnb3 = arrayLNB.get(x).toString();
                    }
                }

                for (int x = 0; x < arrayODU.size(); x++) {
                    if (x == 0) {
                        odu1 = arrayODU.get(x).toString();
                    }
                    if (x == 1) {
                        odu2 = arrayODU.get(x).toString();
                    }
                    if (x == 2) {
                        odu3 = arrayODU.get(x).toString();
                    }
                }

                for (int x = 0; x < arrayDSD.size(); x++) {
                    if (x == 0) {
                        dsd1 = arrayDSD.get(x).toString();
                    }
                    if (x == 1) {
                        dsd2 = arrayDSD.get(x).toString();
                    }
                    if (x == 2) {
                        dsd3 = arrayDSD.get(x).toString();
                    }
                }
                /*try {
                    if (selRadio.getText().toString().equalsIgnoreCase("") || selRadio.getText().toString().length() < 0) {
                        Toast.makeText(activity, "Cek Hw Status", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (NullPointerException x) {
                    Toast.makeText(activity, "Cek Hw Status", Toast.LENGTH_SHORT).show();
                    return;
                }*/


                /*saveStatus = selRadio.getText().toString();
                if (saveStatus.equalsIgnoreCase("Pinjam")) {
                    saveStatus = "1";
                } else {
                    saveStatus = "2";
                }*/

                saveBrand = mBrandListItem.get(spinBrand.getSelectedItemPosition()).getBrand_id();
                //savePromo = mPromoListItems.get(spinPromo.getSelectedItemPosition()).getId();
                //saveBiayaLain = biayaLain.getText().toString();
                //saveKetLain = ketLain.getText().toString();

                JSONObject basicJson = new JSONObject();
                JSONObject alaJson = new JSONObject();
                JSONObject packageDet = new JSONObject();
                JSONObject vcJson = new JSONObject();
                JSONObject lnbJson = new JSONObject();
                JSONObject oduJson = new JSONObject();
                JSONObject dsdJson = new JSONObject();
                JSONObject objBundling = new JSONObject();

                try {
                    basicJson.put("1", basic1);
                    basicJson.put("2", basic2);
                    basicJson.put("3", basic3);

                    alaJson.put("1", alacarte1);
                    alaJson.put("2", alacarte2);
                    alaJson.put("3", alacarte3);

                    vcJson.put("1", vc1);
                    vcJson.put("2", vc2);
                    vcJson.put("3", vc3);

                    lnbJson.put("1", lnb1);
                    lnbJson.put("2", lnb2);
                    lnbJson.put("3", lnb3);

                    oduJson.put("1", odu1);
                    oduJson.put("2", odu2);
                    oduJson.put("3", odu3);

                    dsdJson.put("1", dsd1);
                    dsdJson.put("2", dsd2);
                    dsdJson.put("3", dsd3);
                    packageDet.put("BRAND", saveBrand);

                    objBundling.put("sim_id", simcardId);
                    objBundling.put("sim_value", simcardValue.getText().toString());
                    objBundling.put("router_id", routerId);
                    objBundling.put("router_value", routerValue.getText().toString());


                    /*if (numberForm.substring(0, 1).equalsIgnoreCase("p")) {
                        saveStatus = "2";
                    } else {
                        saveStatus = "1";
                    }
                    packageDet.put("HW_STATUS", saveStatus);*/

                    packageDet.put("BASIC", basicJson);
                    packageDet.put("ALACARTE", alaJson);
                    packageDet.put("VC", vcJson);
                    packageDet.put("LNB", lnbJson);
                    packageDet.put("ODU", oduJson);
                    packageDet.put("DSD", dsdJson);
                    packageDet.put("isMulty", isMulty);
                    packageDet.put("additional", addResult);
                    packageDet.put("bundling", objBundling);
                    //packageDet.put("VC", _vc);
                    // packageDet.put("LNB", _lnb);
                    //packageDet.put("DSD", _dsd);
                    //packageDet.put("ODU", _odu);
                    packageDet.put("promoCode", promoCode);
                    packageDet.put("INSTALL_BILL", priceInstalasi.getText().toString());
                    packageDet.put("MONTHLY_BILL", pricePackage.getText().toString());
                    packageDet.put("amount", String.valueOf(hrgBasic + hrgAlacarte + hargaAdditional +
                            Integer.parseInt(priceInstalasi.getText().toString())));
                    //packageDet.put("PROMO", savePromo);
                    //packageDet.put("BIAYA_LAIN", saveBiayaLain);
                    //packageDet.put("KET_LAIN", saveKetLain);

                    Log.d("jsonku", packageDet.toString());
                    TableFormAppAdapter db = new TableFormAppAdapter(activity);
                    List<TableFormApp> sel = db.getDatabyCondition(TableFormApp.fFORM_NO, numberForm);
                    if (sel.size() > 0) {
                        if (!isShowBundling) {
                            if (simcardValue.getText().toString().trim().length() < 1) {
                                Toast.makeText(activity, "Sim Card tidak boleh kosong", Toast.LENGTH_SHORT).show();
                            } else if (routerValue.getText().toString().trim().length() < 1) {
                                Toast.makeText(activity, "Sim Card tidak boleh kosong", Toast.LENGTH_SHORT).show();
                            } else if (!simcardValue.getText().toString().startsWith("62")) {
                                Toast.makeText(activity, "Sim Card harus di mulai dari 62", Toast.LENGTH_SHORT).show();
                            } else {
                                db.updatePartial(activity, TableFormApp.fVALUES_PACKAGE, packageDet.toString(),
                                        TableFormApp.fFORM_NO, numberForm);
                                Toast.makeText(activity, "Package Saved", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            db.updatePartial(activity, TableFormApp.fVALUES_PACKAGE, packageDet.toString(),
                                    TableFormApp.fFORM_NO, numberForm);
                            Toast.makeText(activity, "Package Saved", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(activity, "Form number " + numberForm + " tidak tersedia!", Toast.LENGTH_SHORT).show();
                    }
                    Log.d("jsonku", packageDet.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("errorku", e.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("errorku", e.toString());
            }
        }
    };


}
