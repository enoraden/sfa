package myskysfa.com.sfa.main.menu;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.sfa.main.menu.dtd.DTDEmergency;
import myskysfa.com.sfa.main.menu.dtd.DTDPaket;
import myskysfa.com.sfa.main.menu.dtd.DTDPayment;
import myskysfa.com.sfa.main.menu.dtd.DTDProfile;
import myskysfa.com.sfa.main.menu.dtd.DTDStatus;
import myskysfa.com.sfa.main.menu.dtd.DTD_Signature;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by admin on 6/21/2016.
 */
public class MainFragmentDTD extends AppCompatActivity {
    public static String noform ;
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 7;
    public static boolean isExistKtp, isExistLing, isExistInstall, isExistRmh, isExistSign, isExistPln,
            isExistBB;
    private List<TableLogLogin> listLogLogin;
    private String employeeId;
    private TableLogLoginAdapter mLogLoginAdapter;
    private Toolbar _toolbar;
    private Utils utils;

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fom_dtd);

        utils = new Utils(this);
        utils.setIMEI();
        _toolbar    = (Toolbar) findViewById(R.id.toolbar);
        msMenuTitle = getResources().getStringArray(R.array.ms_menu_ft_title);
        pager       = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(buildAdapter());
        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(pager);
        tabs.setOnPageChangeListener(viewPagerListener);
        setSupportActionBar(_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        pager.addOnPageChangeListener(changePageListener);
        attachedImage();
        pager.setOffscreenPageLimit(4);
    }*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fom_dtd);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        _toolbar = (Toolbar) findViewById(R.id.toolbar);
        generateFormNo();
        attachedImage();
        viewPager.setOffscreenPageLimit(6); //Start from 1
        viewPager.addOnPageChangeListener(PageListener);
        setSupportActionBar(_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        viewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });
    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {
                case 0:
                    return new DTDProfile();
                case 1:
                    return new DTDEmergency();
                case 2:
                    return new DTDPaket();
                case 3:
                    return new DTDPayment();
                case 4:
                    return new DTD_Signature();
                case 5:
                    fragment = new MSDTD_Checklist();
                    fragment.setArguments(getIntent().getExtras());
                    return fragment;
                case 6:
                    return new DTDStatus();

            }
            return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }


        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Profile";
                case 1:
                    return "Emergency";
                case 2:
                    return "Package";
                case 3:
                    return "1st Payment";
                case 4:
                    return "Signature";
                case 5:
                    return "Checklist";
                case 6:
                    return "Status";

            }
            return null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * Method for generate Form No
     */
    private void generateFormNo() {
        String yy, mm, dd, hh, ss, sss;
        listLogLogin = new ArrayList<>();
        mLogLoginAdapter = new TableLogLoginAdapter(MainFragmentDTD.this);
        listLogLogin = mLogLoginAdapter.getAllData();
        if (listLogLogin.size() > 0) {
            employeeId = listLogLogin.get(listLogLogin.size() - 1).getcEmployeeId();
        } else {
            employeeId = "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        sss = strDate.substring(17, 19);
        StringBuilder sb = new StringBuilder();
        sb.append("A");
        sb.append(employeeId);
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.toString();
        noform = sb.toString() ;
        SharedPreferences preferences = getSharedPreferences(getString(R.string.fn_dtd), Context.MODE_PRIVATE);
        SharedPreferences.Editor shareEditor = preferences.edit();
        shareEditor.putString("fn", sb.toString());
        shareEditor.commit();
    }

    public void attachedImage() {
        isExistKtp = false;
        isExistLing = false;
        isExistInstall = false;
        isExistRmh = false;
        isExistSign = false;
    }

    private ViewPager.OnPageChangeListener PageListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (position == 6) {
                DTDStatus frag1 = (DTDStatus) viewPager.getAdapter().instantiateItem(viewPager, viewPager.getCurrentItem());
                frag1.getDataFromDB();
                frag1.setHideKeyBoard();
            } else if (position == 1) {
                DTDEmergency dtdEmergency = (DTDEmergency) viewPager.getAdapter().instantiateItem(viewPager, viewPager.getCurrentItem());
                dtdEmergency.setFormNumber();
                dtdEmergency.setHideKeyBoard();
            } else if (position == 4) {
                DTD_Signature ftSing = (DTD_Signature) viewPager.getAdapter().instantiateItem(viewPager, viewPager.getCurrentItem());
                ftSing.setFormNumber();
                ftSing.setHideKeyBoard();
            }  else if (position == 2) {
                DTDPaket dtdPaket = (DTDPaket) viewPager.getAdapter().instantiateItem(viewPager, viewPager.getCurrentItem());
                dtdPaket.setFormNumber();
                dtdPaket.setHideKeyBoard();
            } else if (position == 3) {
                DTDPayment ftPay = (DTDPayment) viewPager.getAdapter().instantiateItem(viewPager, viewPager.getCurrentItem());
                ftPay.setFormNumber();
                ftPay.setEstimate();
            } else if (position == 5) {
                MSDTD_Checklist ftChecklist = (MSDTD_Checklist) viewPager.getAdapter().instantiateItem(viewPager, viewPager.getCurrentItem());
                ftChecklist.setFormNumber();
                ftChecklist.setHideKeyBoard();
            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    @Override
    public void onBackPressed() {
        set_exit();
    }

    private void set_exit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainFragmentDTD.this);
        builder.setMessage("Anda yakin akan keluar dari form?");
        builder.setTitle("EXIT");
        builder.setPositiveButton(getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SharedPreferences sm = getSharedPreferences(getString(
                                R.string.fn_dtd), Context.MODE_PRIVATE);
                        String numberForm = sm.getString("fn", null);

                        if (numberForm != null) {
                            TableFormAppAdapter adapter = new TableFormAppAdapter(MainFragmentDTD.this);
                            adapter.deleteBy(MainFragmentDTD.this, TableFormApp.fFORM_NO, numberForm);
                        }

                        SharedPreferences sessionPref = getSharedPreferences(getString(
                                R.string.fn_dtd), Context.MODE_PRIVATE);
                        SharedPreferences.Editor shareEditor = sessionPref.edit();
                        shareEditor.remove("fn");
                        shareEditor.commit();
                        finish();

                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                set_exit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
