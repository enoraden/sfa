package myskysfa.com.sfa.main.menu;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.sfa.main.menu.ms.MS_Signature;
import myskysfa.com.sfa.main.menu.ms.Ms_Fragment_Emergency;
import myskysfa.com.sfa.main.menu.ms.Ms_Fragment_Package;
import myskysfa.com.sfa.main.menu.ms.Ms_Fragment_Payment;
import myskysfa.com.sfa.main.menu.ms.Ms_Fragment_Profile;
import myskysfa.com.sfa.main.menu.ms.Ms_Fragment_Status;

/**
 * Created by Eno on 6/13/2016.
 */
public class MainFragmentMS extends AppCompatActivity {

    private static TabLayout tabLayout;
    private static ViewPager viewPager;
    public static int int_items = 7;
    public static boolean isExistKtp, isExistLing, isExistInstall, isExistRmh, isExistSign;
    private List<TableLogLogin> listLogLogin;
    private String employeeId;
    private TableLogLoginAdapter mLogLoginAdapter;
    private Toolbar _toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_fragment_ms);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        _toolbar = (Toolbar) findViewById(R.id.toolbar);
        generateFormNo();
        attachedImage();
        viewPager.setOffscreenPageLimit(6); //Start from 1
        viewPager.addOnPageChangeListener(PageListener);
        setSupportActionBar(_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        viewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });
    }

    private ViewPager.OnPageChangeListener PageListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (position == 6) {
                Ms_Fragment_Status frag1 = (Ms_Fragment_Status) viewPager.getAdapter().instantiateItem(viewPager, viewPager.getCurrentItem());
                frag1.getDataFromDB();
                frag1.setHideKeyBoard();
            } else if (position == 1) {
                Ms_Fragment_Emergency ftEmergency = (Ms_Fragment_Emergency) viewPager.getAdapter().instantiateItem(viewPager, viewPager.getCurrentItem());
                ftEmergency.setFormNumber();
                ftEmergency.setHideKeyBoard();
            } else if (position == 2) {
                Ms_Fragment_Package ftPaket = (Ms_Fragment_Package) viewPager.getAdapter().instantiateItem(viewPager, viewPager.getCurrentItem());
                ftPaket.setFormNumber();
                ftPaket.setHideKeyBoard();
            } else if (position == 3) {
                Ms_Fragment_Payment msPay = (Ms_Fragment_Payment) viewPager.getAdapter().instantiateItem(viewPager, viewPager.getCurrentItem());
                msPay.setFormNumber();
                msPay.setEstimate();
                msPay.setHideKeyBoard();
            }else if (position == 4){
                MS_Signature msSign = (MS_Signature) viewPager.getAdapter().instantiateItem(viewPager, viewPager.getCurrentItem());
                msSign.setFormNumber();
            } else if (position == 5) {
                MSDTD_Checklist Checklist = (MSDTD_Checklist) viewPager.getAdapter().instantiateItem(viewPager, viewPager.getCurrentItem());
                Checklist.setFormNumber();
                Checklist.setHideKeyBoard();
            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {
                case 0:
                    return new Ms_Fragment_Profile();
                case 1:
                    return new Ms_Fragment_Emergency();
                case 2:
                    return new Ms_Fragment_Package();
                case 3:
                    return new Ms_Fragment_Payment();
                case 4:
                    return new MS_Signature();
                case 5:
                    fragment = new MSDTD_Checklist();
                    fragment.setArguments(getIntent().getExtras());
                    return fragment;
                case 6:
                    return new Ms_Fragment_Status();
            }
            return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }


        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Profile";
                case 1:
                    return "Emergency";
                case 2:
                    return "Package";
                case 3:
                    return "1st Payment";
                case 4:
                    return "Signature";
                case 5:
                    return "Checklist";
                case 6:
                    return "Status";
            }
            return null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * Method for generate Form No
     */
    private void generateFormNo() {
        String yy, mm, dd, hh, ss, sss;
        listLogLogin = new ArrayList<>();
        mLogLoginAdapter = new TableLogLoginAdapter(MainFragmentMS.this);
        listLogLogin = mLogLoginAdapter.getAllData();
        if (listLogLogin.size() > 0) {
            employeeId = listLogLogin.get(listLogLogin.size() - 1).getcEmployeeId();
        } else {
            employeeId = "";
        }
        //String sflCode = sessionPref.getString("sfl_code", "");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        sss = strDate.substring(17, 19);
        StringBuilder sb = new StringBuilder();
        sb.append("M");
        sb.append(employeeId);
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.toString();
        SharedPreferences preferences = getSharedPreferences(getString(R.string.fn_ms), Context.MODE_PRIVATE);
        SharedPreferences.Editor shareEditor = preferences.edit();
        shareEditor.putString("fn", sb.toString());
        shareEditor.commit();
    }

    public void attachedImage() {
        isExistKtp = false;
        isExistLing = false;
        isExistInstall = false;
        isExistRmh = false;
        isExistSign = false;
    }

    @Override
    public void onBackPressed() {
        set_exit();
    }

    private void set_exit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainFragmentMS.this);
        builder.setMessage("Anda yakin akan keluar dari form?");
        builder.setTitle("EXIT");
        builder.setPositiveButton(getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SharedPreferences sm = getSharedPreferences(getString(
                                R.string.fn_ms), Context.MODE_PRIVATE);
                        String numberForm = sm.getString("fn", null);

                        if (numberForm != null) {
                            TableFormAppAdapter adapter = new TableFormAppAdapter(MainFragmentMS.this);
                            adapter.deleteBy(MainFragmentMS.this, TableFormApp.fFORM_NO, numberForm);
                        }

                        SharedPreferences sessionPref = getSharedPreferences(getString(
                                R.string.fn_ms), Context.MODE_PRIVATE);
                        SharedPreferences.Editor shareEditor = sessionPref.edit();
                        shareEditor.remove("fn");
                        shareEditor.commit();
                        finish();

                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                set_exit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /*@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);
    }*/

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.info_menu, menu);
        this.menu = menu;
        alert = this.menu.findItem(R.id.count);
        alert.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }*/


}