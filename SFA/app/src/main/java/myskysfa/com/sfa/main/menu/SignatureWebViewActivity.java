package myskysfa.com.sfa.main.menu;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.main.menu.dtd.DTD_Signature;
import myskysfa.com.sfa.main.menu.dtdproject.ProSign;
import myskysfa.com.sfa.main.menu.ms.MS_Signature;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.widget.SignatureView;

/**
 * Created by Hari Hendryan on 11/2/2015.
 */
public class SignatureWebViewActivity extends AppCompatActivity {
    private Context context;
    private WebView webView;
    private static String type;
    //    private String fileName;
    private String numberForm, uId;
    private int id_webview;
    private boolean wvReady = false;
    private String url;
    private static final String TAG = SignatureWebViewActivity.class.getSimpleName();
    private String contentWV;
    private Bitmap bitmap;
    private Button ok, save, clear;
    private ImageView closer, sign;
    private EditText editTextMulti;
    public static String sales_name, nik, sales_code, pos, cust, custAddress,
            profileValue,packageValue, no_iden, appType, name, imagePathSign, installAddress, gender, birthPlace,
            rt, rw, zipCode, kelurahan, kecamatan, kota, status_rumah = "",
            customeredMulti = "", reasonMulti = "", reasonBedaAlamat = "", imageSale, image,
            imagePathCust = "", imagePathSales = "", value_vc = "", value_cust = "", sales_fullname,
            payment_method, router, iccid, imei, telephone, email, promoCode;

    private SignatureView signatureView;
    private JavaScriptClickSignatureCustomer signatureCustomer;
    private JavaScriptClickSignatureSALES signatureSALES;
    private JavaScriptClickCheckBox checkBox;
    private JavaScriptClickBerlangganan checkBoxSub;
    private JavaScriptClickReasoMulti checkBoxReason;
    private JavaScriptChangeReason changeReason;
    private JavaScriptNoPelanggan setNoCust;
    private JavaScriptClickPaymentMethod checkBoxPayment;
    private JavaScriptInputRouter inputRouter;
    private JavaScriptInputICCID inputICCID;
    private JavaScriptInputIMEI inputIMEI;
    private SharedPreferences sessionPref;
    private List<TableFormApp> listFormApp;
    private TableFormAppAdapter adapter;
    private DTD_Signature dtd_signature;
    private MS_Signature ms_signature;
    private ProSign dtd_bb;

    @SuppressLint("AddJavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature_web_view);
        this.context = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        type = getIntent().getStringExtra(getResources().getString(R.string.type));
//        imagePath = getIntent().getStringExtra("imagePath");
//        fileName = getIntent().getStringExtra("fileName");
        numberForm = getIntent().getStringExtra("numberForm");
        id_webview = getIntent().getIntExtra("id_webview", 0);
        checkBox = new JavaScriptClickCheckBox(this);
        checkBoxSub = new JavaScriptClickBerlangganan(this);
        checkBoxReason = new JavaScriptClickReasoMulti(this);
        checkBoxPayment = new JavaScriptClickPaymentMethod(this);
        inputRouter = new JavaScriptInputRouter(this);
        inputICCID = new JavaScriptInputICCID(this);
        inputIMEI = new JavaScriptInputIMEI(this);
        getSupportActionBar().setTitle(type);
        appType = getIntent().getStringExtra("appType");
        signatureCustomer = new JavaScriptClickSignatureCustomer(this);
        changeReason = new JavaScriptChangeReason(this);
        signatureSALES = new JavaScriptClickSignatureSALES(this);
        setNoCust = new JavaScriptNoPelanggan(this);
        webViewProperties();
    }

    private void getDataSales() throws JSONException {
        try {
            listFormApp = new ArrayList<>();
            adapter = new TableFormAppAdapter(SignatureWebViewActivity.this);
            listFormApp = adapter.getDatabyCondition(TableFormApp.fFORM_NO, numberForm);
            sessionPref = getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
            sales_name = sessionPref.getString(TableLogLogin.C_USER_NAME, "");
            sales_fullname = sessionPref.getString(TableLogLogin.C_FULL_NAME, "");
            sales_code = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
            if (!sessionPref.getString(TableLogLogin.C_NIK, "").equals("")
                    && sessionPref.getString(TableLogLogin.C_NIK, "") != null) {
                nik = sessionPref.getString(TableLogLogin.C_NIK, "");
            } else {
                nik = sales_code;
            }
            pos = sessionPref.getString(TableLogLogin.C_REGIONNAME, "");
            cust = listFormApp.get(0).getFIRST_NAME();
            no_iden = listFormApp.get(0).getIDENTITY_NO();
            profileValue = listFormApp.get(0).getVALUES_PROFILE();
            packageValue = listFormApp.get(0).getVALUES_PACKAGE();
            JSONObject jsonObject = new JSONObject(profileValue);

            custAddress = jsonObject.getString("address");
            installAddress = jsonObject.getString("installAddress");
            gender = jsonObject.getString("gender").equals("1") ? "laki-laki" : "perempuan";
            birthPlace = jsonObject.getString("birthPlace") + " " + jsonObject.getString("birthDate");
            rt = jsonObject.getString("rt");
            rw = jsonObject.getString("rw");
            zipCode = jsonObject.getString("zipCode");
            kelurahan = jsonObject.getString("kelurahan");
            kecamatan = jsonObject.getString("kecamatan");
            kota = jsonObject.getString("city");
            telephone = jsonObject.getString("telephone");
            email = jsonObject.getString("email");

            if (packageValue != null){
                JSONObject objectPackage = new JSONObject(packageValue);
                promoCode = objectPackage.getString("promoCode");
            }else {
                promoCode = " - ";
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void webViewProperties() {
        if (id_webview == 0) {
            finish();
        } else if (id_webview == 1) {
            url = "html/surat_pernyataan_beda_alamat.html";
        } else if (id_webview == 2) {
            url = "html/surat_pernyataan_pelanggan_rumah_sewa_kost_kontrak.html";
        } else if (id_webview == 3) {
            url = "html/surat_pernyataan_multyAccount.html";
        } else if (id_webview == 4) {
            url = "html/surat_pernyataan_bundle_indosat.html";
        }
        else {
            finish();
        }

        webView = (WebView) findViewById(R.id.signatureWV);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result) {
                new AlertDialog.Builder(SignatureWebViewActivity.this)
                        .setTitle("Warning!")
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok,
                                new AlertDialog.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        result.confirm();
                                    }
                                })
                        .setCancelable(false)
                        .create()
                        .show();

                return true;
            }
        });
        webView.requestFocus(View.FOCUS_DOWN);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            webSettings.setAllowUniversalAccessFromFileURLs(true);
            webSettings.setAllowFileAccessFromFileURLs(true);
        }
        webView.loadUrl("file:///android_asset/" + url);
        webView.setWebViewClient(new MyWebViewClient());

        if (id_webview == 1) {
            webView.addJavascriptInterface(checkBox, "checkbox");
            webView.addJavascriptInterface(signatureCustomer, "signatureCustomer");
            webView.addJavascriptInterface(changeReason, "changeReason");
            webView.addJavascriptInterface(signatureSALES, "SignatureSales");

        } else if (id_webview == 2) {
            webView.addJavascriptInterface(signatureCustomer, "signatureCustomer");
        } else if (id_webview == 3) {
            webView.addJavascriptInterface(checkBoxSub, "checkboxSub");
            webView.addJavascriptInterface(checkBoxReason, "checkboxreason");
            webView.addJavascriptInterface(signatureCustomer, "signatureCustomer");
            webView.addJavascriptInterface(setNoCust, "setNoCust");
        } else if (id_webview == 4) {
            webView.addJavascriptInterface(checkBoxPayment, "checkBoxPayment");
            webView.addJavascriptInterface(inputRouter, "inputRouter");
            webView.addJavascriptInterface(inputICCID, "inputICCID");
            webView.addJavascriptInterface(inputIMEI, "inputIMEI");
            webView.addJavascriptInterface(signatureCustomer, "signatureCustomer");
        }
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            wvReady = true;
            try {
                if (id_webview == 1) {
                    getDataSales();
                    webView.loadUrl("javascript:OnAttachInfo(\"" + numberForm + "\",\"" + sales_fullname
                            + "\",\"" + nik + "\",\"" + sales_code + "\",\"" + pos + "\", \"" + cust + "\"," +
                            "\"" + custAddress + "\",\"" + installAddress + "\")");
                } else if (id_webview == 2) {
                    getDataSales();
                    webView.loadUrl("javascript:OnAttachInfo(\""+numberForm+"\",\""+cust
                            +"\",\""+no_iden+"\",\""+custAddress+"\")");
                } else if (id_webview == 3) {
                    getDataSales();
                    webView.loadUrl("javascript:OnAttachInfo(\"" + numberForm + "\"," +
                            "\"" + no_iden + "\",\"" + gender + "\", \"" + cust + "\"," +
                            "\"" + birthPlace + "\",\"" + installAddress + "\",\"" + rt + "\", \"" + rw + "\"," +
                            "\"" + zipCode + "\", \"" + kelurahan + "\",\"" + kecamatan + "\", " +
                            "\"" + kota + "\" )");
                } else if (id_webview == 4) {
                    webView.loadUrl("javascript:OnAttachInfo(\""+numberForm+"\",\""+cust
                            +"\",\""+telephone+"\",\""+email+"\",\""+promoCode+"\")");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                       SslError error) {
            Log.e("Error VAGARO", error.toString());
            handler.proceed();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("http")) {
                view.loadUrl(url);
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        if (name != null) {
            setImagePreview();
        } else {
            switch (appType) {
                case "dtd":
                    dtd_signature = new DTD_Signature();
                    dtd_signature.setInvisibleCheckbox(type);
                    break;
                case "ms":
                    ms_signature = new MS_Signature();
                    ms_signature.setInvisibleCheckbox(type);
                    break;
                case "dtdbb":
                    dtd_bb = new ProSign();
                    dtd_bb.setInvisibleCheckbox(type);
                    break;
            }
        }

    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                setImagePreview();
                break;
        }
        return super.onOptionsItemSelected(item);
    }*/

    private void setImagePreview() {
        Log.e("setImagePreview", appType+", "+name);
        switch (appType) {
            case "dtd":
                switch (name) {
                    case "sales_beda_alamat":
                        dtd_signature = new DTD_Signature();
                        dtd_signature.setValueHtmlBedaAlamat(status_rumah, reasonBedaAlamat, imagePathSales);
                        dtd_signature.decodeFile(imagePathCust, "bedalamat");
                        break;
                    case "beda_alamat":
                        dtd_signature = new DTD_Signature();
                        dtd_signature.setValueHtmlBedaAlamat(status_rumah, reasonBedaAlamat, imagePathSales);
                        dtd_signature.decodeFile(imagePathCust, "bedalamat");
                        break;
                    case "rumah_sewa":
                        Log.e("case", "rumah_sewa");
                        dtd_signature = new DTD_Signature();
                        dtd_signature.decodeFile(imagePathSign, "multy");
                        break;
                    case "multi_account":
                        Log.e("case", "multi_account");
                        dtd_signature = new DTD_Signature();
                        dtd_signature.decodeFile(imagePathSign, "multi_account");
                        dtd_signature.setValueHtmlMultiAccount(customeredMulti, reasonMulti,
                                value_cust, value_vc);
                        break;
                    case "bundle_indosat":
                        Log.e("case", imagePathSign);
                        dtd_signature = new DTD_Signature();
                        dtd_signature.setvalueHtmlBundleIndosat(payment_method, router, iccid, imei);
                        dtd_signature.decodeFile(imagePathSign, "bundle_indosat");
                        break;
                }
                break;
            case "ms":
                switch (name) {
                    case "sales_beda_alamat":
                        ms_signature = new MS_Signature();
                        ms_signature.setValueHtmlBedaAlamat(status_rumah, reasonBedaAlamat, imagePathSales);
                        ms_signature.decodeFile(imagePathCust, "bedalamat");
                        break;
                    case "beda_alamat":
                        ms_signature = new MS_Signature();
                        ms_signature.setValueHtmlBedaAlamat(status_rumah, reasonBedaAlamat, imagePathSales);
                        ms_signature.decodeFile(imagePathCust, "bedalamat");
                        break;
                    case "rumah_sewa":
                        ms_signature = new MS_Signature();
                        ms_signature.decodeFile(imagePathSign, "multy");
                        break;
                    case "multi_account":
                        ms_signature = new MS_Signature();
                        ms_signature.decodeFile(imagePathSign, "multi_account");
                        ms_signature.setValueHtmlMultiAccount(customeredMulti, reasonMulti,
                                value_cust, value_vc);
                        break;
                    case "bundle_indosat":
                        Log.e("case", imagePathSign);
                        ms_signature = new MS_Signature();
                        ms_signature.setvalueHtmlBundleIndosat(payment_method, router, iccid, imei);
                        ms_signature.decodeFile(imagePathSign, "bundle_indosat");
                        break;
                }
                break;

            case "dtdbb":
                switch (name) {
                    case "sales_beda_alamat":
                        dtd_bb = new ProSign();
                        dtd_bb.setValueHtmlBedaAlamat(status_rumah, reasonBedaAlamat, imagePathSales);
                        dtd_bb.decodeFile(imagePathCust, "bedalamat");
                        break;
                    case "beda_alamat":
                        dtd_bb = new ProSign();
                        dtd_bb.setValueHtmlBedaAlamat(status_rumah, reasonBedaAlamat, imagePathSales);
                        dtd_bb.decodeFile(imagePathCust, "bedalamat");
                        break;
                    case "rumah_sewa":
                        dtd_bb = new ProSign();
                        dtd_bb.decodeFile(imagePathSign, "multy");
                        break;
                    case "multi_account":
                        dtd_bb = new ProSign();
                        dtd_bb.decodeFile(imagePathSign, "multi_account");
                        dtd_bb.setValueHtmlMultiAccount(customeredMulti, reasonMulti,
                                value_cust, value_vc);
                        break;
                    case "bundle_indosat":
                        Log.e("case", imagePathSign);
                        dtd_bb = new ProSign();
                        dtd_bb.setvalueHtmlBundleIndosat(payment_method, router, iccid, imei);
                        dtd_bb.decodeFile(imagePathSign, "bundle_indosat");
                        break;
                }
                break;

        }
    }


    private void SignatureClick(final String name) {
        this.name = name;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_sign);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        closer = (ImageView) dialog.findViewById(R.id.imageView_close);
        clear = (Button) dialog.findViewById(R.id.clear);
        ok = (Button) dialog.findViewById(R.id.ok);
        signatureView = (SignatureView) dialog.findViewById(R.id.signature);
        closer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (signatureView.isClear()) {
                    Toast.makeText(context, "Silahkan tanda tangan!", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    StringBuilder fileName = new StringBuilder();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date now = new Date();
                    String strDate = sdf.format(now);
                    String yy = strDate.substring(2, 4);
                    String mm = strDate.substring(5, 7);
                    String dd = strDate.substring(8, 10);
                    if (!Environment.getExternalStorageState().equals(
                            Environment.MEDIA_MOUNTED)) {
                        Toast.makeText(context, "Error! No SDCARD Found!", Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        if (uId == null) {
                            uId = "";
                        }
                        fileName.append(numberForm + "_");
                        fileName.append(name);
                        fileName.append("_");
                        fileName.append("Signature_");
                        fileName.append(yy);
                        fileName.append(mm);
                        fileName.append(dd);
                        fileName.append(".jpg");
                        String _fileNameSign = fileName.toString();
                        String path = Environment.getExternalStorageDirectory() + File.separator + Config.path;
                        signatureView.exportFile(path, _fileNameSign);
                        dialog.dismiss();
                        //Toast.makeText(context, "Gambar tersimpan", Toast.LENGTH_SHORT).show();
                        imagePathSign = path + File.separator + fileName.toString();
                        decodeFile(imagePathSign);
                        bitmap = BitmapFactory.decodeFile(imagePathSign);

                        try {
                            InputStream is = getAssets().open(url);
                            final Document doc = Jsoup.parse(is, "UTF-8", "");
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            if (bitmap != null) {
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                                byte[] byteArray = byteArrayOutputStream.toByteArray();
                                String imgageBase64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                                if (name.equalsIgnoreCase("sales_beda_alamat")) {
                                    imagePathSales = imagePathSign;
                                    imageSale = "data:image/png;base64," + imgageBase64;
                                    doc.getElementsByClass("ttdsales").attr("src", imageSale);

                                    if (image != null) {
                                        if (!image.equals("")) {
                                            doc.getElementsByClass("ttdPelanggan").attr("src", image);
                                        }
                                    }
                                } else {
                                    imagePathCust = imagePathSign;
                                    image = "data:image/png;base64," + imgageBase64;
                                    doc.getElementsByClass("ttdPelanggan").attr("src", image);

                                    if (imageSale != null) {
                                        if (!imageSale.equals("")) {
                                            doc.getElementsByClass("ttdsales").attr("src", imageSale);
                                        }
                                    }
                                }

                                //String docS = doc.getElementsByClass("ttd" + name).attr("src", image).toString();
                            }


                            /*tinggal eksekusi update database dengan nama image*/
                            if (id_webview == 1) {

//                                        db.updatePartial(getActivity(), TableFormApp.fVALUES_SIGNATURE, imagePath, TableFormApp.fFORM_NO, numberForm);

                            } else if (id_webview == 2) {

//                                        db.updatePartial(getActivity(), TableFormApp.fVALUES_SIGNATURE, imagePath, TableFormApp.fFORM_NO, numberForm);

                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    webView.loadDataWithBaseURL(null, doc.toString(), "text/html", "utf-8", null);
                                }
                            });

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        });


        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signatureView.clear();
            }
        });
        dialog.show();
    }


    public void decodeFile(final String filePath) {
        // Decode ukuran gambar
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);
        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        // Decode with inSampleSize
        final BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;

        bitmap = BitmapFactory.decodeFile(filePath, o2);
//        prev.setImageBitmap(bitmap);

    }


    private void setValueEditText(final String res) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_set_multi_account);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        closer = (ImageView) dialog.findViewById(R.id.close);
        ok = (Button) dialog.findViewById(R.id.ok);
        editTextMulti = (EditText) dialog.findViewById(R.id.input_multi);
        closer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    wvReady = true;
                    if (!wvReady) {
                        System.out.println("Webview not ready");
                        return;
                    }
                    dialog.dismiss();
                    android.os.Handler handler = new android.os.Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (res.equalsIgnoreCase("noCust")) {
                                value_cust = editTextMulti.getText().toString();
                            } else {
                                value_vc = editTextMulti.getText().toString();
                            }
                            webView.loadUrl("javascript:OnAttachMulty(\"" + res + "\",\"" + editTextMulti.getText().toString() + "\")");
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
    }

    public class JavaScriptClickCheckBox {
        Context mContext;

        JavaScriptClickCheckBox(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void performClick(String webMessage) {
            status_rumah = webMessage;
        }
    }

    public class JavaScriptChangeReason {
        Context mContext;

        JavaScriptChangeReason(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void performChange(String webMessage) {
            reasonBedaAlamat = webMessage;
        }
    }

    public class JavaScriptClickBerlangganan {
        Context mContext;

        JavaScriptClickBerlangganan(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void performClickBerlangganan(String webMessage) {
            customeredMulti = webMessage;
        }
    }


    public class JavaScriptClickReasoMulti {
        Context mContext;

        JavaScriptClickReasoMulti(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void performClickReason(String webMessage) {
            reasonMulti = webMessage;
        }
    }

    public class JavaScriptClickPaymentMethod {
        Context mContext;

        JavaScriptClickPaymentMethod(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void performClickPaymentMethod(String webMessage) {
            payment_method = webMessage;
        }
    }

    public class JavaScriptInputRouter {
        Context mContext;

        JavaScriptInputRouter(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void performInputRouter(String webMessage) {
            router = webMessage;
        }
    }

    public class JavaScriptInputICCID {
        Context mContext;

        JavaScriptInputICCID(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void performInputICCID(String webMessage) {
            iccid = webMessage;
        }
    }

    public class JavaScriptInputIMEI {
        Context mContext;

        JavaScriptInputIMEI(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void performInputIMEI(String webMessage) {
            imei = webMessage;
        }
    }

    /*public class JavaScriptClickSignatureWAKA {
        Context mContext;

        JavaScriptClickSignatureWAKA(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void performClickWaka(String webMessage) {
            Toast.makeText(mContext, webMessage, Toast.LENGTH_SHORT).show();
            SignatureClick("waka");

        }
    }*/


    public class JavaScriptClickSignatureSALES {
        Context mContext;

        JavaScriptClickSignatureSALES(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void performClickSales(String webMessage) {
            SignatureClick("sales_beda_alamat");

        }
    }

    public class JavaScriptClickSignatureCustomer {
        Context context;

        public JavaScriptClickSignatureCustomer(Context context) {
            this.context = context;
        }

        @JavascriptInterface
        public void performClickCustomer(String webMessage) {
            //Toast.makeText(context, webMessage, Toast.LENGTH_SHORT).show();
            if (id_webview == 1) {
                SignatureClick("beda_alamat");
            } else if (id_webview == 2) {
                SignatureClick("rumah_sewa");
            } else if (id_webview == 3) {
                SignatureClick("multi_account");
            } else if (id_webview == 4){
                SignatureClick("bundle_indosat");
            }
        }
    }


    public class JavaScriptNoPelanggan {
        Context context;

        public JavaScriptNoPelanggan(Context context) {
            this.context = context;
        }

        @JavascriptInterface
        public void performInputCust(String res) {
            setValueEditText(res);
        }
    }

}
