package myskysfa.com.sfa.main.menu.dtd;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.text.Line;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.main.menu.ms.ShowListDropDown;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.Utils;
import myskysfa.com.sfa.widget.SignatureView;

/**
 * Created by admin on 6/22/2016.
 */
public class DTDPayment extends Fragment {

    private static Context _context;
    private TextView txtByr, txtBerlaku, txtPeriodebyr, jumlah, imageName, list_payment_cc,
            txtdatePayment, lblTglByar;
    private EditText noRek, edtName, edtBank, edtRemark, eAmout, ccNumb;
    private Button save;
    private String uId, numberForm, value, idMethodBayar;
    private SharedPreferences sessionPref;
    private LinearLayout ccCardType, ccCardNO, linearNoReg, bank_name, line_masa_berlaku;
    private Utils utils;
    private TableFormAppAdapter mFormAppAdapter;
    private List<TableFormApp> list;
    private ShowListDropDown showList;
    private View view;
    private ArrayAdapter<String> arrayAdapter;
    private List<String> assignValue;
    private Boolean isCC = false, isAtm = false, isExpired = true;

    public static Fragment newInstance(Context context) {
        _context = context;
        DTDPayment dtdPayment = new DTDPayment();
        return dtdPayment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dtd_frag_payment, container, false);
        sessionPref = getActivity().getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);

        utils = new Utils(getActivity());
        utils.setIMEI();

        mFormAppAdapter = new TableFormAppAdapter(getActivity());
        uId = sessionPref.getString(TableLogLogin.C_UID, "");

        //prev = (ImageView) view.findViewById(R.id.imagepreview);
        //sign = (ImageView) view.findViewById(R.id.signature);
        save = (Button) view.findViewById(R.id.save);
        //imageName = (TextView) view.findViewById(R.id.sig_pic);

        txtByr = (TextView) view.findViewById(R.id.list_payment_method);
        txtBerlaku = (TextView) view.findViewById(R.id.list_payment_berlaku);
        txtPeriodebyr = (TextView) view.findViewById(R.id.list_payment_period);
        jumlah = (TextView) view.findViewById(R.id.add_new_jumlah);
        list_payment_cc = (TextView) view.findViewById(R.id.list_payment_cc);
        txtdatePayment = (TextView) view.findViewById(R.id.list_payment_date);
        lblTglByar = (TextView) view.findViewById(R.id.lblTglByar);

        noRek = (EditText) view.findViewById(R.id.txtRekNo);
        edtName = (EditText) view.findViewById(R.id.txtNama);
        edtBank = (EditText) view.findViewById(R.id.txtBank);
        edtRemark = (EditText) view.findViewById(R.id.txtRemark);
        eAmout = (EditText) view.findViewById(R.id.valuePay);
        ccNumb = (EditText) view.findViewById(R.id.txtCardNo);

        ccCardType = (LinearLayout) view.findViewById(R.id.linearCardType);
        ccCardNO = (LinearLayout) view.findViewById(R.id.linearCardNo);
        linearNoReg = (LinearLayout) view.findViewById(R.id.linearNoReg);
        bank_name = (LinearLayout) view.findViewById(R.id.bank_name);
        line_masa_berlaku = (LinearLayout) view.findViewById(R.id.line_masa_berlaku);

        /*SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.fn_ms), Context.MODE_PRIVATE);
        numberForm = sm.getString("fn", null);*/

        //sign.setOnClickListener(ibListener);
        save.setOnClickListener(saveListener);
        txtBerlaku.setOnClickListener(textBerlaku);
        txtPeriodebyr.setOnClickListener(textPeriode);
        txtByr.setOnClickListener(textCaraByr);
        list_payment_cc.setOnClickListener(textPayCC);
        txtdatePayment.setOnClickListener(txtPayDate);
        return view;
    }

    private View.OnClickListener textCaraByr = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            /*showList = new ShowListDropDown(getActivity(), v, "Cara Bayar",
                    txtByr, getResources().getStringArray(R.array.payment_method));
            showList.Show();*/
            assignValue = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.payment_method2)));
            arrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item
                    , assignValue);
            new AlertDialog.Builder(getActivity())
                    .setTitle("Cara Bayar")
                    .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            txtByr.setText(assignValue.get(which));
                            idMethodBayar = assignValue.get(which).substring(0, 1);

                            if (idMethodBayar.equalsIgnoreCase("1")) {
                                line_masa_berlaku.setVisibility(View.VISIBLE);
                                ccCardType.setVisibility(View.VISIBLE);
                                ccCardNO.setVisibility(View.VISIBLE);
                                bank_name.setVisibility(View.GONE);
                                lblTglByar.setText("*Tanggal Deposit");
                                linearNoReg.setVisibility(View.GONE);
                                isCC = true;
                                isExpired = true;
                                isAtm = false;
                            } else if (idMethodBayar.equalsIgnoreCase("2")) {
                                line_masa_berlaku.setVisibility(View.VISIBLE);
                                ccCardType.setVisibility(View.GONE);
                                ccCardNO.setVisibility(View.GONE);
                                bank_name.setVisibility(View.GONE);
                                linearNoReg.setVisibility(View.GONE);
                                list_payment_cc.setText("");
                                lblTglByar.setText("*Tanggal Bayar");
                                ccNumb.setText("");
                                isCC = false;
                                isAtm = false;
                                isExpired = true;
                            } else if (idMethodBayar.equalsIgnoreCase("3")) {
                                line_masa_berlaku.setVisibility(View.VISIBLE);
                                ccCardType.setVisibility(View.GONE);
                                ccCardNO.setVisibility(View.GONE);
                                linearNoReg.setVisibility(View.VISIBLE);
                                bank_name.setVisibility(View.VISIBLE);
                                lblTglByar.setText("*Tanggal Bayar");
                                list_payment_cc.setText("");
                                ccNumb.setText("");
                                isCC = false;
                                isAtm = true;
                                isExpired = true;
                            } else {
                                line_masa_berlaku.setVisibility(View.GONE);
                                ccCardType.setVisibility(View.GONE);
                                ccCardNO.setVisibility(View.GONE);
                                bank_name.setVisibility(View.GONE);
                                linearNoReg.setVisibility(View.GONE);
                                list_payment_cc.setText("");
                                ccNumb.setText("");
                                isCC = false;
                                isAtm = false;
                                isExpired = false;
                            }
                        }
                    }).create().show();
        }
    };

    private View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                checkMandatoryForm();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private View.OnClickListener textBerlaku = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            utils.setDateField(getActivity(), txtBerlaku);
        }
    };

    private View.OnClickListener textPeriode = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //utils.setDateField(getActivity(), txtPeriodebyr);
            showList = new ShowListDropDown(getActivity(), v, "Payment Period",
                    txtPeriodebyr, getResources().getStringArray(R.array.payment_p));
            showList.Show();
        }
    };


    private void checkMandatoryForm() {
        if (txtByr.getText().toString() == null
                || txtByr.getText().toString().equalsIgnoreCase("")
                || txtByr.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Cek kembali Cara Bayar", Toast.LENGTH_SHORT).show();
            return;

        }
        if (isCC) {
            if (list_payment_cc.getText().toString() == null
                    || list_payment_cc.getText().toString().equalsIgnoreCase("")
                    || list_payment_cc.getText().toString().length() < 1) {
                Toast.makeText(getActivity(), "Cek kembali Type Card", Toast.LENGTH_SHORT).show();
                return;
            } else if (ccNumb.getText().toString() == null
                    || ccNumb.getText().toString().equalsIgnoreCase("")
                    || ccNumb.getText().toString().length() < 1) {
                Toast.makeText(getActivity(), "Cek No Card", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (isAtm) {
            if (noRek.getText().toString() == null
                    || noRek.getText().toString().equalsIgnoreCase("")
                    || noRek.getText().toString().length() < 1) {
                Toast.makeText(getActivity(), "Cek kembali no Rekening", Toast.LENGTH_SHORT).show();
                return;
            } else if (edtBank.getText().toString() == null
                    || edtBank.getText().toString().equalsIgnoreCase("")
                    || edtBank.getText().toString().length() < 1) {
                Toast.makeText(getActivity(), "Cek kembali nama bank", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (edtName.getText().toString() == null
                || edtName.getText().toString().equalsIgnoreCase("")
                || edtName.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Cek kembali nama", Toast.LENGTH_SHORT).show();
            return;
        }
        if (isExpired) {
            if (txtBerlaku.getText().toString() == null
                    || txtBerlaku.getText().toString().equalsIgnoreCase("")
                    || txtBerlaku.getText().toString().length() < 1) {
                Toast.makeText(getActivity(), "Cek kembali masa berlaku", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (txtPeriodebyr.getText().toString() == null
                || txtPeriodebyr.getText().toString().equalsIgnoreCase("")
                || txtPeriodebyr.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Cek kembali periode bayar", Toast.LENGTH_SHORT).show();
            return;
        } else if (eAmout.getText().toString() == null
                || eAmout.getText().toString().equalsIgnoreCase("")
                || eAmout.getText().toString().trim().length() < 1) {
            Toast.makeText(getActivity(), "Masukkan Jumlah Pembayaran!", Toast.LENGTH_SHORT).show();
            return;
        } else if (txtdatePayment.getText().toString() == null
                || txtdatePayment.getText().toString().equalsIgnoreCase("")
                || txtdatePayment.getText().toString().trim().length() < 1) {
            Toast.makeText(getActivity(), "Masukkan Tanggal Pembayaran", Toast.LENGTH_SHORT).show();
            return;
        } else {
            try {
                value = jsonValueCreator();
                mFormAppAdapter.updatePartial(getActivity(), TableFormApp.fVALUES_PAYMENT,
                        value, TableFormApp.fFORM_NO, numberForm);
                Toast.makeText(getActivity(), "Payment Saved", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Payment Saved Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private View.OnClickListener textPayCC = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showList = new ShowListDropDown(getActivity(), v, "Card Type",
                    list_payment_cc, getResources().getStringArray(R.array.card_type));
            showList.Show();
        }
    };

    private View.OnClickListener txtPayDate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            utils.setDateTimeField(getActivity(), txtdatePayment);
        }
    };

    /**
     * Method for create json from form value
     *
     * @return
     * @throws Exception
     */
    private String jsonValueCreator() throws Exception {
        JSONObject obj = new JSONObject();
        //File newFile = new File(imagePath);

        try {
            obj.put("ftId", numberForm);
            obj.put("total", eAmout.getText().toString());
            obj.put("caraBayar", txtByr.getText().toString());
            obj.put("name", edtName.getText().toString());
            if (ccNumb.getText().toString().trim().length() > 0) {
                obj.put("cc_no", ccNumb.getText().toString());
            }
            if (list_payment_cc.getText().toString().trim().length() > 0) {
                obj.put("cc_type", showList.getCardType());
            }
            if (edtBank.getText().toString().trim().length() > 0) {
                obj.put("bank", edtBank.getText().toString());
            }
            if (noRek.getText().toString().trim().length() > 0) {
                obj.put("noRek", noRek.getText().toString());
            }
            obj.put("payment_date", txtdatePayment.getText().toString());

            if (txtBerlaku.getText().toString().trim().length() > 0) {
                obj.put("masaBerlaku", txtBerlaku.getText().toString());
            }
            obj.put("periode", txtPeriodebyr.getText().toString());
            obj.put("remark", edtRemark.getText().toString());
            Log.d("jsonku", obj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj.toString();
    }

    public void setEstimate() {
        try {
            mFormAppAdapter = new TableFormAppAdapter(getActivity());
            list = new ArrayList<>();
            list = mFormAppAdapter.getDatabyCondition(TableFormApp.fFORM_NO, numberForm);
            if (list != null && list.size() > 0) {
                try {
                    JSONObject jsonObject = new JSONObject(list.get(0).getVALUES_PACKAGE());
                    if (jsonObject != null) {
                        int hasil = jsonObject.getInt("amount");
                        NumberFormat defaultF = NumberFormat.getInstance();
                        String amount = defaultF.format(hasil)
                                .replace(",", ".");
                        jumlah.setText(amount);
                    } else {
                        jumlah.setText("");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
        }
    }

    public void setFormNumber() {
        SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.fn_dtd),
                Context.MODE_PRIVATE);
        numberForm = sm.getString("fn", null);
    }

    public void setFormNumberBB() {
        SharedPreferences sm = getActivity().getSharedPreferences(
                "formId", Context.MODE_PRIVATE);
        numberForm = sm.getString("fn", null);
    }
}
