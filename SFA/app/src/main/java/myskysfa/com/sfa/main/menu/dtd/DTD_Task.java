package myskysfa.com.sfa.main.menu.dtd;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.MSTaskAdapter;
import myskysfa.com.sfa.database.TableFreeTrial;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.TablePlan;
import myskysfa.com.sfa.database.db_adapter.TableFreeTrialAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePlanAdapter;
import myskysfa.com.sfa.main.menu.MainFragmentDTD;
import myskysfa.com.sfa.main.menu.ms.Ms_Task;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by Eno on 6/27/2016.
 */
public class DTD_Task extends Fragment {
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private MSTaskAdapter adaper;
    private ProgressDialog _progressDialog;
    private ImageButton fab;
    private Menu menu;
    private MenuItem alert;
    private String url, response, message, token, imei, sflCode, data, ftId, regDate, name, phone, hp,
            brand, createdDate, statusDesc, username, value, mFtId, planId, planDate, target, zipCode,
            area, tsName, tsCode, planUrl, productName, mFilter, header;
    private SharedPreferences sessionPref;
    private Utils utils;
    private Handler handler;
    private int signal;
    private JSONArray jsonArray;
    private TablePlanAdapter planDbAdapter;
    private TableFreeTrialAdapter dbAdapter;
    private List<TableFreeTrial> list;
    private int offset = 0, count;
    private boolean status = false;
    public int NUM_ITEMS_PAGE = 10;
    private boolean isTaskRunning = false;
    private PlanTask planTask;
    private TextView ts_name;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dtd_fragment_task, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        fab = (ImageButton) view.findViewById(R.id.fab);
        ts_name = (TextView) view.findViewById(R.id.ts_name);
        ts_name.setSelected(true);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        //Refresh();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new LoadDataListDTD().execute();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainFragmentDTD.class);
                i.putExtra(getString(R.string.category_app), "dtd");
                startActivity(i);
            }
        });

        sessionPref = getContext().getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        utils = new Utils(getActivity());
        handler = new Handler();
        new LoadDataListDTD().execute();
        return view;
    }


    protected class LoadDataListDTD extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            signal = utils.checkSignal(getActivity());
            if (!swipeRefreshLayout.isRefreshing()) {
                if (isTaskRunning) {
                    this.cancel(true);
                    return;
                }
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
                _progressDialog = new ProgressDialog(getActivity());
                _progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
                _progressDialog.setIndeterminate(false);
                _progressDialog.setCancelable(false);
                _progressDialog.show();
            } else {
                swipeRefreshLayout.setRefreshing(true);
            }

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                list = new ArrayList<TableFreeTrial>();
                if (sessionPref == null) {
                    //Toast.makeText(getContext(),"Silahkah refresh!",Toast.LENGTH_LONG).show();
                } else {
                    sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
                    username = sessionPref.getString(TableLogLogin.C_USER_NAME, "");
                    token = sessionPref.getString(TableLogLogin.C_TOKEN, "");
                    offset = 0;
                    imei = utils.getIMEI();
                    list.clear();
                    url = ConnectionManager.CM_LIST_PROSPECT + "/" + sflCode + "/" + "A";

                    response = ConnectionManager.requestFreeTrial(url, sflCode, imei, token, username, offset, Config.version, String.valueOf(signal), getActivity());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject != null) {
                        status = jsonObject.getBoolean(Config.KEY_STATUS);
                        jsonArray = jsonObject.getJSONArray(Config.KEY_DATA);
                        count = jsonObject.getInt("count");
                        if (status) {
                            dbAdapter = new TableFreeTrialAdapter(getActivity());
                            dbAdapter.deleteAll();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                ftId = obj.getString(Config.KEY_FT_ID);
                                sflCode = obj.getString(Config.KEY_SFLCODE);

                                regDate = "";
                                name = obj.getString("prospect_name");
                                //phone = obj.getString("home_phone");
                                hp = obj.getString("mobile_phone");
                                brand = obj.getString("brand_code");
                                createdDate = obj.getString("created_date");
                                statusDesc = obj.getString("status");
                                username = obj.getString("user_name");
                                area = obj.getString("alamat");
                                productName = obj.getString("product_name");
                                planId = obj.getString("plan_id");
                                value = obj.toString();
                                //getHeaderList(createdDate);

                                dbAdapter.insertData(new TableFreeTrial(), ftId, "", brand, sflCode, createdDate, statusDesc, value, planId, header);
                            }



                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (status) {
                    getDTD();
                    ShowList();
                    StartAsyncTaskInParallel(planTask);
                    //getStockTaskValue();
                } /*else {
                    if (_progressDialog != null) {
                        _progressDialog.dismiss();
                    }
                    Toast.makeText(getContext(), "Silahkan Refresh!", Toast.LENGTH_LONG).show();
                    //utils.showErrorDlg(handler, getActivity().getResources().getString(R.string.network_error),getActivity());
                }*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void StartAsyncTaskInParallel(PlanTask task) {
        task = new PlanTask();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            task.execute();
    }


    private void ShowList() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutParams);
        adaper = new MSTaskAdapter(list);
        recyclerView.setAdapter(adaper);
    }

    private class PlanTask extends AsyncTask<String, String, Boolean> {

        boolean status;
        JSONObject jsonObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            signal = utils.checkSignal(getActivity());
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                status = false;
                imei = utils.getIMEI();
                sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
                token = sessionPref.getString(TableLogLogin.C_TOKEN, "");
                username = sessionPref.getString(TableLogLogin.C_USER_NAME, "");
                planUrl = ConnectionManager.CM_PLAN_ALL + sflCode + "/2";
                response = ConnectionManager.requestPlan(planUrl, imei, token, username, Config.version, String.valueOf(signal), getActivity());
                Log.d("FreeTrial", "response= " + response);
                jsonObject = new JSONObject(response.toString());
                if (jsonObject != null) {
                    status = jsonObject.getBoolean(Config.KEY_STATUS);
                    JSONObject obj = new JSONObject(jsonObject.getString(Config.KEY_DATA));
                    if (status) {
                        planDbAdapter = new TablePlanAdapter(getActivity());
                        planDbAdapter.delete(getActivity());

                        sflCode = obj.getString(Config.KEY_PLAN_SFL);
                        planId = obj.getString(Config.KEY_PLAN_ID);
                        planDate = obj.getString(Config.KEY_PLAN_DATE);
                        zipCode = obj.getString(Config.KEY_PLAN_ZIPCODE);
                        area    = obj.getString(Config.KEY_PLAN_AREA);
                        tsCode = obj.getString(Config.KEY_PLAN_TSCODE);
                        tsName = obj.getString(Config.KEY_PLAN_TSNAME);
                        target = obj.getString(Config.KEY_PLAN_TARGET);
                        value = obj.toString();

                        planDbAdapter.insertData(new TablePlan(), planId, planDate, zipCode, area, tsCode,
                                tsName, target, "0", sflCode, value, "0");

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return status;
        }

        @Override
        protected void onPostExecute(Boolean s) {
            super.onPostExecute(s);
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
            if (_progressDialog != null) {
                _progressDialog.dismiss();
            }
            try {
                if (s) {
                    getStockTaskValue();
                } else {
                    if (_progressDialog != null) {
                        _progressDialog.dismiss();
                    }
                    if (!jsonObject.getString("data").equalsIgnoreCase("No Data Found")){
                        utils.showErrorDlg(handler, getActivity().getResources().getString(R.string.network_error),
                                getActivity());
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getStockTaskValue() {
        TablePlanAdapter tablePlanAdapter = new TablePlanAdapter(getActivity());
        List<TablePlan> listPlan = new ArrayList<TablePlan>();
        List<TableFreeTrial> listFreetrial = new ArrayList<TableFreeTrial>();
        String date = utils.getCurrentDate();
        listPlan = tablePlanAdapter.getDatabySfl(getActivity(), TablePlan.KEY_PLAN_DATE, date);


        if (listPlan != null && listPlan.size() > 0) {
            TablePlan item = listPlan.get(0);
            int result = count;
            /*listFreetrial = dbAdapter.getDatabyCondition(TableFreeTrial.KEY_PLAN_ID, item.getPlan_id());
            int result = 0;
            for (int a = 0; a < listFreetrial.size(); a++) {
                if (!listFreetrial.get(a).getValue().equalsIgnoreCase("TITLE")) {
                    result = result + 1;
                }
            }*/
            ts_name.setText(item.getPlan_id() + " - " + item.getTs_name());
            if (result < Integer.parseInt(item.getTarget())) {
                String target = result + "/" + item.getTarget();
                alert.setTitle(target);
                fab.setVisibility(View.VISIBLE);
            } else if (result == Integer.parseInt(item.getTarget())) {
                String target = result + "/" + item.getTarget();
                alert.setTitle(target);
                fab.setVisibility(View.INVISIBLE);
            } else {
                alert.setTitle("0/0");
                fab.setVisibility(View.INVISIBLE);
            }
        } else {
            alert.setTitle("0/0");
            fab.setVisibility(View.INVISIBLE);
        }

        if (_progressDialog != null) {
            _progressDialog.dismiss();
        }
    }

    /**
     * Method for loading data in listview
     *
     * @param number
     */
    private void loadList(int number) {
        List<TableFreeTrial> sort = new ArrayList<TableFreeTrial>();
        int start = number * NUM_ITEMS_PAGE;
        for (int i = start; i < (start) + NUM_ITEMS_PAGE; i++) {
            if (i < list.size()) {
                sort.add(list.get(i));
            } else {
                break;
            }
        }
    }

    private void getDTD() {
        dbAdapter = new TableFreeTrialAdapter(getActivity());
        list = dbAdapter.getAllData();
        loadList(0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.info_menu, menu);
        this.menu = menu;
        alert = this.menu.findItem(R.id.count);
        alert.setTitle("0/0");
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();
        new LoadDataListDTD().execute();
    }
}
