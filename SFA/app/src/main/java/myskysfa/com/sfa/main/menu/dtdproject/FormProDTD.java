package myskysfa.com.sfa.main.menu.dtdproject;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.ProTabPagerAdapter;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.sfa.main.menu.MainFragmentMS;
import myskysfa.com.sfa.main.menu.dtd.DTDPayment;
import myskysfa.com.sfa.utils.DatabaseManager;
import myskysfa.com.sfa.widget.CustomViewPager;

/**
 * Created by Hari Hendryan on 12/17/2015.
 */
public class FormProDTD extends AppCompatActivity {
    private static CustomViewPager pager;
    private TabLayout tabs;
    private Toolbar _toolbar;
    private DatabaseManager dbManager;
    private String imei, token, employeeId;
    List<TableLogLogin> listLogLogin;
    private TableLogLoginAdapter mLogLoginAdapter;
    private ProTabPagerAdapter adapterPage;
    public boolean statusState = false;
    public static boolean isExistKtp, isExistLing, isExistInstall, isExistRmh, isExistSign,
            isExistBB, isExistPln;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frag_form_ft);
        _toolbar = (Toolbar) findViewById(R.id.toolbar);
        pager = (CustomViewPager) findViewById(R.id.pager);
        pager.setPagingEnabled(true);
        tabs = (TabLayout) findViewById(R.id.tabs);
        setSupportActionBar(_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mLogLoginAdapter = new TableLogLoginAdapter(FormProDTD.this);
        Log.d("Edit data", "isEdit" + getIntent().getStringExtra("isEdit"));
        //pager.addOnPageChangeListener(pageChangeListener);
        attachedImage();
        generateFtID();
        generateDtdID();
        generateDtdPPID();
        pager.setOffscreenPageLimit(6);
        pager.addOnPageChangeListener(changePageListener);
        pager.setAdapter(new MyAdapter(getSupportFragmentManager()));
        tabs.post(new Runnable() {
            @Override
            public void run() {
                tabs.setupWithViewPager(pager);
            }
        });
    }


    private ViewPager.OnPageChangeListener changePageListener = new ViewPager.OnPageChangeListener() {
        //private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (position == 1) {
                ProEmergency ftEmergency = (ProEmergency) pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                ftEmergency.setFormNumber();
            } else if (position == 2) {
                ProPaket ftPaket = (ProPaket) pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                ftPaket.setFormNumber();
            } else if (position == 3) {
                DTDPayment ftPay = (DTDPayment) pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                ftPay.setFormNumberBB();
            } else if (position == 4) {
                ProSign ftSign = (ProSign) pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                ftSign.setFormNumber();
            } else if (position == 5) {
                ProStatus frag1 = (ProStatus) pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
                frag1.changeStatus();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    private ViewPager.SimpleOnPageChangeListener viewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            //setActionBarTitle(position);
            setTitle(getResources().getString(R.string.dtd));
        }
    };

    private FragmentPagerAdapter buildAdapter() {
        adapterPage = new ProTabPagerAdapter(FormProDTD.this, getSupportFragmentManager());
        return adapterPage;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onBackPressed() {
        set_exit();
    }

    private void set_exit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(FormProDTD.this);
        builder.setMessage("Anda yakin akan keluar dari form?");
        builder.setTitle("EXIT");
        builder.setPositiveButton(getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SharedPreferences sm = getSharedPreferences(
                                "formId", Context.MODE_PRIVATE);
                        String numberForm = sm.getString("fn", null);

                        if (numberForm != null) {
                            TableFormAppAdapter adapter = new TableFormAppAdapter(FormProDTD.this);
                            adapter.deleteBy(FormProDTD.this, TableFormApp.fFORM_NO, numberForm);
                        }

                        SharedPreferences sessionPref = getSharedPreferences("formId", Context.MODE_PRIVATE);
                        SharedPreferences.Editor shareEditor = sessionPref.edit();
                        shareEditor.remove("fn");
                        shareEditor.commit();
                        finish();

                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                set_exit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * Method for generate FTID
     */
    private String generateFtID() {
        String yy, mm, dd, hh, ss;
        listLogLogin = new ArrayList<>();
        listLogLogin = mLogLoginAdapter.getAllData();
        if (listLogLogin.size() > 0) {
            employeeId = listLogLogin.get(listLogLogin.size() - 1).getcEmployeeId();
        } else {
            employeeId = "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        StringBuilder sb = new StringBuilder();
        sb.append("F");
        sb.append(employeeId);
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.toString();
        SharedPreferences sessionPref = getSharedPreferences("ftId", Context.MODE_PRIVATE);
        SharedPreferences.Editor shareEditor = sessionPref.edit();
        shareEditor.putString("ft", sb.toString());
        shareEditor.commit();
        return sb.toString();
    }

    /**
     * Method for generate DTDID
     */
    private String generateDtdID() {
        String yy, mm, dd, hh, ss;
        listLogLogin = new ArrayList<>();
        listLogLogin = mLogLoginAdapter.getAllData();
        if (listLogLogin.size() > 0) {
            employeeId = listLogLogin.get(listLogLogin.size() - 1).getcEmployeeId();
        } else {
            employeeId = "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        StringBuilder sb = new StringBuilder();
        sb.append("P");
        sb.append(employeeId);
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.toString();
        SharedPreferences sessionPref = getSharedPreferences("dtdId", Context.MODE_PRIVATE);
        SharedPreferences.Editor shareEditor = sessionPref.edit();
        shareEditor.putString("dtd", sb.toString());
        shareEditor.commit();
        return sb.toString();
    }

    /**
     * Method for generate DTDID
     */
    private String generateDtdPPID() {
        String yy, mm, dd, hh, ss;
        listLogLogin = new ArrayList<>();
        listLogLogin = mLogLoginAdapter.getAllData();
        if (listLogLogin.size() > 0) {
            employeeId = listLogLogin.get(listLogLogin.size() - 1).getcEmployeeId();
        } else {
            employeeId = "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        StringBuilder sb = new StringBuilder();
        sb.append("T");
        sb.append(employeeId);
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.toString();
        SharedPreferences sessionPref = getSharedPreferences("dtdPPId", Context.MODE_PRIVATE);
        SharedPreferences.Editor shareEditor = sessionPref.edit();
        shareEditor.putString("dtdpp", sb.toString());
        shareEditor.commit();
        return sb.toString();
    }

    public void attachedImage() {
        isExistKtp = false;
        isExistLing = false;
        isExistInstall = false;
        isExistRmh = false;
        isExistSign = false;
        isExistBB = false;
        isExistPln = false;
    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ProProfile();
                case 1:
                    return new ProEmergency();
                case 2:
                    return new ProPaket();
                case 3:
                    return new DTDPayment();
                case 4:
                    return new ProSign();
                case 5:
                    return new ProStatus();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 6;
        }


        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Profile";
                case 1:
                    return "Emergency";
                case 2:
                    return "Package";
                case 3:
                    return "1st Payment";
                case 4:
                    return "Signature";
                case 5:
                    return "Status";

            }
            return null;
        }
    }
}
