package myskysfa.com.sfa.main.menu.dtdproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.main.menu.ms.ShowListDropDown;
import myskysfa.com.sfa.utils.Utils;
import myskysfa.com.sfa.utils.listItemObject.EmergencyListItem;

/**
 * Created by Eno.
 */
public class ProEmergency extends Fragment {

    private ViewGroup root;
    private static Context _context;
    private Utils utils;
    private TextView formNo, relationship;
    private EditText firstName, middleName, lastName, telephone, address;
    private Button save;
    private EmergencyListItem item;
    private TableFormAppAdapter mFormAppAdapter;
    private ArrayList<String> relationArrayList, idRelationArrayList;
    private String _formNo, _relationship, _firstName, _middleName, _lastName,
            _telephone, _address, value, numberForm, _idRelation = "", emergency;
    private int dataCount;
    private ShowListDropDown showList;

    public static Fragment newInstance(Context context) {
        _context = context;
        ProEmergency ftEmergency = new ProEmergency();
        return ftEmergency;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.ft_emergency, container, false);

        try {
            utils = new Utils(getActivity());
            initView(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root;
    }

    /**
     * Initial view
     */
    private void initView(ViewGroup root) {
        formNo = (TextView) root.findViewById(R.id.form_no);
        relationship = (TextView) root.findViewById(R.id.relationship);
        firstName = (EditText) root.findViewById(R.id.first_name);
        middleName = (EditText) root.findViewById(R.id.middle_name);
        lastName = (EditText) root.findViewById(R.id.last_name);
        telephone = (EditText) root.findViewById(R.id.tlp);
        address = (EditText) root.findViewById(R.id.address);
        save = (Button) root.findViewById(R.id.save);

        /**
         * get Form Number from Profile
         */
        if (getActivity().getIntent().getStringExtra("ftId")!=null
                && getActivity().getIntent().getStringExtra("ftId").length()>1) {
            numberForm = getActivity().getIntent().getStringExtra("ftId");
        } else {
            SharedPreferences sm = getActivity().getSharedPreferences(
                    "formId", Context.MODE_PRIVATE);
            numberForm = sm.getString("fn", null);
        }
        formNo.setText(numberForm);
        //getDataFromDB();
        relationship.setOnClickListener(relationshipListener);
        save.setOnClickListener(saveListener);
        if (emergency != null) {
            try {
                JSONObject refObj = new JSONObject(emergency);
                firstName.setText(refObj.getString("eFirstName"));
                middleName.setText(refObj.getString("eMiddleName"));
                lastName.setText(refObj.getString("eLastName"));
                telephone.setText(refObj.getString("eTelephone"));
                address.setText(refObj.getString("eAddress"));
                relationship.setText(relationArrayList.get(Integer.parseInt(refObj.getString("eRelationship"))).toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Listener for Button
     */
    View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                checkMandatoryForm();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Listener for spinner view
     */
    View.OnClickListener relationshipListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showList = new ShowListDropDown(getActivity(), view, "Pilih hubungan", relationship,
                    getResources().getStringArray(R.array.family_relationship));
            showList.Show();
        }
    };

    public void setFormNumber() {
        SharedPreferences sm = getActivity().getSharedPreferences(
                "formId", Context.MODE_PRIVATE);
        numberForm = sm.getString("fn", null);
    }

    private void checkMandatoryForm() {

        _formNo = numberForm;
        _firstName = firstName.getText().toString();
        _middleName = middleName.getText().toString();
        _lastName = lastName.getText().toString();
        _telephone = telephone.getText().toString();
        _address = address.getText().toString();
        _relationship = showList.getRelationship();
        if (_firstName == null || _firstName.equalsIgnoreCase("") || _firstName.length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukan nama.", Toast.LENGTH_SHORT).show();
            return;
        } else if (_telephone == null || _telephone.equalsIgnoreCase("") || _telephone.length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukan nomor telephone.", Toast.LENGTH_SHORT).show();
            return;
        } else if (_address == null || _address.equalsIgnoreCase("") || _address.length() < 0) {
            Toast.makeText(getActivity(), "Silahkan masukan alamat.", Toast.LENGTH_SHORT).show();
            return;
        } else if (_relationship == null || _relationship.equalsIgnoreCase("") || _relationship.length() < 0) {
            Toast.makeText(getActivity(), "Silahkan pilih hubungan.", Toast.LENGTH_SHORT).show();
            return;
        } else {
            try {
                if (numberForm != null) {
                    mFormAppAdapter = new TableFormAppAdapter(getActivity());
                    Long count = mFormAppAdapter.getDataCount(TableFormApp.fFORM_NO, numberForm);
                    if (count != null) {
                        dataCount = (int) (long) count;
                    } else {
                        dataCount = 0;
                    }
                } else {
                    utils.showAlertDialog(getActivity(), "Gagal", "Silahkan isi data profile terlebih dahulu.", false);
                    return;
                }

                if (dataCount == 0) {
                    utils.showAlertDialog(getActivity(), "Gagal", "Silahkan isi data profile terlebih dahulu.", false);
                } else {
                    mFormAppAdapter.updatePartial(getActivity(), TableFormApp.fVALUES_EMERGENCY, jsonValueCreator(),
                            TableFormApp.fFORM_NO, numberForm);
                    Toast.makeText(getActivity(), "Emergency Saved", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method for create json from form value
     *
     * @return
     * @throws Exception
     */
    private String jsonValueCreator() throws Exception {
        item = new EmergencyListItem(getActivity());
        item.setFormNo(_formNo);
        item.setFirstName(_firstName);
        item.setMiddleName(_middleName);
        item.setLastName(_lastName);
        item.setTelephone(_telephone);
        item.setAddress(_address);
        item.setRelationship(_relationship);
        JSONObject obj = new JSONObject();
        try {
            obj.put("eFormNo", item.getFormNo());
            obj.put("eFirstName", item.getFirstName());
            obj.put("eMiddleName", item.getMiddleName());
            obj.put("eLastName", item.getLastName());
            obj.put("eRelationship", item.getRelationship());
            obj.put("eTelephone", item.getTelephone());
            obj.put("eAddress", item.getAddress());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj.toString();
    }
}
