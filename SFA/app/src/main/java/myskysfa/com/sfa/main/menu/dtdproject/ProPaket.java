package myskysfa.com.sfa.main.menu.dtdproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.MSAlacarteListAdapter;
import myskysfa.com.sfa.adapter.SpinnerBrandAdapter;
import myskysfa.com.sfa.adapter.SpinnerBundlingAdapter;
import myskysfa.com.sfa.adapter.SpinnerHWAdapter;
import myskysfa.com.sfa.adapter.SpinnerMaterialAdapter;
import myskysfa.com.sfa.adapter.SpinnerPackAdapter;
import myskysfa.com.sfa.adapter.SpinnerPromoAdapter;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableFreeTrial;
import myskysfa.com.sfa.database.TableFreeTrialCoh;
import myskysfa.com.sfa.database.TableMBrand;
import myskysfa.com.sfa.database.TableMasterPackage;
import myskysfa.com.sfa.database.TableMasterPromo;
import myskysfa.com.sfa.database.TablePlan;
import myskysfa.com.sfa.database.db_adapter.TableFTSOHAdapter;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.database.db_adapter.TableFreeTrialAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMBrandAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMasterPackageAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMasterPromoAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePlanAdapter;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by Hari Hendryan on 12/17/2015.
 */
public class ProPaket extends Fragment {

    private static Context _context;
    private ViewGroup root;
    private Utils utils;
    private String resultVC = "";
    private int totPakacge = 0, isMulty, totAdditional = 0, hargaAdditional;
    private static int hrgBasic, hrgAlacarte;
    List<TableFreeTrial> listFreetrial = new ArrayList<TableFreeTrial>();
    TablePlanAdapter tablePlanAdapter;
    TableFreeTrialAdapter dbAdapter;
    List<TablePlan> listPlan = new ArrayList<TablePlan>();

    private ArrayList<String> listProductPackage, listPromo, listBrand, listVc, listDsd, listLnb,
            listDish, listMaterial, listSimCard, listRouter, listLnbMulty;

    private List<TableFreeTrialCoh> listSOH;
    private TableMasterPackageAdapter mTablePackageProductAdapter;
    private TableMasterPromoAdapter mPromoAdapter;
    private TableMBrandAdapter mBrandAdapter;
    private TableFTSOHAdapter mMaterialAdapter;
    private List<TableMasterPackage> mPackageProductListItemList;
    private List<TableMasterPromo> mPromoListItems;
    private List<TableMBrand> mBrandListItem;
    private List<TableFreeTrialCoh> mVcItem, mDsdItem, mLnbItem, mDishItem;
    private ArrayAdapter<String> spinnerProductPackage, spinnerPromo, spinnerBrand, spinnerVc,
            spinnerDsd, spinnerLnb, spinnerDish, spinnerLnbMulty;
    private Spinner spinPackage, spinBrand, spinVc, spinLnb, spinDsd, spinDish, spinType,
            materialAdd;
    private SearchableSpinner spinPromo;
    private SpinnerPackAdapter spinAdapter;
    private SpinnerHWAdapter spinAdapterVC, spinAdapterLNB, spinAdapterDSD, spinAdapterODU,
            spinAdapterType, spinAdapterLNBMulty;
    private SpinnerPromoAdapter spinPromoAdapter;
    private SpinnerBrandAdapter spinBrandAdapter;
    private SpinnerBundlingAdapter spinnerBundlingAdapter;
    private SpinnerMaterialAdapter spinnerMaterialAdapter;
    private PopupWindow mpopup;
    private RecyclerView recyclerView;
    private Button btnSave;
    private LinearLayout mLayoutPackage, mLayoutAdd, mLayoutAdditional, packageBundling, LinearBundling;
    private MSAlacarteListAdapter mPackageProductAdapter;
    private List<TableMasterPackage> mPackageProductListItem;
    private ArrayList<String> selAlacarte = new ArrayList<String>();
    private List<TableFreeTrialCoh> mMaterialListItems = new ArrayList<>();
    private List<TableFreeTrialCoh> mMaterialListBundling = new ArrayList<>();
    private ArrayList<ArrayList<String>> selAlacarteSaveAll = new ArrayList<>();
    private String saveBrand = "", saveStatus = "", savePromo, saveBiayaLain, saveKetLain;
    private RadioGroup radioStatus;
    private RadioButton selRadio, rentRadioButton, selRadioButton;
    //private ImageButton ibVc, ibLnb, ibDsd, ibDish;
    private static boolean isVc, isLnb, isDsd, isDish;
    private TextView total, instalasi, totalAwal;
    private String numberForm, formattednumber, str, paket, promoCode, priceMaterial = "", strPaket,
            productId = "", simcardId = "", routerId = "", basic_id, valueSim, valueRouter;
    private TableFormAppAdapter mFormAppAdapter;
    private Double duit;
    private NumberFormat defaultF;
    private List<TableFreeTrialCoh> mListMaster;
    private Handler handler;
    private List<TableFormApp> listFormApp;
    private TableFTSOHAdapter dbCohAdapter;
    private int target, index, count;
    private ImageView additional, delete_additional, btnBundling;
    private EditText countAdd;
    private Spinner spinSimCard, spinRouter, bundling;
    private Boolean isShowBundling = false, isShowAdditional = false;

    public static Fragment newInstance(Context context) {
        _context = context;
        ProPaket proPaket = new ProPaket();
        return proPaket;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.pro_paket, container, false);
        try {
            utils = new Utils(getActivity());
            handler = new Handler();
            // totPakacge = 0;

            /**
             * get Form Number from Profile
             */
            /*if (getActivity().getIntent().getStringExtra("ftId") != null
                    || getActivity().getIntent().getStringExtra("ftId").length() > 1) {
                numberForm = getActivity().getIntent().getStringExtra("ftId");
            } else {
                SharedPreferences sm = getActivity().getSharedPreferences(
                        "formId", Context.MODE_PRIVATE);
                numberForm = sm.getString("fn", null);
            }*/
            //initView(root);
        } catch (Exception e) {
            e.printStackTrace();
            //addPackage();
        }

        return root;
    }

    private void initView(ViewGroup mView) {
        ImageView btnAdd = (ImageView) mView.findViewById(R.id.btn_add_package);
        btnAdd.setClickable(true);
        Button btnSave = (Button) mView.findViewById(R.id.save);
        btnSave.setOnClickListener(actSaveAll);

        radioStatus = (RadioGroup) mView.findViewById(R.id.radio_status);
        rentRadioButton = (RadioButton) mView.findViewById(R.id.radio_rent);
        selRadioButton = (RadioButton) mView.findViewById(R.id.radio_sale);
        //ibVc = (ImageButton) mView.findViewById(R.id.ib_vc);
        //ibDsd = (ImageButton) mView.findViewById(R.id.ib_dsd);
        //ibLnb = (ImageButton) mView.findViewById(R.id.ib_lnb);
        //ibDish = (ImageButton) mView.findViewById(R.id.ib_dish);
        total = (TextView) mView.findViewById(R.id.priceInstalasi);
        totalAwal = (TextView) mView.findViewById(R.id.total1);
        instalasi = (TextView) mView.findViewById(R.id.instalasi);
        mLayoutAdd = (LinearLayout) mView.findViewById(R.id.linier_package);
        additional = (ImageView) mView.findViewById(R.id.btn_add_additional);
        delete_additional = (ImageView) mView.findViewById(R.id.btn_min_additional);
        btnBundling = (ImageView) mView.findViewById(R.id.btn_add_bundling);
        packageBundling = (LinearLayout) mView.findViewById(R.id.bundling);
        LinearBundling = (LinearLayout) mView.findViewById(R.id.itemBundling);
        spinSimCard = (Spinner) mView.findViewById(R.id.sValue);
        spinRouter = (Spinner) mView.findViewById(R.id.rValue);
        bundling = (Spinner) mView.findViewById(R.id.spinner_bundling);
        bundling.setOnItemSelectedListener(bundlingListener);
        spinSimCard.setOnItemSelectedListener(simListener);
        spinRouter.setOnItemSelectedListener(routerListener);
        spinSimCard.setOnTouchListener(simClick);
        spinRouter.setOnTouchListener(routerClick);
        rentRadioButton.setChecked(true);

        // generate basic list
        dbCohAdapter = new TableFTSOHAdapter(this.getContext());
        mFormAppAdapter = new TableFormAppAdapter(getActivity());
        listSOH = new ArrayList<TableFreeTrialCoh>();
        mTablePackageProductAdapter = new TableMasterPackageAdapter(getActivity());
        mPromoAdapter = new TableMasterPromoAdapter(getActivity());
        mBrandAdapter = new TableMBrandAdapter(getActivity());
        mMaterialAdapter = new TableFTSOHAdapter(getActivity());
        mBrandListItem = mBrandAdapter.getAllData();
        mPromoListItems = mPromoAdapter.getAllData();
        mMaterialListItems = mMaterialAdapter.getDatabyCondition(TableFreeTrialCoh.KEY_IS_SERIALIZE, "N");
        listFormApp = mFormAppAdapter.getDatabyCondition(TableFormApp.fFORM_NO, numberForm);
        //listSOH = dbCohAdapter.getDatabyCondition(TableFreeTrialCoh.KEY_HW_STATUS, "ADD");
        //listSOH = dbCohAdapter.getDatabyCondition(TableFreeTrialCoh.KEY_HW_STATUS, "1");

        listProductPackage = new ArrayList<String>();
        listPromo = new ArrayList<String>();
        listBrand = new ArrayList<String>();
        listMaterial = new ArrayList<String>();
        listSimCard = new ArrayList<String>();
        listRouter = new ArrayList<String>();


        for (int i = 0; i < mPromoListItems.size(); i++) {
            TableMasterPromo item = mPromoListItems.get(i);
            listPromo.add(item.getPromotion_code());
        }

        for (int i = 0; i < mBrandListItem.size(); i++) {
            TableMBrand item = mBrandListItem.get(i);
            listBrand.add(item.getBrand_name());
        }

        for (int i = 0; i < listFormApp.size(); i++) {
            TableFormApp item = listFormApp.get(i);
            if (item != null) {
                paket = item.getVALUES_PACKAGE();
            }
        }

        for (int i = 0; i < mMaterialListItems.size(); i++) {
            TableFreeTrialCoh item = mMaterialListItems.get(i);
            listMaterial.add(item.getName());
        }

        //spinnerPromo = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, listPromo);
        //spinnerBrand = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, listBrand);

        //spinPromoAdapter = new SpinnerPromoAdapter(getActivity(), R.layout.status_spinner, mPromoListItems);
        //spinPromoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinBrandAdapter = new SpinnerBrandAdapter(getActivity(), R.layout.status_spinner, mBrandListItem);
        spinBrandAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinPromoAdapter = new SpinnerPromoAdapter(getActivity(), R.layout.status_spinner, listPromo);
        spinPromoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerMaterialAdapter = new SpinnerMaterialAdapter(getActivity(), R.layout.status_spinner, mMaterialListItems);
        spinnerMaterialAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayList<String> typeList = new ArrayList<String>();
        typeList.add("Single");
        typeList.add("Multi");

        spinAdapterType = new SpinnerHWAdapter(getContext(), R.layout.status_spinner, typeList);
        spinAdapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //spinPromo = (Spinner) mView.findViewById(R.id.spinner_promo);
        //spinPromo.setAdapter(spinPromoAdapter);
        //spinPromo.setOnItemSelectedListener(spinPromoListener);

        spinType = (Spinner) mView.findViewById(R.id.spinner_type);
        spinType.setAdapter(spinAdapterType);
        spinType.setSelection(0);
        spinType.setOnItemSelectedListener(spinerTypeListener);

        spinBrand = (Spinner) mView.findViewById(R.id.spinner_brand);
        spinBrand.setAdapter(spinBrandAdapter);
        spinBrand.setSelection(2);
        spinBrand.setOnItemSelectedListener(spinBrandListener);

        spinPromo = (SearchableSpinner) mView.findViewById(R.id.spinner_promo);
        spinPromo.setAdapter(spinPromoAdapter);
        spinPromo.setSelection(0);
        spinPromo.setTitle("Select Item");
        spinPromo.setOnItemSelectedListener(spinerPromoListener);

        //ibVc.setOnClickListener(vcListener);
        //ibLnb.setOnClickListener(lnbListener);
        //ibDsd.setOnClickListener(dsdListener);
        //ibDish.setOnClickListener(dishListener);
        additional.setOnClickListener(addAdditionalItems);
        delete_additional.setOnClickListener(deleteAdditional);
        btnAdd.setOnClickListener(actAddPackage);
        btnBundling.setOnClickListener(addBundling);

        total.addTextChangedListener(totalWatcher);
        instalasi.addTextChangedListener(instalWatcher);
        spinHw(mView);

        if (paket != null) {
            try {
                JSONObject paketObj = new JSONObject(paket);
                String vc = paketObj.getString("VC");
                String odu = paketObj.getString("ODU");
                String lnb = paketObj.getString("LNB");
                String dsd = paketObj.getString("DSD");
                if (vc != null)
                    setSelectHw("VC", vc);
                if (odu != null)
                    setSelectHw("ANT", odu);
                if (lnb != null)
                    setSelectHw("LNB", lnb);
                if (dsd != null)
                    setSelectHw("DEC", dsd);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void spinHw(ViewGroup mView) {
        TableFTSOHAdapter dbCohAdapter = new TableFTSOHAdapter(this.getContext());
        //listSOH = dbCohAdapter.getDatabyCondition(TableFreeTrialCoh.KEY_HW_STATUS, "ADD");
        listSOH = dbCohAdapter.getDatabyConditionMultyCondition(
                TableFreeTrialCoh.KEY_HW_STATUS, "1", TableFreeTrialCoh.KEY_IS_SERIALIZE, "Y");
        listVc = new ArrayList<String>();
        listDsd = new ArrayList<String>();
        listLnb = new ArrayList<String>();
        //listLnbMulty = new ArrayList<String>();
        listDish = new ArrayList<String>();
        spinVc = (Spinner) mView.findViewById(R.id.spinner_vc);
        spinDsd = (Spinner) mView.findViewById(R.id.spinner_dsd);
        spinDish = (Spinner) mView.findViewById(R.id.spinner_dish);
        spinLnb = (Spinner) mView.findViewById(R.id.spinner_lnb);
        //int iVc = 1, iLnb = 1, iDsd = 1, iDish = 1;
        listVc.add("");
        listDsd.add("");
        listLnb.add("");
        //listLnbMulty.add("");
        listDish.add("");
        for (int i = 0; i < listSOH.size(); i++) {
            TableFreeTrialCoh item = listSOH.get(i);
            if (item.getType().equalsIgnoreCase("VC")) {
                listVc.add(item.getSn());
                //iVc++;
            }
            if (item.getType().equalsIgnoreCase("DEC")) {
                listDsd.add(item.getSn());
                //iDsd++;
            }
            if (item.getType().equalsIgnoreCase("LNB")) {
                if (isMulty == 0) {
                    //listLnbMulty.clear();
                    listLnb.add(item.getSn());
                } else {
                    listLnb.clear();
                    if (item.getHw_id().equals("645")) {
                        listLnb.add(item.getSn());
                    }
                }

                //iLnb++;
            }
            if (item.getType().equalsIgnoreCase("ANT")) {
                listDish.add(item.getSn());
                //iDish++;
            }
        }

        spinnerVc = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listVc);
        spinnerDsd = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listDsd);
        spinnerDish = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listDish);
        /*spinnerLnb = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listLnb);
        spinnerLnbMulty = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listLnbMulty);*/


        // Drop down layout style - list view with radio button
        spinnerVc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDsd.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDish.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        /*spinnerLnb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLnbMulty.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/

        // attaching data adapter to spinner
        //spinVc.setAdapter(spinnerVc);
        //spinDsd.setAdapter(spinnerDsd);
        //spinDish.setAdapter(spinnerDish);
        /*if (isMulty == 0) {
            spinLnb.setAdapter(spinnerLnb);
        } else {
            spinLnb.setAdapter(spinnerLnbMulty);
        }*/

    }

    public void setFormNumber() {
        SharedPreferences sm = getActivity().getSharedPreferences(
                "formId", Context.MODE_PRIVATE);
        numberForm = sm.getString("fn", null);
        if (numberForm == null) {
            numberForm = "";
            Toast.makeText(getActivity(), "harap isi profile", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (numberForm == null) {
            numberForm = "";
            //Toast.makeText(getActivity(), "harap isi profile", Toast.LENGTH_SHORT).show();
        }
        initView(root);

    }

    private TextWatcher totalWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String a, b, totB, totA;
            a = total.getText().toString();
            b = instalasi.getText().toString();
            totB = b.replace("Rp.", "");
            totB = totB.replace(".", "");
            totA = a.replace("Rp.", "");
            totA = totA.replace(".", "");
            duit = Double.parseDouble(String.valueOf(Integer.parseInt(totA) + Integer.parseInt(totB)));
            defaultF = NumberFormat.getInstance();
            formattednumber = defaultF.format(duit);
            str = formattednumber.replace(",", ".");
            //totalAwal.setText("Rp."+str);
            totalAwal.setText("Rp.0");
        }
    };

    private TextWatcher instalWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            totalAwal.setText("Rp.0");
            total.setText("Rp.0");
        }
    };

    private View.OnClickListener vcListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isVc = true;
            isLnb = false;
            isDish = false;
            isDsd = false;
            IntentIntegrator integrator = new IntentIntegrator(getActivity());
            integrator.initiateScan();
        }
    };

    private View.OnClickListener lnbListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isVc = false;
            isLnb = true;
            isDish = false;
            isDsd = false;
            IntentIntegrator integrator = new IntentIntegrator(getActivity());
            integrator.initiateScan();
        }
    };

    private View.OnClickListener dsdListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isVc = false;
            isLnb = false;
            isDish = false;
            isDsd = true;
            IntentIntegrator integrator = new IntentIntegrator(getActivity());
            integrator.initiateScan();
        }
    };

    private View.OnClickListener dishListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isVc = false;
            isLnb = false;
            isDish = true;
            isDsd = false;
            IntentIntegrator integrator = new IntentIntegrator(getActivity());
            integrator.initiateScan();
        }
    };

    private View.OnClickListener actAddPackage = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (target > 2 && totPakacge == 3) {
                Toast.makeText(getActivity(), "Paket tidak boleh lebih dari 3", Toast.LENGTH_SHORT).show();
            } else if (target == 2 && totPakacge == 2) {
                Toast.makeText(getActivity(), "Paket tidak boleh lebih dari 2", Toast.LENGTH_SHORT).show();
            } else {
                /*View childView = mLayoutPackage.getChildAt(1); // Start From 0
                Spinner packageSpinner = (Spinner) childView.findViewWithTag("VC_2"); // Start from 1
                if (packageSpinner.getSelectedItem().toString().equalsIgnoreCase("")
                        || packageSpinner.getSelectedItem() == null) {
                    Toast.makeText(getActivity(), "harap isi multi ke_2", Toast.LENGTH_SHORT).show();
                } else {

                }*/
                addPackage();

            }

        }
    };

    private AdapterView.OnItemSelectedListener bundlingListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (bundling.getSelectedItem().toString().equalsIgnoreCase("xl") ||
                    position == 0) {
                listSimCard.clear();
                listRouter.clear();
                spinSimCard.setAdapter(null);
                spinRouter.setAdapter(null);
                simcardId = "882";
                routerId = "881";
            } else if (bundling.getSelectedItem().toString().equalsIgnoreCase("indosat") ||
                    position == 1) {
                listSimCard.clear();
                listRouter.clear();
                spinSimCard.setAdapter(null);
                spinRouter.setAdapter(null);
                simcardId = "888";
                routerId = "887";
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private View.OnTouchListener simClick = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            mMaterialListBundling = mMaterialAdapter.getDatabyCondition(TableFreeTrialCoh.KEY_HW_ID,
                    simcardId);

            for (int i = 0; i < mMaterialListBundling.size(); i++) {
                TableFreeTrialCoh item = mMaterialListBundling.get(i);
                listSimCard.add(item.getSn());
            }

            spinnerBundlingAdapter = new SpinnerBundlingAdapter(getActivity(),
                    R.layout.status_spinner, mMaterialListBundling);
            spinnerBundlingAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinSimCard.setAdapter(spinnerBundlingAdapter);
            return false;
        }
    };

    private View.OnTouchListener routerClick = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            mMaterialListBundling = mMaterialAdapter.getDatabyCondition(TableFreeTrialCoh.KEY_HW_ID,
                    routerId);
            for (int i = 0; i < mMaterialListBundling.size(); i++) {
                TableFreeTrialCoh item = mMaterialListBundling.get(i);
                listRouter.add(item.getSn());
            }

            spinnerBundlingAdapter = new SpinnerBundlingAdapter(getActivity(),
                    R.layout.status_spinner, mMaterialListBundling);
            spinnerBundlingAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinRouter.setAdapter(spinnerBundlingAdapter);
            return false;
        }
    };

    private AdapterView.OnItemSelectedListener simListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private AdapterView.OnItemSelectedListener routerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private View.OnClickListener addBundling = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isShowBundling) {
                packageBundling.setVisibility(View.VISIBLE);
                isShowBundling = false;
                LinearBundling.setVisibility(View.VISIBLE);
                btnBundling.setImageResource(R.drawable.ic_cancel_bundling);

                //default value
                simcardId = "882";
                routerId = "881";
            } else {
                packageBundling.setVisibility(View.GONE);
                isShowBundling = true;
                LinearBundling.setVisibility(View.GONE);
                btnBundling.setImageResource(R.drawable.ic_plus);
                simcardId = "";
                routerId = "";
                valueSim = "";
                valueRouter = "";
                spinSimCard.setAdapter(null);
                spinRouter.setAdapter(null);
            }

        }
    };

    private View.OnClickListener addAdditionalItems = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AdditionalPackage();
            delete_additional.setVisibility(View.VISIBLE);
            isShowAdditional = true;
        }
    };

    private View.OnClickListener deleteAdditional = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isShowAdditional = false;
            hargaAdditional = 0;
            defaultF = NumberFormat.getInstance();
            formattednumber = defaultF.format(hrgBasic + hargaAdditional);
            strPaket = formattednumber.replace(",", ".");
            //pricePackage.setText(strPaket);
            totAdditional = 0;
            mLayoutAdditional.removeAllViews();
            delete_additional.setVisibility(View.GONE);

            /*for (int i = 0; i < mLayoutAdditional.getChildCount(); i++) {
                View childView = mLayoutAdditional.getChildAt(i);
                int b = mLayoutAdditional.indexOfChild(childView);
                materialAdd = (Spinner) childView.findViewWithTag("add_" + (b + 1));
                priceMaterial = mMaterialListItems.get(materialAdd.getSelectedItemPosition()).getRate();
                hargaAdditional = hargaAdditional + Integer.parseInt(priceMaterial);
                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(hrgBasic - hargaAdditional);
                strPaket = formattednumber.replace(",", ".");
                pricePackage.setText(strPaket);
                //mLayoutAdditional.removeView(mLayoutAdditional.findViewWithTag("ADDITIONAL_" + (b)));
            }*/

        }
    };

    private void addPackage() {
        totPakacge = totPakacge + 1;
        mLayoutPackage = (LinearLayout) root.findViewById(R.id.package_list);

        LinearLayout linearPackage = new LinearLayout(getContext());
        linearPackage.setBackgroundColor(getResources().getColor(R.color.white));
        linearPackage.setOrientation(LinearLayout.VERTICAL);
        linearPackage.setTag("PACK_" + totPakacge);

        LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLinearPackage.setMargins(0, 10, 0, 0);
        linearPackage.setLayoutParams(pLinearPackage);
        mLayoutPackage.addView(linearPackage);

        LinearLayout.LayoutParams pLineTitlePack = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout lineTitlePack = new LinearLayout(getContext());
        lineTitlePack.setBackgroundColor(getResources().getColor(R.color.white));
        lineTitlePack.setTag("TITLE_" + totPakacge);
        lineTitlePack.setOrientation(LinearLayout.HORIZONTAL);
        lineTitlePack.setLayoutParams(pLineTitlePack);
        linearPackage.addView(lineTitlePack);

        LinearLayout.LayoutParams pTxtPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtPack = new TextView(getContext());
        txtPack.setLayoutParams(pTxtPackage);
        txtPack.setText("Package " + totPakacge);
        txtPack.setPadding(5, 5, 5, 5);
        txtPack.setTypeface(Typeface.DEFAULT_BOLD);
        lineTitlePack.addView(txtPack);

        LinearLayout.LayoutParams pLineView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 0.5f);
        View lineView = new View(getContext());
        lineView.setLayoutParams(pLineView);
        lineView.setBackgroundColor(getResources().getColor(R.color.black_translucent));
        linearPackage.addView(lineView);

        LinearLayout.LayoutParams pLineForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearForm = new LinearLayout(getContext());
        linearForm.setLayoutParams(pLineForm);
        linearForm.setOrientation(LinearLayout.HORIZONTAL);
        linearForm.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearForm);

        LinearLayout.LayoutParams pTxtBasic = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtBasic = new TextView(getContext());
        txtBasic.setLayoutParams(pTxtBasic);
        txtBasic.setText("Basic");
        txtBasic.setPadding(5, 0, 0, 0);
        linearForm.addView(txtBasic);

        LinearLayout.LayoutParams pTxtBasicForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtBasicForm = new Spinner(getContext());
        txtBasicForm.setLayoutParams(pTxtBasicForm);
        txtBasicForm.setPadding(0, 16, 0, 16);
        txtBasicForm.setTag("BASIC_" + totPakacge);
        genBasic("BASIC_" + totPakacge, mLayoutPackage);
        linearForm.addView(txtBasicForm);

        LinearLayout.LayoutParams pLineAlacarte = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearAlacarte = new LinearLayout(getContext());
        linearAlacarte.setLayoutParams(pLineAlacarte);
        linearAlacarte.setOrientation(LinearLayout.HORIZONTAL);
        linearAlacarte.setPadding(16, 10, 16, 10);
        linearPackage.addView(linearAlacarte);

        LinearLayout.LayoutParams pTxtAlacarte = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtAlacarte = new TextView(getContext());
        txtAlacarte.setLayoutParams(pTxtAlacarte);
        txtAlacarte.setText("Alacarte");
        txtAlacarte.setGravity(TextView.TEXT_ALIGNMENT_CENTER);
        linearAlacarte.addView(txtAlacarte);

        LinearLayout.LayoutParams pListAlacarte = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        pListAlacarte.setMargins(0, 10, 0, 0);
        LinearLayout linearListAlacarte = new LinearLayout(getContext());
        linearListAlacarte.setLayoutParams(pListAlacarte);
        linearListAlacarte.setOrientation(LinearLayout.VERTICAL);
        linearListAlacarte.setTag("LIST_" + totPakacge);
        linearAlacarte.addView(linearListAlacarte);

        LinearLayout.LayoutParams pContentAla = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pContentAla.setMargins(5, 5, 0, 0);
        TextView txtAdd = new TextView(getContext());
        txtAdd.setText("+ Add Alacarte");
        txtAdd.setTag("ADD_" + totPakacge);
        txtAdd.setPadding(10, 10, 10, 10);
        txtAdd.setClickable(true);
        txtAdd.setLayoutParams(pContentAla);
        txtAdd.setTextColor(getResources().getColor(R.color.white));
        txtAdd.setBackgroundResource(R.color.blue_10);
        txtAdd.setOnClickListener(addAlacarteListener);
        linearListAlacarte.addView(txtAdd);

        addListAlacarte();

        LinearLayout.LayoutParams pLineTitleHW = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineTitleHW.setMargins(0, 0, 0, 10);
        LinearLayout lineTitleHW = new LinearLayout(getContext());
        lineTitleHW.setBackgroundColor(getResources().getColor(R.color.white));
        lineTitleHW.setTag("TITLE_" + totPakacge);
        lineTitleHW.setOrientation(LinearLayout.HORIZONTAL);
        lineTitleHW.setLayoutParams(pLineTitleHW);
        linearPackage.addView(lineTitleHW);

        LinearLayout.LayoutParams pTxtHW = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtHW = new TextView(getContext());
        txtHW.setLayoutParams(pTxtHW);
        txtHW.setText("Hardware");
        txtHW.setPadding(5, 5, 5, 5);
        txtHW.setTypeface(Typeface.DEFAULT_BOLD);
        lineTitleHW.addView(txtHW);

        LinearLayout.LayoutParams pLineFormVC = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineFormVC.setMargins(0, 0, 0, 20);
        LinearLayout linearFormVC = new LinearLayout(getContext());
        linearFormVC.setLayoutParams(pLineFormVC);
        linearFormVC.setOrientation(LinearLayout.HORIZONTAL);
        linearFormVC.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormVC);

        LinearLayout.LayoutParams pTxtVC = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtVC = new TextView(getContext());
        txtVC.setLayoutParams(pTxtVC);
        txtVC.setText("VC");
        txtVC.setPadding(5, 0, 0, 0);
        linearFormVC.addView(txtVC);

        LinearLayout.LayoutParams pTxtVCForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtVCForm = new Spinner(getContext());
        txtVCForm.setLayoutParams(pTxtVCForm);
        txtVCForm.setPadding(0, 16, 0, 16);
        txtVCForm.setTag("VC_" + totPakacge);
        //genBasic("VC_" + totPakacge, mLayoutPackage);
        genVC("VC_" + totPakacge, mLayoutPackage);
        linearFormVC.addView(txtVCForm);

        LinearLayout.LayoutParams pLineFormDSD = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineFormDSD.setMargins(0, 0, 0, 20);
        LinearLayout linearFormDSD = new LinearLayout(getContext());
        linearFormDSD.setLayoutParams(pLineFormDSD);
        linearFormDSD.setOrientation(LinearLayout.HORIZONTAL);
        linearFormDSD.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormDSD);

        LinearLayout.LayoutParams pTxtDSD = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtDSD = new TextView(getContext());
        txtDSD.setLayoutParams(pTxtDSD);
        txtDSD.setText("DSD");
        txtDSD.setPadding(5, 0, 0, 0);
        linearFormDSD.addView(txtDSD);

        LinearLayout.LayoutParams pTxtDSDForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtDSDForm = new Spinner(getContext());
        txtDSDForm.setLayoutParams(pTxtDSDForm);
        txtDSDForm.setPadding(0, 16, 0, 16);
        txtDSDForm.setTag("DSD_" + totPakacge);
        genDSD("DSD_" + totPakacge, mLayoutPackage);
        linearFormDSD.addView(txtDSDForm);

        LinearLayout.LayoutParams pLineFormLNB = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineFormLNB.setMargins(0, 0, 0, 20);
        LinearLayout linearFormLNB = new LinearLayout(getContext());
        linearFormLNB.setLayoutParams(pLineFormLNB);
        linearFormLNB.setOrientation(LinearLayout.HORIZONTAL);
        linearFormLNB.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormLNB);

        LinearLayout.LayoutParams pTxtLNB = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtLNB = new TextView(getContext());
        txtLNB.setLayoutParams(pTxtLNB);
        txtLNB.setText("LNB");
        txtLNB.setPadding(5, 0, 0, 0);
        linearFormLNB.addView(txtLNB);

        LinearLayout.LayoutParams pTxtLNBForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtLNBForm = new Spinner(getContext());
        txtLNBForm.setLayoutParams(pTxtLNBForm);
        txtLNBForm.setPadding(0, 16, 0, 16);
        txtLNBForm.setTag("LNB_" + totPakacge);
        genLNB("LNB_" + totPakacge, mLayoutPackage);
        linearFormLNB.addView(txtLNBForm);

        LinearLayout.LayoutParams pLineFormODU = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineFormODU.setMargins(0, 0, 0, 20);
        LinearLayout linearFormODU = new LinearLayout(getContext());
        linearFormODU.setLayoutParams(pLineFormODU);
        linearFormODU.setOrientation(LinearLayout.HORIZONTAL);
        linearFormODU.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormODU);

        LinearLayout.LayoutParams pTxtODU = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtODU = new TextView(getContext());
        txtODU.setLayoutParams(pTxtODU);
        txtODU.setText("ODU");
        txtODU.setPadding(5, 0, 0, 0);
        linearFormODU.addView(txtODU);

        LinearLayout.LayoutParams pTxtODUForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtODUForm = new Spinner(getContext());
        txtODUForm.setLayoutParams(pTxtODUForm);
        txtODUForm.setPadding(0, 16, 0, 16);
        txtODUForm.setTag("ODU_" + totPakacge);
        genODU("ODU_" + totPakacge, mLayoutPackage);
        linearFormODU.addView(txtODUForm);
    }

    private void AdditionalPackage() {
        totAdditional = totAdditional + 1;
        mLayoutAdditional = (LinearLayout) root.findViewById(R.id.package_additional);

        final LinearLayout linearPackage = new LinearLayout(getActivity());
        linearPackage.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        linearPackage.setOrientation(LinearLayout.VERTICAL);
        linearPackage.setTag("ADDITIONAL_" + totPakacge);

        LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLinearPackage.setMargins(0, 10, 0, 0);
        linearPackage.setLayoutParams(pLinearPackage);
        mLayoutAdditional.addView(linearPackage);

        LinearLayout.LayoutParams pLineTitlePack = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout lineTitlePack = new LinearLayout(getActivity());
        lineTitlePack.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        lineTitlePack.setTag("TITLE_" + totAdditional);
        lineTitlePack.setOrientation(LinearLayout.HORIZONTAL);
        lineTitlePack.setLayoutParams(pLineTitlePack);
        linearPackage.addView(lineTitlePack);

        LinearLayout.LayoutParams pTxtPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtPack = new TextView(getActivity());
        txtPack.setLayoutParams(pTxtPackage);
        txtPack.setText("Additional Item ");
        txtPack.setPadding(5, 0, 0, 0);
        txtPack.setTypeface(Typeface.DEFAULT_BOLD);
        lineTitlePack.addView(txtPack);
        //Line For Change Items

        LinearLayout.LayoutParams pLineItems = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineItems.setMargins(0, 0, 0, 20);
        LinearLayout linearFormVC = new LinearLayout(getActivity());
        linearFormVC.setLayoutParams(pLineItems);
        linearFormVC.setOrientation(LinearLayout.HORIZONTAL);
        linearFormVC.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormVC);

        LinearLayout.LayoutParams pTxtItems = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtTitleItem = new TextView(getActivity());
        txtTitleItem.setLayoutParams(pTxtItems);
        txtTitleItem.setText("Items :");
        txtTitleItem.setPadding(5, 0, 0, 0);
        linearFormVC.addView(txtTitleItem);

        LinearLayout.LayoutParams pTxtAdditionalForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner spinnerAdditional = new Spinner(getActivity());
        spinnerAdditional.setLayoutParams(pTxtAdditionalForm);
        spinnerAdditional.setPadding(0, 5, 0, 5);
        spinnerAdditional.setTag("add_" + totAdditional);
        //genBasic("VC_" + totPakacge, mLayoutPackage);
        //genVC("item_" + totPakacge, mLayoutPackage);
        genMaterial("add_" + totAdditional, mLayoutAdditional);
        linearFormVC.addView(spinnerAdditional);

        //End Line

        LinearLayout.LayoutParams pLineCounter = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineCounter.setMargins(0, 0, 0, 20);
        LinearLayout linearFormDSD = new LinearLayout(getActivity());
        linearFormDSD.setLayoutParams(pLineCounter);
        linearFormDSD.setOrientation(LinearLayout.HORIZONTAL);
        linearFormDSD.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormDSD);

        LinearLayout.LayoutParams pTxtCounter = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.30f);
        TextView txtDSD = new TextView(getActivity());
        txtDSD.setLayoutParams(pTxtCounter);
        txtDSD.setText("Total :");
        txtDSD.setPadding(5, 0, 0, 0);
        linearFormDSD.addView(txtDSD);

        LinearLayout.LayoutParams pTxtCounterForm = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.39f);
        EditText counterEdit = new EditText(getActivity());
        counterEdit.setLayoutParams(pTxtCounterForm);
        counterEdit.setPadding(5, 0, 0, 0);
        counterEdit.setBackgroundResource(R.drawable.border_item_zipcode);
        counterEdit.setInputType(InputType.TYPE_CLASS_DATETIME);
        counterEdit.setGravity(Gravity.CENTER);
        counterEdit.setTag("addCount_" + totAdditional);
        //genDSD("DSD_" + totPakacge, mLayoutPackage);
        linearFormDSD.addView(counterEdit);

        LinearLayout.LayoutParams pTxtSpinerSatuan = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.2f);
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setLayoutParams(pTxtSpinerSatuan);
        /*pTxtSpinerSatuan.setMargins(25,0,50,0);
        spinnerSatuan.setPadding(0, 16, 0, 16);*/
        //txtDSDForm.setTag("DSD_" + totPakacge);
        //genDSD("DSD_" + totPakacge, mLayoutPackage);
        linearFormDSD.addView(linearLayout);
    }

    private void addListAlacarte() {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < totPakacge; i++) {
                    LinearLayout mLayoutList = (LinearLayout) mLayoutPackage.findViewWithTag("LIST_" + (i + 1));
                    LinearLayout.LayoutParams params;
                    params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(5, 5, 0, 0);
                    selAlacarte.clear();
                    if (selAlacarteSaveAll.size() > 0) {
                        try {
                            for (int j = 0; j < selAlacarteSaveAll.get(i).size(); j++) {
                                selAlacarte.add(selAlacarteSaveAll.get(i).get(j).toString());
                            }
                        } catch (IndexOutOfBoundsException e) {
                        }
                    }

                    try {
                        mLayoutList.removeAllViews();

                        if (mLayoutList.findViewWithTag("ADD_" + (i + 1)) == null) {
                            TextView txtAdd = new TextView(getActivity());
                            txtAdd.setText("+ Add Alacarte");
                            txtAdd.setTag("ADD_" + (i + 1));
                            txtAdd.setPadding(10, 10, 10, 10);
                            txtAdd.setClickable(true);
                            txtAdd.setLayoutParams(params);
                            txtAdd.setTextColor(getActivity().getResources().getColor(R.color.white));
                            txtAdd.setBackgroundResource(R.color.blue_10);
                            txtAdd.setOnClickListener(addAlacarteListener);
                            mLayoutList.addView(txtAdd);
                        }

                        if (selAlacarte.size() > 0) {
                            for (int k = 0; k < selAlacarte.size(); k++) {
                                String[] alaData = selAlacarte.get(k).toString().split("#");
                                String alaName = alaData[1].toString();
                                String alaId = alaData[0].toString();
                                TableMasterPackageAdapter mPackageProductAdapter
                                        = new TableMasterPackageAdapter(getActivity());
                                List<TableMasterPackage> packageProductListItems
                                        = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fROWID,
                                        alaId);/*
                                for (int n=0;n<packageProductListItems.size();n++){
                                    PackageProductListItem item = packageProductListItems.get(n);
                                    try {
                                        String price = item.getPrice();
                                        if (item.getPrice()==""||item.getPrice()==null) {
                                            price = "0";
                                        }

                                        hrgAlacarte = hrgAlacarte+Integer.parseInt(price);
                                        duit = Double.parseDouble(String.valueOf(hrgBasic + hrgAlacarte));
                                        defaultF = NumberFormat.getInstance();
                                        formattednumber = defaultF.format(duit);
                                        strPaket = formattednumber.replace(",", ".");
                                        pricePackage.setText("Rp."+strPaket);
                                        //pricePackage.setText(String.valueOf(hrgBasic+hrgAlacarte));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    Log.d("FTPaket", "Harga basic= "+ hrgBasic);
                                }*/
                                LinearLayout LL = new LinearLayout(getActivity());
                                LL.setOrientation(LinearLayout.HORIZONTAL);
                                LL.setGravity(Gravity.LEFT | Gravity.START);
                                LL.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                if (LL.findViewWithTag(alaId) == null) {
                                    TextView textView = new TextView(getActivity());
                                    textView.setText(alaName);
                                    textView.setTag(alaId);
                                    textView.setPadding(10, 10, 10, 10);
                                    textView.setLayoutParams(params);
                                    textView.setTextColor(getActivity().getResources().getColor(R.color.white));
                                    textView.setBackgroundResource(R.color.blue_grey_11);
                                    LL.addView(textView);

                                    ImageView imgDel = new ImageView(getActivity());
                                    imgDel.setImageResource(R.drawable.ic_close);
                                    imgDel.setTag(i + "_" + alaId);
                                    LinearLayout.LayoutParams imgParam = new LinearLayout.LayoutParams(60, 60);
                                    imgParam.setMargins(5, 5, 0, 0);
                                    imgDel.setLayoutParams(imgParam);
                                    imgDel.setBackgroundColor(getActivity().getResources().getColor(R.color.blue_grey_11));
                                    imgDel.setOnClickListener(delAlacarteListener);
                                    LL.addView(imgDel);

                                    mLayoutList.addView(LL);
                                }
                            }
                        }
                        int seq = i + 1;
                        LinearLayout layoutTitle = (LinearLayout) mLayoutPackage.findViewWithTag("TITLE_" + seq);
                        if (seq == totPakacge) {
                            if (layoutTitle.findViewWithTag(seq) == null) {
                                LinearLayout.LayoutParams pDelPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
                                pDelPackage.gravity = Gravity.END;
                                pDelPackage.setMargins(0, 0, 5, 0);
                                ImageView delPack = new ImageView(getActivity());
                                if (totPakacge > 2) {
                                    delPack.setImageResource(R.drawable.ic_close_package);
                                }

                                delPack.setTag(totPakacge);
                                delPack.setLayoutParams(pDelPackage);
                                delPack.setOnClickListener(deletePackage);
                                layoutTitle.addView(delPack);
                            }
                        } else {
                            layoutTitle.removeView(layoutTitle.findViewWithTag(seq));
                        }
                    } catch (NullPointerException e) {
                    }
                }

                hrgAlacarte = 0;
                for (int x = 0; x < selAlacarteSaveAll.size(); x++) {

                    for (int y = 0; y < selAlacarteSaveAll.get(x).size(); y++) {
                        if (x == 0) {

                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(getActivity());
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                            }
                            //alaBuild1.append(selAlacarteSaveAll.get(x).get(y).toString());
                            //alaBuild1.append(",");
                        }
                        if (x == 1) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(getActivity());
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                            }
                        }
                        if (x == 2) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();

                            TableMasterPackageAdapter mPackageProductAdapter
                                    = new TableMasterPackageAdapter(getActivity());

                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                            }


                        }

                    }
                }
                /*duit = Double.parseDouble(String.valueOf(hrgBasic + hrgAlacarte));
                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(duit);
                strPaket = formattednumber.replace(",", ".");*/
                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(hrgBasic + hargaAdditional);
                strPaket = formattednumber.replace(",", ".");

                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(hrgAlacarte);
                //strAlacarte = formattednumber.replace(",", ".");

                //pricePackage.setText(strPaket);
                //priceAlacarte.setText(strAlacarte);

                /*
                for (int i = 0; i < totPakacge; i++) {
                    spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (i + 1));
                    String product_id = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getProductId();
                    TableMPackageProductAdapter mPackageProductAdapter = new TableMPackageProductAdapter(activity);
                    List<PackageProductListItem> packageProductListItems = mPackageProductAdapter.fetchByCondition(activity, product_id,
                            TableMPackageProduct.fROWID);
                    for (int n=0;n<packageProductListItems.size();n++){
                        PackageProductListItem item = packageProductListItems.get(n);
                        coba = coba +Integer.parseInt(item.getPrice());
                    }
                }
                Log.d("FTPaket","Coba= "+ coba);*/
            }
        });
    }

    private View.OnClickListener delAlacarteListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String[] dataTag = view.getTag().toString().split("_");
            ArrayList<String> dataObject = selAlacarteSaveAll.get(Integer.parseInt(dataTag[0]));
            for (int i = 0; i < dataObject.size(); i++) {
                String[] ala = dataObject.get(i).toString().split("#");
                String idChannel = ala[0];
                if (idChannel.toString().equals(dataTag[1])) {
                    dataObject.remove(i);
                    addListAlacarte();
                }
            }
        }
    };

    private View.OnClickListener deletePackage = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d("FTPaket", "tag = " + view.getTag().toString());
            delPackage(Integer.parseInt(view.getTag().toString()));
        }
    };

    private void delPackage(int tag) {
        try {
            if (mLayoutPackage.findViewWithTag("PACK_" + tag) != null) {
                mLayoutPackage.removeView(mLayoutPackage.findViewWithTag("PACK_" + tag));
                totPakacge = totPakacge - 1;
                hrgBasic = 0;
                for (int i = 0; i < totPakacge; i++) {
                    spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (i + 1));
                    String product_id = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getProduct_id();
                    TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(getActivity());
                    List<TableMasterPackage> packageProductListItems
                            = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fROWID, product_id);

                    for (int n = 0; n < packageProductListItems.size(); n++) {
                        TableMasterPackage item = packageProductListItems.get(n);
                        hrgBasic = hrgBasic + Integer.parseInt(item.getPrice());
                    }
                }
                Log.d("FTPaket", "Coba= " + hrgBasic);

                for (int j = 0; j < selAlacarteSaveAll.size(); j++) {
                    if (j == (tag - 1)) {
                        selAlacarteSaveAll.remove(j);
                    }
                }

                hrgAlacarte = 0;
                for (int x = 0; x < selAlacarteSaveAll.size(); x++) {

                    for (int y = 0; y < selAlacarteSaveAll.get(x).size(); y++) {
                        if (x == 0) {

                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(getActivity());
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                            }
                            //alaBuild1.append(selAlacarteSaveAll.get(x).get(y).toString());
                            //alaBuild1.append(",");
                        }
                        if (x == 1) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter
                                    = new TableMasterPackageAdapter(getActivity());
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                            }
                        }
                        if (x == 2) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter
                                    = new TableMasterPackageAdapter(getActivity());
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                            }
                        }

                    }
                }

                /*duit = Double.parseDouble(String.valueOf(hrgBasic + hrgAlacarte));
                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(duit);
                strPaket = formattednumber.replace(",", ".");*/
                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(hrgBasic + hargaAdditional);
                strPaket = formattednumber.replace(",", ".");

                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(hrgAlacarte);
                //strAlacarte = formattednumber.replace(",", ".");

                //pricePackage.setText(strPaket);
                //priceAlacarte.setText(strAlacarte);


                LinearLayout layoutTitle = (LinearLayout) mLayoutPackage.findViewWithTag("TITLE_" + totPakacge);
                if (layoutTitle.findViewWithTag(totPakacge) == null) {
                    LinearLayout.LayoutParams pDelPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
                    pDelPackage.gravity = Gravity.END;
                    pDelPackage.setMargins(0, 0, 5, 0);
                    if (totPakacge > 2) {
                        ImageView delPack = new ImageView(getActivity());
                        delPack.setImageResource(R.drawable.ic_close_package);
                        delPack.setTag(totPakacge);
                        delPack.setLayoutParams(pDelPackage);
                        delPack.setOnClickListener(deletePackage);
                        layoutTitle.addView(delPack);
                    }
                }
            }

        } catch (Exception e) {
        }
    }

    private void genMaterial(final String tagId, final LinearLayout mLayoutAdd) {
        mLayoutAdd.post(new Runnable() {
            @Override
            public void run() {
                materialAdd = (Spinner) mLayoutAdd.findViewWithTag(tagId);
                materialAdd.setAdapter(spinnerMaterialAdapter);
                materialAdd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        //priceMaterial = mMaterialListItems.get(position).getRate();
                        hargaAdditional = 0;
                        for (int i = 0; i < totAdditional; i++) {
                            materialAdd = (Spinner) mLayoutAdditional.findViewWithTag("add_" + (i + 1));
                            countAdd = (EditText) mLayoutAdditional.findViewWithTag("addCount_" + (i + 1));
                            priceMaterial = mMaterialListItems.get(materialAdd.getSelectedItemPosition()).getRate();
                            if (!priceMaterial.equalsIgnoreCase("null")) {
                                hargaAdditional = hargaAdditional + Integer.parseInt(priceMaterial);
                                defaultF = NumberFormat.getInstance();
                                formattednumber = defaultF.format(hrgBasic + hargaAdditional);
                                strPaket = formattednumber.replace(",", ".");
                                //pricePackage.setText(strPaket);
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }
        });
    }

    private void genBasic(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                spinPackage = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                spinPackage.setAdapter(spinAdapter);
                spinPackage.setOnItemSelectedListener(spinPackageListener);
            }
        });
    }

    private void ControlSpinner(String tag, String preMessaage) {
        count = mLayoutPackage.getChildCount();
        JSONObject obj;
        JSONArray result = new JSONArray();
        Spinner packageSpinner = null;
        int b = 0;

        for (int i = 0; i < count; i++) {
            obj = new JSONObject();
            View childView = mLayoutPackage.getChildAt(i);
            packageSpinner = (Spinner) childView.findViewWithTag(tag + (i + 1));
            Log.d("tagku", tag + (i + 1));
            b = mLayoutPackage.indexOfChild(childView);
            if (packageSpinner.getSelectedItem().toString() != "") {
                try {
                    obj.put("id", b);
                    obj.put("value", packageSpinner.getSelectedItem().toString());
                    result.put(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
        if (result != null || result.length() > 0) {
            int m;
            for (m = 0; m < result.length(); m++) {
                try {
                    JSONObject objres = result.getJSONObject(m);
                    int id = objres.getInt("id");
                    String value = objres.getString("value");
                    executeValidation(id, value, preMessaage, tag);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void executeValidation(int id, String value, String preMessaage, String tag) {
        Spinner spinner = null;
        if (count > 0) {
            for (int i = 1; i < count; i++) {
                View childLnb = mLayoutPackage.getChildAt(i - 1); // Start From 0
                spinner = (Spinner) childLnb.findViewWithTag(tag + i); // Start from 1
                int index = mLayoutPackage.indexOfChild(childLnb);
                if (id != index && value.equals(spinner.getSelectedItem().toString())) {
                    Toast.makeText(getContext(), preMessaage + " ada yang sama, mohon periksa kembali", Toast.LENGTH_SHORT).show();
                    spinner.setSelection(0);
                }
            }
        }
    }

    private void autoTextLNBandOdu(String tag, int position) {
        int count = mLayoutPackage.getChildCount();
        if (count > 1) {
            Spinner spinner = null;
            for (int n = 1; n < count; n++) {
                switch (tag) {
                    case "LNB_":
                        View childLnb = mLayoutPackage.getChildAt(n); // Start From 0
                        spinner = (Spinner) childLnb.findViewWithTag(tag + (n + 1)); // Start from 1
                        spinner.setSelection(position);
                        spinner.setEnabled(false);
                        break;
                    case "ODU_":
                        View childOdu = mLayoutPackage.getChildAt(n); // Start From 0
                        spinner = (Spinner) childOdu.findViewWithTag(tag + (n + 1)); // Start from 1
                        spinner.setSelection(position);
                        spinner.setEnabled(false);
                        break;
                }
            }
        }
    }


    private void genVC(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                spinVc = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                spinVc.setAdapter(spinAdapterVC);
                spinVc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                        index = mLayoutPackage.indexOfChild(childView);
                        ControlSpinner("VC_", "VC");
                    }

                    @Override

                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
    }

    private void genDSD(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                spinDsd = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                spinDsd.setAdapter(spinAdapterDSD);
                spinDsd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                        index = mLayoutPackage.indexOfChild(childView);
                        ControlSpinner("DSD_", "DSD");
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
    }

    private void genLNB(final String tagId, final LinearLayout mLayoutPackage) {
        TableFTSOHAdapter dbCohAdapter = new TableFTSOHAdapter(this.getContext());
        //listSOH = dbCohAdapter.getDatabyCondition(TableFreeTrialCoh.KEY_HW_STATUS, "ADD");
        List<TableFreeTrialCoh> listSOH = dbCohAdapter.getDatabyConditionMultyCondition(
                TableFreeTrialCoh.KEY_HW_STATUS, "1", TableFreeTrialCoh.KEY_HW_TYPE, "LNB");

        listLnb = new ArrayList<String>();
        listLnb.add("");

        for (int i = 0; i < listSOH.size(); i++) {
            TableFreeTrialCoh item = listSOH.get(i);
            if (isMulty == 0) {
                if (item.getType().equalsIgnoreCase("LNB")) {
                    listLnb.add(item.getSn());
                }
            } else {
                if (item.getType().equalsIgnoreCase("LNBSKU") ||
                        item.getType().equalsIgnoreCase("LNBDUAL")) {
                    listLnb.add(item.getSn());
                }
            }
        }

        spinAdapterLNB = new SpinnerHWAdapter(getContext(), R.layout.status_spinner,
                listLnb);
        spinAdapterLNB.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinAdapterLNBMulty = new SpinnerHWAdapter(getContext(), R.layout.status_spinner,
                listLnb);
        spinAdapterLNBMulty.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                spinLnb = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                /*if (totPakacge > 1) {
                    spinLnb.setClickable(true);
                }*/

                spinAdapterLNB.notifyDataSetChanged();
                spinLnb.setAdapter(spinAdapterLNB);

                spinLnb.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                        index = mLayoutPackage.indexOfChild(childView);
                        //ControlSpinner("LNB_", "LNB", position, index);
                        autoTextLNBandOdu("LNB_", position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
    }

    private void genODU(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                spinDish = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                /*if (totPakacge > 1) {
                    spinDish.setClickable(true);
                }*/
                spinDish.setAdapter(spinAdapterODU);
                spinDish.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                        index = mLayoutPackage.indexOfChild(childView);
                        //ControlSpinner("ODU_", "ODU", position, index);
                        autoTextLNBandOdu("ODU_", position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
    }


    private View.OnClickListener addAlacarteListener = new View.OnClickListener() {
        private ImageView closeAla;

        @Override
        public void onClick(View view) {

            View popUpView = getActivity().getLayoutInflater().inflate(R.layout.ms_package_alacarte_list, null);
            mpopup = new PopupWindow(popUpView, LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT, true);
            mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
            mpopup.showAtLocation(popUpView, Gravity.START, 0, 0);
            closeAla = (ImageView) popUpView.findViewById(R.id.close_alacarte);
            closeAla.setClickable(true);
            closeAla.setOnClickListener(closeAlaListener);
            loadAlacarte(popUpView, view.getTag().toString());
        }
    };

    private View.OnClickListener closeAlaListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            mpopup.dismiss();
        }
    };

    private void loadAlacarte(View popUpView, String tagId) {
        String[] tag = tagId.split("_");

        Spinner spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (tag[1]));
        String productId = mPackageProductListItemList
                .get(spinPackage.getSelectedItemPosition())
                .getProduct_id();

        basic_id = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getBasic_id();

        TableMasterPackageAdapter db = new TableMasterPackageAdapter(getActivity());
        recyclerView = (RecyclerView) popUpView.findViewById(R.id.package_alacarte_list);
        btnSave = (Button) popUpView.findViewById(R.id.btn_save_alacarte);
        btnSave.setTag("SAVE_" + tag[1]);
        btnSave.setOnClickListener(btnSaveListener);
        final LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutParams);
        mPackageProductListItem = db.fetchByProfile("1",
                mBrandListItem.get(spinBrand.getSelectedItemPosition()).getBrand_code(), productId);//IVDGT
        mPackageProductAdapter = new MSAlacarteListAdapter(mPackageProductListItem);
        recyclerView.setAdapter(mPackageProductAdapter);
    }

    private AdapterView.OnItemSelectedListener spinPackageListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            for (int i = 0; i < totPakacge; i++) {
                spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (i + 1));
                productId = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getProduct_id();
            }
            TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(getActivity());
            List<TableMasterPackage> packageProductListItems;
             /*
            for (int n=0;n<packageProductListItems.size();n++){
                PackageProductListItem item = packageProductListItems.get(n);
                try {
                    String price = item.getPrice();
                    if (item.getPrice()==""||item.getPrice()==null) {
                        price = "0";
                    }
                    hrgBasic = Integer.parseInt(price);

                    duit = Double.parseDouble(String.valueOf(hrgBasic + hrgAlacarte));
                    defaultF = NumberFormat.getInstance();
                    formattednumber = defaultF.format(duit);
                    str = formattednumber.replace(",", ".");
                    total.setText("Rp."+str);
                    //total.setText(String.valueOf(hrgBasic+hrgAlacarte));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("FTPaket", "Harga basic= "+ hrgBasic);
            }*/

            hrgBasic = 0;
            for (int i = 0; i < totPakacge; i++) {
                spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (i + 1));
                productId = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getProduct_id();
                mPackageProductAdapter = new TableMasterPackageAdapter(getActivity());
                packageProductListItems
                        = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fROWID, productId);
                for (int n = 0; n < packageProductListItems.size(); n++) {
                    TableMasterPackage item = packageProductListItems.get(n);
                    hrgBasic = hrgBasic + Integer.parseInt(item.getPrice());
                }
            }
            Log.d("FTPaket", "Coba= " + hrgBasic);
            duit = Double.parseDouble(String.valueOf(hrgBasic + hrgAlacarte));
            defaultF = NumberFormat.getInstance();
            formattednumber = defaultF.format(duit);
            str = formattednumber.replace(",", ".");
            //total.setText("Rp." + str);
            total.setText("Rp.0");
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapter) {
        }
    };


    private AdapterView.OnItemSelectedListener spinPromoListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapter) {
        }
    };

    private int getTarget() {
        tablePlanAdapter = new TablePlanAdapter(getActivity());
        dbAdapter = new TableFreeTrialAdapter(getActivity());
        String date = utils.getCurrentDate();
        listPlan = tablePlanAdapter.getDatabySfl(getActivity(), TablePlan.KEY_PLAN_DATE, date);

        if (listPlan != null && listPlan.size() > 0) {
            TablePlan item = listPlan.get(0);
            if (item.getTarget() == null || item.getTarget().equalsIgnoreCase("")) {
                target = 0;
            } else {
                target = Integer.parseInt(item.getTarget());
            }

            listFreetrial = dbAdapter.getDatabyCondition(TableFreeTrial.KEY_PLAN_ID, item.getPlan_id());
            for (int a = 0; a < listFreetrial.size(); a++) {
                Log.d("FTTASKLIST:", "planIdfor= " + listFreetrial.get(a).getPlan_id());
                if (!listFreetrial.get(a).getValue().equalsIgnoreCase("TITLE")) {
                    int result = listFreetrial.size() - 1;
                    Log.d("FTTASKLIST:", "result= " + result);
                    target = target - result;
                }
            }
        }
        return target;
    }

    AdapterView.OnItemSelectedListener spinerPromoListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            promoCode = mPromoListItems.get(position).getPromotion_code();
            //Toast.makeText(getActivity(), "promoCode= "+promoCode, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    AdapterView.OnItemSelectedListener spinerTypeListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0) {
                for (int i = totPakacge; i >= 1; i--) {
                    delPackage(i);
                }
                isMulty = 0;
                addPackage();
                mLayoutAdd.setVisibility(View.GONE);

            } else {
                /*addPackage();
                mLayoutAdd.setVisibility(View.VISIBLE);*/
                getTarget();
                isMulty = 1;
                if (target >= 2) {
                    try {
                        mLayoutPackage.removeAllViews();
                        selAlacarteSaveAll.clear();
                        totPakacge = 0;
                        for (int i = 0; i < 2; i++) {
                            addPackage();
                        }

                    } catch (NullPointerException e) {

                    }

                    mLayoutAdd.setVisibility(View.VISIBLE);
                } else {
                    spinType.setSelection(0);
                    Toast.makeText(getActivity(), "Sisa plan anda kurang dari 2", Toast.LENGTH_SHORT).show();
                }

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private AdapterView.OnItemSelectedListener spinBrandListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            String code = mBrandListItem.get(position).getBrand_code();
            TableMasterPackageAdapter tableMPackageProductAdapter
                    = new TableMasterPackageAdapter(getActivity());
            List<TableMasterPackage> paketList = new ArrayList<TableMasterPackage>();
            paketList = tableMPackageProductAdapter.fetchByProfile("2", code, productId);
            for (int m = 0; m < paketList.size(); m++) {
                TableMasterPackage item = paketList.get(m);
                if (item != null) {
                    item.getPrice();
                    Double install = Double.parseDouble(item.getPrice());
                    NumberFormat defaultF = NumberFormat.getInstance();
                    String formattednumber = defaultF.format(install);
                    String str = formattednumber.replace(",", ".");
                    //instalasi.setText("Rp." + str);
                    instalasi.setText("Rp.0");
                }
            }

            LinearLayout mLayoutBackProduct = (LinearLayout) root.findViewById(R.id.back_product);
            switch (code) {
                case "0":
                    mLayoutBackProduct.setBackgroundColor(Color.parseColor("#43a047"));
                    break;
                case "1":
                    mLayoutBackProduct.setBackgroundColor(Color.parseColor("#ffd600"));
                    break;
                case "2":
                    mLayoutBackProduct.setBackgroundColor(Color.parseColor("#01579b"));
                    break;
                default:
                    mLayoutBackProduct.setBackgroundColor(Color.parseColor("#01579b"));
                    break;
            }

            mPackageProductListItemList = mTablePackageProductAdapter.fetchBasic(code);
            for (int i = 0; i < mPackageProductListItemList.size(); i++) {
                TableMasterPackage item = mPackageProductListItemList.get(i);
                listProductPackage.add(item.getProduct_name());
            }
            spinnerProductPackage = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, listProductPackage);
            spinAdapter = new SpinnerPackAdapter(getContext(), R.layout.status_spinner, mPackageProductListItemList);
            spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            /*for (int i=0; i<listSOH.size();i++) {
                if(listSOH.get(i).getType().equalsIgnoreCase("VC")) {
                    listVc.add(listSOH.get(i).getSn());
                } else if (listSOH.get(i).getType().equalsIgnoreCase("LNB")) {
                    listLnb.add(listSOH.get(i).getSn());
                } else if (listSOH.get(i).getType().equalsIgnoreCase("ANT")) {
                    listDish.add(listSOH.get(i).getSn());
                } else if (listSOH.get(i).getType().equalsIgnoreCase("DEC")) {
                    listDsd.add(listSOH.get(i).getSn());
                }
            }*/

            spinAdapterVC = new SpinnerHWAdapter(getContext(), R.layout.status_spinner, listVc);
            spinAdapterVC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinAdapterODU = new SpinnerHWAdapter(getContext(), R.layout.status_spinner, listDish);
            spinAdapterODU.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinAdapterDSD = new SpinnerHWAdapter(getContext(), R.layout.status_spinner, listDsd);
            spinAdapterDSD.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            try {
                mLayoutPackage.removeAllViews();
                selAlacarteSaveAll.clear();
                totPakacge = 0;
                addPackage();
            } catch (NullPointerException e) {

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapter) {
        }
    };

    private View.OnClickListener btnSaveListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            try {
                String[] tag = view.getTag().toString().split("_");
                int objArray = Integer.parseInt(tag[1]) - 1;
                int dataAll = selAlacarteSaveAll.size();
                if (objArray + 1 > dataAll) {
                    selAlacarteSaveAll.add(objArray, mPackageProductAdapter.itemAlacarte.getList());
                } else {
                    selAlacarteSaveAll.set(objArray, mPackageProductAdapter.itemAlacarte.getList());
                }
                addListAlacarte();

            } catch (IndexOutOfBoundsException e) {

            }
            mpopup.dismiss();
        }
    };

    private View.OnClickListener actSaveAll = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            try {
                String _vc, _lnb, _dsd, _odu, vcList = "", lnbList = "", dsdList = "", oduList = "",
                        lnbId;
                /*_vc     = vc.getText().toString();
                _lnb    = lnb.getText().toString();
                _dsd    = dsd.getText().toString();
                _odu    = dish.getText().toString();*/
                _vc = spinVc.getSelectedItem().toString();
                _lnb = spinLnb.getSelectedItem().toString();
                _dsd = spinDsd.getSelectedItem().toString();
                _odu = spinDish.getSelectedItem().toString();

                if (_vc == null || _vc.equalsIgnoreCase("") || _vc.length() <= 0) {
                    Toast.makeText(getActivity(), "Silahkan masukan no VC.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (_dsd == null || _dsd.equalsIgnoreCase("") || _dsd.length() <= 0) {
                    Toast.makeText(getActivity(), "Silahkan masukan no DSD.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (_lnb == null || _lnb.equalsIgnoreCase("") || _lnb.length() <= 0) {
                    Toast.makeText(getActivity(), "Silahkan masukan no LNB.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (_odu == null || _odu.equalsIgnoreCase("") || _odu.length() <= 0) {
                    Toast.makeText(getActivity(), "Silahkan masukan no ODU.", Toast.LENGTH_SHORT).show();
                    return;
                }

                for (int i = 0; i < listSOH.size(); i++) {
                    if (listSOH.get(i).getType().equalsIgnoreCase("VC")
                            && _vc.equalsIgnoreCase(listSOH.get(i).getSn())) {
                        vcList = _vc;
                    }

                    if (listSOH.get(i).getType().equalsIgnoreCase("LNB")
                            && _lnb.equalsIgnoreCase(listSOH.get(i).getSn())) {
                        lnbList = _lnb;
                    }

                    if (listSOH.get(i).getType().equalsIgnoreCase("ANT")
                            && _odu.equalsIgnoreCase(listSOH.get(i).getSn())) {
                        oduList = _odu;
                    }

                    if (listSOH.get(i).getType().equalsIgnoreCase("DEC")
                            && _dsd.equalsIgnoreCase(listSOH.get(i).getSn())) {
                        dsdList = _dsd;
                    }
                }

                /*if (vcList == null || vcList.equalsIgnoreCase("")|| vcList.length()<=0) {
                    Toast.makeText(getActivity(), "Silahkan cek kembali no VC.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (dsdList == null || dsdList.equalsIgnoreCase("") || dsdList.length()<=0) {
                    Toast.makeText(getActivity(), "Silahkan cek kembali no DSD.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (lnbList == null || lnbList.equalsIgnoreCase("") || lnbList.length()<=0) {
                    Toast.makeText(getActivity(), "Silahkan cek kembali no LNB.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (oduList == null || oduList.equalsIgnoreCase("") || oduList.length()<=0) {
                    Toast.makeText(getActivity(), "Silahkan cek kembali no ODU.", Toast.LENGTH_SHORT).show();
                    return;
                }*/

                spinBrand = (Spinner) root.findViewById(R.id.spinner_brand);
                //spinPromo = (Spinner) root.findViewById(R.id.spinner_promo);
                EditText biayaLain = (EditText) root.findViewById(R.id.biaya_lain);
                EditText ketLain = (EditText) root.findViewById(R.id.ket_lain);

                int selectedId = radioStatus.getCheckedRadioButtonId();
                selRadio = (RadioButton) root.findViewById(selectedId);

                //alacarte
                ArrayList alacarte = new ArrayList();
                alacarte.add(selAlacarteSaveAll);

                ArrayList basic = new ArrayList();
                ArrayList arrayVc = new ArrayList();
                ArrayList arrayLNB = new ArrayList();
                ArrayList arrayODU = new ArrayList();
                ArrayList arrayDSD = new ArrayList();

                arrayVc.clear();
                arrayLNB.clear();
                arrayODU.clear();
                arrayDSD.clear();
                basic.clear();

                for (int i = 0; i < totPakacge; i++) {
                    spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (i + 1));
                    spinVc = (Spinner) mLayoutPackage.findViewWithTag("VC_" + (i + 1));
                    spinLnb = (Spinner) mLayoutPackage.findViewWithTag("LNB_" + (i + 1));
                    spinDish = (Spinner) mLayoutPackage.findViewWithTag("ODU_" + (i + 1));
                    spinDsd = (Spinner) mLayoutPackage.findViewWithTag("DSD_" + (i + 1));

                    String vcId = listVc.get(spinVc.getSelectedItemPosition());
                    arrayVc.add(vcId);

                    lnbId = listLnb.get(spinLnb.getSelectedItemPosition());
                    arrayLNB.add(lnbId);

                    String oduId = listDish.get(spinDish.getSelectedItemPosition());
                    arrayODU.add(oduId);

                    String dsdId = listDsd.get(spinDsd.getSelectedItemPosition());
                    arrayDSD.add(dsdId);

                    String basicId = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getProduct_id();
                    basic.add(basicId);
                }

                JSONObject objAdd = null;
                JSONArray arrayAdd = new JSONArray();
                JSONObject addResult = new JSONObject();

                for (int i = 0; i < totAdditional; i++) {
                    materialAdd = (Spinner) mLayoutAdditional.findViewWithTag("add_" + (i + 1));
                    countAdd = (EditText) mLayoutAdditional.findViewWithTag("addCount_" + (i + 1));

                    if (isShowAdditional) {
                        if (materialAdd != null && materialAdd.getSelectedItem() != null) {
                            objAdd = new JSONObject();
                            objAdd.put("additional_id", mMaterialListItems.get
                                    (materialAdd.getSelectedItemPosition()).getHw_id());
                            objAdd.put("additional_total", countAdd.getText().toString());
                            arrayAdd.put(objAdd);
                            addResult.put("data", arrayAdd);
                        } else {
                            Toast.makeText(getActivity(), "Harap isi additional ke - " + String.valueOf(i)
                                    , Toast.LENGTH_SHORT);
                            break;
                        }
                    }

                }

                String basic1 = "";
                String basic2 = "";
                String basic3 = "";
                String alacarte1 = "";
                String alacarte2 = "";
                String alacarte3 = "";
                String vc1 = "";
                String vc2 = "";
                String vc3 = "";
                String lnb1 = "";
                String lnb2 = "";
                String lnb3 = "";
                String odu1 = "";
                String odu2 = "";
                String odu3 = "";
                String dsd1 = "";
                String dsd2 = "";
                String dsd3 = "";

                StringBuilder alaBuild1 = new StringBuilder();
                StringBuilder alaBuild2 = new StringBuilder();
                StringBuilder alaBuild3 = new StringBuilder();
                for (int x = 0; x < selAlacarteSaveAll.size(); x++) {

                    for (int y = 0; y < selAlacarteSaveAll.get(x).size(); y++) {
                        if (x == 0) {
                            alaBuild1.append(selAlacarteSaveAll.get(x).get(y).toString());
                            alaBuild1.append(",");
                        }
                        if (x == 1) {
                            alaBuild2.append(selAlacarteSaveAll.get(x).get(y).toString());
                            alaBuild2.append(",");
                        }
                        if (x == 2) {
                            alaBuild3.append(selAlacarteSaveAll.get(x).get(y).toString());
                            alaBuild3.append(",");
                        }
                    }

                    if (x == 0) {
                        alacarte1 = alaBuild1.toString().substring(0, alaBuild1.length() - 1);
                    }
                    if (x == 1) {
                        alacarte2 = alaBuild2.toString().substring(0, alaBuild2.length() - 1);
                    }
                    if (x == 2) {
                        alacarte3 = alaBuild3.toString().substring(0, alaBuild3.length() - 1);
                    }
                }

                for (int x = 0; x < basic.size(); x++) {
                    if (x == 0) {
                        basic1 = basic.get(x).toString();
                    }
                    if (x == 1) {
                        basic2 = basic.get(x).toString();
                    }
                    if (x == 2) {
                        basic3 = basic.get(x).toString();
                    }
                }

                for (int x = 0; x < arrayVc.size(); x++) {
                    if (x == 0) {
                        vc1 = arrayVc.get(x).toString();
                    }
                    if (x == 1) {
                        vc2 = arrayVc.get(x).toString();
                    }
                    if (x == 2) {
                        vc3 = arrayVc.get(x).toString();
                    }
                }

                for (int x = 0; x < arrayLNB.size(); x++) {
                    if (x == 0) {
                        lnb1 = arrayLNB.get(x).toString();
                    }
                    if (x == 1) {
                        lnb2 = arrayLNB.get(x).toString();
                    }
                    if (x == 2) {
                        lnb3 = arrayLNB.get(x).toString();
                    }
                }

                for (int x = 0; x < arrayODU.size(); x++) {
                    if (x == 0) {
                        odu1 = arrayODU.get(x).toString();
                    }
                    if (x == 1) {
                        odu2 = arrayODU.get(x).toString();
                    }
                    if (x == 2) {
                        odu3 = arrayODU.get(x).toString();
                    }
                }

                for (int x = 0; x < arrayDSD.size(); x++) {
                    if (x == 0) {
                        dsd1 = arrayDSD.get(x).toString();
                    }
                    if (x == 1) {
                        dsd2 = arrayDSD.get(x).toString();
                    }
                    if (x == 2) {
                        dsd3 = arrayDSD.get(x).toString();
                    }
                }
                try {
                    if (selRadio.getText().toString().equalsIgnoreCase("") || selRadio.getText().toString().length() < 0) {
                        Toast.makeText(getActivity(), "Cek Hw Status", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (NullPointerException x) {
                    Toast.makeText(getActivity(), "Cek Hw Status", Toast.LENGTH_SHORT).show();
                    return;
                }


                saveStatus = selRadio.getText().toString();
                if (saveStatus.equalsIgnoreCase("Pinjam")) {
                    saveStatus = "1";
                } else {
                    saveStatus = "2";
                }

                saveBrand = mBrandListItem.get(spinBrand.getSelectedItemPosition()).getBrand_id();
                //savePromo = mPromoListItems.get(spinPromo.getSelectedItemPosition()).getId();
                //saveBiayaLain = biayaLain.getText().toString();
                //saveKetLain = ketLain.getText().toString();

                JSONObject basicJson = new JSONObject();
                JSONObject alaJson = new JSONObject();
                JSONObject packageDet = new JSONObject();
                JSONObject vcJson = new JSONObject();
                JSONObject lnbJson = new JSONObject();
                JSONObject oduJson = new JSONObject();
                JSONObject dsdJson = new JSONObject();
                JSONObject objBundling = new JSONObject();

                try {
                    basicJson.put("1", basic1);
                    basicJson.put("2", basic2);
                    basicJson.put("3", basic3);

                    alaJson.put("1", alacarte1);
                    alaJson.put("2", alacarte2);
                    alaJson.put("3", alacarte3);

                    vcJson.put("1", vc1);
                    vcJson.put("2", vc2);
                    vcJson.put("3", vc3);

                    lnbJson.put("1", lnb1);
                    lnbJson.put("2", lnb2);
                    lnbJson.put("3", lnb3);

                    oduJson.put("1", odu1);
                    oduJson.put("2", odu2);
                    oduJson.put("3", odu3);

                    dsdJson.put("1", dsd1);
                    dsdJson.put("2", dsd2);
                    dsdJson.put("3", dsd3);
                    packageDet.put("BRAND", saveBrand);

                    if (spinSimCard != null && spinSimCard.getSelectedItem() != null) {
                        objBundling.put("sim_id", simcardId);
                        objBundling.put("sim_value", listSimCard.get(spinSimCard.getSelectedItemPosition()));
                        objBundling.put("router_id", routerId);
                        objBundling.put("router_value", listRouter.get(spinRouter.getSelectedItemPosition()));
                    }

                    if (numberForm.substring(0, 1).equalsIgnoreCase("p")) {
                        saveStatus = "2";
                    } else {
                        saveStatus = "1";
                    }
                    packageDet.put("HW_STATUS", saveStatus);
                    packageDet.put("BASIC", basicJson);
                    packageDet.put("ALACARTE", alaJson);
                    packageDet.put("VC", vcJson);
                    packageDet.put("LNB", lnbJson);
                    packageDet.put("ODU", oduJson);
                    packageDet.put("DSD", dsdJson);
                    packageDet.put("isMulty", isMulty);
                    packageDet.put("additional", addResult);
                    packageDet.put("bundling", objBundling);
                    //packageDet.put("VC", _vc);
                    // packageDet.put("LNB", _lnb);
                    //packageDet.put("DSD", _dsd);
                    //packageDet.put("ODU", _odu);
                    packageDet.put("promoCode", promoCode);
                    packageDet.put("INSTALL_BILL", instalasi.getText().toString());
                    packageDet.put("MONTHLY_BILL", total.getText().toString());
                    //packageDet.put("PROMO", savePromo);
                    //packageDet.put("BIAYA_LAIN", saveBiayaLain);
                    //packageDet.put("KET_LAIN", saveKetLain);

                    TableFormAppAdapter db = new TableFormAppAdapter(getActivity());
                    List<TableFormApp> sel = db.getDatabyCondition(TableFormApp.fFORM_NO, numberForm);
                    if (sel.size() > 0) {
                        if (!isShowBundling) {
                            if (spinSimCard == null && spinSimCard.getSelectedItem() == null) {
                                Toast.makeText(getActivity(), "Sim Card tidak boleh kosong", Toast.LENGTH_SHORT).show();
                            } else if (spinRouter == null && spinRouter.getSelectedItem() == null) {
                                Toast.makeText(getActivity(), "Router tidak boleh kosong", Toast.LENGTH_SHORT).show();
                            } else {
                                db.updatePartial(getContext(), TableFormApp.fVALUES_PACKAGE, packageDet.toString(), TableFormApp.fFORM_NO, numberForm);
                                dbCohAdapter.updatePartial(getActivity(), TableFreeTrialCoh.KEY_HW_STATUS, "1", TableFreeTrialCoh.KEY_HW_SN, _vc);
                                dbCohAdapter.updatePartial(getActivity(), TableFreeTrialCoh.KEY_HW_STATUS, "1", TableFreeTrialCoh.KEY_HW_SN, _dsd);
                                dbCohAdapter.updatePartial(getActivity(), TableFreeTrialCoh.KEY_HW_STATUS, "1", TableFreeTrialCoh.KEY_HW_SN, _lnb);
                                dbCohAdapter.updatePartial(getActivity(), TableFreeTrialCoh.KEY_HW_STATUS, "1", TableFreeTrialCoh.KEY_HW_SN, _odu);
                                Toast.makeText(getActivity(), "Package Saved", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            db.updatePartial(getContext(), TableFormApp.fVALUES_PACKAGE, packageDet.toString(), TableFormApp.fFORM_NO, numberForm);
                            dbCohAdapter.updatePartial(getActivity(), TableFreeTrialCoh.KEY_HW_STATUS, "1", TableFreeTrialCoh.KEY_HW_SN, _vc);
                            dbCohAdapter.updatePartial(getActivity(), TableFreeTrialCoh.KEY_HW_STATUS, "1", TableFreeTrialCoh.KEY_HW_SN, _dsd);
                            dbCohAdapter.updatePartial(getActivity(), TableFreeTrialCoh.KEY_HW_STATUS, "1", TableFreeTrialCoh.KEY_HW_SN, _lnb);
                            dbCohAdapter.updatePartial(getActivity(), TableFreeTrialCoh.KEY_HW_STATUS, "1", TableFreeTrialCoh.KEY_HW_SN, _odu);
                            Toast.makeText(getActivity(), "Package Saved", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Form number " + numberForm + " tidak tersedia!", Toast.LENGTH_SHORT).show();
                    }

                    Log.d("paketku", packageDet.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void setSelectHw(String type, String val) {
        if (type.equalsIgnoreCase("VC")) {
            for (int i = 0; i < listVc.size(); i++) {
                if (listVc.get(i).equalsIgnoreCase(val)) {
                    spinVc.setSelection(i);
                }
            }
        }

        if (type.equalsIgnoreCase("DEC")) {
            for (int i = 0; i < listDsd.size(); i++) {
                if (listDsd.get(i).equalsIgnoreCase(val)) {
                    spinDsd.setSelection(i);
                }
            }
        }

        if (type.equalsIgnoreCase("ANT")) {
            for (int i = 0; i < listDish.size(); i++) {
                if (listDish.get(i).equalsIgnoreCase(val)) {
                    spinDish.setSelection(i);
                }
            }
        }

        if (type.equalsIgnoreCase("LNB")) {
            for (int i = 0; i < listLnb.size(); i++) {
                if (listLnb.get(i).equalsIgnoreCase(val)) {
                    spinLnb.setSelection(i);
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {
            String re = scanResult.getContents();
            String a, b, c;
            TableFTSOHAdapter db = new TableFTSOHAdapter(this.getContext());
            String[] params;
            if (isVc) {
                if (re.length() < 14) {
                    a = re.substring(0, 2);
                    b = re.substring(2, re.length());
                    StringBuilder sb = new StringBuilder();
                    sb.append(a);
                    sb.append("0000");
                    sb.append(b);
                    re = sb.toString();
                }
                params = new String[2];
                params[0] = "VC";
                params[1] = "ADD";
                mListMaster = db._fetchDesc(TableFreeTrialCoh.KEY_HW_TYPE, TableFreeTrialCoh.KEY_HW_STATUS, "VC", "ADD");
                if (mListMaster.size() > 0
                        && !mListMaster.get(mListMaster.size() - 1).getSn().equalsIgnoreCase(re)) {
                    //utils.showErrorDlg(handler, "Silahkan Pasang " + mListMaster.get(mListMaster.size() - 1).getSn() + " terlebih dahulu.", getActivity());
                    //vc.setText("");
                    setSelectHw("VC", re);
                } else {
                    //vc.setText(re);
                    setSelectHw("VC", re);
                }
            } else if (isLnb) {
                params = new String[2];
                params[0] = "LNB";
                params[1] = "ADD";
                mListMaster = db._fetchDesc(TableFreeTrialCoh.KEY_HW_TYPE, TableFreeTrialCoh.KEY_HW_STATUS, "LNB", "ADD");
                if (mListMaster.size() > 0
                        && !mListMaster.get(mListMaster.size() - 1).getSn().equalsIgnoreCase(re)) {
                    //utils.showErrorDlg(handler, "Silahkan Pasang " + mListMaster.get(mListMaster.size() - 1).getSn() + " terlebih dahulu.", getActivity());
                    //lnb.setText("");
                    setSelectHw("LNB", re);
                } else {
                    //lnb.setText(re);
                    setSelectHw("LNB", re);
                }
            } else if (isDsd) {
                params = new String[2];
                params[0] = "DSD";
                params[1] = "ADD";
                mListMaster = db._fetchDesc(TableFreeTrialCoh.KEY_HW_TYPE, TableFreeTrialCoh.KEY_HW_STATUS, "DSD", "ADD");
                if (mListMaster.size() == 0) {
                    params[0] = "DEC";
                    mListMaster = db._fetchDescSingleParam(TableFreeTrialCoh.KEY_HW_TYPE, "DEC");
                }

                if (mListMaster.size() > 0
                        && !mListMaster.get(mListMaster.size() - 1).getSn().equalsIgnoreCase(re)) {
                    //utils.showErrorDlg(handler, "Silahkan Pasang "+ mListMaster.get(mListMaster.size()-1).getSn()+" terlebih dahulu.", getActivity());
                    //dsd.setText(re);
                    //dsd.setText("");
                    setSelectHw("DEC", re);
                } else {
                    //dsd.setText(re);
                    setSelectHw("DEC", re);
                }

            } else if (isDish) {
                params = new String[2];
                params[0] = "ODU";
                params[1] = "ADD";
                mListMaster = db._fetchDesc(TableFreeTrialCoh.KEY_HW_TYPE, TableFreeTrialCoh.KEY_HW_STATUS, "ODU", "ADD");
                if (mListMaster.size() == 0) {
                    params[0] = "ANT";
                    mListMaster = db._fetchDescSingleParam(TableFreeTrialCoh.KEY_HW_TYPE, "ANT");
                }

                if (mListMaster.size() > 0
                        && !mListMaster.get(mListMaster.size() - 1).getSn().equalsIgnoreCase(re)) {
                    //utils.showErrorDlg(handler, "Silahkan Pasang "+ mListMaster.get(mListMaster.size()-1).getSn()+" terlebih dahulu.", getActivity());
                    //dish.setText(re);
                    //spinDish.setSelection(0);
                    setSelectHw("ANT", re);
                } else {
                    //dish.setText(re);
                    setSelectHw("ANT", re);
                }
            } else {
                /*vc.setText("");
                lnb.setText("");
                dsd.setText("");
                dish.setText("");*/
                spinVc.setSelection(0);
                spinDsd.setSelection(0);
                spinDish.setSelection(0);
                spinLnb.setSelection(0);
            }
        }
    }

    /**
     * Method for create json from form value
     *
     * @return
     * @throws Exception
     */
    private String jsonValueCreator() throws Exception {
        JSONObject obj = new JSONObject();
        return obj.toString();
    }

    private String vcValidation(String vc) {
        String tmp, tmp1, tmpBuff = "", tmpRes, stmpCountS, vc01;
        int tmpCount = 0;
        vc01 = vc.substring(0, 2);
        if (vc.length() != 14) {
            Toast.makeText(getActivity(), "Harus sama dengan 14", Toast.LENGTH_LONG).show();
            return "";
        } else if (!vc01.equalsIgnoreCase("01")) {
            Toast.makeText(getActivity(), "Error Header", Toast.LENGTH_LONG).show();
            return "";
        } else {
            tmp = vc.substring(0, vc.length() - 8);
            tmp1 = vc.substring(vc.length() - 8, 13);
            for (int m = 1; m < 8; m++) {
                if ((m % 2) == 0) {
                    tmpBuff = tmpBuff + String.valueOf(Integer.parseInt(tmp1.substring(m - 1, m)) * 1);
                } else {
                    tmpBuff = tmpBuff + String.valueOf(Integer.parseInt(tmp1.substring(m - 1, m)) * 2);
                }
            }

            for (int n = 1; n < tmpBuff.length() + 1; n++) {
                tmpCount = tmpCount + Integer.parseInt(tmpBuff.substring(n - 1, n));
            }

            stmpCountS = String.valueOf(tmpCount);
            tmpRes = String.valueOf(10 - Integer.parseInt(stmpCountS.substring(stmpCountS.length() - 1)));
            tmpRes = tmpRes.substring(tmpRes.length() - 1);
        }
        return tmp + tmp1 + tmpRes;
    }
}
