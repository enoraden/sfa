package myskysfa.com.sfa.main.menu.dtdproject;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.TableZipcode;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.database.db_adapter.TableZipcodeAdapter;
import myskysfa.com.sfa.main.menu.MainFragmentDTD;
import myskysfa.com.sfa.main.menu.ms.ShowListDropDown;
import myskysfa.com.sfa.utils.AlbumStorageDirFactory;
import myskysfa.com.sfa.utils.BaseAlbumDirFactory;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by Eno on 10/24/2016.
 */
public class ProProfile extends Fragment implements View.OnClickListener, View.OnLongClickListener {
    private ViewGroup root;
    private Button save;
    private EditText identity_no, first_name, middle_name, last_name, birth_place, address, no_home,
            rt, rw, install_address, patokan, billing_address, telp, hp, email;
    private TextView form_no, list_identity, gender, religion, birth_date, provinci_identity, kota_identity,
            kecamatan_identity, kelurahan_identity, zipcode_identity, valid_period, install_date,
            install_time, time_confirm_start, time_confirm_end, provinci_install, kota_install,
            kecamatan_install, kelurahan_install, zipcode_install, provinci_billing, kota_billing,
            kecamatan_billing, kelurahan_billing, zipcode_billing, home_kind, home_status, job, income,
            hardwareStatus, idPln, rt_install, rt_bill, rw_install, rw_bill, no_home_install,
            no_home_bill;

    private String uId = "", value;

    private TextView txt, codezip;
    private CheckBox c_install, c_billing, c_zipcode;
    private ShowListDropDown showList;
    private Utils utils;
    private String picturePath, imageName;
    private File f;
    private ImageView prev;
    private static final int PICK_IMAGE_INSTALASI = 313;
    private static final int TAKE_PICTURE_INSTALASI = 333;
    private static final int PICK_IMAGE_LINGKUNGAN = 414;
    private static final int TAKE_PICTURE_LINGKUNGAN = 444;
    private static final int PICK_IMAGE_RUMAH = 121;
    private static final int TAKE_PICTURE_RUMAH = 111;
    private static final int PICK_IMAGE_KTP = 505;
    private static final int TAKE_PICTURE_KTP = 555;
    private static final int PICK_IMAGE_BB = 605;
    private static final int TAKE_PICTURE_BB = 666;
    private static final int PICK_IMAGE_PLN = 705;
    private static final int TAKE_PICTURE_PLN = 777;
    ArrayList<String> pathArraysAll = new ArrayList<String>();
    private static Bitmap bitmapInstalasi, bitmapLingkungan, bitmapRumah, bitmapKtp,
            bitmapBB, bitmapPln;
    private static List<ProProfile.PathListItem> listPathIdentitas;
    private static List<ProProfile.PathListItem> listPathLingkungan;
    private static List<ProProfile.PathListItem> listPathRumah;
    private static List<ProProfile.PathListItem> listPathKtp;
    private static List<ProProfile.PathListItem> listPathBB;
    private static List<ProProfile.PathListItem> listPathPln;

    private ArrayList<String> rumah = new ArrayList<String>();
    private ArrayList<String> lingkungan = new ArrayList<String>();
    private ArrayList<String> instalasi = new ArrayList<String>();
    private ArrayList<String> ktp = new ArrayList<String>();
    private ArrayList<String> bb = new ArrayList<String>();
    private ArrayList<String> pln = new ArrayList<String>();

    private ArrayList<String> rumah_name = new ArrayList<String>();
    private ArrayList<String> lingkungan_name = new ArrayList<String>();
    private ArrayList<String> instalasi_name = new ArrayList<String>();
    private ArrayList<String> ktp_name = new ArrayList<String>();
    private ArrayList<String> bb_name = new ArrayList<String>();
    private ArrayList<String> pln_name = new ArrayList<String>();

    private ArrayList<String> listTblName, listTblID;
    private LinearLayout mLayoutPackage;
    private ImageButton ib_instalasi, ib_lingkungan, ib_rumah, ib_ktp, ib_bb, ib_pln;
    private SharedPreferences sm;

    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private static Context _contex;
    private TableZipcodeAdapter zipcodeAdapter;
    private List<TableZipcode> listTblZipCode;

    private static String provinceID, cityID, kecID, kelID;
    private TableFormAppAdapter tblFormApp;
    private Boolean isSendImage = false;

    public static ProProfile newInstance(Context contex) {
        _contex = contex;
        ProProfile fragment = new ProProfile();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.pro_profile, container, false);
        ConstructorInit();
        initView(root);
        return root;
    }

    private void ConstructorInit() {
        utils = new Utils(getActivity());
        zipcodeAdapter = new TableZipcodeAdapter(getActivity());
        tblFormApp = new TableFormAppAdapter(getActivity());
    }

    private void initView(ViewGroup root) {
        form_no = (TextView) root.findViewById(R.id.form_no);
        identity_no = (EditText) root.findViewById(R.id.identity_no);
        first_name = (EditText) root.findViewById(R.id.first_name);
        middle_name = (EditText) root.findViewById(R.id.middle_name);
        last_name = (EditText) root.findViewById(R.id.last_name);
        birth_place = (EditText) root.findViewById(R.id.birthplace);
        address = (EditText) root.findViewById(R.id.address);
        no_home = (EditText) root.findViewById(R.id.home_no);
        identity_no = (EditText) root.findViewById(R.id.identity_no);
        rt = (EditText) root.findViewById(R.id.rt);
        rw = (EditText) root.findViewById(R.id.rw);
        install_address = (EditText) root.findViewById(R.id.install_address);
        patokan = (EditText) root.findViewById(R.id.direction);
        billing_address = (EditText) root.findViewById(R.id.billing_address);
        telp = (EditText) root.findViewById(R.id.telephon);
        hp = (EditText) root.findViewById(R.id.hp);
        email = (EditText) root.findViewById(R.id.email_address);
        rt_install = (EditText) root.findViewById(R.id.rt_install);
        rt_bill = (EditText) root.findViewById(R.id.rt_bill);
        rw_install = (EditText) root.findViewById(R.id.rw_install);
        rw_bill = (EditText) root.findViewById(R.id.rw_bill);
        no_home_install = (EditText) root.findViewById(R.id.home_no_install);
        no_home_bill = (EditText) root.findViewById(R.id.home_no_bill);

        list_identity = (TextView) root.findViewById(R.id.identity_list);
        gender = (TextView) root.findViewById(R.id.gender);
        religion = (TextView) root.findViewById(R.id.religion);
        birth_date = (TextView) root.findViewById(R.id.birth_date);
        provinci_identity = (TextView) root.findViewById(R.id.province);
        kota_identity = (TextView) root.findViewById(R.id.city);
        kecamatan_identity = (TextView) root.findViewById(R.id.kecamatan);
        kelurahan_identity = (TextView) root.findViewById(R.id.kelurahan);
        zipcode_identity = (TextView) root.findViewById(R.id.zip_code);
        valid_period = (TextView) root.findViewById(R.id.validity_period);
        install_date = (TextView) root.findViewById(R.id.install_date);
        install_time = (TextView) root.findViewById(R.id.install_time);
        time_confirm_start = (TextView) root.findViewById(R.id.confirm_time_start);
        time_confirm_end = (TextView) root.findViewById(R.id.confirm_time_end);
        provinci_install = (TextView) root.findViewById(R.id.province_install);
        kota_install = (TextView) root.findViewById(R.id.city_install);
        kecamatan_install = (TextView) root.findViewById(R.id.kecamatan_install);
        kelurahan_install = (TextView) root.findViewById(R.id.kelurahan_install);
        zipcode_install = (TextView) root.findViewById(R.id.zipcode_install);
        provinci_billing = (TextView) root.findViewById(R.id.province_billing);
        kota_billing = (TextView) root.findViewById(R.id.city_billing);
        kecamatan_billing = (TextView) root.findViewById(R.id.kecamatan_billing);
        kelurahan_billing = (TextView) root.findViewById(R.id.kelurahan_billing);
        zipcode_billing = (TextView) root.findViewById(R.id.zipcode_billing);
        home_kind = (TextView) root.findViewById(R.id.home_kind);
        home_status = (TextView) root.findViewById(R.id.home_status);
        job = (TextView) root.findViewById(R.id.job);
        income = (TextView) root.findViewById(R.id.income);
        hardwareStatus = (TextView) root.findViewById(R.id.sh);
        idPln = (EditText) root.findViewById(R.id.pln);

        c_install = (CheckBox) root.findViewById(R.id.install_checkbox);
        c_billing = (CheckBox) root.findViewById(R.id.bill_checkbox);
        c_zipcode = (CheckBox) root.findViewById(R.id.zipcode_checkbox);

        ib_instalasi = (ImageButton) root.findViewById(R.id.ib_instalasi);
        ib_lingkungan = (ImageButton) root.findViewById(R.id.ib_lingkungan);
        ib_rumah = (ImageButton) root.findViewById(R.id.ib_rumah);
        ib_ktp = (ImageButton) root.findViewById(R.id.ib_ktp);
        ib_bb = (ImageButton) root.findViewById(R.id.ib_bb);
        ib_pln = (ImageButton) root.findViewById(R.id.ib_pln);

        save = (Button) root.findViewById(R.id.save);

        list_identity.setOnClickListener(this);
        gender.setOnClickListener(this);
        religion.setOnClickListener(this);
        birth_date.setOnClickListener(this);
        install_time.setOnClickListener(this);
        install_date.setOnClickListener(this);
        valid_period.setOnClickListener(this);
        time_confirm_start.setOnClickListener(this);
        time_confirm_end.setOnClickListener(this);
        home_kind.setOnClickListener(this);
        home_status.setOnClickListener(this);
        job.setOnClickListener(this);
        income.setOnClickListener(this);
        hardwareStatus.setOnClickListener(this);

        ib_instalasi.setOnClickListener(this);
        ib_lingkungan.setOnClickListener(this);
        ib_rumah.setOnClickListener(this);
        ib_ktp.setOnClickListener(this);
        ib_bb.setOnClickListener(this);
        ib_pln.setOnClickListener(this);

        c_install.setOnClickListener(this);
        c_billing.setOnClickListener(this);
        c_zipcode.setOnClickListener(this);

        provinci_install.setOnClickListener(this);
        kota_install.setOnClickListener(this);
        kecamatan_install.setOnClickListener(this);
        kelurahan_install.setOnClickListener(this);

        provinci_billing.setOnClickListener(this);
        kota_billing.setOnClickListener(this);
        kecamatan_billing.setOnClickListener(this);
        kelurahan_billing.setOnClickListener(this);

        provinci_identity.setOnClickListener(this);
        kota_identity.setOnClickListener(this);
        kecamatan_identity.setOnClickListener(this);
        kelurahan_identity.setOnClickListener(this);


        birth_date.setOnLongClickListener(this);
        valid_period.setOnLongClickListener(this);
        install_date.setOnLongClickListener(this);

        save.setOnClickListener(this);

        mAlbumStorageDirFactory = new BaseAlbumDirFactory();


        sm = getActivity().getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        uId = sm.getString(TableLogLogin.C_UID, "");


    }

    /**
     * OnClick Listener TextView
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.identity_list:
                showList = new ShowListDropDown(getActivity(), v, "identity",
                        list_identity, getResources().getStringArray(R.array.type_identity));
                showList.Show();
                break;
            case R.id.gender:
                showList = new ShowListDropDown(getActivity(), v, "gender",
                        gender, getResources().getStringArray(R.array.gender_list));
                showList.Show();
                break;
            case R.id.religion:
                showList = new ShowListDropDown(getActivity(), v, "Agama",
                        religion, getResources().getStringArray(R.array.religion));
                showList.Show();
                break;
            case R.id.birth_date:
                utils.setDateField(getActivity(), birth_date);
                break;
            case R.id.install_time:
                utils.setTimeField(getActivity(), install_time);
                break;
            case R.id.validity_period:
                utils.setDateField(getActivity(), valid_period);
                break;
            case R.id.install_date:
                utils.setDateField(getActivity(), install_date);
                break;
            case R.id.confirm_time_start:
                utils.setTimeField(getActivity(), time_confirm_start);
                break;
            case R.id.confirm_time_end:
                utils.setTimeField(getActivity(), time_confirm_end);
                break;
            case R.id.home_kind:
                showList = new ShowListDropDown(getActivity(), v, "Jenis Rumah",
                        home_kind, getResources().getStringArray(R.array.home_tipe));
                showList.Show();
                break;
            case R.id.home_status:
                showList = new ShowListDropDown(getActivity(), v, "Status Rumah",
                        home_status, getResources().getStringArray(R.array.home_owner));
                showList.Show();
                break;
            case R.id.job:
                showList = new ShowListDropDown(getActivity(), v, "Pekerjaan",
                        job, getResources().getStringArray(R.array.pekerjaan));
                showList.Show();
                break;
            case R.id.income:
                showList = new ShowListDropDown(getActivity(), v, "Penghasilan",
                        income, getResources().getStringArray(R.array.penghasilan));
                showList.Show();
                break;
            case R.id.install_checkbox:
                CheckListInstall();
                break;
            case R.id.bill_checkbox:
                CheckListBilling();
                break;
            case R.id.zipcode_checkbox:
                CheckListZipcode();
                break;
            case R.id.ib_instalasi:
                //TakeChooseImage(v);
                takeImage(TAKE_PICTURE_INSTALASI);
                break;
            case R.id.ib_lingkungan:
                //TakeChooseImage(v);
                takeImage(TAKE_PICTURE_LINGKUNGAN);
                break;
            case R.id.ib_rumah:
                takeImage(TAKE_PICTURE_RUMAH);
                //TakeChooseImage(v);
                break;
            case R.id.ib_ktp:
                takeImage(TAKE_PICTURE_KTP);
                //TakeChooseImage(v);
                break;
            case R.id.ib_bb:
                takeImage(TAKE_PICTURE_BB);
                //TakeChooseImage(v);
                break;
            case R.id.ib_pln:
                takeImage(TAKE_PICTURE_PLN);
                //TakeChooseImage(v);
                break;
            case R.id.sh:
                showList = new ShowListDropDown(getActivity(), v, "Hardware Status", hardwareStatus,
                        getResources().getStringArray(R.array.hardware_status));
                showList.Show();
                removeAllImage();
                break;
            case R.id.province_install:
            case R.id.province:
            case R.id.province_billing:
                if (v.getId() == R.id.province_install) {
                    txt = provinci_install;

                    kota_install.setText("");
                    kecamatan_install.setText("");
                    kelurahan_install.setText("");
                    zipcode_install.setText("");
                } else if (v.getId() == R.id.province) {
                    txt = provinci_identity;

                    kota_identity.setText("");
                    kecamatan_identity.setText("");
                    kelurahan_identity.setText("");
                    zipcode_identity.setText("");
                } else {
                    txt = provinci_billing;

                    kota_billing.setText("");
                    kecamatan_billing.setText("");
                    kelurahan_billing.setText("");
                    zipcode_billing.setText("");
                }
                getDataZipcode(v, null);
                showList = new ShowListDropDown(getActivity(), v, "Pilih provinsi", txt,
                        listTblName, listTblID, null);
                showList.ShowZipcode();
                break;
            case R.id.city_install:
            case R.id.city:
            case R.id.city_billing:
                if (v.getId() == R.id.city_install) {
                    txt = kota_install;
                    provinceID = showList.getProvIDInstall();

                    kecamatan_install.setText("");
                    kelurahan_install.setText("");
                    zipcode_install.setText("");
                } else if (v.getId() == R.id.city) {
                    txt = kota_identity;
                    provinceID = showList.getProvID();

                    kecamatan_identity.setText("");
                    kelurahan_identity.setText("");
                    zipcode_identity.setText("");
                } else {
                    txt = kota_billing;
                    provinceID = showList.getProvIDBilling();

                    kecamatan_billing.setText("");
                    kelurahan_billing.setText("");
                    zipcode_billing.setText("");
                }
                getDataZipcode(v, provinceID);
                showList = new ShowListDropDown(getActivity(), v, "Pilih Kota", txt,
                        listTblName, listTblID, null);
                showList.ShowZipcode();
                break;
            case R.id.kecamatan_install:
            case R.id.kecamatan:
            case R.id.kecamatan_billing:
                if (v.getId() == R.id.kecamatan_install) {
                    txt = kecamatan_install;
                    cityID = showList.getCityIDInstall();

                    kelurahan_install.setText("");
                    zipcode_install.setText("");
                } else if (v.getId() == R.id.kecamatan) {
                    txt = kecamatan_identity;
                    cityID = showList.getCityID();

                    kelurahan_identity.setText("");
                    zipcode_identity.setText("");
                } else {
                    txt = kecamatan_billing;
                    cityID = showList.getCityIDBilling();

                    kelurahan_billing.setText("");
                    zipcode_billing.setText("");
                }
                getDataZipcode(v, cityID);
                showList = new ShowListDropDown(getActivity(), v, "Pilih kecamatan", txt,
                        listTblName, listTblID, null);
                showList.ShowZipcode();
                break;
            case R.id.kelurahan_install:
            case R.id.kelurahan:
            case R.id.kelurahan_billing:
                if (v.getId() == R.id.kelurahan_install) {
                    txt = kelurahan_install;
                    kecID = showList.getKecIDInstall();
                    kelID = showList.getKelIDInstall();
                    codezip = zipcode_install;
                    zipcode_install.setText("");
                } else if (v.getId() == R.id.kelurahan) {
                    txt = kelurahan_identity;
                    kecID = showList.getKecID();
                    kelID = showList.getKelID();
                    codezip = zipcode_identity;
                    zipcode_identity.setText("");
                } else {
                    txt = kelurahan_billing;
                    kecID = showList.getKecIDBilling();
                    kelID = showList.getKelIDBilling();
                    codezip = zipcode_billing;
                    zipcode_billing.setText("");
                }
                getDataZipcode(v, kecID);
                showList = new ShowListDropDown(getActivity(), v, "Pilih kelurahan", txt,
                        listTblName, listTblID, codezip);
                showList.ShowZipcode();
                break;
            case R.id.save:
                checkMandatoryForm();
                //saveImage();
                break;

        }

    }

    /**
     * getData From DB
     */

    private void getDataZipcode(View v, @Nullable String params) {
        listTblName = new ArrayList<String>();
        listTblID = new ArrayList<String>();

        switch (v.getId()) {
            case R.id.province_install:
            case R.id.province:
            case R.id.province_billing:
                try {
                    listTblZipCode = zipcodeAdapter.listProvinsi();
                    for (int i = 0; i < listTblZipCode.size(); i++) {
                        TableZipcode item = listTblZipCode.get(i);
                        listTblID.add(item.getDISTRICT_CODE());
                        listTblName.add(item.getDISTRICT_NAME());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.city_install:
            case R.id.city:
            case R.id.city_billing:
                try {
                    listTblZipCode = zipcodeAdapter.listCity(params);
                    for (int i = 0; i < listTblZipCode.size(); i++) {
                        TableZipcode item = listTblZipCode.get(i);
                        listTblID.add(item.getCITY_CODE());
                        listTblName.add(item.getCITY_NAME());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.kecamatan_install:
            case R.id.kecamatan:
            case R.id.kecamatan_billing:
                try {
                    listTblZipCode = zipcodeAdapter.listKecamatan(params);
                    for (int i = 0; i < listTblZipCode.size(); i++) {
                        TableZipcode item = listTblZipCode.get(i);
                        listTblID.add(item.getAREA_CODE());
                        listTblName.add(item.getAREA_NAME());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.kelurahan_install:
            case R.id.kelurahan:
            case R.id.kelurahan_billing:
                try {
                    listTblZipCode = zipcodeAdapter.listKelurahan(params);
                    for (int i = 0; i < listTblZipCode.size(); i++) {
                        TableZipcode item = listTblZipCode.get(i);
                        listTblID.add(item.getSTREET_CODE());
                        listTblName.add(item.getSTREET_NAME());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

        }

    }


    private void CheckListInstall() {
        if (c_install.isChecked()) {
            install_address.setText(address.getText().toString());
            provinci_install.setText(provinci_identity.getText().toString());
            kota_install.setText(kota_identity.getText().toString());
            kecamatan_install.setText(kecamatan_identity.getText().toString());
            kelurahan_install.setText(kelurahan_identity.getText().toString());
            zipcode_install.setText(zipcode_identity.getText().toString());
            no_home_install.setText(no_home.getText().toString());
            rt_install.setText(rt.getText().toString());
            rw_install.setText(rw.getText().toString());
            c_install.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    install_address.setText(address.getText().toString());
                    provinci_install.setText(provinci_identity.getText().toString());
                    kota_install.setText(kota_identity.getText().toString());
                    kecamatan_install.setText(kecamatan_identity.getText().toString());
                    kelurahan_install.setText(kelurahan_identity.getText().toString());
                    zipcode_install.setText(zipcode_identity.getText().toString());
                    no_home_install.setText(no_home.getText().toString());
                    rt_install.setText(rt.getText().toString());
                    rw_install.setText(rw.getText().toString());
                }
            });
        } else {
            install_address.setText("");
            provinci_install.setText("");
            kota_install.setText("");
            kecamatan_install.setText("");
            kelurahan_install.setText("");
            zipcode_install.setText("");
            no_home_install.setText("");
            rt_install.setText("");
            rw_install.setText("");
            c_install.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        install_address.setText("");
                        provinci_install.setText("");
                        kota_install.setText("");
                        kecamatan_install.setText("");
                        kelurahan_install.setText("");
                        zipcode_install.setText("");
                        no_home_install.setText("");
                        rt_install.setText("");
                        rw_install.setText("");
                    } catch (Exception e) {
                    }
                }
            });
        }

    }

    private void CheckListBilling() {
        if (c_billing.isChecked()) {
            c_zipcode.setChecked(false);
            billing_address.setText(address.getText().toString());
            provinci_billing.setText(provinci_identity.getText().toString());
            kota_billing.setText(kota_identity.getText().toString());
            kecamatan_billing.setText(kecamatan_identity.getText().toString());
            kelurahan_billing.setText(kelurahan_identity.getText().toString());
            zipcode_billing.setText(zipcode_identity.getText().toString());
            no_home_bill.setText(no_home.getText().toString());
            rt_bill.setText(rt.getText().toString());
            rw_bill.setText(rw.getText().toString());
            c_billing.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    c_zipcode.setChecked(false);
                    billing_address.setText(address.getText().toString());
                    provinci_billing.setText(provinci_identity.getText().toString());
                    kota_billing.setText(kota_identity.getText().toString());
                    kecamatan_billing.setText(kecamatan_identity.getText().toString());
                    kelurahan_billing.setText(kelurahan_identity.getText().toString());
                    zipcode_billing.setText(zipcode_identity.getText().toString());
                    no_home_bill.setText(no_home.getText().toString());
                    rt_bill.setText(rt.getText().toString());
                    rw_bill.setText(rw.getText().toString());
                }
            });
        } else {
            billing_address.setText("");
            provinci_billing.setText("");
            kota_billing.setText("");
            kecamatan_billing.setText("");
            kelurahan_billing.setText("");
            zipcode_billing.setText("");
            no_home_bill.setText("");
            rt_bill.setText("");
            rw_bill.setText("");
            c_billing.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    billing_address.setText("");
                    provinci_billing.setText("");
                    kota_billing.setText("");
                    kecamatan_billing.setText("");
                    kelurahan_billing.setText("");
                    zipcode_billing.setText("");
                    no_home_bill.setText("");
                    rt_bill.setText("");
                    rw_bill.setText("");
                }
            });
        }
    }

    private void CheckListZipcode() {
        if (c_zipcode.isChecked()) {
            c_billing.setChecked(false);
            billing_address.setText(install_address.getText().toString());
            provinci_billing.setText(provinci_install.getText().toString());
            kota_billing.setText(kota_install.getText().toString());
            kecamatan_billing.setText(kecamatan_install.getText().toString());
            kelurahan_billing.setText(kelurahan_install.getText().toString());
            zipcode_billing.setText(zipcode_install.getText().toString());
            no_home_bill.setText(no_home_install.getText().toString());
            rt_bill.setText(rt_install.getText().toString());
            rw_bill.setText(rw_install.getText().toString());
            c_zipcode.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    c_billing.setChecked(false);
                    billing_address.setText(install_address.getText().toString());
                    provinci_billing.setText(provinci_install.getText().toString());
                    kota_billing.setText(kota_install.getText().toString());
                    kecamatan_billing.setText(kecamatan_install.getText().toString());
                    kelurahan_billing.setText(kelurahan_install.getText().toString());
                    zipcode_billing.setText(zipcode_install.getText().toString());
                    no_home_bill.setText(no_home_install.getText().toString());
                    rt_bill.setText(rt_install.getText().toString());
                    rw_bill.setText(rw_install.getText().toString());
                }
            });
        } else {
            billing_address.setText("");
            provinci_billing.setText("");
            kota_billing.setText("");
            kecamatan_billing.setText("");
            kelurahan_billing.setText("");
            zipcode_billing.setText("");
            no_home_bill.setText("");
            rt_bill.setText("");
            rw_bill.setText("");
            c_zipcode.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    billing_address.setText("");
                    provinci_billing.setText("");
                    kota_billing.setText("");
                    kecamatan_billing.setText("");
                    kelurahan_billing.setText("");
                    zipcode_billing.setText("");
                    no_home_bill.setText("");
                    rt_bill.setText("");
                    rw_bill.setText("");
                }
            });
        }
    }

    /*private void TakeChooseImage(final View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setMessage("Type of Upload")
                .setPositiveButton("Choose From File", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (view.getId()) {
                            case R.id.ib_instalasi:
                                selectImageFromGallery(PICK_IMAGE_INSTALASI);
                                break;
                            case R.id.ib_lingkungan:
                                selectImageFromGallery(PICK_IMAGE_LINGKUNGAN);
                                break;
                            case R.id.ib_rumah:
                                selectImageFromGallery(PICK_IMAGE_RUMAH);
                                break;
                            case R.id.ib_ktp:
                                selectImageFromGallery(PICK_IMAGE_KTP);
                                break;
                            case R.id.ib_bb:
                                selectImageFromGallery(PICK_IMAGE_BB);
                                break;
                            case R.id.ib_pln:
                                selectImageFromGallery(PICK_IMAGE_PLN);
                                break;
                        }
                    }
                })
                .setNegativeButton("Take Picture", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (view.getId()) {
                            case R.id.ib_instalasi:
                                takeImage(TAKE_PICTURE_INSTALASI);
                                break;
                            case R.id.ib_lingkungan:
                                takeImage(TAKE_PICTURE_LINGKUNGAN);
                                break;
                            case R.id.ib_rumah:
                                takeImage(TAKE_PICTURE_RUMAH);
                                break;
                            case R.id.ib_ktp:
                                takeImage(TAKE_PICTURE_KTP);
                                break;
                            case R.id.ib_bb:
                                takeImage(TAKE_PICTURE_BB);
                                break;
                            case R.id.ib_pln:
                                takeImage(TAKE_PICTURE_PLN);
                                break;
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }*/

    /*public void selectImageFromGallery(int code) {
        Intent intent = new Intent();
        intent.setType("image*//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                code);
    }*/

    public void takeImage(int code) {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            f = null;
            f = createImageFile(code);
            picturePath = f.getAbsolutePath();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            getActivity().startActivityForResult(takePictureIntent, code);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private File createImageFile(int code) throws IOException {
        String imageFileName = "";
        switch (code) {
            case TAKE_PICTURE_INSTALASI:
                imageFileName = form_no.getText().toString() + "_instalasi" + "_";
                break;
            case TAKE_PICTURE_LINGKUNGAN:
                imageFileName = form_no.getText().toString() + "_lingkungan" + "_";
                break;
            case TAKE_PICTURE_RUMAH:
                imageFileName = form_no.getText().toString() + "_rumah" + "_";
                break;
            case TAKE_PICTURE_KTP:
                imageFileName = form_no.getText().toString() + "_ktp" + "_";
                break;
            case TAKE_PICTURE_BB:
                imageFileName = form_no.getText().toString() + "_bb" + "_";
                break;
            case TAKE_PICTURE_PLN:
                imageFileName = form_no.getText().toString() + "_pln" + "_";
                break;
        }

        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, ".jpg", albumF);
        return imageF;
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir("SFA");
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (PICK_IMAGE_INSTALASI):
                if (resultCode == Activity.RESULT_OK) {
                    GetPickImage(PICK_IMAGE_INSTALASI, requestCode, data);
                }
                break;
            case (PICK_IMAGE_LINGKUNGAN):
                if (resultCode == Activity.RESULT_OK) {
                    GetPickImage(PICK_IMAGE_LINGKUNGAN, requestCode, data);
                }
                break;
            case (PICK_IMAGE_RUMAH):
                if (resultCode == Activity.RESULT_OK) {
                    GetPickImage(PICK_IMAGE_RUMAH, requestCode, data);
                }
                break;
            case (PICK_IMAGE_KTP):
                if (resultCode == Activity.RESULT_OK) {
                    GetPickImage(PICK_IMAGE_KTP, requestCode, data);
                }
                break;
            case (PICK_IMAGE_BB):
                if (resultCode == Activity.RESULT_OK) {
                    GetPickImage(PICK_IMAGE_BB, requestCode, data);
                }

                break;
            case (PICK_IMAGE_PLN):
                if (resultCode == Activity.RESULT_OK) {
                    GetPickImage(PICK_IMAGE_PLN, requestCode, data);
                }

                break;
            case (TAKE_PICTURE_INSTALASI):
                if (resultCode == Activity.RESULT_OK) {
                    {
                        listPathIdentitas = new ArrayList<ProProfile.PathListItem>();
                        setPic(bitmapInstalasi, TAKE_PICTURE_INSTALASI);
                        galleryAddPic();
                    }
                }
                break;
            case TAKE_PICTURE_LINGKUNGAN:
                if (resultCode == Activity.RESULT_OK) {
                    {
                        listPathLingkungan = new ArrayList<ProProfile.PathListItem>();
                        setPic(bitmapLingkungan, TAKE_PICTURE_LINGKUNGAN);
                        galleryAddPic();

                    }
                }
                break;
            case TAKE_PICTURE_RUMAH:
                if (resultCode == Activity.RESULT_OK) {
                    {
                        listPathRumah = new ArrayList<ProProfile.PathListItem>();
                        setPic(bitmapRumah, TAKE_PICTURE_RUMAH);
                        galleryAddPic();
                    }
                }
                break;
            case TAKE_PICTURE_KTP:
                if (resultCode == Activity.RESULT_OK) {
                    {
                        listPathKtp = new ArrayList<ProProfile.PathListItem>();
                        setPic(bitmapKtp, TAKE_PICTURE_KTP);
                        galleryAddPic();
                    }
                }
                break;
            case TAKE_PICTURE_BB:
                if (resultCode == Activity.RESULT_OK) {
                    {
                        listPathBB = new ArrayList<ProProfile.PathListItem>();
                        setPic(bitmapBB, TAKE_PICTURE_BB);
                        galleryAddPic();
                    }
                }
                break;
            case TAKE_PICTURE_PLN:
                if (resultCode == Activity.RESULT_OK) {
                    {
                        listPathPln = new ArrayList<ProProfile.PathListItem>();
                        setPic(bitmapPln, TAKE_PICTURE_PLN);
                        galleryAddPic();
                    }
                }
                break;
        }
    }

    private void setPic(Bitmap bitmap, final int code) {
        /* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picturePath, bmOptions);
        bmOptions.inJustDecodeBounds = false;
        final int REQUIRED_SIZE = 1024;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = bmOptions.outWidth, height_tmp = bmOptions.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        bmOptions.inSampleSize = scale;
        /* Decode the JPEG file into a Bitmap */
        bitmap = BitmapFactory.decodeFile(picturePath, bmOptions);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View root = inflater.inflate(R.layout.image_preview, null);
        prev = (ImageView) root.findViewById(R.id.imagepreview);
        alertDialog.setView(root);
        alertDialog.setTitle("Image Preview");
        alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                File file = new File(picturePath);
                imageName = file.getName();
                switch (code) {
                    case TAKE_PICTURE_INSTALASI:
                        addViewImageList(TAKE_PICTURE_INSTALASI, instalasi, imageName, picturePath);
                        break;
                    case TAKE_PICTURE_LINGKUNGAN:
                        addViewImageList(TAKE_PICTURE_LINGKUNGAN, lingkungan, imageName, picturePath);
                        break;
                    case TAKE_PICTURE_RUMAH:
                        addViewImageList(TAKE_PICTURE_RUMAH, rumah, imageName, picturePath);
                        break;
                    case TAKE_PICTURE_KTP:
                        addViewImageList(TAKE_PICTURE_KTP, ktp, imageName, picturePath);
                        break;
                    case TAKE_PICTURE_BB:
                        addViewImageList(TAKE_PICTURE_BB, bb, imageName, picturePath);
                        break;
                    case TAKE_PICTURE_PLN:
                        addViewImageList(TAKE_PICTURE_PLN, pln, imageName, picturePath);
                        break;
                }

            }
        });
        alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //Cancel set Visible Bitmap
            }
        });

		/* Associate the Bitmap to the ImageView */
        prev.setImageBitmap(bitmap);
        prev.setVisibility(View.VISIBLE);
        alertDialog.show();
        //mVideoView.setVisibility(View.INVISIBLE);
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(picturePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private void GetPickImage(int pick, int requestCode, Intent data) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn,
                null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        picturePath = cursor.getString(columnIndex);
        cursor.close();
        //Get Image Name
        File f = new File(picturePath);
        switch (pick) {
            case PICK_IMAGE_INSTALASI:
                if (!form_no.getText().toString().equalsIgnoreCase("")) {
                    imageName = utils.setImageName(form_no.getText().toString(), "instalasi", f.getName());
                } else {
                    imageName = utils.setImageName(utils.getCurrentDateandTimeMerge(), "instalasi", f.getName());
                }
                break;
            case PICK_IMAGE_RUMAH:
                if (!form_no.getText().toString().equalsIgnoreCase("")) {
                    imageName = utils.setImageName(form_no.getText().toString(), "rumah", f.getName());
                } else {
                    imageName = utils.setImageName(utils.getCurrentDateandTimeMerge(), "rumah", f.getName());
                }
                break;
            case PICK_IMAGE_LINGKUNGAN:
                if (!form_no.getText().toString().equalsIgnoreCase("")) {
                    imageName = utils.setImageName(form_no.getText().toString(), "lingkungan", f.getName());
                } else {
                    imageName = utils.setImageName(utils.getCurrentDateandTimeMerge(), "lingkungan", f.getName());
                }
                break;
            case PICK_IMAGE_KTP:
                if (!form_no.getText().toString().equalsIgnoreCase("")) {
                    imageName = utils.setImageName(form_no.getText().toString(), "ktp", f.getName());
                } else {
                    imageName = utils.setImageName(utils.getCurrentDateandTimeMerge(), "ktp", f.getName());
                }
                break;
            case PICK_IMAGE_BB:
                if (!form_no.getText().toString().equalsIgnoreCase("")) {
                    imageName = utils.setImageName(form_no.getText().toString(), "bb", f.getName());
                } else {
                    imageName = utils.setImageName(utils.getCurrentDateandTimeMerge(), "bb", f.getName());
                }
                break;
            case PICK_IMAGE_PLN:
                if (!form_no.getText().toString().equalsIgnoreCase("")) {
                    imageName = utils.setImageName(form_no.getText().toString(), "pln", f.getName());
                } else {
                    imageName = utils.setImageName(utils.getCurrentDateandTimeMerge(), "pln", f.getName());
                }
                break;
        }
        decodeFile(picturePath, requestCode);
    }

    public void decodeFile(final String filePath, final int code) {
        Log.d("MSProfile", "code=" + code);
        // Decode ukuran gambar
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);
        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        // Decode with inSampleSize
        final BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View root = inflater.inflate(R.layout.image_preview, null);
        alertDialog.setView(root);
        alertDialog.setTitle("Image Preview");
        alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (code == PICK_IMAGE_INSTALASI) {
                    listPathIdentitas = new ArrayList<ProProfile.PathListItem>();
                    addViewImageList(PICK_IMAGE_INSTALASI, instalasi,
                            imageName, picturePath);
                } else if (code == PICK_IMAGE_LINGKUNGAN) {
                    listPathLingkungan = new ArrayList<ProProfile.PathListItem>();
                    addViewImageList(PICK_IMAGE_LINGKUNGAN, lingkungan,
                            imageName, picturePath);
                } else if (code == PICK_IMAGE_RUMAH) {
                    listPathRumah = new ArrayList<ProProfile.PathListItem>();
                    addViewImageList(PICK_IMAGE_RUMAH, rumah,
                            imageName, picturePath);
                } else if (code == PICK_IMAGE_KTP) {
                    listPathKtp = new ArrayList<ProProfile.PathListItem>();
                    addViewImageList(PICK_IMAGE_KTP, ktp,
                            imageName, picturePath);
                } else if (code == PICK_IMAGE_BB) {
                    listPathBB = new ArrayList<ProProfile.PathListItem>();
                    addViewImageList(PICK_IMAGE_BB, bb,
                            imageName, picturePath);
                } else if (code == PICK_IMAGE_PLN) {
                    listPathPln = new ArrayList<ProProfile.PathListItem>();
                    addViewImageList(PICK_IMAGE_PLN, pln,
                            imageName, picturePath);
                }
            }
        });
        alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //Cancel set Visible Bitmap
            }
        });
        prev = (ImageView) root.findViewById(R.id.imagepreview);
        prev.setVisibility(View.VISIBLE);
        if (code == PICK_IMAGE_INSTALASI) {
            bitmapInstalasi = BitmapFactory.decodeFile(filePath, o2);
            prev.setImageBitmap(bitmapInstalasi);
        } else if (code == PICK_IMAGE_LINGKUNGAN) {
            bitmapLingkungan = BitmapFactory.decodeFile(filePath, o2);
            prev.setImageBitmap(bitmapLingkungan);
        } else if (code == PICK_IMAGE_RUMAH) {
            bitmapRumah = BitmapFactory.decodeFile(filePath, o2);
            prev.setImageBitmap(bitmapRumah);
        } else if (code == PICK_IMAGE_KTP) {
            bitmapKtp = BitmapFactory.decodeFile(filePath, o2);
            prev.setImageBitmap(bitmapKtp);
        } else if (code == PICK_IMAGE_BB) {
            bitmapBB = BitmapFactory.decodeFile(filePath, o2);
            prev.setImageBitmap(bitmapBB);
        } else {
            bitmapPln = BitmapFactory.decodeFile(filePath, o2);
            prev.setImageBitmap(bitmapPln);
        }
        alertDialog.show();
    }

    private void checkMandatoryForm() {
        /*final EmailValidator emailValid = new EmailValidator();*/
        if (hardwareStatus.getText().toString() == null
                || hardwareStatus.getText().toString().equalsIgnoreCase("")
                || hardwareStatus.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Cek kembali Hw status", Toast.LENGTH_SHORT).show();
            return;
        } else if (identity_no.getText().toString() == null
                || identity_no.getText().toString().equalsIgnoreCase("")
                || identity_no.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Cek kembali no identitas", Toast.LENGTH_SHORT).show();
            return;
        } else if (list_identity.getText().toString() == null
                || list_identity.getText().toString().equalsIgnoreCase("")
                || list_identity.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan type identity", Toast.LENGTH_SHORT).show();
            return;
        } else if (first_name.getText().toString() == null
                || first_name.getText().toString().equalsIgnoreCase("")
                || first_name.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan nama profile.", Toast.LENGTH_SHORT).show();
            return;
        } else if (gender.getText().toString() == null
                || gender.getText().toString().equalsIgnoreCase("")
                || gender.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan jenis kelamin.", Toast.LENGTH_SHORT).show();
            return;
        } else if (religion.getText().toString() == null
                || religion.getText().toString().equalsIgnoreCase("")
                || religion.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan agama.", Toast.LENGTH_SHORT).show();
            return;
        } else if (birth_place.getText().toString() == null
                || birth_place.getText().toString().equalsIgnoreCase("")
                || birth_place.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan tempat lahir.", Toast.LENGTH_SHORT).show();
            return;
        } else if (birth_date.getText().toString() == null
                || birth_date.getText().toString().equalsIgnoreCase("")
                || birth_date.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan tanggal lahir.", Toast.LENGTH_SHORT).show();
            return;
        } else if (address.getText().toString() == null
                || address.getText().toString().equalsIgnoreCase("")
                || address.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan alamat pada identitas.", Toast.LENGTH_SHORT).show();
            return;
        } else if (zipcode_identity.getText().toString() == null
                || zipcode_identity.getText().toString().equalsIgnoreCase("")
                || zipcode_identity.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukan Zipcode identitas.", Toast.LENGTH_SHORT).show();
            return;
        } else if (valid_period.getText().toString() == null
                || valid_period.getText().toString().equalsIgnoreCase("")
                || valid_period.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan masa berlaku identitas", Toast.LENGTH_SHORT).show();
            return;
        } else if (install_date.getText().toString() == null
                || install_date.getText().toString().equalsIgnoreCase("")
                || install_date.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan tgl. pasang", Toast.LENGTH_SHORT).show();
            return;
        } else if (install_time.getText().toString() == null
                || install_time.getText().toString().equalsIgnoreCase("")
                || install_time.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan jam pasang", Toast.LENGTH_SHORT).show();
            return;
        } else if (time_confirm_start.getText().toString() == null
                || time_confirm_start.getText().toString().equalsIgnoreCase("")
                || time_confirm_start.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan lengkapi jam konfim", Toast.LENGTH_SHORT).show();
            return;
        } else if (time_confirm_end.getText().toString() == null
                || time_confirm_end.getText().toString().equalsIgnoreCase("")
                || time_confirm_end.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan lengkapi jam konfim", Toast.LENGTH_SHORT).show();
            return;
        } else if (install_address.getText().toString() == null
                || install_address.getText().toString().equalsIgnoreCase("")
                || install_address.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan alamat pemasangan.", Toast.LENGTH_SHORT).show();
            return;
        } else if (patokan.getText().toString() == null
                || patokan.getText().toString().equalsIgnoreCase("")
                || patokan.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan direction/petunjuk.", Toast.LENGTH_SHORT).show();
            return;
        } else if (zipcode_install.getText().toString() == null
                || zipcode_install.getText().toString().equalsIgnoreCase("")
                || zipcode_install.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan zipcode pemasangan.", Toast.LENGTH_SHORT).show();
            return;
        } else if (telp.getText().toString() == null
                || telp.getText().toString().equalsIgnoreCase("")
                || telp.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan nomor telephone.", Toast.LENGTH_SHORT).show();
            return;
        } else if (hp.getText().toString() == null
                || hp.getText().toString().equalsIgnoreCase("")
                || hp.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan nomor handphone.", Toast.LENGTH_SHORT).show();
            return;
        } else if (instalasi.size() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan foto instalasi", Toast.LENGTH_SHORT).show();
            return;
        } else if (lingkungan.size() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan foto lingkungan", Toast.LENGTH_SHORT).show();
            return;
        } else if (rumah.size() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan foto rumah", Toast.LENGTH_SHORT).show();
            return;
        } else if (ktp.size() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan foto ktp", Toast.LENGTH_SHORT).show();
            return;
        } // Bukti Bayar dan PLN Not Mandatory
        else {
            try {
                value = jsonValueCreator();
                showList = new ShowListDropDown();
                if (tblFormApp.getDataCount(TableFormApp.fFORM_NO, form_no.getText().toString()) == 0) {
                    tblFormApp.insertData(new TableFormApp(), form_no.getText().toString(), "DTDBB", uId, "", "", showList.getIdentityList(),
                            identity_no.getText().toString(), first_name.getText().toString(), last_name.getText().toString(),
                            birth_date.getText().toString(), value, "", "", "", "", "", "1", "", "");

                    Toast.makeText(getActivity(), "Profile Saved", Toast.LENGTH_SHORT).show();
                    saveImage();
                } else {
                    updateProfileDB();
                    Toast.makeText(getActivity(), "Update Profile", Toast.LENGTH_SHORT).show();
                    if (!isSendImage) {
                        saveImage();
                    }
                }


                SharedPreferences sessionPref = getActivity().getSharedPreferences("formId", Context.MODE_PRIVATE);
                SharedPreferences.Editor shareEditor = sessionPref.edit();
                shareEditor.putString("fn", form_no.getText().toString());
                shareEditor.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateProfileDB() {
        tblFormApp = new TableFormAppAdapter(getActivity());
        tblFormApp.updatePartial(getActivity(), TableFormApp.fIDENTITY_TYPE, showList.getIdentityList(), TableFormApp.fFORM_NO, form_no.getText().toString());
        tblFormApp.updatePartial(getActivity(), TableFormApp.fIDENTITY_NO, identity_no.getText().toString(), TableFormApp.fFORM_NO, form_no.getText().toString());
        tblFormApp.updatePartial(getActivity(), TableFormApp.fFIRST_NAME, first_name.getText().toString(), TableFormApp.fFORM_NO, form_no.getText().toString());
        tblFormApp.updatePartial(getActivity(), TableFormApp.fLAST_NAME, last_name.getText().toString(), TableFormApp.fFORM_NO, form_no.getText().toString());
        tblFormApp.updatePartial(getActivity(), TableFormApp.fBIRTH_DATE, birth_date.getText().toString(), TableFormApp.fFORM_NO, form_no.getText().toString());
        tblFormApp.updatePartial(getActivity(), TableFormApp.fVALUES_PROFILE, value, TableFormApp.fFORM_NO, form_no.getText().toString());
    }

    /**
     * Method for create json from form value
     *
     * @return
     * @throws Exception
     */
    private String jsonValueCreator() throws Exception {
        JSONObject obj = new JSONObject();
        showList = new ShowListDropDown();
        try {
            obj.put("ftId", form_no.getText().toString());
            /*obj.put("regDate", regDate.getText().toString());*/
            obj.put("identityType", showList.getIdentityList());
            obj.put("identityNo", identity_no.getText().toString());
            obj.put("firstName", first_name.getText().toString());
            obj.put("middleName", middle_name.getText().toString());
            obj.put("lastName", last_name.getText().toString());
            obj.put("gender", showList.getGender());
            obj.put("religion", showList.getReligion());
            obj.put("birthPlace", birth_place.getText().toString());
            obj.put("birthDate", birth_date.getText().toString());
            obj.put("address", address.getText().toString());
            obj.put("zipCode", zipcode_identity.getText().toString());
            obj.put("kelurahan", kelurahan_identity.getText().toString());
            obj.put("kecamatan", kecamatan_identity.getText().toString());
            obj.put("city", kota_identity.getText().toString());
            obj.put("province", provinci_identity.getText().toString());
            obj.put("validityPriod", valid_period.getText().toString());
            obj.put("installDate", install_date.getText().toString());
            obj.put("installTime", install_time.getText().toString());
            obj.put("confirmStart", time_confirm_start.getText().toString());
            obj.put("confirmEnd", time_confirm_end.getText().toString());
            obj.put("installAddress", install_address.getText().toString());
            obj.put("installZipcode", zipcode_install.getText().toString());
            obj.put("installKelurahan", kelurahan_install.getText().toString());
            obj.put("installKecamatan", kecamatan_install.getText().toString());
            obj.put("installCity", kota_install.getText().toString());
            obj.put("installProvince", provinci_install.getText().toString());
            obj.put("billingAddress", billing_address.getText().toString());
            obj.put("billingZipCode", zipcode_billing.getText().toString());
            obj.put("billingKelurahan", kelurahan_billing.getText().toString());
            obj.put("billingKecamatan", kecamatan_billing.getText().toString());
            obj.put("billingCity", kota_billing.getText().toString());
            obj.put("billingProvince", provinci_billing.getText().toString());
            obj.put("telephone", telp.getText().toString());
            obj.put("hp", hp.getText().toString());
            obj.put("email", email.getText().toString());
            //obj.put("print", isPrint);
            if (showList.get_job() != null) {
                obj.put("occupation", showList.get_job());
            } else {
                obj.put("occupation", "");
            }

            if (showList.get_homeType() != null) {
                obj.put("homeType", showList.get_homeType());
            } else {
                obj.put("homeType", "");
            }

            if (showList.get_homeStatus() != null) {
                obj.put("homeStatus", showList.get_homeStatus());
            } else {
                obj.put("homeStatus", "");
            }

            if (showList.get_income() != null) {
                obj.put("income", showList.get_income());
            } else {
                obj.put("income", "");
            }
            //obj.put("imagePath", picturePath);
            obj.put("userId", uId);
            obj.put("homeNo", no_home.getText().toString());
            obj.put("rt", rt.getText().toString());
            obj.put("rw", rw.getText().toString());
            obj.put("homeNo_install", no_home_install.getText().toString());
            obj.put("homeNo_bill", no_home_bill.getText().toString());
            obj.put("rt_install", rt_install.getText().toString());
            obj.put("rt_bill", rt_bill.getText().toString());
            obj.put("rw_install", rw_install.getText().toString());
            obj.put("rw_bill", rw_bill.getText().toString());
            obj.put("direction", patokan.getText().toString());
            obj.put("pln", idPln.getText().toString());
            obj.put("image", CreateImageJson());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("profileku", obj.toString());
        return obj.toString();
    }

    private JSONArray CreateImageJson() {
        JSONArray array = new JSONArray();
        try {
            for (int j = 0; j < ktp_name.size(); j++) {
                JSONObject list = new JSONObject();
                list.put("image_type_id", "ktp");
                list.put("image_name", ktp_name.get(j));
                array.put(list);
            }

            for (int j = 0; j < instalasi_name.size(); j++) {
                JSONObject list = new JSONObject();
                list.put("image_type_id", "instalasi");
                list.put("image_name", instalasi_name.get(j));
                array.put(list);
            }

            for (int j = 0; j < lingkungan_name.size(); j++) {
                JSONObject list = new JSONObject();
                list.put("image_type_id", "lingkungan");
                list.put("image_name", lingkungan_name.get(j));
                array.put(list);
            }

            for (int j = 0; j < rumah_name.size(); j++) {
                JSONObject list = new JSONObject();
                list.put("image_type_id", "rumah");
                list.put("image_name", rumah_name.get(j));
                array.put(list);
            }

            for (int j = 0; j < bb_name.size(); j++) {
                JSONObject list = new JSONObject();
                list.put("image_type_id", "bb");
                list.put("image_name", bb_name.get(j));
                array.put(list);
            }

            for (int j = 0; j < pln_name.size(); j++) {
                JSONObject list = new JSONObject();
                list.put("image_type_id", "pln");
                list.put("image_name", pln_name.get(j));
                array.put(list);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return array;
    }

    private void saveImage() {
        String fileName, filePath;
        if (instalasi.size() > 0) {
            LinearLayout mInstal = (LinearLayout) root.findViewById(R.id.list_pic_instalasi);
            for (int a = 0; a < instalasi.size(); a++) {
                filePath = instalasi.get(a);
                fileName = instalasi_name.get(a);
                System.out.println("running " + fileName);
                System.out.println("filePath " + filePath);
                TextView txtInstal = (TextView) mInstal.findViewWithTag(filePath);
                txtInstal.setText(fileName + " 0%");
                uploadImage(filePath, fileName, txtInstal, "install");
            }
        }
        if (lingkungan.size() > 0) {
            LinearLayout mLing = (LinearLayout) root.findViewById(R.id.list_pic_lingkungan);
            for (int a = 0; a < lingkungan.size(); a++) {
                filePath = lingkungan.get(a);
                fileName = lingkungan_name.get(a);
                System.out.println("running " + fileName);
                System.out.println("filePath " + filePath);
                TextView txtLing = (TextView) mLing.findViewWithTag(filePath);
                txtLing.setText(fileName + " 0%");
                uploadImage(filePath, fileName, txtLing, "lingkungan");
            }
        }
        if (rumah.size() > 0) {
            LinearLayout mRmh = (LinearLayout) root.findViewById(R.id.list_pic_rumah);
            for (int a = 0; a < rumah.size(); a++) {
                filePath = rumah.get(a);
                fileName = rumah_name.get(a);
                System.out.println("running " + fileName);
                System.out.println("filePath " + filePath);
                TextView txtRmh = (TextView) mRmh.findViewWithTag(filePath);
                txtRmh.setText(fileName + " 0%");
                uploadImage(filePath, fileName, txtRmh, "rumah");
            }
        }
        if (ktp.size() > 0) {
            LinearLayout mKtp = (LinearLayout) root.findViewById(R.id.list_pic_ktp);
            for (int a = 0; a < ktp.size(); a++) {
                filePath = ktp.get(a);
                fileName = ktp_name.get(a);
                System.out.println("running " + fileName);
                System.out.println("filePath " + filePath);
                TextView txtKtp = (TextView) mKtp.findViewWithTag(filePath);
                txtKtp.setText(fileName + " 0%");
                uploadImage(filePath, fileName, txtKtp, "ktp");
            }
        }

        if (bb.size() > 0) {
            LinearLayout mBb = (LinearLayout) root.findViewById(R.id.list_pic_bb);
            for (int a = 0; a < bb.size(); a++) {
                filePath = bb.get(a);
                fileName = bb_name.get(a);
                System.out.println("running " + fileName);
                System.out.println("filePath " + filePath);
                TextView txtBb = (TextView) mBb.findViewWithTag(filePath);
                txtBb.setText(fileName + " 0%");
                uploadImage(filePath, fileName, txtBb, "bb");
            }
        }

        if (pln.size() > 0) {
            LinearLayout mPln = (LinearLayout) root.findViewById(R.id.list_pic_pln);
            for (int a = 0; a < pln.size(); a++) {
                filePath = pln.get(a);
                fileName = pln_name.get(a);
                System.out.println("running " + fileName);
                System.out.println("filePath " + filePath);
                TextView txtPln = (TextView) mPln.findViewWithTag(filePath);
                txtPln.setText(fileName + " 0%");
                uploadImage(filePath, fileName, txtPln, "pln");
            }
        }
    }

    private void uploadImage(final String filePath, final String fileName, final TextView txtCent,
                             final String from) {
        try {
            String url = ConnectionManager.CM_URL_UPLOAD + form_no.getText().toString();
            System.out.println(url);
            final File myFile = new File(filePath);
            RequestParams params = new RequestParams();
            params.put("image", myFile);
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(300000);
            client.post(url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    int writen = (int) bytesWritten;
                    float proportionCorrect = ((float) writen) / ((float) myFile.length());
                    float percentFl = proportionCorrect * 100;
                    System.out.println("percentFl " + String.format("%.0f", percentFl));
                    if (!String.format("%.0f", percentFl).equalsIgnoreCase("0")) {
                        if (percentFl > 100) {
                            percentFl = 100;
                            txtCent.setText(fileName + " " + String.format("%.0f", percentFl) + " %");

                        }

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] bytes) {
                    isSendImage = true;
                    System.out.println("onSuccess " + filePath);
                    //Toast.makeText(getContext(), "Success upload " + fileName, Toast.LENGTH_LONG).show();
                    checkImage(fileName, true);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable throwable) {
                    System.out.println("onFailure " + filePath);
                    txtCent.setText(fileName + " 0%");
                    //Toast.makeText(getContext(), responseString, Toast.LENGTH_LONG).show();
                    checkImage(fileName, false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addViewImageList(int code, final ArrayList<String> arrayListImage, String imageName, String picturePath) {
        switch (code) {
            case PICK_IMAGE_INSTALASI:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_instalasi);
                break;
            case PICK_IMAGE_LINGKUNGAN:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_lingkungan);
                break;
            case PICK_IMAGE_RUMAH:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_rumah);
                break;
            case PICK_IMAGE_KTP:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_ktp);
                break;
            case PICK_IMAGE_BB:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_bb);
                break;
            case PICK_IMAGE_PLN:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_pln);
                break;
            case TAKE_PICTURE_INSTALASI:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_instalasi);
                break;
            case TAKE_PICTURE_LINGKUNGAN:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_lingkungan);
                break;
            case TAKE_PICTURE_RUMAH:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_rumah);
                break;
            case TAKE_PICTURE_KTP:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_ktp);
                break;
            case TAKE_PICTURE_BB:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_bb);
                break;
            case TAKE_PICTURE_PLN:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_pln);
                break;
        }

        final LinearLayout Mainlinear = new LinearLayout(getContext());
        Mainlinear.setBackgroundColor(getResources().getColor(R.color.white));
        Mainlinear.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLinearPackage.setMargins(0, 20, 0, 0);
        Mainlinear.setLayoutParams(pLinearPackage);
        mLayoutPackage.addView(Mainlinear);


        //Liner Title Image
        LinearLayout.LayoutParams pLineTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearTitle = new LinearLayout(getContext());
        linearTitle.setLayoutParams(pLineTitle);
        linearTitle.setOrientation(LinearLayout.HORIZONTAL);
        linearTitle.setPadding(10, 0, 10, 0);
        Mainlinear.addView(linearTitle);

        LinearLayout.LayoutParams pTxtTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        final TextView txtTitle = new TextView(getContext());
        txtTitle.setLayoutParams(pTxtTitle);
        txtTitle.setText(imageName);
        txtTitle.setTag(picturePath);
        txtTitle.setTextSize(10);
        txtTitle.setFreezesText(true);
        txtTitle.setPadding(5, 25, 0, 0);
        linearTitle.addView(txtTitle);


        //Linear Cancel Button
        LinearLayout.LayoutParams pLineCancel = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        LinearLayout linearCancel = new LinearLayout(getContext());
        linearTitle.setLayoutParams(pLineCancel);
        linearTitle.setOrientation(LinearLayout.HORIZONTAL);
        linearTitle.addView(linearCancel);

        LinearLayout.LayoutParams pBtnCancel = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
        ImageButton btnCancel = new ImageButton(getContext());
        btnCancel.setLayoutParams(pBtnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String m = txtTitle.getTag().toString();
                Mainlinear.removeAllViews();
                int i = arrayListImage.indexOf(m);
                arrayListImage.remove(i);
            }
        });
        btnCancel.setBackgroundResource(R.drawable.ic_close_package);
        btnCancel.setPadding(0, 16, 0, 16);
        linearCancel.addView(btnCancel);
        ProProfile.PathListItem item = new ProProfile.PathListItem();
        item.setPath(picturePath);
        item.setImageName(imageName);
        switch (code) {
            //Exec Pick Picture
            case PICK_IMAGE_INSTALASI:
                listPathIdentitas.add(item);
                ListInstalasi();
                break;
            case PICK_IMAGE_LINGKUNGAN:
                listPathLingkungan.add(item);
                ListLingkungan();
                break;
            case PICK_IMAGE_RUMAH:
                listPathRumah.add(item);
                ListRumah();
                break;
            case PICK_IMAGE_KTP:
                listPathKtp.add(item);
                ListKtp();
                break;
            case PICK_IMAGE_BB:
                listPathBB.add(item);
                ListBb();
                break;
            case PICK_IMAGE_PLN:
                listPathPln.add(item);
                ListPln();
                break;

            //Exec Take Picture
            case TAKE_PICTURE_INSTALASI:
                listPathIdentitas.add(item);
                ListInstalasi();
                break;
            case TAKE_PICTURE_LINGKUNGAN:
                listPathLingkungan.add(item);
                ListLingkungan();
                break;
            case TAKE_PICTURE_RUMAH:
                listPathRumah.add(item);
                ListRumah();
                break;
            case TAKE_PICTURE_KTP:
                listPathKtp.add(item);
                ListKtp();
                break;
            case TAKE_PICTURE_BB:
                listPathBB.add(item);
                ListBb();
                break;
            case TAKE_PICTURE_PLN:
                listPathPln.add(item);
                ListPln();
                break;
        }

    }

    private void ListInstalasi() {
        try {
            instalasi.add(picturePath);
            instalasi_name.add(imageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ListLingkungan() {
        try {
            lingkungan.add(picturePath);
            lingkungan_name.add(imageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ListRumah() {
        try {
            rumah.add(picturePath);
            rumah_name.add(imageName);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void ListKtp() {
        try {
            ktp.add(picturePath);
            ktp_name.add(imageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ListBb() {
        try {
            bb.add(picturePath);
            bb_name.add(imageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ListPln() {
        try {
            pln.add(picturePath);
            pln_name.add(imageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()){
            case R.id.birth_date:
                utils.showEditOwnDate(getActivity(), "Tgl. Lahir", birth_date, false);
                break;
            case R.id.validity_period:
                utils.showEditOwnDate(getActivity(), "Masa Berlaku", valid_period, true);
                break;
            case R.id.install_date:
                utils.showEditOwnDate(getActivity(), "Tgl. Pasang", install_date, false);
                break;
        }
        return false;
    }

    public class PathListItem {
        String imageName;
        String path;

        public PathListItem() {
            super();
        }

        public void setPath(String path) {
            this.path = path;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public String getImageName() {
            return imageName;
        }

        public String getPath() {
            return path;
        }
    }

    private void checkImage(String fileName, Boolean status) {
        Boolean findKtp = utils.findStringImage(fileName, "ktp");
        Boolean findLing = utils.findStringImage(fileName, "lingkungan");
        Boolean findInstall = utils.findStringImage(fileName, "instalasi");
        Boolean findRmh = utils.findStringImage(fileName, "rumah");
        Boolean findBB = utils.findStringImage(fileName, "bb");
        Boolean findPln = utils.findStringImage(fileName, "pln");

        if (findKtp) {
            if (status) {
                FormProDTD.isExistKtp = true;
            } else {
                FormProDTD.isExistKtp = false;
            }
        }

        if (findLing) {
            if (status) {
                FormProDTD.isExistLing = true;
            } else {
                FormProDTD.isExistLing = false;
            }
        }

        if (findRmh) {
            if (status) {
                FormProDTD.isExistRmh = true;
            } else {
                FormProDTD.isExistRmh = false;
            }
        }

        if (findInstall) {
            if (status) {
                FormProDTD.isExistInstall = true;
            } else {
                FormProDTD.isExistInstall = false;
            }
        }

        if (findBB) {
            if (status) {
                FormProDTD.isExistBB = true;
            } else {
                FormProDTD.isExistBB = false;
            }
        }

        if (findPln) {
            if (status) {
                FormProDTD.isExistPln = true;
            } else {
                FormProDTD.isExistPln = false;
            }
        }
    }

    private void removeAllImage(){
        instalasi.clear();
        lingkungan.clear();
        rumah.clear();
        ktp.clear();
        bb.clear();
        pln.clear();

        instalasi_name.clear();
        lingkungan_name.clear();
        rumah_name.clear();
        ktp_name.clear();
        bb_name.clear();
        pln_name.clear();

        mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_instalasi);
        mLayoutPackage.removeAllViews();

        mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_lingkungan);
        mLayoutPackage.removeAllViews();

        mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_rumah);
        mLayoutPackage.removeAllViews();

        mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_ktp);
        mLayoutPackage.removeAllViews();

        mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_bb);
        mLayoutPackage.removeAllViews();

        mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_pln);
        mLayoutPackage.removeAllViews();

    }
}
