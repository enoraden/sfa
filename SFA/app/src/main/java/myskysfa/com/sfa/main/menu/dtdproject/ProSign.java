package myskysfa.com.sfa.main.menu.dtdproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.sfa.main.menu.AgreementActivity;
import myskysfa.com.sfa.main.menu.SignatureWebViewActivity;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;
import myskysfa.com.sfa.widget.SignatureView;

/**
 * Created by Hari Hendryan on 12/24/2015.
 */
public class ProSign extends Fragment {
    private static Context _context;
    private ViewGroup root;
    private Utils utils;
    private static ImageView prev, closer, sign, bedaAlamatSign, sewaSign, multiSign, salesBedaAlamat, bundleIndosatSign;
    private SignatureView signatureView;
    private static Bitmap bitmap;
    private Button ok, save, clear;
    private String numberForm, uId;
    private TextView imageName, txtCent;
    private Long dataCount;
    private TableFormAppAdapter mFormAppAdapter;
    private static CheckBox bedAlamat, sewaCust, multyAccount, bundleIndosat;
    private static String imagePath = "", imagePathSewa, imagePathBedaAlamat, imagePathSalesBedaAlamat, imagePathBundleIndosat,
            _fileName = "", imagePathMulti, status_rumah, reasonBeda, resultStatus, reasonMulti, cust_nbr,
            vc_nbr, payment_method, router, iccid, imei;
    private JSONObject objBedaAlamat;
    private JSONObject objMultiAccount;
    private JSONObject objBundleIndosat;

    public static Fragment newInstance(Context context) {
        _context = context;
        ProSign ftSign = new ProSign();
        return ftSign;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.pro_sign, container, false);
        try {
            utils = new Utils(getActivity());
            initView(root);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return root;
    }

    private void initView(ViewGroup view) {
        prev = (ImageView) root.findViewById(R.id.imagepreview);
        sign = (ImageView) view.findViewById(R.id.signature);
        bedaAlamatSign = (ImageView) view.findViewById(R.id.imagepreviewBeda);
        sewaSign = (ImageView) view.findViewById(R.id.imagepreviewSewa);
        multiSign = (ImageView) view.findViewById(R.id.imagepreviewMulti);
        salesBedaAlamat = (ImageView) root.findViewById(R.id.imageprevSalesBed);
        bundleIndosatSign = (ImageView) view.findViewById(R.id.imagepreviewIndosat);
        imageName = (TextView) root.findViewById(R.id.sig_pic);
        save = (Button) root.findViewById(R.id.save);
        txtCent = (TextView) root.findViewById(R.id.cent_image);
        bedAlamat = (CheckBox) root.findViewById(R.id.ttdbedalamat);
        sewaCust = (CheckBox) root.findViewById(R.id.ttdRumahSewa);
        multyAccount = (CheckBox) root.findViewById(R.id.ttdmultyAccount);
        bundleIndosat = (CheckBox) root.findViewById(R.id.ttdbundleIndosat);

        /*if (getActivity().getIntent().getStringExtra("ftId")!=null
                ||getActivity().getIntent().getStringExtra("ftId").length()>1) {
            numberForm = getActivity().getIntent().getStringExtra("ftId");
        } else {
            SharedPreferences sm = getActivity().getSharedPreferences(
                    "formId", Context.MODE_PRIVATE);
            numberForm = sm.getString("fn", null);
        }*/

        /*SharedPreferences sm = getActivity().getSharedPreferences(
                "formId", Context.MODE_PRIVATE);
        numberForm = sm.getString("fn", null);*/

        TableLogLoginAdapter mLogLoginAdapter;
        mFormAppAdapter = new TableFormAppAdapter(getActivity());
        List<TableLogLogin> listLogLogin;
        mLogLoginAdapter = new TableLogLoginAdapter(getActivity());
        //listLogLogin = mLogLoginAdapter.getLastData();
        listLogLogin = mLogLoginAdapter.getAllData();

        for (int i = 0; i < listLogLogin.size(); i++) {
            TableLogLogin item = listLogLogin.get(i);
            uId = item.getcUid();
        }

        sign.setOnClickListener(ibListener);
        save.setOnClickListener(saveListener);
        bedAlamat.setOnCheckedChangeListener(checkBedaAlamatListener);
        sewaCust.setOnCheckedChangeListener(checkMultyListener);
        multyAccount.setOnCheckedChangeListener(checkMuliAccount);
        bundleIndosat.setOnCheckedChangeListener(checkBundleIndosatListener);
    }

    private View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (imagePath == null || imagePath.length() <= 0) {
                Toast.makeText(getActivity(), "Silahkan tanda tangan!", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    dataCount = mFormAppAdapter.getDataCount(TableFormApp.fFORM_NO, numberForm);
                    if (dataCount == 0) {
                        utils.showAlertDialog(getActivity(), "Gagal", "Silahkan isi data profile terlebih dahulu.", false);
                    } else {
                        TableFormAppAdapter db;
                        db = new TableFormAppAdapter(getActivity());
                        db.updatePartial(getActivity(), TableFormApp.fVALUES_SIGNATURE,
                                getFileName(imagePath), TableFormApp.fFORM_NO, numberForm);
                        txtCent.setText("0 %");
                        uploadImage(imagePath, getFileName(imagePath));
                        if (imagePathSewa != null) {
                            if (!imagePathSewa.equalsIgnoreCase("")) {
                                uploadImage(imagePathSewa, getFileName(imagePathSewa));
                                db = new TableFormAppAdapter(getActivity());
                                db.updatePartial(getActivity(), TableFormApp.fVALUES_SIGNMULTY,
                                        getFileName(imagePathSewa), TableFormApp.fFORM_NO,
                                        numberForm);
                            } else {
                                db = new TableFormAppAdapter(getActivity());
                                db.updatePartial(getActivity(), TableFormApp.fVALUES_SIGNMULTY, "",
                                        TableFormApp.fFORM_NO, numberForm);
                            }
                        }

                        if (imagePathBedaAlamat != null) {
                            if (!imagePathBedaAlamat.equalsIgnoreCase("")) {
                                uploadImage(imagePathBedaAlamat, getFileName(imagePathBedaAlamat));
                                if (imagePathSalesBedaAlamat != null) {
                                    if (!imagePathSalesBedaAlamat.equalsIgnoreCase("")) {
                                        uploadImage(imagePathSalesBedaAlamat,
                                                getFileName(imagePathSalesBedaAlamat));
                                    }
                                }
                                objBedaAlamat = new JSONObject();
                                objBedaAlamat.put("sign", getFileName(imagePathBedaAlamat));
                                objBedaAlamat.put("reason", reasonBeda);
                                objBedaAlamat.put("home_status", status_rumah);
                                objBedaAlamat.put("sales_sign", getFileName(imagePathSalesBedaAlamat));
                                db = new TableFormAppAdapter(getActivity());
                                db.updatePartial(getActivity(), TableFormApp.fVALUES_SIGNBEDA,
                                        objBedaAlamat, TableFormApp.fFORM_NO,
                                        numberForm);
                            } else {
                                db = new TableFormAppAdapter(getActivity());
                                db.updatePartial(getActivity(), TableFormApp.fVALUES_SIGNBEDA, "",
                                        TableFormApp.fFORM_NO, numberForm);
                            }
                        }

                        if (imagePathMulti != null) {
                            if (!imagePathMulti.equalsIgnoreCase("")) {
                                uploadImage(imagePathMulti, getFileName(imagePathMulti));
                                objMultiAccount = new JSONObject();
                                objMultiAccount.put("sign", getFileName(imagePathMulti));
                                objMultiAccount.put("status", resultStatus);
                                objMultiAccount.put("reason", reasonMulti);
                                objMultiAccount.put("cust_number", cust_nbr);
                                objMultiAccount.put("vc_number", vc_nbr);
                                db = new TableFormAppAdapter(getActivity());
                                db.updatePartial(getActivity(), TableFormApp.fVALUES_SIGNACCOUNT, objMultiAccount,
                                        TableFormApp.fFORM_NO, numberForm);
                            } else {
                                db = new TableFormAppAdapter(getActivity());
                                db.updatePartial(getActivity(), TableFormApp.fVALUES_SIGNACCOUNT, "",
                                        TableFormApp.fFORM_NO, numberForm);
                            }
                        }

                        if (imagePathBundleIndosat != null) {
                            if (!imagePathBundleIndosat.equalsIgnoreCase("")) {
                                uploadImage(imagePathBundleIndosat, getFileName(imagePathBundleIndosat));
                                objBundleIndosat = new JSONObject();
                                objBundleIndosat.put("sign", getFileName(imagePathBundleIndosat));
                                objBundleIndosat.put("payment_method", payment_method);
                                objBundleIndosat.put("router", router);
                                objBundleIndosat.put("iccid", iccid);
                                objBundleIndosat.put("imei", imei);
                                db = new TableFormAppAdapter(getActivity());
                                db.updatePartial(getActivity(), TableFormApp.fVALUES_SIGNINDOSAT, objBundleIndosat,
                                        TableFormApp.fFORM_NO, numberForm);
                            } else {
                                db = new TableFormAppAdapter(getActivity());
                                db.updatePartial(getActivity(), TableFormApp.fVALUES_SIGNINDOSAT, "",
                                        TableFormApp.fFORM_NO, numberForm);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void uploadImage(final String filePath, final String fileName) {
        try {
            String url = ConnectionManager.CM_URL_UPLOAD + numberForm;
            System.out.println(url);

            final File myFile = new File(filePath);
            RequestParams params = new RequestParams();
            params.put("image", myFile);

            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(300000);
            client.post(url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);

                    int writen = (int) bytesWritten;
                    float proportionCorrect = ((float) writen) / ((float) myFile.length());
                    float percentFl = proportionCorrect * 100;
                    String percentStr = String.format("%.0f", percentFl);
                    System.out.println("percentFl " + String.format("%.0f", percentFl));

                    if (Integer.valueOf(percentStr) > Integer.valueOf(txtCent.getText().toString().replace(" %", ""))) {
                        txtCent.setText(percentStr + " %");
                    }

                    if (Integer.valueOf(percentStr) > 100) {
                        txtCent.setText("100 %");
                    }

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] bytes) {
                    System.out.println("onSuccess " + filePath);
                    Toast.makeText(getContext(), "Success upload " + fileName, Toast.LENGTH_LONG).show();
                    ((FormProDTD) getActivity()).isExistSign = true;
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable throwable) {
                    System.out.println("onFailure " + filePath);
                    txtCent.setText(fileName + " 0 %");
                    String responseString = "Error occurred! Http Status Code: " + statusCode + " " + fileName;
                    Toast.makeText(getContext(), responseString, Toast.LENGTH_LONG).show();
                    ((FormProDTD) getActivity()).isExistSign = false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View.OnClickListener ibListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dataCount = mFormAppAdapter.getDataCount(TableFormApp.fFORM_NO, numberForm);
            if (dataCount == 0) {
                utils.showAlertDialog(getActivity(),
                        "Gagal", "Silahkan isi data profile terlebih dahulu.", false);
            } else {
                final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
                final LayoutInflater inflater = LayoutInflater.from(getActivity());
                final View root = inflater.inflate(R.layout.text_syarat, null);
                alertDialogBuilder.setView(root);
                final android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alertDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                Button indo = (Button) root.findViewById(R.id.indov);
                Button okev = (Button) root.findViewById(R.id.okev);
                Button top = (Button) root.findViewById(R.id.top);
                indo.setText("Indovison");
                okev.setText("Oke Vision");
                top.setText("Top Tv");

                indo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getActivity(), AgreementActivity.class);
                        i.putExtra(getActivity().getResources().getString(R.string.type),
                                getActivity().getResources().getString(R.string.brand_type_indovision));
                        i.putExtra("appType", "dtdbb");
                        i.putExtra("numberForm", numberForm);
                        startActivity(i);
                    }
                });

                okev.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getActivity(), AgreementActivity.class);
                        i.putExtra(getActivity().getResources().getString(R.string.type),
                                getActivity().getResources().getString(R.string.brand_type_okevision));
                        i.putExtra("appType", "dtdbb");
                        i.putExtra("numberForm", numberForm);
                        startActivity(i);
                    }
                });

                top.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), AgreementActivity.class);
                        intent.putExtra(getActivity().getResources().getString(R.string.type),
                                getActivity().getResources().getString(R.string.brand_type_top_tv));
                        intent.putExtra("appType", "dtdbb");
                        intent.putExtra("numberForm", numberForm);
                        startActivity(intent);
                    }
                });
                alertDialog.show();
            }


        }
    };


    public void decodeFile(final String filePath, String name) {
        if (filePath != null) {
            // Decode ukuran gambar0
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 1024;
            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }
            // Decode with inSampleSize
            final BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;

            bitmap = BitmapFactory.decodeFile(filePath, o2);
            switch (name) {
                case "aggrement":
                    imagePath = filePath;
                    if (imagePath != null) {
                        prev.setImageBitmap(bitmap);
                    }

                    break;
                case "bedalamat":
                    imagePathBedaAlamat = filePath;
                    if (bedaAlamatSign != null) {
                        bedaAlamatSign.setImageBitmap(bitmap);
                    }

                    if (imagePathSalesBedaAlamat != null &&
                            !imagePathSalesBedaAlamat.equalsIgnoreCase("")){
                        bitmap = BitmapFactory.decodeFile(imagePathSalesBedaAlamat, o2);
                        salesBedaAlamat.setImageBitmap(bitmap);
                    }
                    break;
                case "multy":
                    imagePathSewa = filePath;
                    if (sewaSign != null) {
                        sewaSign.setImageBitmap(bitmap);
                    }
                    break;
                case "multi_account":
                    imagePathMulti = filePath;
                    if (multiSign != null) {
                        multiSign.setImageBitmap(bitmap);
                    }
                case "bundle_indosat":
                    imagePathBundleIndosat = filePath;
                    Log.e("ZZZ", imagePathBundleIndosat);
                    if (imagePathBundleIndosat != null) {
                        bundleIndosatSign.setImageBitmap(bitmap);
                    }
                    break;
            }
        }

    }

    public void setFormNumber() {
        SharedPreferences sm = getActivity().getSharedPreferences(
                "formId", Context.MODE_PRIVATE);
        numberForm = sm.getString("fn", null);
    }

    private CompoundButton.OnCheckedChangeListener checkBedaAlamatListener =
            new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        dataCount = mFormAppAdapter.getDataCount(TableFormApp.fFORM_NO, numberForm);
                        if (dataCount == 0) {
                            utils.showAlertDialog(getActivity(), "Gagal", "Silahkan isi data profile terlebih dahulu.", false);
                            bedAlamat.setChecked(false);
                        } else {
                            Intent i = new Intent(getActivity(), SignatureWebViewActivity.class);
                            i.putExtra(getActivity().getResources().getString(R.string.type), getActivity().getResources().getString(R.string.beda_alamat));
                            i.putExtra("id_webview", 1);
                            i.putExtra("numberForm", numberForm);
                            i.putExtra("appType", "dtdbb");
                            startActivity(i);
                        }
                    } else {
                        new SignatureWebViewActivity().name = null;
                        imagePathBedaAlamat = "";
                        imagePathSalesBedaAlamat = "";
                        status_rumah = "";
                        reasonBeda = "";
                        bedaAlamatSign.setImageResource(0);
                        salesBedaAlamat.setImageResource(0);
                    }
                }
            };

    private CompoundButton.OnCheckedChangeListener checkMultyListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                dataCount = mFormAppAdapter.getDataCount(TableFormApp.fFORM_NO, numberForm);
                if (dataCount == 0) {
                    utils.showAlertDialog(getActivity(), "Gagal", "Silahkan isi data profile terlebih dahulu.", false);
                    sewaCust.setChecked(false);
                } else {
                    Intent i = new Intent(getActivity(), SignatureWebViewActivity.class);
                    i.putExtra(getActivity().getResources().getString(R.string.type), getActivity().getResources().getString(R.string.sewa_pelanggan));
                    i.putExtra("id_webview", 2);
                    i.putExtra("numberForm", numberForm);
                    i.putExtra("appType", "dtdbb");
                    startActivity(i);
                }
            } else {
                new SignatureWebViewActivity().name = null;
                imagePathSewa = "";
                sewaSign.setImageResource(0);
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener checkMuliAccount = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                dataCount = mFormAppAdapter.getDataCount(TableFormApp.fFORM_NO, numberForm);
                if (dataCount == 0) {
                    utils.showAlertDialog(getActivity(), "Gagal", "Silahkan isi data profile terlebih dahulu.", false);
                    multyAccount.setChecked(false);
                } else {
                    Intent i = new Intent(getActivity(), SignatureWebViewActivity.class);
                    i.putExtra(getActivity().getResources().getString(R.string.type), getActivity().getResources().getString(R.string.multi_account));
                    i.putExtra("id_webview", 3);
//                    i.putExtra("imagePath", imagePath);
//                    i.putExtra("fileName", _fileName);
                    i.putExtra("numberForm", numberForm);
                    i.putExtra("appType", "dtdbb");
                    startActivity(i);
                }
            } else {
                new SignatureWebViewActivity().name = null;
                imagePathMulti = "";
                vc_nbr = "";
                cust_nbr = "";
                resultStatus = "";
                reasonMulti = "";
                multiSign.setImageResource(0);
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener checkBundleIndosatListener =
            new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        dataCount = mFormAppAdapter.getDataCount(TableFormApp.fFORM_NO, numberForm);
                        if (dataCount == 0) {
                            utils.showAlertDialog(getActivity(), "Gagal", "Silahkan isi data profile terlebih dahulu.", false);
                            bundleIndosat.setChecked(false);
                        } else {
                            Intent i = new Intent(getActivity(), SignatureWebViewActivity.class);
                            i.putExtra(getActivity().getResources().getString(R.string.type), getActivity().getResources().getString(R.string.bundle_indosat));
                            i.putExtra("id_webview", 4);
                            i.putExtra("numberForm", numberForm);
                            i.putExtra("appType", "dtdbb");
                            startActivity(i);
                        }
                    } else {
                        new SignatureWebViewActivity().name = null;
                        imagePathBundleIndosat = "";
                        payment_method = "";
                        router = "";
                        iccid = "";
                        imei = "";
                        bundleIndosatSign.setImageResource(0);
                    }
                }
            };

    public void setInvisibleCheckbox(String from) {
        switch (from) {
            case "Beda Alamat":
                bedAlamat.setChecked(false);
                imagePathBedaAlamat = "";
                status_rumah = "";
                reasonBeda = "";
                break;
            case "Rumah Sewa":
                sewaCust.setChecked(false);
                imagePathSewa = "";
                break;
            case "Multi Account":
                multyAccount.setChecked(false);
                imagePathMulti = "";
                vc_nbr = "";
                cust_nbr = "";
                resultStatus = "";
                reasonMulti = "";
            case "Bundle Indosat":
                bundleIndosat.setChecked(false);
                imagePathBundleIndosat = "";
                payment_method = "";
                router = "";
                iccid = "";
                imei = "";
                break;

        }
    }

    public void setValueHtmlBedaAlamat(String value, String reasonBeda, String imagePathSalesBedaAlamat) {
        status_rumah = value;
        this.reasonBeda = reasonBeda;
        this.imagePathSalesBedaAlamat = imagePathSalesBedaAlamat;
    }

    public void setValueHtmlMultiAccount(String resultStatus, String reasonMulti,
                                         String cust_nbr, String vc_nbr) {
        this.resultStatus = resultStatus;
        this.reasonMulti = reasonMulti;
        this.cust_nbr = cust_nbr;
        this.vc_nbr = vc_nbr;
    }

    public void setvalueHtmlBundleIndosat(String payment_method, String router, String iccid, String imei){
        this.payment_method = payment_method;
        this.router = router;
        this.iccid = iccid;
        this.imei = imei;
        Log.e("HTML_Indosat", payment_method+","+router+","+iccid+","+imei);
    }

    private String getFileName(String path) {
        String filename = path.substring(path.lastIndexOf("/") + 1);
        return filename;
    }

}
