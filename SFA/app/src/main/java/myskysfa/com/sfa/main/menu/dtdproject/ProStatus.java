package myskysfa.com.sfa.main.menu.dtdproject;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.StatusAdapter;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableFreeTrialCoh;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.TableMasterPackage;
import myskysfa.com.sfa.database.TablePlan;
import myskysfa.com.sfa.database.db_adapter.TableFTSOHAdapter;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMasterPackageAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePlanAdapter;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionDetector;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.DatabaseManager;
import myskysfa.com.sfa.utils.PermissionUtils;
import myskysfa.com.sfa.utils.Utils;
import myskysfa.com.sfa.utils.listItemObject.StatusInfo;

/**
 * Created by Hari Hendryan on 12/17/2015.
 */
public class ProStatus extends Fragment implements GoogleMap.OnMyLocationButtonClickListener, OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMarkerDragListener {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    // -- begin of -- ari
    public static boolean stopMap = false;
    // -- end of -- ari
    private static Context _context;
    private ViewGroup root;
    private Utils utils;
    private RecyclerView recyclerView;
    private DatabaseManager dbManager;
    private LinearLayoutManager linearLayoutManager;
    private String numberForm, photo, signature, profile, payment, paket, emergency,
            imei, token, data, status, sflCode, planId, message;
    private static String file1, md5;
    private ArrayList<String> list;
    private TableFormApp item;
    private JSONObject jsonObject;
    public static GoogleMap mMap;
    public static Double latitude, longitude;
    private Location latLng;
    private boolean mPermissionDenied = false;
    private Handler handler;
    public static String addressLine;
    private static TextView _addressLine;
    private ProgressDialog _progressDialog;
    private boolean isTaskRunning = false, isInternetPresent = false;
    private ArrayList<TableLogLogin> listLogin = new ArrayList<>();
    private TableLogLoginAdapter loginAdapter;
    private TableFormAppAdapter mFormAppAdapter;
    private List<TableFormApp> listFormApp;
    //private ArrayList<ChunkListItem> listChunk = new ArrayList<>();
    //private ArrayList<PathImageListItem> imageList = new ArrayList<>();
    private ArrayList<String> strImageList;
    private SupportMapFragment fragment;
    private Marker marker;
    int countPlan = 0;
    private SharedPreferences sessionPref;
    //private TableImageChunkAdapter dbchunkAdapter;
    private String hp, billZipcode, direction, installAddress, installProvince, birthPlace, city, billingKelurahan, installDate,
            billingProvince, identityNo, userId, zipCode, province, billingCity, installZipcode, birthDate, firstName,
            installCity, homeNo, middleName, ftId, lastName, identityType, installKelurahan, confirmStart, regDate,
            installKecamatan, kelurahan, billingKecamatan, kecamatan, email, address, confirmEnd, rt, billingAddress,
            validityPriod, installTime, rw, telephone, eFirstName, eAddress, eFormNo, eTelephone, eLastName, eRelationship,
            eMiddleName, ODU, VC, BRAND, LNB, DSD, HW_STATUS, ala1, ala2, ala3, bsc1, bsc2, bsc3, religion, job, income, gender,
            homeType, homeStatus, url, urlImage, response, value, image, pictureName, picture, indexFile, content, sequence,
            vc1, vc2, vc3, lnb1, lnb2, lnb3, odu1, odu2, odu3, dsd1, dsd2, dsd3, promoCode, pln, additional,
            routeId, routeValue, simId, simValue, expiry_date, account_number, bank, name, amount,
            method, period, remark, ccNumber, ccType, paymentDate, rt_install, rt_bill, rw_install, rw_bill,
            signBedaAlamat, signSewa, signMultiAccount, signBundlingIndosat;
    private Menu menu;
    private MenuItem alert;
    ConnectionDetector cd;
    private StatusAdapter ca;
    private JSONArray imageArray;
    private int signal, isMulty;

    public static Fragment newInstance(Context context) {
        _context = context;
        ProStatus ProStatus = new ProStatus();
        return ProStatus;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root == null) {
            root = (ViewGroup) inflater.inflate(R.layout.pro_status, container, false);
        }

        try {
            utils = new Utils(getActivity());
            recyclerView = (RecyclerView) root.findViewById(R.id.cardListStatus);
            sessionPref = getContext().getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
            linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

            recyclerView.setLayoutManager(linearLayoutManager);
            //dbManager           = new DatabaseManager(getActivity());
            changeStatus();
            handler = new Handler();
            utils.setIMEI();
            utils.setGeoLocation();
            /*url                 = ConnectionManager.CM_URL_FT_STS;*/
            //url = ConnectionManager.CM_URL_FT_STSTEST;
            urlImage = ConnectionManager.CM_URL_DTD_CHUNK;
            stopMap = false; //ari
            setMapMarker();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return root;
    }

    public void changeStatus() {
        try {
            String a = String.valueOf(latitude) + ";" + String.valueOf(longitude) + ";" + addressLine;
            getDataFromDB(a);
            ca = new StatusAdapter(getActivity(), createList(3), list);
            recyclerView.setAdapter(ca);
            ca.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentManager fm = getChildFragmentManager();
        fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
        if (fragment == null) {
            fragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map, fragment).commit();
        }
        fragment.getMapAsync(this);
    }

    private List<StatusInfo> createList(int size) {
        List<StatusInfo> result = new ArrayList<StatusInfo>();
        List<String> pathName = new ArrayList<String>();

        //pathName.add("Photo");

        pathName.add("Data Profile");
        pathName.add("Data Emergency/Keluarga");
        //pathName.add("Data Payment");
        pathName.add("Data Paket");
        pathName.add("Signature");
        // pathName.add("Data Tagging");


        for (int i = 0; i <= size; i++) {

            StatusInfo si = new StatusInfo();
            si.Status = StatusInfo.STATUS_PREFIX + "Data Belum Diambil";
            si.Keterangan = StatusInfo.KETERANGAN_PREFIX;
            si.Flag = StatusInfo.FLAG_PREFIX;
            si.Title = StatusInfo.TITLE_PREFIX + pathName.get(i);
            result.add(si);
        }
        return result;
    }

    private void getDataFromDB(String a) {
        try {
            /*if (getActivity().getIntent().getStringExtra("ftId")!=null
                    ||getActivity().getIntent().getStringExtra("ftId").length()>1) {
                numberForm = getActivity().getIntent().getStringExtra("ftId");
            } else {
                SharedPreferences sm = getActivity().getSharedPreferences(
                        "formId", Context.MODE_PRIVATE);
                numberForm = sm.getString("fn", null);
            }*/

            /**
             * get Form Number from Profile
             */
            SharedPreferences sm = getActivity().getSharedPreferences(
                    "formId", Context.MODE_PRIVATE);
            numberForm = sm.getString("fn", null);

            if (numberForm == null) {
                numberForm = "";
            }

            loginAdapter = new TableLogLoginAdapter(getActivity());
            listLogin = (ArrayList<TableLogLogin>) loginAdapter.getAllData();
            mFormAppAdapter = new TableFormAppAdapter(getActivity());
            listFormApp = mFormAppAdapter.getDatabyCondition(TableFormApp.fFORM_NO, numberForm);
            list = new ArrayList<String>();
            for (int i = 0; i < listFormApp.size(); i++) {
                item = listFormApp.get(i);
                if (item != null) {
                    signature = item.getVALUES_SIGNATURE();
                    profile = item.getVALUES_PROFILE();
                    payment = item.getVALUES_PAYMENT();
                    paket = item.getVALUES_PACKAGE();
                    emergency = item.getVALUES_EMERGENCY();
                    signBedaAlamat = item.getVALUES_SIGNBEDA();
                    signSewa = item.getVALUES_SIGNMULTY();
                    signMultiAccount = item.getVALUES_SIGNACCOUNT();
                    signBundlingIndosat = item.getVALUES_SIGNINDOSAT();
                }
            }

            if (listLogin.size() > 0) {
                token = listLogin.get(listLogin.size() - 1).getcToken();
            }

            if (profile != null) {
                //jsonObject = new JSONObject(profile);
                //photo      = jsonObject.getString("imagePath");
            }
            if (photo == null)
                photo = "";
            if (signature == null)
                signature = "";
            if (profile == null)
                profile = "";
            if (payment == null)
                payment = "";
            if (paket == null)
                paket = "";
            if (emergency == null)
                emergency = "";

            if (a == null || a.length() < 1) {
                list.add("0.0;0.0;0.0");
            } else {
                list.add(a);
            }
            list.add(profile);
            list.add(emergency);
            list.add(paket);
            list.add(signature);
            //list.add(photo);
            //list.add(payment);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String generateJson() {
        loginAdapter = new TableLogLoginAdapter(getContext());
        TableMasterPackageAdapter productAdapter = new TableMasterPackageAdapter(getActivity());
        List<TableMasterPackage> listProduct;
        List<TableLogLogin> listLogin = loginAdapter.getAllData();
        String sflCode = listLogin.get(listLogin.size() - 1).getcSflCode();
        String token = listLogin.get(listLogin.size() - 1).getcToken();
        String username = listLogin.get(listLogin.size() - 1).getcUserName();
        String brandCode = listLogin.get(listLogin.size() - 1).getcBranchId();
        Log.d("FTStatus", "token=" + token);


        try {
            JSONObject profObj = new JSONObject(profile);
            JSONObject refObj = new JSONObject(emergency);
            JSONObject paketObj = new JSONObject(paket);
            JSONObject paymentObj = new JSONObject(payment);

            hp = profObj.getString("hp");
            billZipcode = profObj.getString("billingZipCode");
            direction = profObj.getString("direction");
            installAddress = profObj.getString("installAddress");
            installProvince = profObj.getString("installProvince");
            birthPlace = profObj.getString("birthPlace");
            city = profObj.getString("city");
            billingKelurahan = profObj.getString("billingKelurahan");
            installDate = profObj.getString("installDate");
            billingProvince = profObj.getString("billingProvince");
            identityNo = profObj.getString("identityNo");
            userId = profObj.getString("userId");
            zipCode = profObj.getString("zipCode");
            province = profObj.getString("province");
            billingCity = profObj.getString("billingCity");
            installZipcode = profObj.getString("installZipcode");
            birthDate = profObj.getString("birthDate");
            firstName = profObj.getString("firstName");
            installCity = profObj.getString("installCity");
            homeNo = profObj.getString("homeNo");
            middleName = profObj.getString("middleName");
            ftId = profObj.getString("ftId");
            lastName = profObj.getString("lastName");
            identityType = profObj.getString("identityType");
            installKelurahan = profObj.getString("installKelurahan");
            confirmStart = profObj.getString("confirmStart");
            //regDate = profObj.getString("regDate");
            installKecamatan = profObj.getString("installKecamatan");
            kelurahan = profObj.getString("kelurahan");
            billingKecamatan = profObj.getString("billingKecamatan");
            kecamatan = profObj.getString("kecamatan");
            email = profObj.getString("email");
            address = profObj.getString("address");
            confirmEnd = profObj.getString("confirmEnd");
            rt = profObj.getString("rt");
            rt_install = profObj.getString("rt_install");
            rt_bill = profObj.getString("rt_bill");
            rw_install = profObj.getString("rw_install");
            rw_bill = profObj.getString("rw_bill");
            pln = profObj.getString("pln");
            billingAddress = profObj.getString("billingAddress");
            validityPriod = profObj.getString("validityPriod");
            installTime = profObj.getString("installTime");
            rw = profObj.getString("rw");
            telephone = profObj.getString("telephone");
            religion = profObj.getString("religion");
            job = profObj.getString("occupation");
            income = profObj.getString("income");
            gender = profObj.getString("gender");
            homeType = profObj.getString("homeType");
            homeStatus = profObj.getString("homeStatus");
            imageArray = profObj.getJSONArray("image");
            //image = profObj.getString("image");


            eFirstName = refObj.getString("eFirstName");
            eAddress = refObj.getString("eAddress");
            eFormNo = refObj.getString("eFormNo");
            eTelephone = refObj.getString("eTelephone");
            eLastName = refObj.getString("eLastName");
            eRelationship = refObj.getString("eRelationship");
            eMiddleName = refObj.getString("eMiddleName");

            //Payment Get Data
            if (!payment.equals("") && payment != null) {
                if (paymentObj.has("masaBerlaku")) {
                    expiry_date = paymentObj.getString("masaBerlaku");
                } else {
                    expiry_date = "";
                }
                if (paymentObj.has("noRek")) {
                    account_number = paymentObj.getString("noRek");
                } else {
                    account_number = "";
                }
                if (paymentObj.has("bank")) {
                    bank = paymentObj.getString("bank");
                } else {
                    bank = "";
                }
                name   = paymentObj.getString("name");
                amount = paymentObj.getString("total");
                method = paymentObj.getString("caraBayar");
                period = paymentObj.getString("periode");
                remark = paymentObj.getString("remark");
                paymentDate = paymentObj.getString("payment_date");
                if (paymentObj.has("cc_type")) {
                    ccType = paymentObj.getString("cc_type");
                } else {
                    ccType = "";
                }
                if (paymentObj.has("cc_no")) {
                    ccNumber = paymentObj.getString("cc_no");
                } else {
                    ccNumber = "";
                }
            } else {
                expiry_date = "";
                account_number = "";
                bank = "";
                name = "";
                amount = "";
                method = "";
                period = "";
                remark = "";
                paymentDate = "";
                ccType = "";
                ccNumber = "";

            }


           /* ODU = paketObj.getString("ODU");
            VC = paketObj.getString("VC");*/
            BRAND = paketObj.getString("BRAND");
            isMulty = paketObj.getInt("isMulty");
            promoCode = paketObj.getString("promoCode");
            additional = paketObj.getString("additional");
            /*LNB = paketObj.getString("LNB");
            DSD = paketObj.getString("DSD");*/

            HW_STATUS = paketObj.getString("HW_STATUS");
            String ALACARTE = paketObj.getString("ALACARTE");
            String BASIC = paketObj.getString("BASIC");
            String VC = paketObj.getString("VC");
            String LNB = paketObj.getString("LNB");
            String ODU = paketObj.getString("ODU");
            String DSD = paketObj.getString("DSD");
            String Bundling = paketObj.getString("bundling");

            JSONObject alaObj = new JSONObject(ALACARTE);
            ala1 = alaObj.getString("1");
            ala2 = alaObj.getString("2");
            ala3 = alaObj.getString("3");

            JSONObject bscObj = new JSONObject(BASIC);
            bsc1 = bscObj.getString("1");
            bsc2 = bscObj.getString("2");
            bsc3 = bscObj.getString("3");

            JSONObject vcObj = new JSONObject(VC);
            vc1 = vcObj.getString("1");
            vc2 = vcObj.getString("2");
            vc3 = vcObj.getString("3");

            JSONObject lnbObj = new JSONObject(LNB);
            lnb1 = lnbObj.getString("1");
            lnb2 = lnbObj.getString("2");
            lnb3 = lnbObj.getString("3");

            JSONObject oduObj = new JSONObject(ODU);
            odu1 = oduObj.getString("1");
            odu2 = oduObj.getString("2");
            odu3 = oduObj.getString("3");

            JSONObject dsdObj = new JSONObject(DSD);
            dsd1 = dsdObj.getString("1");
            dsd2 = dsdObj.getString("2");
            dsd3 = dsdObj.getString("3");

            JSONObject bundleObj = new JSONObject(Bundling);
            simId = bundleObj.getString("sim_id");
            simValue = bundleObj.getString("sim_value");
            routeId = bundleObj.getString("router_id");
            routeValue = bundleObj.getString("router_value");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject obj = new JSONObject();
        try {
            obj.put("prospect_nbr", ftId);
            obj.put("sfl_code", sflCode);
            obj.put("pos_code", "DTD");
            obj.put("imei", utils.getIMEI());
            obj.put("user_name", username);
            obj.put("created_by", "sales");
            obj.put("brand_code", BRAND);
            //obj.put("register_date", regDate);
            obj.put("occupation", "WNI");

            JSONObject objProfile = new JSONObject();
            objProfile.put("first_name", firstName);
            objProfile.put("middle_name", middleName);
            objProfile.put("last_name", lastName);
            objProfile.put("tgl_lahir", birthDate);
            objProfile.put("tempat_lahir", birthPlace);
            obj.put("profile", objProfile);

            obj.put("home_phone", telephone);
            obj.put("mobile_phone", hp);
            obj.put("work_phone", "");
            obj.put("email", email);
            obj.put("religion", religion);
            obj.put("work", job);
            obj.put("income", income);
            obj.put("gender", gender);

            JSONObject objIdentity = new JSONObject();
            objIdentity.put("identity_type", identityType);
            objIdentity.put("identity_nbr", identityNo);
            objIdentity.put("identity_period", validityPriod);
            obj.put("identity", objIdentity);

            JSONObject objEmergency = new JSONObject();
            objEmergency.put("emergency_first_name", eFirstName);
            objEmergency.put("emergency_middle_name", eMiddleName);
            objEmergency.put("emergency_last_name", eLastName);
            objEmergency.put("emergency_phone", eTelephone);
            objEmergency.put("emergency_address", eAddress);
            objEmergency.put("relation", eRelationship);
            obj.put("emergency_contact", objEmergency);

            JSONObject objAddress = new JSONObject();
            objAddress.put("building_type", homeType);
            objAddress.put("biulding_status", homeStatus);

            JSONArray dataArray = new JSONArray();
            JSONObject objData = new JSONObject();
            JSONObject objData2 = new JSONObject();
            JSONObject objData3 = new JSONObject();
            objData.put("address_type", "BIL");
            objData.put("street", billingAddress);
            objData.put("kelurahan", billingKelurahan);
            objData.put("kecamatan", billingKecamatan);
            objData.put("kota", billingCity);
            objData.put("provinsi", billingProvince);
            objData.put("zipcode", billZipcode);
            objData.put("rt", rt_bill);
            objData.put("rw", rw_bill);

            objData2.put("address_type", "INS");
            objData2.put("street", installAddress);
            objData2.put("kelurahan", installKelurahan);
            objData2.put("kecamatan", installKecamatan);
            objData2.put("kota", installCity);
            objData2.put("provinsi", installProvince);
            objData2.put("zipcode", installZipcode);
            objData2.put("rt", rt_install);
            objData2.put("rw", rw_install);

            objData3.put("address_type", "PRI");
            objData3.put("street", address);
            objData3.put("kelurahan", kelurahan);
            objData3.put("kecamatan", kecamatan);
            objData3.put("kota", city);
            objData3.put("provinsi", province);
            objData3.put("zipcode", zipCode);
            objData3.put("no_rumah", homeNo);
            objData3.put("rt", rt);
            objData3.put("rw", rw);
            objData3.put("pln", pln);
            dataArray.put(objData);
            dataArray.put(objData2);
            dataArray.put(objData3);
            objAddress.put("data", dataArray);
            obj.put("address", objAddress);
            obj.put("image", imageArray);

            JSONObject objPayment = new JSONObject();
            objPayment.put("period", period.substring(0, 1));
            objPayment.put("method", method.substring(0, 1));
            if (!amount.isEmpty()) {
                objPayment.put("amount", amount);
            } else {
                objPayment.put("amount", "");
            }
            objPayment.put("remark", remark);
            objPayment.put("account_number", account_number);
            objPayment.put("name", name);
            objPayment.put("bank", bank);
            objPayment.put("expiry_date", expiry_date);
            if (!ccType.equalsIgnoreCase("")) {
                objPayment.put("cc_type", ccType.substring(0, 1));
            } else {
                objPayment.put("cc_type", "");
            }
            objPayment.put("cc_no", ccNumber);
            objPayment.put("payment_date", paymentDate);
            obj.put("payment", objPayment);

            obj.put("latitude", latitude);
            obj.put("longitude", longitude);
            JSONObject objServices = new JSONObject();
            objServices.put("brand", BRAND);
            objServices.put("promo_code", promoCode);
            objServices.put("isMulti", isMulty);
            objServices.put("hardware_status", HW_STATUS);

            objServices.put("install_schedule_date", installDate);
            objServices.put("install_schedule_time", installTime);

            JSONArray hardwareArray = new JSONArray();
            JSONObject objHardware = new JSONObject();
            JSONObject objHardware2 = new JSONObject();
            JSONObject objHardware3 = new JSONObject();
            objHardware.put("vc", vc1);
            objHardware2.put("vc", vc2);
            objHardware3.put("vc", vc3);
            objHardware.put("dsd", dsd1);
            objHardware2.put("dsd", dsd2);
            objHardware3.put("dsd", dsd3);
            objHardware.put("lnb", lnb1);
            objHardware2.put("lnb", lnb2);
            objHardware3.put("lnb", lnb3);
            objHardware.put("dish", odu1);
            objHardware2.put("dish", odu2);
            objHardware3.put("dish", odu3);
            hardwareArray.put(objHardware);
            hardwareArray.put(objHardware2);
            hardwareArray.put(objHardware3);
            objServices.put("hardware", hardwareArray);

            JSONObject objPackages;
            JSONArray packagesArray = new JSONArray();

            if (bsc1 != null && !bsc1.equalsIgnoreCase("")) {
                objPackages = new JSONObject();
                JSONArray alacarteArray = new JSONArray();
                JSONObject objAlacarte;
                listProduct = productAdapter.getDatabyCondition(TableMasterPackage.fROWID, bsc1);
                objPackages.put("code", bsc1);
                Log.d("FTStatus", "brand =" + listProduct.get(listProduct.size() - 1).getProduct_name());
                objPackages.put("name", listProduct.get(listProduct.size() - 1).getProduct_name());
                // {"ALACARTE":{"3":"","2":"","1":"5#CINEMA1,6#CINEMA2,7#CINEMA3"}
                if (ala1 != null) {
                    try {
                        String[] ar = ala1.split("[,]");
                        for (int i = 0; i < ar.length; i++) {
                            String[] a = ar[i].split("#");
                            objAlacarte = new JSONObject();
                            objAlacarte.put("code", a[0]);
                            listProduct = productAdapter.getDatabyCondition(TableMasterPackage.fROWID, a[0]);
                            Log.d("FTStatus", "alacarte =" + listProduct.get(listProduct.size() - 1).getProduct_name());
                            objAlacarte.put("name", listProduct.get(listProduct.size() - 1).getProduct_name());
                            alacarteArray.put(objAlacarte);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                objPackages.put("ala_carte", alacarteArray);
                packagesArray.put(objPackages);
            } else {
                objPackages = new JSONObject();
                JSONArray alacarteArray = new JSONArray();
                JSONObject objAlacarte;
                objPackages.put("code", "1");
                objPackages.put("name", "SUPER GALAXY 2014");
                objAlacarte = new JSONObject();
                objAlacarte.put("code", "");
                objAlacarte.put("name", "");
                alacarteArray.put(objAlacarte);
                objPackages.put("ala_carte", alacarteArray);
                packagesArray.put(objPackages);
            }

            if (bsc2 != null && !bsc2.equalsIgnoreCase("")) {
                objPackages = new JSONObject();
                JSONArray alacarteArray = new JSONArray();
                JSONObject objAlacarte;
                listProduct = productAdapter.getDatabyCondition(TableMasterPackage.fROWID, bsc2);
                objPackages.put("code", bsc2);
                objPackages.put("name", listProduct.get(listProduct.size() - 1).getProduct_name());
                if (ala2 != null) {
                    try {
                        String[] ar = ala2.split("[,]");
                        for (int i = 0; i < ar.length; i++) {
                            String[] a = ar[i].split("#");
                            objAlacarte = new JSONObject();
                            objAlacarte.put("code", a[0]);
                            listProduct = productAdapter.getDatabyCondition(TableMasterPackage.fROWID, a[0]);
                            objAlacarte.put("name", listProduct.get(listProduct.size() - 1).getProduct_name());
                            alacarteArray.put(objAlacarte);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                objPackages.put("ala_carte", alacarteArray);
                packagesArray.put(objPackages);
            }

            if (bsc3 != null && !bsc3.equalsIgnoreCase("")) {
                objPackages = new JSONObject();
                JSONArray alacarteArray = new JSONArray();
                JSONObject objAlacarte;
                listProduct = productAdapter.getDatabyCondition(TableMasterPackage.fROWID, bsc3);
                objPackages.put("code", bsc3);
                objPackages.put("name", listProduct.get(listProduct.size() - 1).getProduct_name());
                if (ala3 != null) {
                    try {
                        String[] ar = ala3.split("[,]");
                        for (int i = 0; i < ar.length; i++) {
                            String[] a = ar[i].split("#");
                            objAlacarte = new JSONObject();
                            objAlacarte.put("code", a[0]);
                            listProduct = productAdapter.getDatabyCondition(TableMasterPackage.fROWID, a[0]);
                            objAlacarte.put("name", listProduct.get(listProduct.size() - 1).getProduct_name());
                            alacarteArray.put(objAlacarte);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                objPackages.put("ala_carte", alacarteArray);
                packagesArray.put(objPackages);
            }

            if (!additional.equalsIgnoreCase("{}")) {
                //Proses Additional Convert Json
                JSONObject objectAdditional = new JSONObject(additional);
                JSONObject objAddResult = new JSONObject();
                JSONArray arrayAddResult = new JSONArray();
                JSONArray arrayCheckAdd = objectAdditional.getJSONArray(Config.KEY_DATA);
                for (int i = 0; i < arrayCheckAdd.length(); i++) {
                    JSONObject objectResult = arrayCheckAdd.getJSONObject(i);
                    String add_id = objectResult.getString("additional_id");
                    String add_total = objectResult.getString("additional_total");

                    JSONObject objectChecklist2 = new JSONObject();
                    objectChecklist2.put("additional_id", add_id);
                    objectChecklist2.put("additional_total", add_total);
                    arrayAddResult.put(objectChecklist2);
                    objAddResult.put("data", arrayAddResult);
                }
                //End Proses Additional
                objServices.put("additional", objAddResult);
            } else {
                objServices.put("additional", "");
            }

            JSONObject objectBundle = new JSONObject();
            objectBundle.put("sim_id", simId);
            objectBundle.put("sim_value", simValue);
            objectBundle.put("router_id", routeId);
            objectBundle.put("router_value", routeValue);

            objServices.put("bundling", objectBundle);

            objServices.put("packages", packagesArray);
            obj.put("services", objServices);


            //Additional Signature

            JSONObject addSign = new JSONObject();
            addSign.put("aggrement", signature);
            if (signBedaAlamat != null && !signBedaAlamat.equalsIgnoreCase("")) {
                JSONObject objBedaAlamat = new JSONObject(signBedaAlamat);
                String sign = objBedaAlamat.getString("sign");
                String reason = objBedaAlamat.getString("reason");
                String home_status = objBedaAlamat.getString("home_status");
                String sign_sales = objBedaAlamat.getString("sales_sign");
                JSONObject resultBedaAlamat = new JSONObject();
                resultBedaAlamat.put("sign", sign);
                resultBedaAlamat.put("reason", reason);
                resultBedaAlamat.put("home_status", home_status);
                resultBedaAlamat.put("sales_sign", sign_sales);
                addSign.put("beda_alamat", resultBedaAlamat);
            }
            if (signSewa != null && !signSewa.equalsIgnoreCase("")) {
                addSign.put("rumah_sewa", signSewa);
            }

            if (signMultiAccount != null && !signMultiAccount.equalsIgnoreCase("")) {
                JSONObject objMultiAcc = new JSONObject(signMultiAccount);
                String sign = objMultiAcc.getString("sign");
                String status = objMultiAcc.getString("status");
                String reason = objMultiAcc.getString("reason");
                String cust = objMultiAcc.getString("cust_number");
                String vc = objMultiAcc.getString("vc_number");
                JSONObject resultMultiAcc = new JSONObject();
                resultMultiAcc.put("sign", sign);
                resultMultiAcc.put("status", status);
                resultMultiAcc.put("reason", reason);
                resultMultiAcc.put("cust_number", cust);
                resultMultiAcc.put("vc_number", vc);
                addSign.put("multi_account", resultMultiAcc);
            }

            if (signBundlingIndosat != null && !signBundlingIndosat.equalsIgnoreCase("")) {
                JSONObject objBundling = new JSONObject(signBundlingIndosat);
                String sign = objBundling.getString("sign");
                String payment_method = objBundling.getString("payment_method");
                String router = objBundling.getString("router");
                String iccid = objBundling.getString("iccid");
                String imei = objBundling.getString("imei");
                JSONObject resultBundling = new JSONObject();
                resultBundling.put("sign", sign);
                resultBundling.put("payment_method", payment_method);
                resultBundling.put("router", router);
                resultBundling.put("iccid", iccid);
                resultBundling.put("imei", imei);
                addSign.put("sign_bun_isat", resultBundling);
            }

            if (addSign != null) {
                obj.put("additional_sign", addSign);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("MSStatus", "obj= " + obj);
        String values = obj.toString().replaceAll("'", "''"); //Perubahan dari & ke \'
        return values;
    }

    private void setMapMarker() {
        //latitude    = Double.parseDouble(utils.getLatitude());
        // longitude   = Double.parseDouble(utils.getLongitude());
        addressLine = utils.getAddressLine();
        //Log.d("MSStatus", "long =" + longitude + "  lat=" + latitude);
        if (mMap == null) {
            FragmentManager fm = getChildFragmentManager();
            fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
            if (fragment == null) {
                fragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.map, fragment).commit();
            }
            fragment.getMapAsync(this);
            //mMap = fragment.getMap();
            //mMap =  ((SupportMapFragment)  getChildFragmentManager()
            //        .findFragmentById(R.id.map)).getMap();
        }
    }

    /*private void setUpMap() {
        try {
            if (mMap == null) {
                mMap = fragment.getMap();
                stopMap = false;
            }
            // For showing a move to my loction button
            if (ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);

            mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    if (stopMap) return; //ari
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    latLng = location;
                    Geocoder geocoder;
                    List<Address> addresses = null;
                    geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    if (marker == null && latitude!=null && longitude!=null) {
                        //mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                        // For dropping a marker at a point on the Map
                        marker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("My Location")
                                .snippet("Alamat Prospek").draggable(true).alpha(0.7f).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                        // For zooming automatically to the Dropped PIN Location
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,
                                longitude), 17.0f));
                        try {
                            addresses = geocoder.getFromLocation(latitude, longitude, 1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (addresses != null && addresses.size() != 0) {
                            addressLine = addresses.get(0).getAddressLine(0);
                        } else {
                            addressLine = "";
                        }
                        String a = String.valueOf(latitude) + ";" + String.valueOf(longitude) + ";" + addressLine;
                        getDataFromDB(a);
                        marker.showInfoWindow();
                    }
                    ca = new StatusAdapter(getActivity(), createList(3), list);
                    recyclerView.setAdapter(ca);
                    ca.notifyDataSetChanged();
                    stopMap = true;
                }
            });


            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    View v = getActivity().getLayoutInflater().inflate(R.layout.info_windows, null);
                    _addressLine = (TextView) v.findViewById(R.id.address_line);
                    _addressLine.setText(addressLine);
                    return v;
                }
            });
            //setMapMarker();
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/


    /*@Override
    public void onResume() {
        super.onResume();
        setUpMap();
    }*/

    @Override
    public void onResume() {
        super.onResume();
        String a = String.valueOf(latitude) + ";" + String.valueOf(longitude) + ";" + addressLine;
        getDataFromDB(a);
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }

        if (!checkReady()) {
            return;
        }
        // Clear the map because we don't want duplicates of the markers.
        mMap.clear();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!checkReady()) {
            return;
        }
        mMap.clear();

        if (marker != null) {
            marker.remove();
            marker = null;
        }
    }

    /*@Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class
                    .getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }*/

    private boolean checkReady() {
        if (mMap == null) {
            //Toast.makeText(getActivity(), R.string.map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getChildFragmentManager(), "dialog");
    }

    /*
            @Override
            public void onViewCreated(View view, Bundle savedInstanceState) {
                if (mMap != null)
                    setUpMap();

                if (mMap == null) {
                    mMap =  ((SupportMapFragment) getChildFragmentManager()
                            .findFragmentById(R.id.map)).getMap();
                    mMap.setOnMarkerDragListener(this);
                    mMap.setOnMapLongClickListener(this);
                    mMap.setOnMapClickListener(this);
                    if (mMap != null)
                        setUpMap();
                }
            }
        */
    @Override
    public void onMarkerDrag(Marker arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(getActivity(), LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        LatLng dragPosition = marker.getPosition();
        double dragLat = dragPosition.latitude;
        double dragLong = dragPosition.longitude;
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        int distance = distance(latLng, dragPosition);
        Log.d("FTStatus", "coba= " + distance);
        if (distance > 100) {
            utils.showAlertDialog(getActivity(), "Jarak Marker Lebih Dari 100 Meter", "Silahkan pindahkan posisi marker", false);
            this.marker.remove();
            this.marker = null;
            stopMap = false; // ari
            //setUpMap();
            return;
        }
        latitude = dragLat;
        longitude = dragLong;
        try {
            addresses = geocoder.getFromLocation(dragLat, dragLong, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() != 0) {
            addressLine = addresses.get(0).getAddressLine(0);
        } else {
            addressLine = "";
        }
        marker.showInfoWindow();
        /*
        if (_addressLine!=null ) {
            _addressLine.setText(addressLine);
        }

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View v = getActivity().getLayoutInflater().inflate(R.layout.info_windows, null);
                _addressLine = (TextView) v.findViewById(R.id.address_line);
                _addressLine.setText(addressLine);
                return v;
            }
        });*/

        String a = String.valueOf(latitude) + ";" + String.valueOf(longitude) + ";" + addressLine;
        getDataFromDB(a);
        ca = new StatusAdapter(getActivity(), createList(3), list);
        recyclerView.setAdapter(ca);
        ca.notifyDataSetChanged();
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        marker.showInfoWindow();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_save, menu);
        this.menu = menu;
        alert = this.menu.findItem(R.id.action_menu_commit);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu_commit:
                new SendToServerTask().execute();
                break;
            case R.id.action_menu_location:
                setLocation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * Method for get refresh longitude and latitude
     */
    private void setLocation() {
        String stringLatitude = utils.getLatitude();
        String stringLongitude = utils.getLongitude();
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        if (stringLatitude != null && stringLatitude.length() > 1) {
            latitude = Double.valueOf(stringLatitude);
            longitude = Double.valueOf(stringLongitude);
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (addresses != null && addresses.size() != 0) {
                addressLine = addresses.get(0).getAddressLine(0);
            } else {
                addressLine = "";
            }
            String a = String.valueOf(latitude) + ";" + String.valueOf(longitude) + ";" + addressLine;
            getDataFromDB(a);
            ca = new StatusAdapter(getActivity(), createList(3), list);
            recyclerView.setAdapter(ca);
            ca.notifyDataSetChanged();
            Toast.makeText(getActivity(), "Long= " + stringLongitude + " lat= " + stringLatitude, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Longitude dan latitude kosong", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;

            mMap.setOnMyLocationButtonClickListener(this);
            enableMyLocation();

            mMap.getUiSettings().setZoomControlsEnabled(false);
            mMap.setOnMarkerClickListener(this);
            mMap.setOnMarkerDragListener(this);
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            // For showing a move to my loction button
            if (ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);

            mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    //Toast.makeText(getContext(), "onMyLocationChange", Toast.LENGTH_SHORT).show();
                    //if (stopMap) return; //ari
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    latLng = location;
                    Geocoder geocoder;
                    List<Address> addresses = null;
                    geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    if (marker == null && latitude != null && longitude != null) {
                        //mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                        // For dropping a marker at a point on the Map
                        marker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("My Location")
                                .snippet("Alamat Prospek").draggable(true).alpha(0.7f).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                        // For zooming automatically to the Dropped PIN Location
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,
                                longitude), 17.0f));
                        try {
                            addresses = geocoder.getFromLocation(latitude, longitude, 1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (addresses != null && addresses.size() != 0) {
                            addressLine = addresses.get(0).getAddressLine(0);
                        } else {
                            addressLine = "";
                        }
                        String a = String.valueOf(latitude) + ";" + String.valueOf(longitude) + ";" + addressLine;
                        getDataFromDB(a);
                        marker.showInfoWindow();
                    }
                    /*
                    ca = new StatusAdapter(getActivity(), createList(3), list);
                    recyclerView.setAdapter(ca);
                    ca.notifyDataSetChanged();
                    //stopMap = true;
                    */
                }
            });


            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    View v = getActivity().getLayoutInflater().inflate(R.layout.info_windows, null);
                    _addressLine = (TextView) v.findViewById(R.id.address_line);
                    _addressLine.setText(addressLine);
                    return v;
                }
            });
            //setMapMarker();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class SendToServerTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Log.d("FTStatus", "long=" + longitude + " lat=" + latitude + " add=" + addressLine);
            if (isTaskRunning) {
                this.cancel(true);
                return;
            } else if (profile == null || profile.length() < 1) {
                Toast.makeText(getActivity(), "Silahkan masukkan data profile", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (emergency == null || emergency.length() < 1) {
                Toast.makeText(getActivity(), "Silahkan masukkan data emergency", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (paket == null || paket.length() < 1) {
                Toast.makeText(getActivity(), "Silahkan masukkan data paket", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (signature == null || signature.length() < 1) {
                Toast.makeText(getActivity(), "Silahkan tanda tangan", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (latitude == null || String.valueOf(latitude).equalsIgnoreCase("0.0")) {
                Toast.makeText(getActivity(), "Latitude kosong", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (longitude == null || String.valueOf(longitude).equalsIgnoreCase("0.0")) {
                Toast.makeText(getActivity(), "Longitude kosong", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (!checkImage()) {
                Toast.makeText(getActivity(), "Check attachment", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            }

            signal = utils.checkSignal(getActivity());
            if (_progressDialog != null) {
                _progressDialog.dismiss();
            }
            _progressDialog = new ProgressDialog(getActivity());
            _progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
            _progressDialog.setIndeterminate(false);
            _progressDialog.setCancelable(false);
            _progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                isTaskRunning = true;
                value = generateJson().toString();
                TablePlanAdapter mTablePlanAdapter = new TablePlanAdapter(getActivity());
                TableLogLoginAdapter mTableLogLoginAdapter = new TableLogLoginAdapter(getActivity());
                List<TableLogLogin> listLogLogin = mTableLogLoginAdapter.getLastData();
                token = sessionPref.getString(TableLogLogin.C_TOKEN, "");
                sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
                List<TablePlan> listPlan = mTablePlanAdapter.getDatabySfl(getActivity(), TablePlan.KEY_PLAN_DATE, utils.getCurrentDate());
                if (listPlan != null && listPlan.size() > 0) {
                    planId = listPlan.get(0).getPlan_id();
                }
                imei = utils.getIMEI();
                url = ConnectionManager.CM_ENTRY;
                response = ConnectionManager.requestToServer(url, imei, token, value, planId, Config.version, String.valueOf(signal), "1", getActivity());
                jsonObject = new JSONObject(response.toString());
                if (jsonObject != null) {
                    status = jsonObject.getString(Config.KEY_STATUS);
                    data = jsonObject.getString(Config.KEY_MESSAGE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                isTaskRunning = false;
                TableFTSOHAdapter db = new TableFTSOHAdapter(getActivity());
                //TablePlanAdapter dbPlan = new TablePlanAdapter(getActivity());

                if (data != null) {
                    Toast.makeText(getActivity(), data, Toast.LENGTH_SHORT).show();
                }

                if (status != null && status.equalsIgnoreCase("200")) {
                    String planId = "", target = "", result = "", date = "";
                    try {
                        TablePlanAdapter mPlanAdapter = new TablePlanAdapter(getActivity());
                        List<TablePlan> listPlan = new ArrayList<TablePlan>();
                        listPlan = mPlanAdapter.getDatabySfl(getActivity(), TablePlan.KEY_PLAN_DATE,
                                utils.getCurrentDate());
                    /*
                    if (listPlan!=null && listPlan.size()>0) {
                        planId  = listPlan.get(listPlan.size()-1).getPlanId();
                        result  = listPlan.get(listPlan.size()-1).getResult();
                        target  = listPlan.get(listPlan.size()-1).getTarget();
                        if (!target.equalsIgnoreCase(result)){
                            result = String.valueOf(Integer.parseInt(result)+1);
                            mPlanAdapter.open();
                            mPlanAdapter.updateByCondition(getActivity(), TablePlan.KEY_RESULT, result, planId, TablePlan.KEY_PLAN_ID);
                            mPlanAdapter.close();
                        } else {
                            mPlanAdapter.open();
                            mPlanAdapter.updateByCondition(getActivity(), TablePlan.KEY_STATUS, "1", planId, TablePlan.KEY_PLAN_ID);
                            mPlanAdapter.close();
                        }
                    }*/
                        TableFTSOHAdapter mTableFreeTrialCohAdapter = new TableFTSOHAdapter(getActivity());
                        mTableFreeTrialCohAdapter.updatePartial(getActivity(), TableFreeTrialCoh.KEY_HW_STATUS, "DEL", TableFreeTrialCoh.KEY_HW_SN, VC);
                        mTableFreeTrialCohAdapter.updatePartial(getActivity(), TableFreeTrialCoh.KEY_HW_STATUS, "DEL", TableFreeTrialCoh.KEY_HW_SN, LNB);
                        mTableFreeTrialCohAdapter.updatePartial(getActivity(), TableFreeTrialCoh.KEY_HW_STATUS, "DEL", TableFreeTrialCoh.KEY_HW_SN, DSD);
                        mTableFreeTrialCohAdapter.updatePartial(getActivity(), TableFreeTrialCoh.KEY_HW_STATUS, "DEL", TableFreeTrialCoh.KEY_HW_SN, ODU);
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (_progressDialog != null) {
                            _progressDialog.dismiss();
                        }
                    }
                    if (_progressDialog != null) {
                        _progressDialog.dismiss();
                    }
                    SharedPreferences sessionPref = getActivity().getSharedPreferences("formId", Context.MODE_PRIVATE);
                    SharedPreferences.Editor shareEditor = sessionPref.edit();
                    shareEditor.remove("fn");
                    shareEditor.commit();
                    getActivity().finish();
                } else {
                    if (_progressDialog != null) {
                        _progressDialog.dismiss();
                    }
                    utils.showErrorDlg(handler, getActivity().getResources().getString(R.string.network_error),
                            getActivity());
                }
            } catch (Exception e) {
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
                e.printStackTrace();
            }
        }
    }

    private boolean checkImage() {
        boolean sign = ((FormProDTD) getActivity()).isExistSign;
        boolean ktp = ((FormProDTD) getActivity()).isExistKtp;
        boolean rmh = ((FormProDTD) getActivity()).isExistRmh;
        boolean ling = ((FormProDTD) getActivity()).isExistLing;
        boolean instal = ((FormProDTD) getActivity()).isExistInstall;

        if (!sign) {
            return false;
        }
        if (!ktp) {
            return false;
        }
        if (!rmh) {
            return false;
        }
        if (!ling) {
            return false;
        }
        if (!instal) {
            return false;
        }
        return true;
    }

    /**
     * Method for count distance location
     */
    public double CalculationByDistance(Location StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.getLatitude();
        double lat2 = EndP.latitude;
        double lon1 = StartP.getLongitude();
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);

        return Radius * c;
    }

    public static int distance(Location StartP, LatLng EndP) {
        double earthRadius = 6371000; //meters
        double lat1 = StartP.getLatitude();
        double lat2 = EndP.latitude;
        double lng1 = StartP.getLongitude();
        double lng2 = EndP.longitude;

        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        int dist = (int) (earthRadius * c);
        return dist;
    }
}
