package myskysfa.com.sfa.main.menu.dtdproject;


import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.ListAdapterPro;
import myskysfa.com.sfa.database.TableFreeTrial;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.TablePlan;
import myskysfa.com.sfa.database.db_adapter.TableFreeTrialAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePlanAdapter;
import myskysfa.com.sfa.main.menu.FreeTrialPro;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionDetector;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProTaskList extends Fragment {
    private ImageButton fab;
    private boolean isTaskRunning = false;
    private ProgressDialog _progressDialog;
    private int offset = 0, total_plan;
    private String url, response, message, token, imei, sflCode, data, ftId, regDate, name, phone, hp,
            brand, createdDate, statusDesc, username, value, mFtId, planId, planDate, target, zipCode,
            area, tsName, tsCode, planUrl, productName, mFilter, header;
    private boolean status = false;
    private ListView listView;
    private Utils utils;
    private Handler handler;
    private ProgressBar progressBarFooter;
    private TableFreeTrialAdapter dbAdapter;
    private ListAdapterPro adapter;
    View footer;
    JSONArray jsonArray;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    private PlanTask planTask;
    private List<TableFreeTrial> list;
    private Menu menu;
    private MenuItem alert;
    private TextView tv;
    private SharedPreferences sessionPref;
    private SwipeRefreshLayout swipeLayout;
    private int pageCount, signal;
    private ArrayList<String> dataArray;
    public int TOTAL_LIST_ITEMS;
    public int NUM_ITEMS_PAGE = 10;
    private TablePlanAdapter planDbAdapter;

    public ProTaskList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pro_task_list, container, false);
        utils = new Utils(getActivity());
        handler = new Handler();
        footer = inflater.inflate(R.layout.footer_progress, null);
        sessionPref = getContext().getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        mFilter = this.getArguments().getString("filter");
        utils.setIMEI();
        fab = (ImageButton) view.findViewById(R.id.fab);
        listView = (ListView) view.findViewById(R.id.list);
        progressBarFooter = (ProgressBar) footer.findViewById(R.id.pbFooterLoading);
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeLayout.setEnabled(true);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                handler.post(refreshing);
            }
        });
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        fab.setOnClickListener(fabListener);
        if (_progressDialog != null) {
            _progressDialog.dismiss();
        }
        refreshItems();
        return view;
    }


    private void refreshItems() {
        cd = new ConnectionDetector(getContext());
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new FreeTrialTask().execute();
        } else {
            utils.showAlertDialog(getContext(), getResources().getString(R.string.no_internet1), getResources().getString(R.string.no_internet2), false);
        }
    }

    private final Runnable refreshing = new Runnable() {
        public void run() {
            try {
                // TODO : isRefreshing should be attached to your data request status
                if (swipeLayout.isRefreshing()) {
                    // re run the verification after 1 second
                    //handler.postDelayed(this, 1000);
                    refreshItems();
                } else {
                    // stop the animation after the data is fully loaded
                    swipeLayout.setRefreshing(false);
                    // TODO : update your list with the new data
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.info_menu, menu);
        this.menu = menu;
        alert = this.menu.findItem(R.id.count);
        alert.setTitle("0");
        /*alert.setActionView(R.layout.actionbar_badge_layout);
        RelativeLayout badgeLayout = (RelativeLayout) alert.getActionView();
        tv = (TextView) badgeLayout.findViewById(R.id.actionbar_notifcation_textview);
        tv.setText("0");*/
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu_commit:
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        adapter = new ListAdapterPro(getActivity(), list);
        listView.setAdapter(adapter);

        if (listView.getCount() < 11) {
            hideProgressBar();
        }
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int threshold = 1;
                int count = listView.getCount();
                if (scrollState == SCROLL_STATE_IDLE) {
                    if (listView.getLastVisiblePosition() >= count - threshold) {
                        cd = new ConnectionDetector(getActivity().getApplicationContext());
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            //url = ConnectionManager.CM_URL_FTDTD_LIST;
                            // new LoadMoreTask().execute();
                        } else {
                            utils.showAlertDialog(getActivity(), getActivity().getApplicationContext().getResources().getString(R.string.no_internet1),
                                    getActivity().getApplicationContext().getResources().getString(R.string.no_internet2), false);
                        }
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0 && visibleItemCount > 0 && listView.getChildAt(0).getTop() >= 0 || listView.getCount() == 0) {
                    swipeLayout.setEnabled(true);
                    if (listView.getCount() > 10) {
                        //showProgressBar();
                    } else if (listView.getCount() < 10) {
                        hideProgressBar();
                    }
                } else {
                    swipeLayout.setEnabled(false);
                }
            }
        });

        /*
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    item = (FreeTrialListItem) adapter.getItem(position);
                    if (!item.getStatus().equalsIgnoreCase("2")) {
                        Intent intent = new Intent(getActivity(), FormFTDTD.class);
                        intent.putExtra("isEdit", "true");
                        intent.putExtra("ftId", item.getFtId());
                        //intent.putExtra("value", item.getValue());
                        startActivity(intent);
                        getActivity().finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });*/

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                TableFreeTrial item = (TableFreeTrial) adapter.getItem(position);
                mFtId = item.getFt_id();
                statusDesc = item.getStatus();
                if (statusDesc.equalsIgnoreCase("batal")
                        || statusDesc.equalsIgnoreCase("2")
                        || statusDesc.equalsIgnoreCase("ep")
                        || statusDesc.equalsIgnoreCase("pending")) {
                    return false;
                }
                set_activation(statusDesc, mFtId);
                return true;
            }
        });
    }

    private View.OnClickListener fabListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Intent i = new Intent(getActivity(), FormFTDTD.class);
            generateVDate();
            Intent i = new Intent(getActivity(), FormProDTD.class);
            startActivity(i);
            //getActivity().finish();
        }
    };

    private void setTarget(String target) {
        System.out.println("set: " + target);
        tv.setTextSize(10);
        tv.setText(target);
    }

    private void getStockTaskValue() {
        TablePlanAdapter tablePlanAdapter = new TablePlanAdapter(getActivity());
        List<TablePlan> listPlan = new ArrayList<TablePlan>();
        List<TableFreeTrial> listFreetrial = new ArrayList<TableFreeTrial>();
        String date = utils.getCurrentDate();
        listPlan = tablePlanAdapter.getDatabySfl(getActivity(), TablePlan.KEY_PLAN_DATE, date);

        //dbAdapter = new TableFreeTrialAdapter(getActivity());
        //listFreetrial = dbAdapter.fetchToday(getActivity());

        if (listPlan != null && listPlan.size() > 0) {
            TablePlan item = listPlan.get(0);
            //listFreetrial = dbAdapter.getDatabyCondition(TableFreeTrial.KEY_PLAN_ID, item.getPlan_id());
            int result = total_plan;
            /*for (int a = 0; a < listFreetrial.size(); a++) {
                if (!listFreetrial.get(a).getValue().equalsIgnoreCase("TITLE")) {
                    result = result + 1;
                }
            }*/
            //listFreetrial = dbAdapter.fetchAll(getActivity());
            FreeTrialPro freeTrial = new FreeTrialPro();
            freeTrial.setTsName(item.getPlan_id() + " - " + item.getTs_name());
            if (result < Integer.parseInt(item.getTarget())) {
                String target = result + "/" + item.getTarget();
                alert.setTitle(target);
                fab.setVisibility(View.VISIBLE);
            } else if (result == Integer.parseInt(item.getTarget())) {
                String target = result + "/" + item.getTarget();
                alert.setTitle(target);
                fab.setVisibility(View.INVISIBLE);
            } else {
                alert.setTitle("0/0");
                fab.setVisibility(View.INVISIBLE);
            }
        } else {
            alert.setTitle("0/0");
            fab.setVisibility(View.INVISIBLE);
        }

        if (_progressDialog != null) {
            _progressDialog.dismiss();
        }
    }

    private class FreeTrialTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isTaskRunning) {
                this.cancel(true);
                return;
            }
            signal = utils.checkSignal(getActivity());
            if (_progressDialog != null) {
                _progressDialog.dismiss();
            }
            _progressDialog = new ProgressDialog(getActivity());
            _progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
            _progressDialog.setIndeterminate(false);
            _progressDialog.setCancelable(false);
            _progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                isTaskRunning = true;
                list = new ArrayList<TableFreeTrial>();
                if (sessionPref == null) {
                    //Toast.makeText(getContext(),"Silahkah refresh!",Toast.LENGTH_LONG).show();
                } else {
                    sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
                    username = sessionPref.getString(TableLogLogin.C_USER_NAME, "");
                    token = sessionPref.getString(TableLogLogin.C_TOKEN, "");
                    offset = 0;
                    imei = utils.getIMEI();
                    list.clear();
                    url = ConnectionManager.CM_LIST_PROSPECT + "/" + sflCode + "/" + mFilter;

                    response = ConnectionManager.requestFreeTrial(url, sflCode, imei, token,
                            username, offset, Config.version, String.valueOf(signal), getActivity());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject != null) {
                        status = jsonObject.getBoolean(Config.KEY_STATUS);
                        total_plan = jsonObject.getInt("count");
                        jsonArray = jsonObject.getJSONArray(Config.KEY_DATA);
                        if (status) {
                            dbAdapter = new TableFreeTrialAdapter(getActivity());
                            dbAdapter.deleteAll();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                ftId = obj.getString(Config.KEY_FT_ID);
                                sflCode = obj.getString(Config.KEY_SFLCODE);

                                regDate = "";
                                name = obj.getString("prospect_name");
                                //phone = obj.getString("home_phone");
                                hp = obj.getString("mobile_phone");
                                brand = obj.getString("brand_code");
                                createdDate = obj.getString("created_date");
                                statusDesc = obj.getString("status");
                                username = obj.getString("user_name");
                                area = obj.getString("alamat");
                                productName = obj.getString("product_name");
                                planId = obj.getString("plan_id");
                                value = obj.toString();

                                getHeaderList(createdDate);

                                dbAdapter.insertData(new TableFreeTrial(), ftId, "", brand, sflCode,
                                        createdDate, statusDesc, value, planId, header);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dbAdapter = new TableFreeTrialAdapter(getActivity());
                        dbAdapter.deleteAll();
                        StartAsyncTaskInParallel(planTask);
                        //utils.showErrorDlg(handler, getActivity().getResources().getString(R.string.network_error2),getActivity());
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                isTaskRunning = false;
                swipeLayout.setRefreshing(false);

                if (status) {
                    getData();
                    initView();
                    StartAsyncTaskInParallel(planTask);
                    //getStockTaskValue();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String getHeaderList(String date) {
        try {
            String currentDate = utils.getCurrentDateMerge();
            String listDate = utils.changeFormatDate(date);

            Long resultDay = Long.parseLong(currentDate.substring(6, 8)) - Long.parseLong(listDate.substring(6, 8));
            Long resultMonth = Long.parseLong(currentDate.substring(4, 6)) - Long.parseLong(listDate.substring(4, 6));
            Long resultYear = Long.parseLong(currentDate.substring(0, 4)) - Long.parseLong(listDate.substring(0, 4));

            if (resultYear > 0 && resultYear < 5) {
                header = "Last Year";
            } else if (resultMonth != 0) {
                header = "Last Month";
            } else if (resultDay > 7 && resultDay < 30) {
                header = "Last Week";
            } else if (resultDay > 0 && resultDay < 7) {
                header = "Last Day";
            } else if (resultYear >= 5) {
                header = "Older";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return header;
    }

    private class PlanTask extends AsyncTask<String, String, String> {

        boolean status;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            signal = utils.checkSignal(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                status = false;
                imei = utils.getIMEI();
                sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
                token = sessionPref.getString(TableLogLogin.C_TOKEN, "");
                username = sessionPref.getString(TableLogLogin.C_USER_NAME, "");
                planUrl = ConnectionManager.CM_PLAN_ALL + sflCode + "/1";
                response = ConnectionManager.requestPlan(planUrl, imei, token, username, Config.version, String.valueOf(signal), getActivity());
                Log.d("FreeTrial", "response= " + response);
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject != null) {
                    status = jsonObject.getBoolean(Config.KEY_STATUS);
                    JSONObject obj = new JSONObject(jsonObject.getString(Config.KEY_DATA));
                    if (status) {
                        planDbAdapter = new TablePlanAdapter(getActivity());
                        planDbAdapter.delete(getActivity());

                        sflCode = obj.getString(Config.KEY_PLAN_SFL);
                        planId = obj.getString(Config.KEY_PLAN_ID);
                        planDate = obj.getString(Config.KEY_PLAN_DATE);
                        zipCode = obj.getString(Config.KEY_PLAN_ZIPCODE);
                        area = obj.getString(Config.KEY_PLAN_AREA);
                        tsCode = obj.getString(Config.KEY_PLAN_TSCODE);
                        tsName = obj.getString(Config.KEY_PLAN_TSNAME);
                        target = obj.getString(Config.KEY_PLAN_TARGET);
                        value = obj.toString();

                        planDbAdapter.insertData(new TablePlan(), planId, planDate, zipCode, area, tsCode,
                                tsName, target, "0", sflCode, value, "0");

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (_progressDialog != null) {
                _progressDialog.dismiss();
            }
            try {
                if (status) {
                    getStockTaskValue();
                } else {
                    if (_progressDialog != null) {
                        _progressDialog.dismiss();
                    }
                    /*utils.showErrorDlg(handler, getActivity().getResources().getString(R.string.network_error),
                            getActivity());*/
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void getListFT() {
        dbAdapter = new TableFreeTrialAdapter(getActivity());
        list = dbAdapter.getDatabyPrefix(TableFreeTrial.KEY_FT_ID, "F");
        TOTAL_LIST_ITEMS = list.size();

        dataArray = new ArrayList<String>();

        int val = TOTAL_LIST_ITEMS % NUM_ITEMS_PAGE;
        val = val == 0 ? 0 : 1;
        pageCount = TOTAL_LIST_ITEMS / NUM_ITEMS_PAGE + val;
        loadList(0);
    }

    /*private void getDTDJP() {
        dbAdapter = new TableFreeTrialAdapter(getActivity());
        list = dbAdapter.getDatabyPrefix(TableFreeTrial.KEY_FT_ID, "P");
        TOTAL_LIST_ITEMS = list.size();

        dataArray = new ArrayList<String>();

        int val = TOTAL_LIST_ITEMS % NUM_ITEMS_PAGE;
        val = val == 0 ? 0 : 1;
        pageCount = TOTAL_LIST_ITEMS / NUM_ITEMS_PAGE + val;
        loadList(0);
    }

    private void getDTDPP() {
        dbAdapter = new TableFreeTrialAdapter(getActivity());
        list = dbAdapter.getDatabyPrefix(TableFreeTrial.KEY_FT_ID, "T");
        TOTAL_LIST_ITEMS = list.size();

        dataArray = new ArrayList<String>();

        int val = TOTAL_LIST_ITEMS % NUM_ITEMS_PAGE;
        val = val == 0 ? 0 : 1;
        pageCount = TOTAL_LIST_ITEMS / NUM_ITEMS_PAGE + val;
        loadList(0);
    }*/

    private void getData() {
        dbAdapter = new TableFreeTrialAdapter(getActivity());
        list = dbAdapter.getAllData();
        TOTAL_LIST_ITEMS = list.size();

        dataArray = new ArrayList<String>();

        int val = TOTAL_LIST_ITEMS % NUM_ITEMS_PAGE;
        val = val == 0 ? 0 : 1;
        pageCount = TOTAL_LIST_ITEMS / NUM_ITEMS_PAGE + val;
        loadList(0);
    }

    /*private void getListData() {
        *//*
        TablePlanAdapter tablePlanAdapter = new TablePlanAdapter(getActivity());
        dbAdapter = new TableFreeTrialAdapter(getActivity());
        List<PlanListItem> listPlan = new ArrayList<PlanListItem>();
        List<FreeTrialListItem> listProspect = new ArrayList<FreeTrialListItem>();
        String date = utils.getCurrentDate();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c.getTime());
        listProspect = dbAdapter.fetchBySfl(getActivity(), formattedDate, TableFreeTrial.KEY_CREATED);
        listPlan = tablePlanAdapter.fetchBySfl(getActivity(), date, TablePlan.KEY_PLAN_DATE);
        if (listPlan.size()==0) {
            fab.setVisibility(View.INVISIBLE);
        }
        for (int n=0; n<listPlan.size();n++) {
            PlanListItem item = listPlan.get(n);
            if (item != null) {
                if (item.getTarget().equalsIgnoreCase(String.valueOf(listProspect.size()))) {
                    fab.setVisibility(View.INVISIBLE);
                } else {
                    fab.setVisibility(View.VISIBLE);
                }
            }
        }
        *//*
        //TablePlanAdapter tablePlanAdapter = new TablePlanAdapter(getActivity());
        //String date = utils.getCurrentDate();
        //List<PlanListItem> listPlan = new ArrayList<PlanListItem>();
        //listPlan = tablePlanAdapter.fetchBySfl(getActivity(), date, TablePlan.KEY_PLAN_DATE);
        *//*if (listPlan.size()==0) {
            fab.setVisibility(View.INVISIBLE);
        } else {
            fab.setVisibility(View.VISIBLE);
        }*//*
        dbAdapter = new TableFreeTrialAdapter(getActivity());
        list = dbAdapter.getAllData();
        //lisCondition = dbAdapter.fetchAll(getActivity());
        TOTAL_LIST_ITEMS = list.size();

        dataArray = new ArrayList<String>();

        *//**
     * this block is for checking the number of pages
     * ====================================================
     *//*

        int val = TOTAL_LIST_ITEMS % NUM_ITEMS_PAGE;
        val = val == 0 ? 0 : 1;
        pageCount = TOTAL_LIST_ITEMS / NUM_ITEMS_PAGE + val;
        loadList(0);
    }*/

    /**
     * Method for loading data in listview
     *
     * @param number
     */
    private void loadList(int number) {
        List<TableFreeTrial> sort = new ArrayList<TableFreeTrial>();
        int start = number * NUM_ITEMS_PAGE;
        for (int i = start; i < (start) + NUM_ITEMS_PAGE; i++) {
            if (i < list.size()) {
                sort.add(list.get(i));
            } else {
                break;
            }
        }
    }
/*
    public void showProgressBar() {
        progressBarFooter.setVisibility(View.VISIBLE);
    }*/

    public void hideProgressBar() {
        progressBarFooter.setVisibility(View.GONE);
    }

    private void set_activation(String status, String ftId) {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View view = li.inflate(R.layout.activation_dialog, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        alertDialogBuilder.setView(view);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        TextView message = (TextView) view.findViewById(R.id.message);
        Button activation = (Button) view.findViewById(R.id.aktivasi);
        Button cancel = (Button) view.findViewById(R.id.batal);
        Button close = (Button) view.findViewById(R.id.cancel);
        alertDialog.show();
        if (status.equalsIgnoreCase("1")) {
            message.setText("Apakah anda ingin melakukan aktivasi untuk no prospect " + ftId + "?");
            alertDialogBuilder.setTitle("Aktivasi");
            activation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    url = ConnectionManager.CM_URL_ACTIVATION;
                    new ActivationTask().execute();
                }
            });
        } else if (status.equalsIgnoreCase("active") || status.equalsIgnoreCase("disconnect")) {
            message.setText("Apakah anda ingin berlangganan untuk no plan " + ftId + "?");
            alertDialogBuilder.setTitle("Langganan");
            activation.setText("EP");
            activation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    url = ConnectionManager.CM_URL_ACTIVATION;
                    // new ActivationTask().execute();
                    alertDialog.dismiss();
                }
            });
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                url = ConnectionManager.CM_URL_FTDTD_CANCEL;
                new CancelActivationTask().execute();
                alertDialog.dismiss();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private class ActivationTask extends AsyncTask<String, String, String> {
        String status = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isTaskRunning) {
                this.cancel(true);
                return;
            }
            signal = utils.checkSignal(getActivity());
            if (_progressDialog != null) {
                _progressDialog.dismiss();
            }
            _progressDialog = new ProgressDialog(getActivity());
            _progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
            _progressDialog.setIndeterminate(false);
            _progressDialog.setCancelable(false);
            _progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                isTaskRunning = true;
                token = sessionPref.getString(TableLogLogin.C_TOKEN, "");
                sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
                /*
                mTableLogLoginAdapter = new TableLogLoginAdapter(getActivity());
                listLogLogin          = mTableLogLoginAdapter.fetchLastData(getActivity(), TableLogLogin.fLOG_DATE);
                if (listLogLogin != null && listLogLogin.size()>0) {
                    for (int i=0; i < listLogLogin.size(); i++) {
                        LogLoginListItem item = listLogLogin.get(i);
                        //sflName     = item.getFullName();
                        //employeeId  = item.getEmployeeId();
                        token       = item.getToken();
                        sflCode     = item.getSflCode();
                        //username    = item.getUserName();
                        //userId      = item.getUid();
                    }
                }*/
                url = url + mFtId;
                response = ConnectionManager.requestActivation(url, imei, token, mFtId, Config.version, String.valueOf(signal), getActivity());
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject != null) {
                    status = jsonObject.getString(Config.KEY_STATUS);
                    message = jsonObject.getString(Config.KEY_MESSAGE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return message;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                isTaskRunning = false;
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }

                if (status.equalsIgnoreCase("200")) {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    url = ConnectionManager.CM_URL_MSDTD_LIST;
                    new FreeTrialTask().execute();
                } else {
                    utils.showErrorDlg(handler, getActivity().getResources().getString(R.string.network_error),
                            getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class CancelActivationTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isTaskRunning) {
                this.cancel(true);
                return;
            }
            signal = utils.checkSignal(getActivity());
            if (_progressDialog != null) {
                _progressDialog.dismiss();
            }
            _progressDialog = new ProgressDialog(getActivity());
            _progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
            _progressDialog.setIndeterminate(false);
            _progressDialog.setCancelable(true);
            _progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                isTaskRunning = true;
                status = false;
                token = sessionPref.getString(TableLogLogin.C_TOKEN, "");
                sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
                url = url + mFtId;
                response = ConnectionManager.requestActivation(url, imei, token, mFtId, Config.version, String.valueOf(signal), getActivity());
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject != null) {
                    status = jsonObject.getBoolean(Config.KEY_STATUS);
                    message = jsonObject.getString(Config.KEY_MESSAGE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return message;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                isTaskRunning = false;


                if (status) {
                    Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
                    url = ConnectionManager.CM_URL_MSDTD_LIST;
                    new FreeTrialTask().execute();
                } else {
                    utils.showErrorDlg(handler, getActivity().getResources().getString(R.string.network_error),
                            getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void StartAsyncTaskInParallel(PlanTask task) {
        task = new PlanTask();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            task.execute();
    }


    private String generateVDate() {
        String visit_date = null;
        try {
            visit_date = utils.getCurrentDateandTime();
            visit_date = utils.formatDate(visit_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SharedPreferences sesionPref = getActivity().getSharedPreferences("vDate", Context.MODE_PRIVATE);
        SharedPreferences.Editor shareEditor = sesionPref.edit();
        shareEditor.putString("vdate", visit_date);
        shareEditor.commit();
        return visit_date;
    }
}
