package myskysfa.com.sfa.main.menu.master;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.master.CatalogAdapter;
import myskysfa.com.sfa.database.TableMasterCatalog;
import myskysfa.com.sfa.database.db_adapter.TableMasterCatalogAdapter;
import myskysfa.com.sfa.utils.ConnectionDetector;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by admin on 6/20/2016.
 */
public class Katalog extends Fragment {
    private List<TableMasterCatalog> listMaterial;
    private TableMasterCatalogAdapter tblAdapter;
    private RecyclerView recyclerView;
    private CatalogAdapter adapter;
    private String status;
    private Utils utils;
    private Handler handler = new Handler();
    private SwipeRefreshLayout swipeRefreshLayout;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_katalog, container, false);
        recyclerView = (RecyclerView) viewGroup.findViewById(R.id.list_katalog);
        swipeRefreshLayout = (SwipeRefreshLayout) viewGroup.findViewById(R.id.swipe_container);
        tblAdapter = new TableMasterCatalogAdapter(getActivity());
        showList();
        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                /*cd = new ConnectionDetector(getContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new getDataCatalog().execute();
                } else {
                    utils.showAlertDialog(getContext(), getResources().getString(R.string.no_internet1),getResources().getString(R.string.no_internet2), false);
                }*/
                new getDataCatalog().execute();
            }
        });
        return viewGroup;
    }

    private void showList() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutParams);
        listMaterial = tblAdapter.getAllData();
        adapter = new CatalogAdapter(listMaterial);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Get Data From Server
     */
    protected class getDataCatalog extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeRefreshLayout.setRefreshing(true);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                if (listMaterial.isEmpty()){
                    TableMasterCatalogAdapter db = new TableMasterCatalogAdapter(getContext());
                    db.insertData(new TableMasterCatalog(), "1", "PEKERJAAN","1","PNS");
                    db.insertData(new TableMasterCatalog(), "2", "PEKERJAAN","1","SWASTA");
                    db.insertData(new TableMasterCatalog(), "3", "PEKERJAAN","1","WIRASWASTA");
                    db.insertData(new TableMasterCatalog(), "4", "HUBUNGAN","2","SUAMI");
                    db.insertData(new TableMasterCatalog(), "5", "HUBUNGAN","2","ISTRI");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
                swipeRefreshLayout.setRefreshing(false);
                showList();
        }
    }
}
