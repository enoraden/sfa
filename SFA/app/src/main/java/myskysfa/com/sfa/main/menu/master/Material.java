package myskysfa.com.sfa.main.menu.master;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.master.MaterialAdapter;
import myskysfa.com.sfa.database.TableMasterMaterial;
import myskysfa.com.sfa.database.db_adapter.TableMasterMaterialAdapter;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionDetector;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by Eno on 6/20/2016.
 */
public class Material extends Fragment {
    private List<TableMasterMaterial> listMaterial;
    private TableMasterMaterialAdapter tableAdapter;
    private RecyclerView recyclerView;
    private MaterialAdapter adapter;
    private String status;
    private Utils utils;
    private Handler handler = new Handler();
    private SwipeRefreshLayout swipeRefreshLayout;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_material, container, false);
        recyclerView = (RecyclerView) viewGroup.findViewById(R.id.list_material);
        swipeRefreshLayout = (SwipeRefreshLayout) viewGroup.findViewById(R.id.swipe_container);
        utils = new Utils(getActivity());
        tableAdapter = new TableMasterMaterialAdapter(getActivity());
        showList();
        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cd = new ConnectionDetector(getContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new getDataMaterial().execute();
                } else {
                    utils.showAlertDialog(getContext(), getResources().getString(R.string.no_internet1),getResources().getString(R.string.no_internet2), false);
                }
            }
        });
        return viewGroup;
    }

    private void showList() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        layoutParams.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutParams);
        listMaterial = tableAdapter.getAllData();
        adapter = new MaterialAdapter(getActivity(), listMaterial);
        recyclerView.setAdapter(adapter);
    }



    /**
     * Get Data From Server
     */
    protected class getDataMaterial extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeRefreshLayout.setRefreshing(true);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String url = ConnectionManager.CM_MASTER_MATERIAL;
                String response = ConnectionManager.requestMasterPackage(url, Config.version, getContext());
                System.out.println(response.toString());
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject != null) {
                    status = jsonObject.getString(Config.KEY_STATUS).toString();
                    if (status.equals("true")) {
                        JSONArray jsonData = new JSONArray(jsonObject.getString(Config.KEY_DATA).toString());
                        System.out.println("jsonData: " + jsonData);
                        if (jsonData != null) {
                            TableMasterMaterialAdapter db = new TableMasterMaterialAdapter(getActivity());
                            db.delete();
                            System.out.println("delete all");
                            for (int i = 0; i < jsonData.length(); i++) {
                                String id = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MATERIAL_ID);
                                String code = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MATERIAL_CODE);
                                String description = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MATERIAL_DESC);
                                String rate = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MATERIAL_RATE);
                                String class_code = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MATERIAL_CLASS_CODE);
                                String is_package = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MATERIAL_IS_PACKAGE);

                                db.insertData(new TableMasterMaterial(), id, code, description, rate, class_code, is_package);
                            }
                            //reloadRecycle();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (status != null && status.equals("true")) {
                swipeRefreshLayout.setRefreshing(false);
                showList();
            } else {
                swipeRefreshLayout.setRefreshing(false);
                if (isAdded()){
                    utils.showErrorDlg(handler, getResources().getString(R.string.network_error), getContext());
                }
            }
        }
    }

}
