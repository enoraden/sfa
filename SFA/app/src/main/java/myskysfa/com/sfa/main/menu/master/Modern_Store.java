package myskysfa.com.sfa.main.menu.master;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.List;
import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.master.ModernStoreAdapter;
import myskysfa.com.sfa.database.TableMasterMS;
import myskysfa.com.sfa.database.db_adapter.TableMSAdapter;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionDetector;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;
/**
 * Created by admin on 6/20/2016.
 */
public class Modern_Store extends Fragment {
    private List<TableMasterMS> listMaterial;
    private TableMSAdapter tblAdapter;
    private RecyclerView recyclerView;
    private ModernStoreAdapter adapter;
    private String status;
    private Utils utils;
    private Handler handler = new Handler();
    private SwipeRefreshLayout swipeRefreshLayout;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_modernstore, container, false);
        recyclerView = (RecyclerView) viewGroup.findViewById(R.id.list_modernstore);
        swipeRefreshLayout = (SwipeRefreshLayout) viewGroup.findViewById(R.id.swipe_container);
        utils = new Utils(getActivity());
        showList();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cd = new ConnectionDetector(getContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new getDataMS().execute();
                } else {
                    utils.showAlertDialog(getContext(), getResources().getString(R.string.no_internet1), getResources().getString(R.string.no_internet2), false);
                }
            }
        });
        return viewGroup;
    }
    private void showList() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        layoutParams.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutParams);
        tblAdapter = new TableMSAdapter(getActivity());
        listMaterial = tblAdapter.getAllData();
        adapter = new ModernStoreAdapter(getActivity(), listMaterial);
        recyclerView.setAdapter(adapter);
    }
    /**
     * Get Data From Server
     */
    protected class getDataMS extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeRefreshLayout.setRefreshing(true);
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                String url = ConnectionManager.CM_MASTER_MS;
                String response = ConnectionManager.requestMasterPackage(url, Config.version, getContext());
                System.out.println(response.toString());
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject != null) {
                    status = jsonObject.getString(Config.KEY_STATUS).toString();
                    if (status.equals("true")) {
                        JSONArray jsonData = new JSONArray(jsonObject.getString(Config.KEY_DATA).toString());
                        System.out.println("jsonData: " + jsonData);
                        if (jsonData != null) {
                            TableMSAdapter db = new TableMSAdapter(getContext());
                            db.delete(); // delete all master
                            System.out.println("delete all");
                            for (int i = 0; i < jsonData.length(); i++) {
                                String id = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MS_ID);
                                String ms_code = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MS_CODE);
                                String ms_entity = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MS_ENTITY);
                                String name = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MS_NAME);
                                String address = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MS_ADDRESS);
                                String zipcode = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MS_ZIPCODE);
                                String latitude = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MS_LAT);
                                String longitude = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_MS_LONG);
                                System.out.println("server: " + name);
                                db.insertData(new TableMasterMS(), id, name, address, zipcode,
                                        latitude, longitude, "", "", "", ms_code, ms_entity);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*if (message != null) {
                return message;
            }*/
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (status != null && status.equals("true")) {
                swipeRefreshLayout.setRefreshing(false);
                showList();
            } else {
                swipeRefreshLayout.setRefreshing(false);
                if (isAdded()){
                    utils.showErrorDlg(handler, getResources().getString(R.string.network_error), getContext());
                }
            }
        }
    }
}
