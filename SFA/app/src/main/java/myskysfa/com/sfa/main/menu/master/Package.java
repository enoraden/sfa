package myskysfa.com.sfa.main.menu.master;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.master.PackageAdapter;
import myskysfa.com.sfa.database.TableMasterPackage;
import myskysfa.com.sfa.database.db_adapter.TableMasterPackageAdapter;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionDetector;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by admin on 6/20/2016.
 */
public class Package extends Fragment {
    private List<TableMasterPackage> listPackage;
    private TableMasterPackageAdapter tblAdapter;
    private RecyclerView recyclerView;
    private PackageAdapter adapter;
    private String status;
    private Utils utils;
    private Handler handler = new Handler();
    private SwipeRefreshLayout swipeRefreshLayout;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_package, container, false);
        recyclerView = (RecyclerView) viewGroup.findViewById(R.id.list_package);
        swipeRefreshLayout = (SwipeRefreshLayout) viewGroup.findViewById(R.id.swipe_container);
        tblAdapter = new TableMasterPackageAdapter(getActivity());
        utils = new Utils(getActivity());
        showList();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cd = new ConnectionDetector(getContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new getDataPackage().execute();
                } else {
                    utils.showAlertDialog(getContext(), getResources().getString(R.string.no_internet1), getResources().getString(R.string.no_internet2), false);
                }
            }
        });
        return viewGroup;
    }

    private void showList() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        layoutParams.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutParams);
        listPackage = tblAdapter.getAllData();
        adapter = new PackageAdapter(getActivity(), listPackage);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Get Data From Server
     */
    protected class getDataPackage extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeRefreshLayout.setRefreshing(true);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String url = ConnectionManager.CM_URL_MASTER_PACKAGE;
                String response = ConnectionManager.requestMasterPackage(url, Config.version, getContext());
                System.out.println(response.toString());
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject != null) {
                    status = jsonObject.getString(Config.KEY_STATUS).toString();
                    if (status.equals("true")) {
                        JSONArray jsonData = new JSONArray(jsonObject.getString(Config.KEY_DATA).toString());
                        System.out.println("jsonData: " + jsonData);
                        if (jsonData != null) {
                            TableMasterPackageAdapter db = new TableMasterPackageAdapter(getContext());
                            db.delete(); // delete all master
                            System.out.println("delete all package");
                            for (int i = 0; i < jsonData.length(); i++) {
                                String brand = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_PACKAGE_BRAND);
                                int price = jsonData.getJSONObject(i).getInt(Config.KEY_MASTER_PACKAGE_PRICE);
                                int alacarte = jsonData.getJSONObject(i).getInt(Config.KEY_MASTER_PACKAGE_ALACARTE);
                                String product_name = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_PACKAGE_NAME);
                                String product_id = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_PACKAGE_ID);
                                String key_id = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_PACKAGE_KEY_ID);
                                String basic_id = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_PACKAGE_BASIC_ID);
                                db.insertData(new TableMasterPackage(), product_id, "1",
                                        product_name, price, alacarte, 1, brand, key_id, basic_id);
                            }
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*if (message != null) {
                return message;
            }*/
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (status != null && status.equals("true")) {
                swipeRefreshLayout.setRefreshing(false);
                showList();
            } else {
                swipeRefreshLayout.setRefreshing(false);
                if (isAdded()) {
                    utils.showErrorDlg(handler, getResources().getString(R.string.network_error), getContext());
                }
            }
        }
    }
}
