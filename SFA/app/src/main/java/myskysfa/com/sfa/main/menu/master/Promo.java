package myskysfa.com.sfa.main.menu.master;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.master.PromoAdapter;
import myskysfa.com.sfa.database.TableMasterPromo;
import myskysfa.com.sfa.database.db_adapter.TableMasterPromoAdapter;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionDetector;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by admin on 6/20/2016.
 */
public class Promo extends Fragment {
    private List<TableMasterPromo> listPromo;
    private TableMasterPromoAdapter tblAdapter;
    private RecyclerView recyclerView;
    private PromoAdapter adapter;
    private String status;
    private Utils utils;
    private Handler handler = new Handler();
    private SwipeRefreshLayout swipeRefreshLayout;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_promo, container, false);
        recyclerView = (RecyclerView) viewGroup.findViewById(R.id.list_promo);
        swipeRefreshLayout = (SwipeRefreshLayout) viewGroup.findViewById(R.id.swipe_container);
        tblAdapter = new TableMasterPromoAdapter(getActivity());
        utils = new Utils(getActivity());
        showList();
        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cd = new ConnectionDetector(getContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new getDataPromo().execute();
                } else {
                    utils.showAlertDialog(getContext(), getResources().getString(R.string.no_internet1),getResources().getString(R.string.no_internet2), false);
                }
            }
        });
        return viewGroup;
    }

    private void showList() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        layoutParams.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutParams);
        listPromo = tblAdapter.getAllData();
        adapter = new PromoAdapter(getActivity(), listPromo);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Get Data From Server
     */
    protected class getDataPromo extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeRefreshLayout.setRefreshing(true);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String url = ConnectionManager.CM_URL_MASTER_PROMOTION;
                String response = ConnectionManager.requestMasterPackage(url,Config.version, getContext());
                System.out.println(response.toString());
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject != null) {
                    status = jsonObject.getString(Config.KEY_STATUS).toString();
                    if(status.equals("true")){
                        JSONArray jsonData = new JSONArray (jsonObject.getString(Config.KEY_DATA).toString());
                        System.out.println("jsonData: "+jsonData);
                        if(jsonData != null) {
                            TableMasterPromoAdapter db = new TableMasterPromoAdapter(getContext());
                            db.delete(); // delete all master
                            System.out.println("delete all");
                            for (int i=0;i<jsonData.length();i++) {
                                String promo_id = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_ID);
                                String promo_code = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_CODE);
                                String promo_desc = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_DESC);
                                String promo_start = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_START);
                                String promo_end = jsonData.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_END);

                                db.insertData(new TableMasterPromo(),promo_id,promo_code,promo_desc,promo_start,promo_end,"A","1");
                            }
                        }
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*if (message != null) {
                return message;
            }*/
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (status != null && status.equals("true")) {
                swipeRefreshLayout.setRefreshing(false);
                showList();
            } else {
                swipeRefreshLayout.setRefreshing(false);
                if (isAdded()){
                    utils.showErrorDlg(handler, getResources().getString(R.string.network_error), getContext());
                }
            }
        }
    }
}
