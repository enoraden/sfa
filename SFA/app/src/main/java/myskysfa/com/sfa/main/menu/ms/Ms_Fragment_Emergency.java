package myskysfa.com.sfa.main.menu.ms;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.utils.Utils;


/**
 * Created by Eno on 6/13/2016.
 */
public class Ms_Fragment_Emergency extends Fragment implements View.OnClickListener {
    private ViewGroup root;
    private EditText firstName, middleName, lastName, telp, alamat;
    private TextView relation;
    private Button save;
    private ShowListDropDown showList;
    private String numberForm;
    private Utils utils;
    private TableFormAppAdapter mFormAppAdapter;
    private int dataCount;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.ms_fragment_emergency, container, false);
        mFormAppAdapter = new TableFormAppAdapter(getActivity());
        utils = new Utils(getActivity());
        initView(root);
        return root;
    }

    private void initView(ViewGroup root) {
        firstName = (EditText) root.findViewById(R.id.first_name);
        middleName = (EditText) root.findViewById(R.id.middle_name);
        lastName = (EditText) root.findViewById(R.id.last_name);
        telp = (EditText) root.findViewById(R.id.tlp);
        alamat = (EditText) root.findViewById(R.id.address);
        relation = (TextView) root.findViewById(R.id.relationship);
        save = (Button) root.findViewById(R.id.save);


        relation.setOnClickListener(this);
        save.setOnClickListener(this);
        SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.fn_ms), Context.MODE_PRIVATE);
        numberForm = sm.getString("fn", null);

    }

    private void checkMandatoryForm() {
        if (firstName.getText().toString() == null
                || firstName.getText().toString().equalsIgnoreCase("")
                || firstName.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukan nama", Toast.LENGTH_SHORT).show();
            return;
        } else if (relation.getText().toString() == null
                || relation.getText().toString().equalsIgnoreCase("")
                || relation.getText().toString().length() < 0) {
            Toast.makeText(getActivity(), "Silahkan pilih hubungan", Toast.LENGTH_SHORT).show();
            return;
        } else if (telp.getText().toString() == null
                || telp.getText().toString().equalsIgnoreCase("")
                || telp.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukan nomor telephone", Toast.LENGTH_SHORT).show();
            return;
        } else if (alamat.getText().toString() == null
                || alamat.getText().toString().equalsIgnoreCase("")
                || alamat.getText().toString().length() < 0) {
            Toast.makeText(getActivity(), "Silahkan masukan alamat", Toast.LENGTH_SHORT).show();
            return;
        } else {
            try {
                Long count = mFormAppAdapter.getDataCount(TableFormApp.fFORM_NO, numberForm);
                if (count != null) {
                    dataCount = (int) (long) count;
                } else {
                    dataCount = 0;
                }

                if (dataCount == 0) {
                    utils.showAlertDialog(getActivity(), "Gagal", "Silahkan isi data profile terlebih dahulu.", false);
                } else {
                    mFormAppAdapter.updatePartial(getActivity(), TableFormApp.fVALUES_EMERGENCY, jsonValueCreator(),
                            TableFormApp.fFORM_NO, numberForm);
                    Toast.makeText(getActivity(), "Emergency Saved", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Emergency Saved Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String jsonValueCreator() throws Exception {
        JSONObject obj = new JSONObject();
        try {
            obj.put("eFormNo", numberForm);
            obj.put("eFirstName", firstName.getText().toString());
            obj.put("eMiddleName", middleName.getText().toString());
            obj.put("eLastName", lastName.getText().toString());
            obj.put("eRelationship", relation.getText().toString());
            obj.put("eTelephone", telp.getText().toString());
            obj.put("eAddress", alamat.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("jsonku", obj.toString());
        return obj.toString();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relationship:
                showList = new ShowListDropDown(getActivity(), v, "Pilih hubungan", relation,
                        getResources().getStringArray(R.array.family_relationship));
                showList.Show();
                break;
            case R.id.save:
                checkMandatoryForm();
                break;
        }
    }

    public void setFormNumber() {
        SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.fn_ms),
                Context.MODE_PRIVATE);
        numberForm = sm.getString("fn", null);
    }

    public void setHideKeyBoard() {
        utils.setHideKeyboard(getActivity(), root);
    }

}
