package myskysfa.com.sfa.main.menu.ms;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.main.menu.MSDTDPackage;
import myskysfa.com.sfa.utils.Utils;


/**
 * Created by Eno on 6/13/2016.
 */
public class Ms_Fragment_Package extends Fragment {
    private ViewGroup root;
    //private Utils utils;
    //private int totPakacge;
    private String numberForm;
    private static Context _context;
    private MSDTDPackage msdtdPackage;
    private LinearLayout linier_add, liner_package;
    private Spinner spinner_type, spinner_brand, spinnner_bundling;
    private SearchableSpinner spinner_promo;
    private TextView priceInstalasi, pricePacket, pricePromo, priceAlacarte;
    private Button save;
    private Utils utils;

    public static Fragment newInstance(Context context) {
        _context = context;
        Ms_Fragment_Package dtdPaket = new Ms_Fragment_Package();
        return dtdPaket;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.ms_fragment_package, container, false);
        utils = new Utils(getActivity());
        //handler = new Handler();
        //totPakacge = 0;

        SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.fn_ms), Context.MODE_PRIVATE);
        numberForm = sm.getString("fn", null);
        initView(root);
        return root;
    }

    private void initView(ViewGroup mView) {
        linier_add = (LinearLayout) mView.findViewById(R.id.linier_package);
        liner_package = (LinearLayout) mView.findViewById(R.id.package_list);
        spinner_type = (Spinner) mView.findViewById(R.id.spinner_type);
        spinner_brand = (Spinner) mView.findViewById(R.id.spinner_brand);
        spinner_promo = (SearchableSpinner) mView.findViewById(R.id.spinner_promo);
        pricePacket = (TextView) mView.findViewById(R.id.pricePaket);
        pricePromo = (TextView) mView.findViewById(R.id.pricePromo);
        priceInstalasi = (TextView) mView.findViewById(R.id.priceInst);
        priceAlacarte = (TextView) mView.findViewById(R.id.priceAlacarte);
        spinnner_bundling = (Spinner) mView.findViewById(R.id.spinner_bundling);
        save = (Button) mView.findViewById(R.id.save);

        msdtdPackage = new MSDTDPackage(getActivity(), mView, linier_add, liner_package, spinner_type,
                spinner_brand, spinner_promo, priceInstalasi, pricePacket, pricePromo, numberForm,
                "m", spinnner_bundling, priceAlacarte, save);
        msdtdPackage.InitView();
    }

    public void setFormNumber() {
        SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.fn_ms),
                Context.MODE_PRIVATE);
        numberForm = sm.getString("fn", null);
    }

    public void setHideKeyBoard() {
        utils.setHideKeyboard(getActivity(), root);
    }

}
