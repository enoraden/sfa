package myskysfa.com.sfa.main.menu.ms;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableAbsensi;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.TableMasterMS;
import myskysfa.com.sfa.database.TableZipcode;
import myskysfa.com.sfa.database.db_adapter.TableAbsenAdapter;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMSAdapter;
import myskysfa.com.sfa.database.db_adapter.TableZipcodeAdapter;
import myskysfa.com.sfa.main.menu.MainFragmentMS;
import myskysfa.com.sfa.utils.AlbumStorageDirFactory;
import myskysfa.com.sfa.utils.BaseAlbumDirFactory;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by Eno on 6/13/2016.
 */
public class Ms_Fragment_Profile extends Fragment implements View.OnClickListener, View.OnLongClickListener {
    private ViewGroup root;
    private Button save;
    private EditText identity_no, first_name, middle_name, last_name, birth_place, address, no_home,
            rt, rw, install_address, patokan, billing_address, telp, hp, email, xphone;
    private TextView form_no, list_identity, gender, religion, birth_date, provinci_identity, kota_identity,
            kecamatan_identity, kelurahan_identity, zipcode_identity, valid_period, install_date,
            install_time, time_confirm_start, time_confirm_end, date_confirm_start, date_confirm_end,
            provinci_install, kota_install, kecamatan_install, kelurahan_install, zipcode_install,
            provinci_billing, kota_billing, kecamatan_billing, kelurahan_billing, zipcode_billing,
            home_kind, home_status, job, income, store, hardwareStatus, no_home_install, no_home_bill,
            rt_install, rt_bill, rw_install, rw_bill, prosType;

    private TextView txt, codezip;
    private CheckBox c_install, c_billing, c_zipcode;
    private ShowListDropDown showList;
    private Utils utils;
    private String picturePath, imageName, uId = "", value, idStore, employId;
    private static String appNumb;
    private File f;
    private ImageView prev;
    private Boolean isSendImage = false;
    private static final int PICK_IMAGE_INSTALASI = 313;
    private static final int TAKE_PICTURE_INSTALASI = 333;
    private static final int PICK_IMAGE_BUKTIBAYAR = 414;
    private static final int TAKE_PICTURE_BUKTIBAYAR = 444;
    private static final int PICK_IMAGE_LAINNYA = 121;
    private static final int TAKE_PICTURE_LAINNYA = 111;
    private static final int PICK_IMAGE_KTP = 505;
    private static final int TAKE_PICTURE_KTP = 555;
    ArrayList<String> pathArraysAll = new ArrayList<String>();
    private static Bitmap bitmapIdentity, bitmapBuktiBayar, bitmapLainnya, bitmapKtp;
    private static List<PathListItem> listPathIdentitas;
    private static List<PathListItem> listPathBuktiBayar;
    private static List<PathListItem> listPathLainnya;
    private static List<PathListItem> listPathKtp;

    private ArrayList<String> lainnya = new ArrayList<String>();
    private ArrayList<String> buktibayar = new ArrayList<String>();
    private ArrayList<String> instalasi = new ArrayList<String>();
    private ArrayList<String> ktp = new ArrayList<String>();

    private ArrayList<String> lainnya_name = new ArrayList<String>();
    private ArrayList<String> buktibayar_name = new ArrayList<String>();
    private ArrayList<String> instalasi_name = new ArrayList<String>();
    private ArrayList<String> ktp_name = new ArrayList<String>();

    private static List<TableAbsensi> absenData = new ArrayList<TableAbsensi>();
    private TableAbsenAdapter absenAdapter;

    private static List<TableMasterMS> msData = new ArrayList<TableMasterMS>();
    private TableMSAdapter msAdapter;

    private ArrayList<String> listTblName, listTblID;
    private LinearLayout mLayoutPackage;
    private ImageButton ib_instalasi, ib_bb, ib_lainnya, ib_ktp;
    private SharedPreferences sm;

    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private static Context _contex;

    private TableZipcodeAdapter zipcodeAdapter;
    private List<TableZipcode> listTblZipCode;

    private static String provinceID, cityID, kecID, kelID;

    private TableFormAppAdapter tblFormApp;

    public static Ms_Fragment_Profile newInstance(Context contex) {
        _contex = contex;
        Ms_Fragment_Profile fragment = new Ms_Fragment_Profile();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.ms_fragment_profile, container, false);
        ConstructorInit();
        initView(root);
        return root;
    }

    private void ConstructorInit() {
        utils = new Utils(getActivity());
        zipcodeAdapter = new TableZipcodeAdapter(getActivity());
        tblFormApp = new TableFormAppAdapter(getActivity());
    }

    public void setHideKeyBoard() {
        utils.setHideKeyboard(getActivity(), root);
    }


    private void initView(ViewGroup root) {
        store = (TextView) root.findViewById(R.id.store);
        form_no = (TextView) root.findViewById(R.id.form_no);
        identity_no = (EditText) root.findViewById(R.id.identity_no);
        first_name = (EditText) root.findViewById(R.id.first_name);
        middle_name = (EditText) root.findViewById(R.id.middle_name);
        last_name = (EditText) root.findViewById(R.id.last_name);
        birth_place = (EditText) root.findViewById(R.id.birthplace);
        address = (EditText) root.findViewById(R.id.address);
        no_home = (EditText) root.findViewById(R.id.home_no);
        identity_no = (EditText) root.findViewById(R.id.identity_no);
        rt = (EditText) root.findViewById(R.id.rt);
        rw = (EditText) root.findViewById(R.id.rw);
        install_address = (EditText) root.findViewById(R.id.install_address);
        patokan = (EditText) root.findViewById(R.id.direction);
        billing_address = (EditText) root.findViewById(R.id.billing_address);
        telp = (EditText) root.findViewById(R.id.telephon);
        hp = (EditText) root.findViewById(R.id.hp);
        xphone = (EditText) root.findViewById(R.id.x_phone);
        email = (EditText) root.findViewById(R.id.email_address);

        list_identity = (TextView) root.findViewById(R.id.identity_list);
        gender = (TextView) root.findViewById(R.id.gender);
        religion = (TextView) root.findViewById(R.id.religion);
        birth_date = (TextView) root.findViewById(R.id.birth_date);
        provinci_identity = (TextView) root.findViewById(R.id.province);
        kota_identity = (TextView) root.findViewById(R.id.city);
        kecamatan_identity = (TextView) root.findViewById(R.id.kecamatan);
        kelurahan_identity = (TextView) root.findViewById(R.id.kelurahan);
        zipcode_identity = (TextView) root.findViewById(R.id.zip_code);
        valid_period = (TextView) root.findViewById(R.id.validity_period);
        install_date = (TextView) root.findViewById(R.id.install_date);
        install_time = (TextView) root.findViewById(R.id.install_time);
        time_confirm_start = (TextView) root.findViewById(R.id.confirm_time_start);
        time_confirm_end = (TextView) root.findViewById(R.id.confirm_time_end);
        date_confirm_start = (TextView) root.findViewById(R.id.confirm_date_start);
        date_confirm_end = (TextView) root.findViewById(R.id.confirm_date_end);
        provinci_install = (TextView) root.findViewById(R.id.province_install);
        kota_install = (TextView) root.findViewById(R.id.city_install);
        kecamatan_install = (TextView) root.findViewById(R.id.kecamatan_install);
        kelurahan_install = (TextView) root.findViewById(R.id.kelurahan_install);
        zipcode_install = (TextView) root.findViewById(R.id.zipcode_install);
        provinci_billing = (TextView) root.findViewById(R.id.province_billing);
        kota_billing = (TextView) root.findViewById(R.id.city_billing);
        kecamatan_billing = (TextView) root.findViewById(R.id.kecamatan_billing);
        kelurahan_billing = (TextView) root.findViewById(R.id.kelurahan_billing);
        zipcode_billing = (TextView) root.findViewById(R.id.zipcode_billing);
        home_kind = (TextView) root.findViewById(R.id.home_kind);
        home_status = (TextView) root.findViewById(R.id.home_status);
        job = (TextView) root.findViewById(R.id.job);
        income = (TextView) root.findViewById(R.id.income);
        prosType = (TextView) root.findViewById(R.id.prosType);
        hardwareStatus = (TextView) root.findViewById(R.id.sh1);
        no_home_install = (EditText) root.findViewById(R.id.home_no_install);
        no_home_bill = (EditText) root.findViewById(R.id.home_no_bill);
        rt_install = (EditText) root.findViewById(R.id.rt_install);
        rt_bill = (EditText) root.findViewById(R.id.rt_bill);
        rw_install = (EditText) root.findViewById(R.id.rw_install);
        rw_bill = (EditText) root.findViewById(R.id.rw_bill);
        c_install = (CheckBox) root.findViewById(R.id.install_checkbox);
        c_billing = (CheckBox) root.findViewById(R.id.bill_checkbox);
        c_zipcode = (CheckBox) root.findViewById(R.id.zipcode_checkbox);

        ib_instalasi = (ImageButton) root.findViewById(R.id.ib_instalasi);
        ib_bb = (ImageButton) root.findViewById(R.id.ib_bb);
        ib_lainnya = (ImageButton) root.findViewById(R.id.ib_lainnya);
        ib_ktp = (ImageButton) root.findViewById(R.id.ib_ktp);

        save = (Button) root.findViewById(R.id.save);

        list_identity.setOnClickListener(this);
        gender.setOnClickListener(this);
        religion.setOnClickListener(this);
        birth_date.setOnClickListener(this);

        install_time.setOnClickListener(this);
        install_date.setOnClickListener(this);
        valid_period.setOnClickListener(this);
        time_confirm_start.setOnClickListener(this);
        time_confirm_end.setOnClickListener(this);
        date_confirm_start.setOnClickListener(this);
        date_confirm_end.setOnClickListener(this);
        home_kind.setOnClickListener(this);
        home_status.setOnClickListener(this);
        job.setOnClickListener(this);
        income.setOnClickListener(this);

        ib_instalasi.setOnClickListener(this);
        ib_bb.setOnClickListener(this);
        ib_lainnya.setOnClickListener(this);
        ib_ktp.setOnClickListener(this);

        c_install.setOnClickListener(this);
        c_billing.setOnClickListener(this);
        c_zipcode.setOnClickListener(this);

        provinci_install.setOnClickListener(this);
        kota_install.setOnClickListener(this);
        kecamatan_install.setOnClickListener(this);
        kelurahan_install.setOnClickListener(this);

        provinci_billing.setOnClickListener(this);
        kota_billing.setOnClickListener(this);
        kecamatan_billing.setOnClickListener(this);
        kelurahan_billing.setOnClickListener(this);

        provinci_identity.setOnClickListener(this);
        kota_identity.setOnClickListener(this);
        kecamatan_identity.setOnClickListener(this);
        kelurahan_identity.setOnClickListener(this);

        hardwareStatus.setOnClickListener(this);
        prosType.setOnClickListener(this);

        save.setOnClickListener(this);


        /**
         * SetLongListener
         */

        birth_date.setOnLongClickListener(this);
        valid_period.setOnLongClickListener(this);
        install_date.setOnLongClickListener(this);
        date_confirm_start.setOnLongClickListener(this);
        date_confirm_end.setOnLongClickListener(this);

        mAlbumStorageDirFactory = new BaseAlbumDirFactory();

        sm = getActivity().getSharedPreferences(getString(R.string.fn_ms), Context.MODE_PRIVATE);
        appNumb = sm.getString("fn", null);
        form_no.setText(appNumb);

        sm = getActivity().getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        uId = sm.getString(TableLogLogin.C_UID, "");
        employId = sm.getString(TableLogLogin.C_EMPLOYEE_ID, "");

        /**
         * Get Id Store From Table Absen
         */
        msAdapter = new TableMSAdapter(getActivity());
        absenAdapter = new TableAbsenAdapter(getActivity());
        absenData = absenAdapter.getDataByCondition(TableAbsensi.UID, employId);
        if (absenData.size() > 0) {
            idStore = absenData.get(0).getId_store();
            msData = msAdapter.getDatabyCondition(TableMasterMS.FLD_ROWID, idStore);
            store.setText(msData.get(0).getName());
        }
    }


    /**
     * OnLongClick Listener TextView
     */
    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.birth_date:
                utils.showEditOwnDate(getActivity(), "Tgl. Lahir", birth_date, false);
                break;
            case R.id.validity_period:
                utils.showEditOwnDate(getActivity(), "Masa Berlaku", valid_period, true);
                break;
            case R.id.install_date:
                utils.showEditOwnDate(getActivity(), "Tgl. Pasang", install_date, false);
                break;
            case R.id.confirm_date_start:
                utils.showEditOwnDate(getActivity(), "Confirm Start", date_confirm_start, false);
                break;
            case R.id.confirm_date_end:
                utils.showEditOwnDate(getActivity(), "Confirm End", date_confirm_end, false);
                break;

        }
        return true;
    }

    /**
     * OnClick Listener TextView
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.identity_list:
                showList = new ShowListDropDown(getActivity(), v, "identity",
                        list_identity, getResources().getStringArray(R.array.type_identity));
                showList.Show();
                break;
            case R.id.gender:
                showList = new ShowListDropDown(getActivity(), v, "gender",
                        gender, getResources().getStringArray(R.array.gender_list));
                showList.Show();
                break;
            case R.id.religion:
                showList = new ShowListDropDown(getActivity(), v, "Agama",
                        religion, getResources().getStringArray(R.array.religion));
                showList.Show();
                break;
            case R.id.birth_date:
                utils.setDateField(getActivity(), birth_date);
                break;
            case R.id.install_time:
                utils.setTimeField(getActivity(), install_time);
                break;
            case R.id.validity_period:
                utils.setDateField(getActivity(), valid_period);
                break;
            case R.id.install_date:
                utils.setDateField(getActivity(), install_date);
                break;
            case R.id.confirm_time_start:
                utils.setTimeField(getActivity(), time_confirm_start);
                break;
            case R.id.confirm_time_end:
                utils.setTimeField(getActivity(), time_confirm_end);
                break;
            case R.id.confirm_date_start:
                utils.setDateField(getActivity(), date_confirm_start);
                break;
            case R.id.confirm_date_end:
                utils.setDateField(getActivity(), date_confirm_end);
                break;
            case R.id.home_kind:
                showList = new ShowListDropDown(getActivity(), v, "Jenis Rumah",
                        home_kind, getResources().getStringArray(R.array.home_tipe));
                showList.Show();
                break;
            case R.id.home_status:
                showList = new ShowListDropDown(getActivity(), v, "Status Rumah",
                        home_status, getResources().getStringArray(R.array.home_owner));
                showList.Show();
                break;
            case R.id.job:
                showList = new ShowListDropDown(getActivity(), v, "Pekerjaan",
                        job, getResources().getStringArray(R.array.pekerjaan));
                showList.Show();
                break;
            case R.id.income:
                showList = new ShowListDropDown(getActivity(), v, "Penghasilan",
                        income, getResources().getStringArray(R.array.penghasilan));
                showList.Show();
                break;
            case R.id.install_checkbox:
                CheckListInstall();
                break;
            case R.id.bill_checkbox:
                CheckListBilling();
                break;
            case R.id.zipcode_checkbox:
                CheckListZipcode();
                break;
            case R.id.ib_instalasi:
                //TakeChooseImage(v);
                takeImage(TAKE_PICTURE_INSTALASI);
                break;
            case R.id.ib_bb:
                //TakeChooseImage(v);
                takeImage(TAKE_PICTURE_BUKTIBAYAR);
                break;
            case R.id.ib_lainnya:
                takeImage(TAKE_PICTURE_LAINNYA);
                //TakeChooseImage(v);
                break;
            case R.id.ib_ktp:
                takeImage(TAKE_PICTURE_KTP);
                //TakeChooseImage(v);
                break;
            case R.id.prosType:
                showList = new ShowListDropDown(getActivity(), v, "Prospek Status", prosType,
                        getResources().getStringArray(R.array.array_pros_type));
                showList.Show();
                break;
            case R.id.sh1:
                showList = new ShowListDropDown(getActivity(), v, "Hardware Status", hardwareStatus,
                        getResources().getStringArray(R.array.hw_status));
                showList.Show();
                break;
            case R.id.province_install:
            case R.id.province:
            case R.id.province_billing:
                if (v.getId() == R.id.province_install) {
                    txt = provinci_install;

                    kota_install.setText("");
                    kecamatan_install.setText("");
                    kelurahan_install.setText("");
                    zipcode_install.setText("");
                } else if (v.getId() == R.id.province) {
                    txt = provinci_identity;

                    kota_identity.setText("");
                    kecamatan_identity.setText("");
                    kelurahan_identity.setText("");
                    zipcode_identity.setText("");
                } else {
                    txt = provinci_billing;

                    kota_billing.setText("");
                    kecamatan_billing.setText("");
                    kelurahan_billing.setText("");
                    zipcode_billing.setText("");
                }
                getDataZipcode(v, null);
                showList = new ShowListDropDown(getActivity(), v, "Pilih provinsi", txt,
                        listTblName, listTblID, null);
                showList.ShowZipcode();
                break;
            case R.id.city_install:
            case R.id.city:
            case R.id.city_billing:
                if (v.getId() == R.id.city_install) {
                    txt = kota_install;
                    provinceID = showList.getProvIDInstall();

                    kecamatan_install.setText("");
                    kelurahan_install.setText("");
                    zipcode_install.setText("");
                } else if (v.getId() == R.id.city) {
                    txt = kota_identity;
                    provinceID = showList.getProvID();

                    kecamatan_identity.setText("");
                    kelurahan_identity.setText("");
                    zipcode_identity.setText("");
                } else {
                    txt = kota_billing;
                    provinceID = showList.getProvIDBilling();

                    kecamatan_billing.setText("");
                    kelurahan_billing.setText("");
                    zipcode_billing.setText("");
                }
                getDataZipcode(v, provinceID);
                showList = new ShowListDropDown(getActivity(), v, "Pilih Kota", txt,
                        listTblName, listTblID, null);
                showList.ShowZipcode();
                break;
            case R.id.kecamatan_install:
            case R.id.kecamatan:
            case R.id.kecamatan_billing:
                if (v.getId() == R.id.kecamatan_install) {
                    txt = kecamatan_install;
                    cityID = showList.getCityIDInstall();

                    kelurahan_install.setText("");
                    zipcode_install.setText("");
                } else if (v.getId() == R.id.kecamatan) {
                    txt = kecamatan_identity;
                    cityID = showList.getCityID();

                    kelurahan_identity.setText("");
                    zipcode_identity.setText("");
                } else {
                    txt = kecamatan_billing;
                    cityID = showList.getCityIDBilling();

                    kelurahan_billing.setText("");
                    zipcode_billing.setText("");
                }
                getDataZipcode(v, cityID);
                showList = new ShowListDropDown(getActivity(), v, "Pilih kecamatan", txt,
                        listTblName, listTblID, null);
                showList.ShowZipcode();
                break;
            case R.id.kelurahan_install:
            case R.id.kelurahan:
            case R.id.kelurahan_billing:
                if (v.getId() == R.id.kelurahan_install) {
                    txt = kelurahan_install;
                    kecID = showList.getKecIDInstall();
                    kelID = showList.getKelIDInstall();
                    codezip = zipcode_install;
                    zipcode_install.setText("");
                } else if (v.getId() == R.id.kelurahan) {
                    txt = kelurahan_identity;
                    kecID = showList.getKecID();
                    kelID = showList.getKelID();
                    codezip = zipcode_identity;
                    zipcode_identity.setText("");
                } else {
                    txt = kelurahan_billing;
                    kecID = showList.getKecIDBilling();
                    kelID = showList.getKelIDBilling();
                    codezip = zipcode_billing;
                    zipcode_billing.setText("");
                }
                getDataZipcode(v, kecID);
                showList = new ShowListDropDown(getActivity(), v, "Pilih kelurahan", txt,
                        listTblName, listTblID, codezip);
                showList.ShowZipcode();
                break;
            case R.id.save:
                checkMandatoryForm();
                //saveImage();
                break;

        }

    }

    /**
     * getData From DB
     */

    private void getDataZipcode(View v, @Nullable String params) {
        listTblName = new ArrayList<String>();
        listTblID = new ArrayList<String>();

        switch (v.getId()) {
            case R.id.province_install:
            case R.id.province:
            case R.id.province_billing:
                try {
                    listTblZipCode = zipcodeAdapter.listProvinsi();
                    for (int i = 0; i < listTblZipCode.size(); i++) {
                        TableZipcode item = listTblZipCode.get(i);
                        listTblID.add(item.getDISTRICT_CODE());
                        listTblName.add(item.getDISTRICT_NAME());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.city_install:
            case R.id.city:
            case R.id.city_billing:
                try {
                    listTblZipCode = zipcodeAdapter.listCity(params);
                    for (int i = 0; i < listTblZipCode.size(); i++) {
                        TableZipcode item = listTblZipCode.get(i);
                        listTblID.add(item.getCITY_CODE());
                        listTblName.add(item.getCITY_NAME());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.kecamatan_install:
            case R.id.kecamatan:
            case R.id.kecamatan_billing:
                try {
                    listTblZipCode = zipcodeAdapter.listKecamatan(params);
                    for (int i = 0; i < listTblZipCode.size(); i++) {
                        TableZipcode item = listTblZipCode.get(i);
                        listTblID.add(item.getAREA_CODE());
                        listTblName.add(item.getAREA_NAME());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.kelurahan_install:
            case R.id.kelurahan:
            case R.id.kelurahan_billing:
                try {
                    listTblZipCode = zipcodeAdapter.listKelurahan(params);
                    for (int i = 0; i < listTblZipCode.size(); i++) {
                        TableZipcode item = listTblZipCode.get(i);
                        listTblID.add(item.getSTREET_CODE());
                        listTblName.add(item.getSTREET_NAME());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

        }

    }


    private void CheckListInstall() {
        if (c_install.isChecked()) {
            install_address.setText(address.getText().toString());
            provinci_install.setText(provinci_identity.getText().toString());
            kota_install.setText(kota_identity.getText().toString());
            kecamatan_install.setText(kecamatan_identity.getText().toString());
            kelurahan_install.setText(kelurahan_identity.getText().toString());
            zipcode_install.setText(zipcode_identity.getText().toString());
            no_home_install.setText(no_home.getText().toString());
            rt_install.setText(rt.getText().toString());
            rw_install.setText(rw.getText().toString());
            c_install.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    install_address.setText(address.getText().toString());
                    provinci_install.setText(provinci_identity.getText().toString());
                    kota_install.setText(kota_identity.getText().toString());
                    kecamatan_install.setText(kecamatan_identity.getText().toString());
                    kelurahan_install.setText(kelurahan_identity.getText().toString());
                    zipcode_install.setText(zipcode_identity.getText().toString());
                    no_home_install.setText(no_home.getText().toString());
                    rt_install.setText(rt.getText().toString());
                    rw_install.setText(rw.getText().toString());
                }
            });
        } else {
            install_address.setText("");
            provinci_install.setText("");
            kota_install.setText("");
            kecamatan_install.setText("");
            kelurahan_install.setText("");
            zipcode_install.setText("");
            no_home_install.setText("");
            rt_install.setText("");
            rw_install.setText("");
            c_install.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        install_address.setText("");
                        provinci_install.setText("");
                        kota_install.setText("");
                        kecamatan_install.setText("");
                        kelurahan_install.setText("");
                        zipcode_install.setText("");
                        no_home_install.setText("");
                        rt_install.setText("");
                        rw_install.setText("");
                    } catch (Exception e) {
                    }
                }
            });
        }

    }

    private void CheckListBilling() {
        if (c_billing.isChecked()) {
            c_zipcode.setChecked(false);
            billing_address.setText(address.getText().toString());
            provinci_billing.setText(provinci_identity.getText().toString());
            kota_billing.setText(kota_identity.getText().toString());
            kecamatan_billing.setText(kecamatan_identity.getText().toString());
            kelurahan_billing.setText(kelurahan_identity.getText().toString());
            zipcode_billing.setText(zipcode_identity.getText().toString());
            no_home_bill.setText(no_home.getText().toString());
            rt_bill.setText(rt.getText().toString());
            rw_bill.setText(rw.getText().toString());
            c_billing.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    c_zipcode.setChecked(false);
                    billing_address.setText(address.getText().toString());
                    provinci_billing.setText(provinci_identity.getText().toString());
                    kota_billing.setText(kota_identity.getText().toString());
                    kecamatan_billing.setText(kecamatan_identity.getText().toString());
                    kelurahan_billing.setText(kelurahan_identity.getText().toString());
                    zipcode_billing.setText(zipcode_identity.getText().toString());
                    no_home_bill.setText(no_home.getText().toString());
                    rt_bill.setText(rt.getText().toString());
                    rw_bill.setText(rw.getText().toString());
                }
            });
        } else {
            billing_address.setText("");
            provinci_billing.setText("");
            kota_billing.setText("");
            kecamatan_billing.setText("");
            kelurahan_billing.setText("");
            zipcode_billing.setText("");
            no_home_bill.setText("");
            rt_bill.setText("");
            rw_bill.setText("");
            c_billing.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    billing_address.setText("");
                    provinci_billing.setText("");
                    kota_billing.setText("");
                    kecamatan_billing.setText("");
                    kelurahan_billing.setText("");
                    zipcode_billing.setText("");
                    no_home_bill.setText("");
                    rt_bill.setText("");
                    rw_bill.setText("");
                }
            });
        }
    }

    private void CheckListZipcode() {
        if (c_zipcode.isChecked()) {
            c_billing.setChecked(false);
            billing_address.setText(install_address.getText().toString());
            provinci_billing.setText(provinci_install.getText().toString());
            kota_billing.setText(kota_install.getText().toString());
            kecamatan_billing.setText(kecamatan_install.getText().toString());
            kelurahan_billing.setText(kelurahan_install.getText().toString());
            zipcode_billing.setText(zipcode_install.getText().toString());
            no_home_bill.setText(no_home_install.getText().toString());
            rt_bill.setText(rt_install.getText().toString());
            rw_bill.setText(rw_install.getText().toString());
            c_zipcode.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    c_billing.setChecked(false);
                    billing_address.setText(install_address.getText().toString());
                    provinci_billing.setText(provinci_install.getText().toString());
                    kota_billing.setText(kota_install.getText().toString());
                    kecamatan_billing.setText(kecamatan_install.getText().toString());
                    kelurahan_billing.setText(kelurahan_install.getText().toString());
                    zipcode_billing.setText(zipcode_install.getText().toString());
                    no_home_bill.setText(no_home_install.getText().toString());
                    rt_bill.setText(rt_install.getText().toString());
                    rw_bill.setText(rw_install.getText().toString());
                }
            });
        } else {
            billing_address.setText("");
            provinci_billing.setText("");
            kota_billing.setText("");
            kecamatan_billing.setText("");
            kelurahan_billing.setText("");
            zipcode_billing.setText("");
            no_home_bill.setText("");
            rt_bill.setText("");
            rw_bill.setText("");
            c_zipcode.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    billing_address.setText("");
                    provinci_billing.setText("");
                    kota_billing.setText("");
                    kecamatan_billing.setText("");
                    kelurahan_billing.setText("");
                    zipcode_billing.setText("");
                    no_home_bill.setText("");
                    rt_bill.setText("");
                    rw_bill.setText("");
                }
            });
        }
    }

    /*private void TakeChooseImage(final View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setMessage("Type of Upload")
                .setPositiveButton("Choose From File", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (view.getId()) {
                            case R.id.ib_instalasi:
                                selectImageFromGallery(PICK_IMAGE_INSTALASI);
                                break;
                            case R.id.ib_bb:
                                selectImageFromGallery(PICK_IMAGE_BUKTIBAYAR);
                                break;
                            case R.id.ib_lainnya:
                                selectImageFromGallery(PICK_IMAGE_LAINNYA);
                                break;
                            case R.id.ib_ktp:
                                selectImageFromGallery(PICK_IMAGE_KTP);
                                break;
                        }
                    }
                })
                .setNegativeButton("Take Picture", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (view.getId()) {
                            case R.id.ib_instalasi:
                                takeImage(TAKE_PICTURE_INSTALASI);
                                break;
                            case R.id.ib_bb:
                                takeImage(TAKE_PICTURE_BUKTIBAYAR);
                                break;
                            case R.id.ib_lainnya:
                                takeImage(TAKE_PICTURE_LAINNYA);
                                break;
                            case R.id.ib_ktp:
                                takeImage(TAKE_PICTURE_KTP);
                                break;
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }*/

    public void selectImageFromGallery(int code) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                code);
    }

    public void takeImage(int code) {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            f = null;
            f = createImageFile(code);
            picturePath = f.getAbsolutePath();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            getActivity().startActivityForResult(takePictureIntent, code);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File createImageFile(int code) throws IOException {
        String imageFileName = "";
        switch (code) {
            case TAKE_PICTURE_INSTALASI:
                imageFileName = appNumb + "_instalasi" + "_";
                break;
            case TAKE_PICTURE_BUKTIBAYAR:
                imageFileName = appNumb + "_buktibayar" + "_";
                break;
            case TAKE_PICTURE_LAINNYA:
                imageFileName = appNumb + "_lainnya" + "_";
                break;
            case TAKE_PICTURE_KTP:
                imageFileName = appNumb + "_ktp" + "_";
                break;
        }

        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, ".jpg", albumF);
        return imageF;
    }

    private File getAlbumDir() {
        File storageDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(Config.path);
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }
        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (PICK_IMAGE_INSTALASI):
                if (resultCode == Activity.RESULT_OK) {
                    GetPickImage(requestCode, data, "instalasi");
                }
                break;
            case (PICK_IMAGE_BUKTIBAYAR):
                if (resultCode == Activity.RESULT_OK) {
                    GetPickImage(requestCode, data, "buktibayar");
                }
                break;
            case (PICK_IMAGE_LAINNYA):
                if (resultCode == Activity.RESULT_OK) {
                    GetPickImage(requestCode, data, "lainnya");
                }
                break;
            case (PICK_IMAGE_KTP):
                GetPickImage(requestCode, data, "ktp");
                break;
            case (TAKE_PICTURE_INSTALASI):
                if (resultCode == Activity.RESULT_OK) {
                    {
                        listPathIdentitas = new ArrayList<PathListItem>();
                        setPic(bitmapIdentity, TAKE_PICTURE_INSTALASI);
                        galleryAddPic();
                    }
                }
                break;
            case TAKE_PICTURE_BUKTIBAYAR:
                if (resultCode == Activity.RESULT_OK) {
                    {
                        listPathBuktiBayar = new ArrayList<PathListItem>();
                        setPic(bitmapBuktiBayar, TAKE_PICTURE_BUKTIBAYAR);
                        galleryAddPic();

                    }
                }
                break;
            case TAKE_PICTURE_LAINNYA:
                if (resultCode == Activity.RESULT_OK) {
                    {
                        listPathLainnya = new ArrayList<PathListItem>();
                        setPic(bitmapLainnya, TAKE_PICTURE_LAINNYA);
                        galleryAddPic();
                    }
                }
                break;
            case TAKE_PICTURE_KTP:
                if (resultCode == Activity.RESULT_OK) {
                    {
                        listPathKtp = new ArrayList<PathListItem>();
                        setPic(bitmapKtp, TAKE_PICTURE_KTP);
                        galleryAddPic();
                    }
                }
                break;
        }
    }

    private void setPic(Bitmap bitmap, final int code) {
        /* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picturePath, bmOptions);
        bmOptions.inJustDecodeBounds = false;
        final int REQUIRED_SIZE = 1024;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = bmOptions.outWidth, height_tmp = bmOptions.outHeight;
        final int finalWidth_tmp = width_tmp;
        final int finalHeight_tmp = height_tmp;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        bmOptions.inSampleSize = scale;
        /* Decode the JPEG file into a Bitmap */
        bitmap = BitmapFactory.decodeFile(picturePath, bmOptions);

        if (finalWidth_tmp > Config.width && finalHeight_tmp > Config.hight) {
            Toast.makeText(getActivity(), "Image harus 2 MP", Toast.LENGTH_SHORT).show();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            final View root = inflater.inflate(R.layout.image_preview, null);
            prev = (ImageView) root.findViewById(R.id.imagepreview);
            alertDialog.setView(root);
            alertDialog.setTitle("Image Preview");

            alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    File file = new File(picturePath);
                    imageName = file.getName();
                    switch (code) {
                        case TAKE_PICTURE_INSTALASI:
                            addViewImageList(TAKE_PICTURE_INSTALASI, instalasi, imageName, picturePath);
                            break;
                        case TAKE_PICTURE_BUKTIBAYAR:
                            addViewImageList(TAKE_PICTURE_BUKTIBAYAR, buktibayar, imageName, picturePath);
                            break;
                        case TAKE_PICTURE_LAINNYA:
                            addViewImageList(TAKE_PICTURE_LAINNYA, lainnya, imageName, picturePath);
                            break;
                        case TAKE_PICTURE_KTP:
                            addViewImageList(TAKE_PICTURE_KTP, ktp, imageName, picturePath);
                            break;
                    }

                }
            });
            alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    //Cancel set Visible Bitmap
                }
            });

            prev.setImageBitmap(bitmap);
            prev.setVisibility(View.VISIBLE);
            alertDialog.show();
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(picturePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private void GetPickImage(int requestCode, Intent data, String suffixName) {
        if (data != null) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn,
                    null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
            //Get Image Name
            File f = new File(picturePath);
            //imageName = f.getName();
            File newFile = new File(appNumb + "_" + suffixName + "_" + System.currentTimeMillis() + ".jpg");
            f.renameTo(newFile);
            imageName = newFile.getName();
            decodeFile(picturePath, requestCode);
        }

    }

    public void decodeFile(final String filePath, final int code) {
        Log.d("MSProfile", "code=" + code);
        // Decode ukuran gambar
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);
        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        final int finalWidth_tmp = width_tmp;
        final int finalHeight_tmp = height_tmp;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        // Decode with inSampleSize
        final BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;

        if (finalWidth_tmp > Config.width && finalHeight_tmp > Config.hight) {
            Toast.makeText(getActivity(), "Image harus 2 MP", Toast.LENGTH_SHORT).show();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            final View root = inflater.inflate(R.layout.image_preview, null);
            alertDialog.setView(root);
            alertDialog.setTitle("Image Preview");
            alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (code == PICK_IMAGE_INSTALASI) {
                        listPathIdentitas = new ArrayList<PathListItem>();
                        addViewImageList(PICK_IMAGE_INSTALASI, instalasi,
                                imageName, picturePath);
                    } else if (code == PICK_IMAGE_BUKTIBAYAR) {
                        listPathBuktiBayar = new ArrayList<PathListItem>();
                        addViewImageList(PICK_IMAGE_BUKTIBAYAR, buktibayar,
                                imageName, picturePath);
                    } else if (code == PICK_IMAGE_LAINNYA) {
                        listPathLainnya = new ArrayList<PathListItem>();
                        addViewImageList(PICK_IMAGE_LAINNYA, lainnya,
                                imageName, picturePath);
                    } else if (code == PICK_IMAGE_KTP) {
                        listPathKtp = new ArrayList<PathListItem>();
                        addViewImageList(PICK_IMAGE_KTP, ktp,
                                imageName, picturePath);
                    } else {
                    }
                }
            });
            alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    //Cancel set Visible Bitmap
                }
            });
            prev = (ImageView) root.findViewById(R.id.imagepreview);
            prev.setVisibility(View.VISIBLE);
            if (code == PICK_IMAGE_INSTALASI) {
                bitmapIdentity = BitmapFactory.decodeFile(filePath, o2);
                prev.setImageBitmap(bitmapIdentity);
            } else if (code == PICK_IMAGE_BUKTIBAYAR) {
                bitmapBuktiBayar = BitmapFactory.decodeFile(filePath, o2);
                prev.setImageBitmap(bitmapBuktiBayar);
            } else if (code == PICK_IMAGE_LAINNYA) {
                bitmapLainnya = BitmapFactory.decodeFile(filePath, o2);
                prev.setImageBitmap(bitmapLainnya);
            } else {
                bitmapKtp = BitmapFactory.decodeFile(filePath, o2);
                prev.setImageBitmap(bitmapKtp);
            }
            alertDialog.show();
        }
    }

    private void checkMandatoryForm() {
        /*final EmailValidator emailValid = new EmailValidator();*/
        if (hardwareStatus.getText().toString() == null
                || hardwareStatus.getText().toString().equalsIgnoreCase("")
                || hardwareStatus.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Cek kembali Hw status", Toast.LENGTH_SHORT).show();
            return;
        } else if (identity_no.getText().toString() == null
                || identity_no.getText().toString().equalsIgnoreCase("")
                || identity_no.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Cek kembali no identitas", Toast.LENGTH_SHORT).show();
            return;
        } else if (list_identity.getText().toString() == null
                || list_identity.getText().toString().equalsIgnoreCase("")
                || list_identity.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan type identity", Toast.LENGTH_SHORT).show();
            return;
        } else if (first_name.getText().toString() == null
                || first_name.getText().toString().equalsIgnoreCase("")
                || first_name.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan nama profile.", Toast.LENGTH_SHORT).show();
            return;
        } else if (gender.getText().toString() == null
                || gender.getText().toString().equalsIgnoreCase("")
                || gender.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan jenis kelamin.", Toast.LENGTH_SHORT).show();
            return;
        } else if (religion.getText().toString() == null
                || religion.getText().toString().equalsIgnoreCase("")
                || religion.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan agama.", Toast.LENGTH_SHORT).show();
            return;
        } else if (birth_place.getText().toString() == null
                || birth_place.getText().toString().equalsIgnoreCase("")
                || birth_place.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan tempat lahir.", Toast.LENGTH_SHORT).show();
            return;
        } else if (birth_date.getText().toString() == null
                || birth_date.getText().toString().equalsIgnoreCase("")
                || birth_date.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan tanggal lahir.", Toast.LENGTH_SHORT).show();
            return;
        } else if (address.getText().toString() == null
                || address.getText().toString().equalsIgnoreCase("")
                || address.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan alamat pada identitas.", Toast.LENGTH_SHORT).show();
            return;
        } else if (zipcode_identity.getText().toString() == null
                || zipcode_identity.getText().toString().equalsIgnoreCase("")
                || zipcode_identity.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukan Zipcode identitas.", Toast.LENGTH_SHORT).show();
            return;
        } else if (valid_period.getText().toString() == null
                || valid_period.getText().toString().equalsIgnoreCase("")
                || valid_period.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan masa berlaku identitas", Toast.LENGTH_SHORT).show();
            return;
        } else if (install_date.getText().toString() == null
                || install_date.getText().toString().equalsIgnoreCase("")
                || install_date.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan tgl. pasang", Toast.LENGTH_SHORT).show();
            return;
        } else if (install_time.getText().toString() == null
                || install_time.getText().toString().equalsIgnoreCase("")
                || install_time.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan jam pasang", Toast.LENGTH_SHORT).show();
            return;
        } else if (date_confirm_start.getText().toString() == null
                || date_confirm_start.getText().toString().equalsIgnoreCase("")
                || date_confirm_start.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan lengkapi Tgl konfim", Toast.LENGTH_SHORT).show();
            return;
        } else if (date_confirm_end.getText().toString() == null
                || date_confirm_end.getText().toString().equalsIgnoreCase("")
                || date_confirm_end.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan lengkapi Tgl konfim", Toast.LENGTH_SHORT).show();
            return;
        } else if (time_confirm_start.getText().toString() == null
                || time_confirm_start.getText().toString().equalsIgnoreCase("")
                || time_confirm_start.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan lengkapi jam konfim", Toast.LENGTH_SHORT).show();
            return;
        } else if (time_confirm_end.getText().toString() == null
                || time_confirm_end.getText().toString().equalsIgnoreCase("")
                || time_confirm_end.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan lengkapi jam konfim", Toast.LENGTH_SHORT).show();
            return;
        } else if (install_address.getText().toString() == null
                || install_address.getText().toString().equalsIgnoreCase("")
                || install_address.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan alamat pemasangan.", Toast.LENGTH_SHORT).show();
            return;
        } else if (patokan.getText().toString() == null
                || patokan.getText().toString().equalsIgnoreCase("")
                || patokan.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan direction/petunjuk.", Toast.LENGTH_SHORT).show();
            return;
        } else if (zipcode_install.getText().toString() == null
                || zipcode_install.getText().toString().equalsIgnoreCase("")
                || zipcode_install.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan zipcode pemasangan.", Toast.LENGTH_SHORT).show();
            return;
        } else if (telp.getText().toString() == null
                || telp.getText().toString().equalsIgnoreCase("")
                || telp.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan nomor telephone.", Toast.LENGTH_SHORT).show();
            return;
        } else if (hp.getText().toString() == null
                || hp.getText().toString().equalsIgnoreCase("")
                || hp.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan nomor handphone.", Toast.LENGTH_SHORT).show();
            return;
        } else if (xphone.getText().toString() == null
                || xphone.getText().toString().equalsIgnoreCase("")
                || xphone.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan Extra phone.", Toast.LENGTH_SHORT).show();
            return;
        } else if (ktp.size() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan foto ktp", Toast.LENGTH_SHORT).show();
            return;
        } else if (buktibayar.size() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukkan foto buktibayar", Toast.LENGTH_SHORT).show();
            return;
        } else {
            try {
                value = jsonValueCreator();
                showList = new ShowListDropDown();
                //tblFormApp.getDataCount(TableFormApp.fFORM_NO, appNumb);
                if (tblFormApp.getDataCount(TableFormApp.fFORM_NO, appNumb) == 0) {
                    tblFormApp.insertData(new TableFormApp(), appNumb, "MS", uId, "", "", showList.getIdentityList(),
                            identity_no.getText().toString(), first_name.getText().toString(), last_name.getText().toString(),
                            birth_date.getText().toString(), value, "", "", "", "", "", "0", "", "");
                    Toast.makeText(getActivity(), "Profile Save", Toast.LENGTH_SHORT).show();
                    saveImage();
                } else {
                    updateProfileDB();
                    Toast.makeText(getActivity(), "Update Profile", Toast.LENGTH_SHORT).show();
                    if (!isSendImage) {
                        saveImage();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateProfileDB() {
        tblFormApp = new TableFormAppAdapter(getActivity());
        tblFormApp.updatePartial(getActivity(), TableFormApp.fIDENTITY_TYPE, showList.getIdentityList(), TableFormApp.fFORM_NO, appNumb);
        tblFormApp.updatePartial(getActivity(), TableFormApp.fIDENTITY_NO, identity_no.getText().toString(), TableFormApp.fFORM_NO, appNumb);
        tblFormApp.updatePartial(getActivity(), TableFormApp.fFIRST_NAME, first_name.getText().toString(), TableFormApp.fFORM_NO, appNumb);
        tblFormApp.updatePartial(getActivity(), TableFormApp.fLAST_NAME, last_name.getText().toString(), TableFormApp.fFORM_NO, appNumb);
        tblFormApp.updatePartial(getActivity(), TableFormApp.fBIRTH_DATE, birth_date.getText().toString(), TableFormApp.fFORM_NO, appNumb);
        tblFormApp.updatePartial(getActivity(), TableFormApp.fVALUES_PROFILE, value, TableFormApp.fFORM_NO, appNumb);
    }

    /**
     * Method for create json from form value
     *
     * @return
     * @throws Exception
     */
    private String jsonValueCreator() throws Exception {
        JSONObject obj = new JSONObject();
        showList = new ShowListDropDown();
        try {
            obj.put("hw_status", showList.getHwStatus1());
            obj.put("ftId", form_no.getText().toString());
            /*obj.put("regDate", regDate.getText().toString());*/
            obj.put("identityType", showList.getIdentityList());
            obj.put("identityNo", identity_no.getText().toString());
            obj.put("firstName", first_name.getText().toString());
            obj.put("middleName", middle_name.getText().toString());
            obj.put("lastName", last_name.getText().toString());
            obj.put("gender", showList.getGender());
            obj.put("religion", showList.getReligion());
            obj.put("birthPlace", birth_place.getText().toString());
            obj.put("birthDate", birth_date.getText().toString());
            obj.put("address", address.getText().toString());
            obj.put("zipCode", zipcode_identity.getText().toString());
            obj.put("kelurahan", kelurahan_identity.getText().toString());
            obj.put("kecamatan", kecamatan_identity.getText().toString());
            obj.put("city", kota_identity.getText().toString());
            obj.put("province", provinci_identity.getText().toString());
            obj.put("validityPriod", valid_period.getText().toString());
            obj.put("installDate", install_date.getText().toString());
            obj.put("installTime", install_time.getText().toString());
            obj.put("confirmStart", date_confirm_start.getText().toString()
                    + " " + time_confirm_start.getText().toString());
            obj.put("confirmEnd", date_confirm_end.getText().toString()
                    + " " + time_confirm_end.getText().toString());
            obj.put("installAddress", install_address.getText().toString());
            obj.put("installZipcode", zipcode_install.getText().toString());
            obj.put("installKelurahan", kelurahan_install.getText().toString());
            obj.put("installKecamatan", kecamatan_install.getText().toString());
            obj.put("installCity", kota_install.getText().toString());
            obj.put("installProvince", provinci_install.getText().toString());
            obj.put("billingAddress", billing_address.getText().toString());
            obj.put("billingZipCode", zipcode_billing.getText().toString());
            obj.put("billingKelurahan", kelurahan_billing.getText().toString());
            obj.put("billingKecamatan", kecamatan_billing.getText().toString());
            obj.put("billingCity", kota_billing.getText().toString());
            obj.put("billingProvince", provinci_billing.getText().toString());
            obj.put("telephone", telp.getText().toString());
            obj.put("hp", hp.getText().toString());
            obj.put("xphone", xphone.getText().toString());
            obj.put("email", email.getText().toString());
            //obj.put("print", isPrint);

            if (showList.get_job() != null) {
                obj.put("occupation", showList.get_job());
            } else {
                obj.put("occupation", "");
            }

            if (showList.get_homeType() != null) {
                obj.put("homeType", showList.get_homeType());
            } else {
                obj.put("homeType", "");
            }

            if (showList.get_homeStatus() != null) {
                obj.put("homeStatus", showList.get_homeStatus());
            } else {
                obj.put("homeStatus", "");
            }

            if (showList.get_income() != null) {
                obj.put("income", showList.get_income());
            } else {
                obj.put("income", "");
            }

            if (showList.getProStatus() != null) {
                if (!showList.getProStatus().equalsIgnoreCase("none")) {
                    obj.put("prosStatus", showList.getProStatus());
                } else {
                    obj.put("prosStatus", "");
                }
            } else {
                obj.put("prosStatus", "");
            }

            obj.put("imagePath", picturePath);
            obj.put("userId", uId);
            obj.put("homeNo", no_home.getText().toString());
            obj.put("rt", rt.getText().toString());
            obj.put("rw", rw.getText().toString());
            obj.put("homeNo_install", no_home_install.getText().toString());
            obj.put("homeNo_bill", no_home_bill.getText().toString());
            obj.put("rt_install", rt_install.getText().toString());
            obj.put("rt_bill", rt_bill.getText().toString());
            obj.put("rw_install", rw_install.getText().toString());
            obj.put("rw_bill", rw_bill.getText().toString());
            obj.put("direction", patokan.getText().toString());
            obj.put("image", CreateImageJson());
            obj.put("store", store.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("jsonku", obj.toString());
        return obj.toString();
    }

    private JSONArray CreateImageJson() {
        JSONArray array = new JSONArray();
        try {
            for (int j = 0; j < ktp_name.size(); j++) {
                JSONObject list = new JSONObject();
                list.put("image_type_id", "ktp");
                list.put("image_name", ktp_name.get(j));
                array.put(list);
            }

            for (int j = 0; j < instalasi_name.size(); j++) {
                JSONObject list = new JSONObject();
                list.put("image_type_id", "instalasi");
                list.put("image_name", instalasi_name.get(j));
                array.put(list);
            }

            for (int j = 0; j < buktibayar_name.size(); j++) {
                JSONObject list = new JSONObject();
                list.put("image_type_id", "buktibayar");
                list.put("image_name", buktibayar_name.get(j));
                array.put(list);
            }

            for (int j = 0; j < lainnya_name.size(); j++) {
                JSONObject list = new JSONObject();
                list.put("image_type_id", "lainnya");
                list.put("image_name", lainnya_name.get(j));
                array.put(list);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return array;
    }

    private void saveImage() {
        String fileName = "", filePath = "";
        if (instalasi.size() > 0) {
            LinearLayout mInstal = (LinearLayout) root.findViewById(R.id.list_pic_instalasi);
            for (int a = 0; a < instalasi.size(); a++) {
                filePath = instalasi.get(a);
                fileName = instalasi_name.get(a);
                System.out.println("running " + fileName);
                System.out.println("filePath " + filePath);
                TextView txtInstal = (TextView) mInstal.findViewWithTag(filePath);
                txtInstal.setText(fileName + " 0%");
                uploadImage(filePath, fileName, txtInstal);
            }
        }
        if (buktibayar.size() > 0) {
            LinearLayout mLing = (LinearLayout) root.findViewById(R.id.list_pic_bb);
            for (int a = 0; a < buktibayar.size(); a++) {
                filePath = buktibayar.get(a);
                fileName = buktibayar_name.get(a);
                System.out.println("running " + fileName);
                System.out.println("filePath " + filePath);
                TextView txtLing = (TextView) mLing.findViewWithTag(filePath);
                txtLing.setText(fileName + " 0%");
                uploadImage(filePath, fileName, txtLing);
            }
        }
        if (lainnya.size() > 0) {
            LinearLayout mRmh = (LinearLayout) root.findViewById(R.id.list_pic_lainnya);
            for (int a = 0; a < lainnya.size(); a++) {
                filePath = lainnya.get(a);
                fileName = lainnya_name.get(a);
                System.out.println("running " + fileName);
                System.out.println("filePath " + filePath);
                TextView txtRmh = (TextView) mRmh.findViewWithTag(filePath);
                txtRmh.setText(fileName + " 0%");
                uploadImage(filePath, fileName, txtRmh);
            }
        }
        if (ktp.size() > 0) {
            LinearLayout mKtp = (LinearLayout) root.findViewById(R.id.list_pic_ktp);
            for (int a = 0; a < ktp.size(); a++) {
                filePath = ktp.get(a);
                fileName = ktp_name.get(a);
                System.out.println("running " + fileName);
                System.out.println("filePath " + filePath);
                TextView txtKtp = (TextView) mKtp.findViewWithTag(filePath);
                txtKtp.setText(fileName + " 0%");
                uploadImage(filePath, fileName, txtKtp);
            }
        }
    }

    private void uploadImage(String filePath, final String fileName, final TextView txtCent) {
        try {
            String url = ConnectionManager.CM_URL_UPLOAD + appNumb;
            System.out.println(url);
            final File myFile = new File(filePath);
            RequestParams params = new RequestParams();
            params.put("image", myFile);
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(300000);
            client.post(url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    int writen = (int) bytesWritten;
                    float proportionCorrect = ((float) writen) / ((float) myFile.length());
                    float percentFl = proportionCorrect * 100;
                    System.out.println("percentFl " + String.format("%.0f", percentFl));
                    if (!String.format("%.0f", percentFl).equalsIgnoreCase("0")) {
                        if (percentFl > 100) {
                            percentFl = 100;
                            txtCent.setText(fileName + " " + String.format("%.0f", percentFl) + " %");
                        }
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] bytes) {
                    isSendImage = true;
                    //Toast.makeText(getContext(), "Success upload " + fileName, Toast.LENGTH_LONG).show();
                    checkImage(fileName, true);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable throwable) {
                    txtCent.setText(fileName + " 0%");
                    //String responseString = "Error occurred! Http Status Code: " + statusCode + " " + fileName;
                    //Toast.makeText(getContext(), responseString, Toast.LENGTH_LONG).show();
                    checkImage(fileName, false);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addViewImageList(int code, final ArrayList<String> arrayListImage, String imageName, String picturePath) {
        switch (code) {
            case PICK_IMAGE_INSTALASI:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_instalasi);
                break;
            case PICK_IMAGE_BUKTIBAYAR:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_bb);
                break;
            case PICK_IMAGE_LAINNYA:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_lainnya);
                break;
            case PICK_IMAGE_KTP:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_ktp);
                break;
            case TAKE_PICTURE_INSTALASI:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_instalasi);
                break;
            case TAKE_PICTURE_BUKTIBAYAR:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_bb);
                break;
            case TAKE_PICTURE_LAINNYA:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_lainnya);
                break;
            case TAKE_PICTURE_KTP:
                mLayoutPackage = (LinearLayout) root.findViewById(R.id.list_pic_ktp);
                break;
        }
        final LinearLayout Mainlinear = new LinearLayout(getContext());
        Mainlinear.setBackgroundColor(getResources().getColor(R.color.white));
        Mainlinear.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLinearPackage.setMargins(0, 20, 0, 0);
        Mainlinear.setLayoutParams(pLinearPackage);
        mLayoutPackage.addView(Mainlinear);


        //Liner Title Image
        LinearLayout.LayoutParams pLineTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearTitle = new LinearLayout(getContext());
        linearTitle.setLayoutParams(pLineTitle);
        linearTitle.setOrientation(LinearLayout.HORIZONTAL);
        linearTitle.setPadding(10, 0, 10, 0);
        Mainlinear.addView(linearTitle);

        LinearLayout.LayoutParams pTxtTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        final TextView txtTitle = new TextView(getContext());
        txtTitle.setLayoutParams(pTxtTitle);
        txtTitle.setText(imageName);
        txtTitle.setTag(picturePath);
        txtTitle.setTextSize(10);
        txtTitle.setFreezesText(true);
        txtTitle.setPadding(5, 25, 0, 0);
        linearTitle.addView(txtTitle);


        //Linear Cancel Button
        LinearLayout.LayoutParams pLineCancel = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        LinearLayout linearCancel = new LinearLayout(getContext());
        linearTitle.setLayoutParams(pLineCancel);
        linearTitle.setOrientation(LinearLayout.HORIZONTAL);
        linearTitle.addView(linearCancel);

        LinearLayout.LayoutParams pBtnCancel = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
        ImageButton btnCancel = new ImageButton(getContext());
        btnCancel.setLayoutParams(pBtnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String m = txtTitle.getTag().toString();
                Mainlinear.removeAllViews();
                int i = arrayListImage.indexOf(m);
                arrayListImage.remove(i);
            }
        });
        btnCancel.setBackgroundResource(R.drawable.ic_close_package);
        btnCancel.setPadding(0, 16, 0, 16);
        linearCancel.addView(btnCancel);
        PathListItem item = new PathListItem();
        item.setPath(picturePath);
        item.setImageName(imageName);
        switch (code) {
            case PICK_IMAGE_INSTALASI:
                listPathIdentitas.add(item);
                ListInstalasi();
                break;
            case PICK_IMAGE_BUKTIBAYAR:
                listPathBuktiBayar.add(item);
                ListLingkungan();
                break;
            case PICK_IMAGE_LAINNYA:
                listPathLainnya.add(item);
                ListRumah();
                break;
            case PICK_IMAGE_KTP:
                listPathKtp.add(item);
                ListKtp();
                break;
            case TAKE_PICTURE_INSTALASI:
                listPathIdentitas.add(item);
                ListInstalasi();
                break;
            case TAKE_PICTURE_BUKTIBAYAR:
                listPathBuktiBayar.add(item);
                ListLingkungan();
                break;
            case TAKE_PICTURE_LAINNYA:
                listPathLainnya.add(item);
                ListRumah();
                break;
            case TAKE_PICTURE_KTP:
                listPathKtp.add(item);
                ListKtp();
                break;
        }

    }

    private void ListInstalasi() {
        try {
            instalasi.add(picturePath);
            instalasi_name.add(imageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ListLingkungan() {
        try {
            buktibayar.add(picturePath);
            buktibayar_name.add(imageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ListRumah() {
        try {
            lainnya.add(picturePath);
            lainnya_name.add(imageName);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void ListKtp() {
        try {
            ktp.add(picturePath);
            ktp_name.add(imageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class PathListItem {
        String imageName;
        String path;

        public PathListItem() {
            super();
        }

        public void setPath(String path) {
            this.path = path;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public String getImageName() {
            return imageName;
        }

        public String getPath() {
            return path;
        }
    }

    private void checkImage(String fileName, Boolean status) {
        Boolean findKtp = utils.findStringImage(fileName, "ktp");
        Boolean findLing = utils.findStringImage(fileName, "buktibayar");
        Boolean findInstall = utils.findStringImage(fileName, "instalasi");
        Boolean findRmh = utils.findStringImage(fileName, "lainnya");

        if (findKtp) {
            if (status) {
                MainFragmentMS.isExistKtp = true;
            } else {
                MainFragmentMS.isExistKtp = false;
            }
        }

        if (findLing) {
            if (status) {
                MainFragmentMS.isExistLing = true;
            } else {
                MainFragmentMS.isExistLing = false;
            }
        }

        if (findRmh) {
            if (status) {
                MainFragmentMS.isExistRmh = true;
            } else {
                MainFragmentMS.isExistRmh = false;
            }
        }

        if (findInstall) {
            if (status) {
                MainFragmentMS.isExistInstall = true;
            } else {
                MainFragmentMS.isExistInstall = false;
            }
        }
    }
}
