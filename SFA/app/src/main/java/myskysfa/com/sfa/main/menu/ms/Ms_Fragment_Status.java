package myskysfa.com.sfa.main.menu.ms;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableDataMSDTDFailed;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.TableMasterPackage;
import myskysfa.com.sfa.database.TablePlanMS;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMSDTDFailedAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMasterPackageAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePlanMSAdapter;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.PermissionUtils;
import myskysfa.com.sfa.utils.Utils;
import myskysfa.com.sfa.widget.WorkaroundMapFragment;

/**
 * Created by Eno on 6/13/2016.
 */
public class Ms_Fragment_Status extends Fragment implements GoogleMap.OnMyLocationButtonClickListener, OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMarkerDragListener {

    private static Context _context;
    private LinearLayout profileLayout, emergencyLayout, paketLayout, pymLayout, signLayout;
    private GoogleMap googleMap;
    private ScrollView mScrollView;
    private Utils utils;
    public static Double lat, lon;
    private Location latLng;
    private ViewGroup view;
    private Marker marker;
    private boolean mPermissionDenied = false;
    public static String addressLine;
    private TextView txtAddressLine, _latitude, _longitude, _address, txtPStatus, pKet, txtEStatus,
            eKet, txtPktStatus, txtPymStatus, txtSignstatus;
    private ImageView imagePFlag, imageEFlag, imagePktFlag, imagePymFlag, imageSignFlag;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private Menu menu;
    private MenuItem alert;
    private String numberForm, photo, signature, profile, payment, paket, emergency, checklist,
            imei, token, value, sflCode, hp, billZipcode, direction,xphone,
            installAddress, installProvince, birthPlace, city, billingKelurahan, installDate,
            billingProvince, identityNo, userId, zipCode, province, billingCity, installZipcode,
            birthDate, firstName, installCity, homeNo, middleName, ftId, lastName, identityType,
            installKelurahan, confirmStart, confirmEnd, installKecamatan, kelurahan, billingKecamatan,
            kecamatan, email, address, rt, rt_install, rt_bill, rw_install, rw_bill, home_no_install,
            home_no_bill, billingAddress, validityPriod, installTime,
            rw, telephone, eFirstName, eAddress, eTelephone, eLastName, eRelationship,
            eMiddleName, BRAND, ala1, ala2, ala3, bsc1, bsc2, bsc3,
            religion, job, income, gender, homeType, homeStatus, url, response, assignId,
            status, data, promoCode, expiry_date, account_number, bank, name, amount, method, period,
            remark, additional, store, routeId, routeValue, simId, simValue, signBedaAlamat, signSewa,
            signMultiAccount, signBundlingIndosat, ccNumber, ccType, paymentDate, hw_status, prosStatus;
    private TableFormAppAdapter mFormAppAdapter;
    private List<TableFormApp> listFormApp;
    private boolean isTaskRunning = false;
    private ProgressDialog _progressDialog;
    private SharedPreferences sessionPref;
    private Handler handler;
    private JSONArray imageArray;
    private SupportMapFragment fragment;
    private int isMulty;
    private ScrollView frame;

    public static Fragment newInstance(Context context) {
        _context = context;
        Ms_Fragment_Status dtdStatus = new Ms_Fragment_Status();
        return dtdStatus;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = (ViewGroup) inflater.inflate(R.layout.ms_fragment_status, container, false);
        }

        sessionPref = getContext().getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);

        utils = new Utils(getActivity());
        utils.setIMEI();
        utils.setGeoLocation();

        handler = new Handler();

        frame = (ScrollView) view.findViewById(R.id.frame);
        _latitude = (TextView) view.findViewById(R.id.latitude);
        _longitude = (TextView) view.findViewById(R.id.longitude);
        txtPStatus = (TextView) view.findViewById(R.id.txt_p_status);
        txtEStatus = (TextView) view.findViewById(R.id.txt_e_status);
        txtPktStatus = (TextView) view.findViewById(R.id.txt_pkt_status);
        txtPymStatus = (TextView) view.findViewById(R.id.txt_pym_status);
        txtSignstatus = (TextView) view.findViewById(R.id.txt_sign_status);

        imagePFlag = (ImageView) view.findViewById(R.id.image_p_flag);
        imageEFlag = (ImageView) view.findViewById(R.id.image_e_flag);
        imagePktFlag = (ImageView) view.findViewById(R.id.image_pkt_flag);
        imagePymFlag = (ImageView) view.findViewById(R.id.image_pym_flag);
        imageSignFlag = (ImageView) view.findViewById(R.id.image_sign_flag);

        profileLayout = (LinearLayout) view.findViewById(R.id.profile_layout);
        emergencyLayout = (LinearLayout) view.findViewById(R.id.emergency_layout);
        paketLayout = (LinearLayout) view.findViewById(R.id.paket_layout);
        pymLayout = (LinearLayout) view.findViewById(R.id.payment_layout);
        signLayout = (LinearLayout) view.findViewById(R.id.sign_layout);

        mScrollView = (ScrollView) view.findViewById(R.id.frame);
        setMapMarker();

        //Handle Catch ScrollView with Even Drag Map

        /*WorkaroundMapFragment mapFragment =
                (WorkaroundMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mapFragment.setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                mScrollView.requestDisallowInterceptTouchEvent(true);
            }
        });*/
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void setHideKeyBoard() {
        utils.setHideKeyboard(getActivity(), view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentManager fm = getChildFragmentManager();
        fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
        if (fragment == null) {
            fragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map, fragment).commit();
        }
        fragment.getMapAsync(this);
    }


    private void setMapMarker() {
        //latitude    = Double.parseDouble(utils.getLatitude());
        // longitude   = Double.parseDouble(utils.getLongitude());
        addressLine = utils.getAddressLine();
        //Log.d("MSStatus", "long =" + longitude + "  lat=" + latitude);
        if (googleMap == null) {
            FragmentManager fm = getChildFragmentManager();
            fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
            if (fragment == null) {
                fragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.map, fragment).commit();
            }

            ((WorkaroundMapFragment) fm.findFragmentById(R.id.map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
                @Override
                public void onTouch() {
                    frame.requestDisallowInterceptTouchEvent(true);
                }
            });


            fragment.getMapAsync(this);
            //googleMap = fragment.getMap();
            //mMap =  ((SupportMapFragment)  getChildFragmentManager()
            //        .findFragmentById(R.id.map)).getMap();
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;

        googleMap.setOnMyLocationButtonClickListener(this);
        enableMyLocation();

        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnMarkerDragListener(this);

        googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

            @Override
            public void onMyLocationChange(Location location) {
                lon = location.getLongitude();
                lat = location.getLatitude();

                latLng = location;
                Geocoder geocoder;
                List<Address> addresses = null;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                if (marker == null) {
                    marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(),
                            location.getLongitude())).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                            .draggable(true).alpha(0.7f));

                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
                            location.getLongitude()), 17.0f));

                    try {
                        addresses = geocoder.getFromLocation(lat, lon, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (addresses != null && addresses.size() != 0) {
                        addressLine = addresses.get(0).getAddressLine(0);
                    } else {
                        addressLine = "";
                    }

                    _longitude.setText(String.valueOf(lon));
                    _latitude.setText(String.valueOf(lat));
                }
            }
        });

        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View v = getActivity().getLayoutInflater().inflate(R.layout.info_windows, null);
                txtAddressLine = (TextView) v.findViewById(R.id.address_line);
                txtAddressLine.setText(addressLine);
                return v;
            }
        });
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(getActivity(), LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (googleMap != null) {
            // Access to the location has been granted to the app.
            googleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        mScrollView.requestDisallowInterceptTouchEvent(true);
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        mScrollView.requestDisallowInterceptTouchEvent(true);
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        LatLng dragPosition = marker.getPosition();
        double dragLat = dragPosition.latitude;
        double dragLong = dragPosition.longitude;
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        int distance = distance(latLng, dragPosition);

        if (distance > 100) {
            utils.showAlertDialog(getActivity(), "Jarak Marker Lebih Dari 100 Meter", "Silahkan pindahkan posisi marker", false);
            this.marker.remove();
            this.marker = null;
            //setUpMap();
            return;
        }

        try {
            addresses = geocoder.getFromLocation(dragLat, dragLong, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses != null && addresses.size() != 0) {
            addressLine = addresses.get(0).getAddressLine(0);
        } else {
            addressLine = "";
        }

        _longitude.setText(String.valueOf(dragLong));
        _latitude.setText(String.valueOf(dragLat));
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_save, menu);
        this.menu = menu;
        alert = this.menu.findItem(R.id.action_menu_commit);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu_commit:

                if (lon == null || lon == 0.0 || lat == null || lat == 0.0) {
                    Toast.makeText(getActivity(), "Location kosong", Toast.LENGTH_SHORT).show();
                } else {
                    new SendToServerTask().execute();
                }
                break;
            case R.id.action_menu_location:
                setLocation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onResume() {
        super.onResume();
        //getDataFromDB();

        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }

        if (!checkReady()) {
            return;
        }
        // Clear the map because we don't want duplicates of the markers.
        googleMap.clear();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!checkReady()) {
            return;
        }
        googleMap.clear();

        if (marker != null) {
            marker.remove();
            marker = null;
        }
    }

    private boolean checkReady() {
        if (googleMap == null) {
            //Toast.makeText(getActivity(), R.string.map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getChildFragmentManager(), "dialog");
    }

    public static int distance(Location StartP, LatLng EndP) {
        double earthRadius = 6371000; //meters
        double lat1 = StartP.getLatitude();
        double lat2 = EndP.latitude;
        double lng1 = StartP.getLongitude();
        double lng2 = EndP.longitude;

        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        int dist = (int) (earthRadius * c);
        return dist;
    }

    /**
     * Method for get refresh longitude and latitude
     */
    private void setLocation() {
        String stringLatitude = utils.getLatitude();
        String stringLongitude = utils.getLongitude();
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        if (stringLatitude != null && stringLatitude.length() > 1) {
            lat = Double.valueOf(stringLatitude);
            lon = Double.valueOf(stringLongitude);
            try {
                addresses = geocoder.getFromLocation(lat, lon, 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (addresses != null && addresses.size() != 0) {
                addressLine = addresses.get(0).getAddressLine(0);
            } else {
                addressLine = "";
            }
            Toast.makeText(getActivity(), "Long= " + stringLongitude + " lat= " + stringLatitude, Toast.LENGTH_SHORT).show();

            _longitude.setText(stringLongitude);
            _latitude.setText(stringLatitude);
        } else {
            Toast.makeText(getActivity(), "Longitude dan latitude kosong", Toast.LENGTH_SHORT).show();
        }
    }

    public void getDataFromDB() {
        try {
            SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.fn_ms), Context.MODE_PRIVATE);
            numberForm = sm.getString("fn", null);

            mFormAppAdapter = new TableFormAppAdapter(getActivity());
            listFormApp = mFormAppAdapter.getDatabyCondition(TableFormApp.fFORM_NO, numberForm);

            /*for (int i = 0; i < listFormApp.size(); i++) {
                TableFormApp item = listFormApp.get(i);
                if (item != null) {
                    signature = item.getVALUES_SIGNATURE();
                    profile = item.getVALUES_PROFILE();
                    payment = item.getVALUES_PAYMENT();
                    paket = item.getVALUES_PACKAGE();
                    emergency = item.getVALUES_EMERGENCY();
                }
            }*/

            signature = listFormApp.get(0).getVALUES_SIGNATURE();
            profile = listFormApp.get(0).getVALUES_PROFILE();
            payment = listFormApp.get(0).getVALUES_PAYMENT();
            paket = listFormApp.get(0).getVALUES_PACKAGE();
            emergency = listFormApp.get(0).getVALUES_EMERGENCY();
            checklist = listFormApp.get(0).getVALUES_CHECKLIST();
            signBedaAlamat = listFormApp.get(0).getVALUES_SIGNBEDA();
            signSewa = listFormApp.get(0).getVALUES_SIGNMULTY();
            signMultiAccount = listFormApp.get(0).getVALUES_SIGNACCOUNT();
            signBundlingIndosat = listFormApp.get(0).getVALUES_SIGNINDOSAT();


            // token   = sessionPref.getString(TableLogLogin.C_TOKEN, "");

            if (!profile.equalsIgnoreCase("")) {
                txtPStatus.setText("Data sudah diambil");
                profileLayout.setBackgroundResource(R.drawable.border_item_master);
                imagePFlag.setImageResource(R.drawable.ic_centang);
            } else {
                txtPStatus.setText("Data belum diambil");
                profileLayout.setBackgroundResource(R.drawable.border_item_red);
                imagePFlag.setImageResource(R.drawable.ic_kali);
            }

            if (!payment.equalsIgnoreCase("")) {
                txtPymStatus.setText("Data sudah diambil");
                pymLayout.setBackgroundResource(R.drawable.border_item_master);
                imagePymFlag.setImageResource(R.drawable.ic_centang);
            } else {
                txtPymStatus.setText("Data belum diambil");
                pymLayout.setBackgroundResource(R.drawable.border_item_red);
                imagePymFlag.setImageResource(R.drawable.ic_kali);
            }

            if (!paket.equalsIgnoreCase("")) {
                txtPktStatus.setText("Data sudah diambil");
                paketLayout.setBackgroundResource(R.drawable.border_item_master);
                imagePktFlag.setImageResource(R.drawable.ic_centang);
            } else {
                txtPktStatus.setText("Data belum diambil");
                paketLayout.setBackgroundResource(R.drawable.border_item_red);
                imagePktFlag.setImageResource(R.drawable.ic_kali);
            }

            if (!emergency.equalsIgnoreCase("")) {
                txtEStatus.setText("Data sudah diambil");
                emergencyLayout.setBackgroundResource(R.drawable.border_item_master);
                imageEFlag.setImageResource(R.drawable.ic_centang);
            } else {
                txtEStatus.setText("Data belum diambil");
                emergencyLayout.setBackgroundResource(R.drawable.border_item_red);
                imageEFlag.setImageResource(R.drawable.ic_kali);
            }

            if (!signature.equalsIgnoreCase("")) {
                txtSignstatus.setText("Data sudah diambil");
                signLayout.setBackgroundResource(R.drawable.border_item_master);
                imageSignFlag.setImageResource(R.drawable.ic_centang);
            } else {
                txtSignstatus.setText("Data belum diambil");
                signLayout.setBackgroundResource(R.drawable.border_item_red);
                imageSignFlag.setImageResource(R.drawable.ic_kali);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String generateJson() {
        TableMasterPackageAdapter productAdapter = new TableMasterPackageAdapter(getActivity());
        List<TableMasterPackage> listProduct;
        String sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
        String token = sessionPref.getString(TableLogLogin.C_TOKEN, "");
        String username = sessionPref.getString(TableLogLogin.C_USER_NAME, "");
        String brandCode = sessionPref.getString(TableLogLogin.C_BRANCH_ID, "");
        Log.d("FTStatus", "token=" + token);
        try {
            JSONObject profObj = new JSONObject(profile);
            JSONObject refObj = new JSONObject(emergency);
            JSONObject paketObj = new JSONObject(paket);
            JSONObject paymentObj = new JSONObject(payment);

            hw_status = profObj.getString("hw_status");
            hp = profObj.getString("hp");
            xphone = profObj.getString("xphone");
            billZipcode = profObj.getString("billingZipCode");
            direction = profObj.getString("direction");
            installAddress = profObj.getString("installAddress");
            installProvince = profObj.getString("installProvince");
            birthPlace = profObj.getString("birthPlace");
            city = profObj.getString("city");
            billingKelurahan = profObj.getString("billingKelurahan");
            installDate = profObj.getString("installDate");
            billingProvince = profObj.getString("billingProvince");
            identityNo = profObj.getString("identityNo");
            userId = profObj.getString("userId");
            zipCode = profObj.getString("zipCode");
            province = profObj.getString("province");
            billingCity = profObj.getString("billingCity");
            installZipcode = profObj.getString("installZipcode");
            birthDate = profObj.getString("birthDate");
            firstName = profObj.getString("firstName");
            installCity = profObj.getString("installCity");
            homeNo = profObj.getString("homeNo");
            middleName = profObj.getString("middleName");
            ftId = profObj.getString("ftId");
            lastName = profObj.getString("lastName");
            identityType = profObj.getString("identityType");
            installKelurahan = profObj.getString("installKelurahan");
            confirmStart = profObj.getString("confirmStart");
            confirmEnd = profObj.getString("confirmEnd");
            installKecamatan = profObj.getString("installKecamatan");
            kelurahan = profObj.getString("kelurahan");
            billingKecamatan = profObj.getString("billingKecamatan");
            kecamatan = profObj.getString("kecamatan");
            email = profObj.getString("email");
            address = profObj.getString("address");
            rt = profObj.getString("rt");
            billingAddress = profObj.getString("billingAddress");
            validityPriod = profObj.getString("validityPriod");
            installTime = profObj.getString("installTime");
            rw = profObj.getString("rw");

            rt_install = profObj.getString("rt_install");
            rt_bill = profObj.getString("rt_bill");
            rw_install = profObj.getString("rw_install");
            rw_bill = profObj.getString("rw_bill");
            home_no_install = profObj.getString("homeNo_install");
            home_no_bill = profObj.getString("homeNo_bill");
            prosStatus = profObj.getString("prosStatus");

            telephone = profObj.getString("telephone");

            religion = profObj.getString("religion");
            job = profObj.getString("occupation");
            income = profObj.getString("income");
            gender = profObj.getString("gender");
            homeType = profObj.getString("homeType");
            homeStatus = profObj.getString("homeStatus");
            store = profObj.getString("store");
            imageArray = profObj.getJSONArray("image");

            eFirstName = refObj.getString("eFirstName");
            eAddress = refObj.getString("eAddress");
            eTelephone = refObj.getString("eTelephone");
            eLastName = refObj.getString("eLastName");
            eRelationship = refObj.getString("eRelationship");
            eMiddleName = refObj.getString("eMiddleName");

            //Payment Get Data
            if (paymentObj.has("masaBerlaku")) {
                expiry_date = paymentObj.getString("masaBerlaku");
            } else {
                expiry_date = "";
            }

            if (paymentObj.has("noRek")) {
                account_number = paymentObj.getString("noRek");
            } else {
                account_number = "";
            }
            if (paymentObj.has("bank")) {
                bank = paymentObj.getString("bank");
            } else {
                bank = "";
            }
            name = paymentObj.getString("name");
            amount = paymentObj.getString("total");
            method = paymentObj.getString("caraBayar");
            period = paymentObj.getString("periode");
            remark = paymentObj.getString("remark");
            paymentDate = paymentObj.getString("payment_date");
            if (paymentObj.has("cc_type")) {
                ccType = paymentObj.getString("cc_type");
            } else {
                ccType = "";
            }

            if (paymentObj.has("cc_no")) {
                ccNumber = paymentObj.getString("cc_no");
            } else {
                ccNumber = "";
            }

            /*ODU = paketObj.getString("ODU");
            VC = paketObj.getString("VC");*/
            BRAND = paketObj.getString("BRAND");
            /*LNB = paketObj.getString("LNB");
            DSD = paketObj.getString("DSD");*/
            //HW_STATUS = paketObj.getString("HW_STATUS");
            isMulty = paketObj.getInt("isMulty");
            promoCode = paketObj.getString("promoCode");
            additional = paketObj.getString("additional");
            String ALACARTE = paketObj.getString("ALACARTE");
            String BASIC = paketObj.getString("BASIC");
            String Bundling = paketObj.getString("bundling");

            JSONObject alaObj = new JSONObject(ALACARTE);
            ala1 = alaObj.getString("1");
            ala2 = alaObj.getString("2");
            ala3 = alaObj.getString("3");

            JSONObject bscObj = new JSONObject(BASIC);
            bsc1 = bscObj.getString("1");
            bsc2 = bscObj.getString("2");
            bsc3 = bscObj.getString("3");

            JSONObject bundleObj = new JSONObject(Bundling);
            simId = bundleObj.getString("sim_id");
            simValue = bundleObj.getString("sim_value");
            routeId = bundleObj.getString("router_id");
            routeValue = bundleObj.getString("router_value");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        JSONObject obj = new JSONObject();
        try {
            obj.put("prospect_nbr", ftId);
            obj.put("sfl_code", sflCode);
            obj.put("imei", utils.getIMEI());
            obj.put("user_name", username);
            obj.put("created_by", "sales");
            obj.put("brand_code", BRAND);
            obj.put("occupation", "WNI");
            obj.put("confirmStart", confirmStart);
            obj.put("confirmEnd", confirmEnd);
            obj.put("direction", direction);
            obj.put("store", store);

            JSONObject objProfile = new JSONObject();
            objProfile.put("first_name", firstName);
            objProfile.put("middle_name", middleName);
            objProfile.put("last_name", lastName);
            objProfile.put("tgl_lahir", birthDate);
            objProfile.put("tempat_lahir", birthPlace);
            obj.put("profile", objProfile);

            obj.put("home_phone", telephone);
            obj.put("x_phone", xphone);
            obj.put("mobile_phone", hp);
            obj.put("work_phone", "");
            obj.put("email", email);
            obj.put("religion", religion);
            obj.put("work", job);
            obj.put("income", income);
            obj.put("gender", gender);
            obj.put("pros_status", prosStatus);

            JSONObject objIdentity = new JSONObject();
            objIdentity.put("identity_type", identityType);
            objIdentity.put("identity_nbr", identityNo);
            objIdentity.put("identity_period", validityPriod);
            obj.put("identity", objIdentity);

            JSONObject objEmergency = new JSONObject();
            objEmergency.put("emergency_first_name", eFirstName);
            objEmergency.put("emergency_middle_name", eMiddleName);
            objEmergency.put("emergency_last_name", eLastName);
            objEmergency.put("emergency_phone", eTelephone);
            objEmergency.put("emergency_address", eAddress);
            objEmergency.put("relation", eRelationship.substring(0, 1));
            obj.put("emergency_contact", objEmergency);

            JSONObject objAddress = new JSONObject();
            objAddress.put("building_type", homeType);
            objAddress.put("building_status", homeStatus);

            JSONArray dataArray = new JSONArray();
            JSONObject objData = new JSONObject();
            JSONObject objData2 = new JSONObject();
            JSONObject objData3 = new JSONObject();
            objData.put("address_type", "BIL");
            objData.put("street", billingAddress);
            objData.put("kelurahan", billingKelurahan);
            objData.put("kecamatan", billingKecamatan);
            objData.put("kota", billingCity);
            objData.put("provinsi", billingProvince);
            objData.put("zipcode", billZipcode);
            objData.put("no_rumah", home_no_bill);
            objData.put("rt", rt_bill);
            objData.put("rw", rw_bill);

            objData2.put("address_type", "INS");
            objData2.put("street", installAddress);
            objData2.put("kelurahan", installKelurahan);
            objData2.put("kecamatan", installKecamatan);
            objData2.put("kota", installCity);
            objData2.put("provinsi", installProvince);
            objData2.put("zipcode", installZipcode);
            objData2.put("no_rumah", home_no_install);
            objData2.put("rt", rt_install);
            objData2.put("rw", rw_install);

            objData3.put("address_type", "PRI");
            objData3.put("street", address);
            objData3.put("kelurahan", kelurahan);
            objData3.put("kecamatan", kecamatan);
            objData3.put("kota", city);
            objData3.put("provinsi", province);
            objData3.put("zipcode", zipCode);
            objData3.put("no_rumah", homeNo);
            objData3.put("rt", rt);
            objData3.put("rw", rw);
            dataArray.put(objData);
            dataArray.put(objData2);
            dataArray.put(objData3);
            objAddress.put("data", dataArray);
            obj.put("address", objAddress);
            obj.put("image", imageArray);

            JSONObject objPayment = new JSONObject();
            objPayment.put("period", period.substring(0, 1));
            objPayment.put("method", method.substring(0, 1));
            if (!amount.isEmpty()) {
                objPayment.put("amount", amount);
            } else {
                objPayment.put("amount", "");
            }
            objPayment.put("remark", remark);
            objPayment.put("account_number", account_number);
            objPayment.put("name", name);
            objPayment.put("bank", bank);
            objPayment.put("expiry_date", expiry_date);
            if (!ccType.equalsIgnoreCase("")) {
                objPayment.put("cc_type", ccType.substring(0, 1));
            } else {
                objPayment.put("cc_type", "");
            }
            objPayment.put("cc_no", ccNumber);
            objPayment.put("payment_date", paymentDate);
            obj.put("payment", objPayment);

            obj.put("latitude", lat);
            obj.put("longitude", lon);
            JSONObject objServices = new JSONObject();
            objServices.put("brand", BRAND);

            objServices.put("install_schedule_date", installDate);
            objServices.put("install_schedule_time", installTime);
            objServices.put("isMulti", isMulty);
            objServices.put("promo_code", promoCode);
            objServices.put("hardware_status", hw_status);


            /*JSONArray hardwareArray = new JSONArray();
            JSONObject objHardware = new JSONObject();
            objHardware.put("vc", VC);
            objHardware.put("dsd", DSD);
            objHardware.put("lnb", LNB);
            objHardware.put("dish", ODU);
            hardwareArray.put(objHardware);
            objServices.put("hardware", hardwareArray);*/

            JSONObject objPackages;
            JSONArray packagesArray = new JSONArray();

            if (bsc1 != null && !bsc1.equalsIgnoreCase("")) {
                objPackages = new JSONObject();
                JSONArray alacarteArray = new JSONArray();
                JSONObject objAlacarte;
                listProduct = productAdapter.getDatabyCondition(TableMasterPackage.fROWID, bsc1);
                objPackages.put("code", bsc1);
                Log.d("FTStatus", "brand =" + listProduct.get(listProduct.size() - 1).getProduct_name());
                objPackages.put("name", listProduct.get(listProduct.size() - 1).getProduct_name());
                // {"ALACARTE":{"3":"","2":"","1":"5#CINEMA1,6#CINEMA2,7#CINEMA3"}
                if (ala1 != null) {
                    try {
                        String[] ar = ala1.split("[,]");
                        for (int i = 0; i < ar.length; i++) {
                            String[] a = ar[i].split("#");
                            objAlacarte = new JSONObject();
                            objAlacarte.put("code", a[0]);
                            listProduct = productAdapter.getDatabyCondition(TableMasterPackage.fROWID, a[0]);
                            Log.d("FTStatus", "alacarte =" + listProduct.get(listProduct.size() - 1).getProduct_name());
                            objAlacarte.put("name", listProduct.get(listProduct.size() - 1).getProduct_name());
                            alacarteArray.put(objAlacarte);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                objPackages.put("ala_carte", alacarteArray);
                packagesArray.put(objPackages);
            } else {
                objPackages = new JSONObject();
                JSONArray alacarteArray = new JSONArray();
                JSONObject objAlacarte;
                objPackages.put("code", "1");
                objPackages.put("name", "SUPER GALAXY 2014");
                objAlacarte = new JSONObject();
                objAlacarte.put("code", "");
                objAlacarte.put("name", "");
                alacarteArray.put(objAlacarte);
                objPackages.put("ala_carte", alacarteArray);
                packagesArray.put(objPackages);
            }

            if (bsc2 != null && !bsc2.equalsIgnoreCase("")) {
                objPackages = new JSONObject();
                JSONArray alacarteArray = new JSONArray();
                JSONObject objAlacarte;
                listProduct = productAdapter.getDatabyCondition(TableMasterPackage.fROWID, bsc2);
                objPackages.put("code", bsc2);
                objPackages.put("name", listProduct.get(listProduct.size() - 1).getProduct_name());
                if (ala2 != null) {
                    try {
                        String[] ar = ala2.split("[,]");
                        for (int i = 0; i < ar.length; i++) {
                            String[] a = ar[i].split("#");
                            objAlacarte = new JSONObject();
                            objAlacarte.put("code", a[0]);
                            listProduct = productAdapter.getDatabyCondition(TableMasterPackage.fROWID, a[0]);
                            objAlacarte.put("name", listProduct.get(listProduct.size() - 1).getProduct_name());
                            alacarteArray.put(objAlacarte);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                objPackages.put("ala_carte", alacarteArray);
                packagesArray.put(objPackages);
            }

            if (bsc3 != null && !bsc3.equalsIgnoreCase("")) {
                objPackages = new JSONObject();
                JSONArray alacarteArray = new JSONArray();
                JSONObject objAlacarte;
                listProduct = productAdapter.getDatabyCondition(TableMasterPackage.fROWID, bsc3);
                objPackages.put("code", bsc3);
                objPackages.put("name", listProduct.get(listProduct.size() - 1).getProduct_name());
                if (ala3 != null) {
                    try {
                        String[] ar = ala3.split("[,]");
                        for (int i = 0; i < ar.length; i++) {
                            String[] a = ar[i].split("#");
                            objAlacarte = new JSONObject();
                            objAlacarte.put("code", a[0]);
                            listProduct = productAdapter.getDatabyCondition(TableMasterPackage.fROWID, a[0]);
                            objAlacarte.put("name", listProduct.get(listProduct.size() - 1).getProduct_name());
                            alacarteArray.put(objAlacarte);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                objPackages.put("ala_carte", alacarteArray);
                packagesArray.put(objPackages);
            }

            if (!additional.equalsIgnoreCase("{}")) {
                //Proses Additional Convert Json
                JSONObject objectAdditional = new JSONObject(additional);
                JSONObject objAddResult = new JSONObject();
                JSONArray arrayAddResult = new JSONArray();
                JSONArray arrayCheckAdd = objectAdditional.getJSONArray(Config.KEY_DATA);
                for (int i = 0; i < arrayCheckAdd.length(); i++) {
                    JSONObject objectResult = arrayCheckAdd.getJSONObject(i);
                    String add_id = objectResult.getString("additional_id");
                    String add_total = objectResult.getString("additional_total");

                    JSONObject objectChecklist2 = new JSONObject();
                    objectChecklist2.put("additional_id", add_id);
                    objectChecklist2.put("additional_total", add_total);
                    arrayAddResult.put(objectChecklist2);
                    objAddResult.put("data", arrayAddResult);
                }
                //End Proses Additional
                objServices.put("additional", objAddResult);
            } else {
                objServices.put("additional", "");
            }


            JSONObject objectBundle = new JSONObject();
            objectBundle.put("sim_id", simId);
            objectBundle.put("sim_value", simValue);
            objectBundle.put("router_id", routeId);
            objectBundle.put("router_value", routeValue);

            objServices.put("bundling", objectBundle);
            objServices.put("packages", packagesArray);
            obj.put("services", objServices);

            if (!checklist.equalsIgnoreCase("")
                    && !checklist.equalsIgnoreCase("{}")) {
                //Proses Checklist Convert Json
                JSONObject objectChecklist = new JSONObject(checklist);
                JSONObject objCheckResult = new JSONObject();
                JSONArray arrayResult = new JSONArray();
                JSONArray arrayCheck = objectChecklist.getJSONArray(Config.KEY_DATA);
                for (int i = 0; i < arrayCheck.length(); i++) {
                    JSONObject objectResult = arrayCheck.getJSONObject(i);
                    String c_value = objectResult.getString("c_value");
                    String c_id = objectResult.getString("c_id");

                    JSONObject objectChecklist2 = new JSONObject();
                    objectChecklist2.put("c_id", c_id);
                    objectChecklist2.put("c_value", c_value);
                    arrayResult.put(objectChecklist2);
                    objCheckResult.put("data", arrayResult);
                }
                obj.put("checklist", objCheckResult);
            } else {
                obj.put("checklist", "");
            }

            JSONObject addSign = new JSONObject();

            addSign.put("aggrement", signature);
            if (signBedaAlamat != null && !signBedaAlamat.equalsIgnoreCase("")) {
                JSONObject objBedaAlamat = new JSONObject(signBedaAlamat);
                String sign = objBedaAlamat.getString("sign");
                String reason = objBedaAlamat.getString("reason");
                String home_status = objBedaAlamat.getString("home_status");
                String sign_sales = objBedaAlamat.getString("sales_sign");
                JSONObject resultBedaAlamat = new JSONObject();
                resultBedaAlamat.put("sign", sign);
                resultBedaAlamat.put("reason", reason);
                resultBedaAlamat.put("home_status", home_status);
                resultBedaAlamat.put("sales_sign", sign_sales);
                addSign.put("beda_alamat", resultBedaAlamat);
            }
            if (signSewa != null && !signSewa.equalsIgnoreCase("")) {
                addSign.put("rumah_sewa", signSewa);
            }

            if (signMultiAccount != null && !signMultiAccount.equalsIgnoreCase("")) {
                JSONObject objMultiAcc = new JSONObject(signMultiAccount);
                String sign = objMultiAcc.getString("sign");
                String status = objMultiAcc.getString("status");
                String reason = objMultiAcc.getString("reason");
                String cust = objMultiAcc.getString("cust_number");
                String vc = objMultiAcc.getString("vc_number");
                JSONObject resultMultiAcc = new JSONObject();
                resultMultiAcc.put("sign", sign);
                resultMultiAcc.put("status", status);
                resultMultiAcc.put("reason", reason);
                resultMultiAcc.put("cust_number", cust);
                resultMultiAcc.put("vc_number", vc);
                addSign.put("multi_account", resultMultiAcc);
            }

            if (signBundlingIndosat != null && !signBundlingIndosat.equalsIgnoreCase("")) {
                JSONObject objBundling = new JSONObject(signBundlingIndosat);
                String sign = objBundling.getString("sign");
                String payment_method = objBundling.getString("payment_method");
                String router = objBundling.getString("router");
                String iccid = objBundling.getString("iccid");
                String imei = objBundling.getString("imei");
                JSONObject resultBundling = new JSONObject();
                resultBundling.put("sign", sign);
                resultBundling.put("payment_method", payment_method);
                resultBundling.put("router", router);
                resultBundling.put("iccid", iccid);
                resultBundling.put("imei", imei);
                addSign.put("sign_bun_isat", resultBundling);
            }

            if (addSign != null) {
                obj.put("additional_sign", addSign);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        String values = obj.toString().replaceAll("'", "''"); //Perubahan dari & ke \'
        Log.d("MSStatus", values);
        return values;
    }

    private class SendToServerTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Log.d("FTStatus", "long=" + lon + " lat=" + lat + " add=" + addressLine);
            if (isTaskRunning) {
                this.cancel(true);
                return;
            } else if (profile == null || profile.length() < 1) {
                Toast.makeText(getActivity(), "Silahkan masukkan data profile", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (emergency == null || emergency.length() < 1) {
                Toast.makeText(getActivity(), "Silahkan masukkan data emergency", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (paket == null || paket.length() < 1) {
                Toast.makeText(getActivity(), "Silahkan masukkan data paket", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (payment == null || payment.length() < 1) {
                Toast.makeText(getActivity(), "Silahkan masukkan data payment", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (signature == null || signature.length() < 1) {
                Toast.makeText(getActivity(), "Silahkan tanda tangan", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (lat == null || String.valueOf(lat).equalsIgnoreCase("0.0")) {
                Toast.makeText(getActivity(), "Latitude kosong", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (lon == null || String.valueOf(lon).equalsIgnoreCase("0.0")) {
                Toast.makeText(getActivity(), "Longitude kosong", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            }


            if (_progressDialog != null) {
                _progressDialog.dismiss();
            }
            _progressDialog = new ProgressDialog(getActivity());
            _progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
            _progressDialog.setIndeterminate(false);
            _progressDialog.setCancelable(false);
            _progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                isTaskRunning = true;
                value = generateJson().toString();
                TablePlanMSAdapter mTablePlanAdapter = new TablePlanMSAdapter(getActivity());
                token = sessionPref.getString(TableLogLogin.C_TOKEN, "");
                sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
                List<TablePlanMS> listPlan = mTablePlanAdapter.getDatabySfl(getActivity(),
                        TablePlanMS.KEY_PLAN_DATE, utils.getCurrentDate());
                if (listPlan != null && listPlan.size() > 0) {
                    assignId = listPlan.get(0).getAssignment_id();
                }
                imei = utils.getIMEI();
                url = ConnectionManager.CM_ENTRY;
                response = ConnectionManager.requestToServer(url, imei, token, value, assignId,
                        Config.version, "9", "3", getActivity());
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject != null) {
                    status = jsonObject.getString(Config.KEY_STATUS);
                    data = jsonObject.getString(Config.KEY_MESSAGE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return status;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                isTaskRunning = false;
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }

                if (data != null) {
                    Toast.makeText(getActivity(), data, Toast.LENGTH_SHORT).show();
                }

                if (s != null && s.equalsIgnoreCase("200")) {
                    getActivity().finish();
                } else {

                    SharedPreferences preferences = getActivity()
                            .getSharedPreferences(getString(R.string.fn_sales), Context.MODE_PRIVATE);
                    SharedPreferences.Editor shareEditor = preferences.edit();
                    shareEditor.putBoolean("fsales", true);
                    shareEditor.commit();

                    String date = utils.getCurrentDate();
                    TableMSDTDFailedAdapter failedAdapter = new TableMSDTDFailedAdapter(getActivity());
                    failedAdapter.insertData(new TableDataMSDTDFailed(), numberForm, value, "ms",
                            date, "0", assignId);
                    getActivity().finish();
                    Toast.makeText(getActivity(), "Pending proses", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
                e.printStackTrace();
            }
        }
    }
}