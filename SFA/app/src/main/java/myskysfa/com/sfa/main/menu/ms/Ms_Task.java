package myskysfa.com.sfa.main.menu.ms;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.MSTaskAdapter;
import myskysfa.com.sfa.database.TableAbsensi;
import myskysfa.com.sfa.database.TableFreeTrial;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.TablePlanMS;
import myskysfa.com.sfa.database.db_adapter.TableAbsenAdapter;
import myskysfa.com.sfa.database.db_adapter.TableFreeTrialAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePlanMSAdapter;
import myskysfa.com.sfa.main.menu.MainFragmentMS;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by Eno on 6/27/2016.
 */
public class Ms_Task extends Fragment {
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private MSTaskAdapter adaper;
    private ProgressDialog _progressDialog;
    private ImageButton fab;
    private Menu menu;
    private MenuItem alert;
    private String assignment_id, assignment_date, entity_Id, plan_type, planUrl,
            imei, sflCode, token, username, response, tsName, value, brand, createdDate, statusDesc,
            prosNbr, url, employeId;
    private SharedPreferences sessionPref;
    private Utils utils;
    private Handler handler;
    private int signal;
    private JSONArray jsonArray;
    private TablePlanMSAdapter planDbAdapter;
    private TableFreeTrialAdapter dbAdapter;
    private List<TableFreeTrial> list;
    private int offset = 0;
    private boolean status = false;
    public int NUM_ITEMS_PAGE = 10;
    private boolean isTaskRunning = false;
    private TextView ts_name;
    private PlanTask planTask;
    private TableAbsenAdapter absenAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ms_fragment_task, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        fab = (ImageButton) view.findViewById(R.id.fab);
        ts_name = (TextView) view.findViewById(R.id.ts_name);
        ts_name.setSelected(true);
        utils = new Utils(getActivity());
        handler = new Handler();
        sessionPref = getContext().getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        employeId = sessionPref.getString(TableLogLogin.C_EMPLOYEE_ID, "");
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new LoadDataListDTD().execute();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                absenAdapter = new TableAbsenAdapter(getActivity());
                if (absenAdapter.getDataCount(TableAbsensi.UID, employeId) != 0) {
                    String clock = absenAdapter
                            .getDataByCondition(TableAbsensi.UID, employeId)
                            .get(0).getAbsen_masuk();
                    if (utils.formatStringToDate(clock)
                            .before(utils.formatStringToDate(utils.getCurrentDateandTime()))) {
                        Toast.makeText(getActivity(), "Harap absen terlebih dahulu!",
                                Toast.LENGTH_SHORT).show();
                    }else{
                        Intent i = new Intent(getActivity(), MainFragmentMS.class);
                        i.putExtra(getString(R.string.category_app), "ms");
                        startActivity(i);

                    }
                } else {
                    Toast.makeText(getActivity(), "Harap absen terlebih dahulu!",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        new LoadDataListDTD().execute();
        return view;
    }

    private void ShowList() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutParams);
        adaper = new MSTaskAdapter(list);
        recyclerView.setAdapter(adaper);
    }

    protected class LoadDataListDTD extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            signal = utils.checkSignal(getActivity());
            if (!swipeRefreshLayout.isRefreshing()) {
                if (isTaskRunning) {
                    this.cancel(true);
                    return;
                }
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
                _progressDialog = new ProgressDialog(getActivity());
                _progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
                _progressDialog.setIndeterminate(false);
                _progressDialog.setCancelable(false);
                _progressDialog.show();
            } else {
                swipeRefreshLayout.setRefreshing(true);
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                list = new ArrayList<TableFreeTrial>();
                if (sessionPref == null) {
                    //Toast.makeText(getContext(),"Silahkah refresh!",Toast.LENGTH_LONG).show();
                } else {
                    sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
                    username = sessionPref.getString(TableLogLogin.C_USER_NAME, "");
                    token = sessionPref.getString(TableLogLogin.C_TOKEN, "");
                    offset = 0;
                    imei = utils.getIMEI();
                    list.clear();
                    url = ConnectionManager.CM_LIST_PROSPECT + "/" + sflCode + "/" + "M";
                    response = ConnectionManager.requestFreeTrial(url, sflCode, imei, token, username, offset, Config.version, String.valueOf(signal), getActivity());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject != null) {
                        status = jsonObject.getBoolean(Config.KEY_STATUS);
                        jsonArray = jsonObject.getJSONArray(Config.KEY_DATA);
                        if (status) {
                            dbAdapter = new TableFreeTrialAdapter(getActivity());
                            dbAdapter.deleteAll();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                prosNbr = obj.getString(Config.KEY_FT_ID);
                                sflCode = obj.getString(Config.KEY_SFLCODE);
                                brand = obj.getString("brand_code");
                                createdDate = obj.getString("created_date");
                                statusDesc = obj.getString("status");
                                username = obj.getString("user_name");
                                assignment_id = obj.getString("plan_id");
                                value = obj.toString();
                                //getHeaderList(createdDate);
                                dbAdapter.insertData(new TableFreeTrial(), prosNbr, "", brand,
                                        sflCode, createdDate, statusDesc, value, assignment_id, "");
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (status) {
                    getMS();
                    ShowList();
                    StartAsyncTaskInParallel(planTask);
                    //getStockTaskValue();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class PlanTask extends AsyncTask<String, String, Boolean> {
        boolean status;
        JSONObject jsonObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            signal = utils.checkSignal(getActivity());
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                status = false;
                imei = utils.getIMEI();
                sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
                token = sessionPref.getString(TableLogLogin.C_TOKEN, "");
                username = sessionPref.getString(TableLogLogin.C_USER_NAME, "");
                planUrl = ConnectionManager.CM_PLAN_ALL + sflCode + "/3";
                response = ConnectionManager.requestPlan(planUrl, imei, token, username, Config.version, String.valueOf(signal), getActivity());
                Log.d("FreeTrial", "response= " + response);
                jsonObject = new JSONObject(response.toString());
                if (jsonObject != null) {
                    status = jsonObject.getBoolean(Config.KEY_STATUS);
                    JSONObject obj = new JSONObject(jsonObject.getString(Config.KEY_DATA));
                    if (status) {
                        planDbAdapter = new TablePlanMSAdapter(getActivity());
                        planDbAdapter.deleteAll();
                        sflCode = obj.getString(Config.KEY_ASS_SFL);
                        assignment_id = obj.getString(Config.KEY_ASS_ID);
                        assignment_date = obj.getString(Config.KEY_ASS_DATE);
                        tsName = obj.getString(Config.KEY_ASS_TS);
                        entity_Id = obj.getString(Config.KEY_ASS_ENTITY);
                        plan_type = obj.getString(Config.KEY_ASS_TYPE);
                        value = obj.toString();
                        planDbAdapter.insertData(new TablePlanMS(), assignment_id, assignment_date, tsName, sflCode, value, "0", entity_Id);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return status;
        }

        @Override
        protected void onPostExecute(Boolean s) {
            super.onPostExecute(s);
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
            if (_progressDialog != null) {
                _progressDialog.dismiss();
            }
            try {
                if (s) {
                    getStockTaskValue();
                } else {
                    if (_progressDialog != null) {
                        _progressDialog.dismiss();
                    }
                    if (!jsonObject.getString("data").equalsIgnoreCase("No Data Found")) {
                        utils.showErrorDlg(handler, getActivity().getResources().getString(R.string.network_error),
                                getActivity());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void StartAsyncTaskInParallel(PlanTask task) {
        task = new PlanTask();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            task.execute();
    }

    private void getStockTaskValue() {
        TablePlanMSAdapter tablePlanAdapter = new TablePlanMSAdapter(getActivity());
        List<TablePlanMS> listPlan = new ArrayList<TablePlanMS>();
        String date = utils.getCurrentDate();
        listPlan = tablePlanAdapter.getDatabySfl(getActivity(), TablePlanMS.KEY_PLAN_DATE, date);
        if (listPlan != null && listPlan.size() > 0) {
            ts_name.setText(listPlan.get(0).getTs_name());
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }
        if (_progressDialog != null) {
            _progressDialog.dismiss();
        }
    }

    private void loadList(int number) {
        List<TableFreeTrial> sort = new ArrayList<TableFreeTrial>();
        int start = number * NUM_ITEMS_PAGE;
        for (int i = start; i < (start) + NUM_ITEMS_PAGE; i++) {
            if (i < list.size()) {
                sort.add(list.get(i));
            } else {
                break;
            }
        }
    }

    private void getMS() {
        dbAdapter = new TableFreeTrialAdapter(getActivity());
        list = dbAdapter.getAllData();
        loadList(0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.info_menu, menu);
        this.menu = menu;
        alert = this.menu.findItem(R.id.count);
        alert.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();
        new LoadDataListDTD().execute();
    }
}