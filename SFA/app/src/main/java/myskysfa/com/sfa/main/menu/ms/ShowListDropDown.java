package myskysfa.com.sfa.main.menu.ms;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableZipcode;
import myskysfa.com.sfa.database.db_adapter.TableZipcodeAdapter;

/**
 * Created by Eno on 6/15/2016.
 */
public class ShowListDropDown extends AppCompatActivity {
    private String title;

    private
    @Nullable
    String assign;

    private
    @Nullable
    String assignName;

    private static
    @Nullable
    String provID;
    private static
    @Nullable
    String cityID;
    private static
    @Nullable
    String kecID;
    private static
    @Nullable
    String kelID;


    private static
    @Nullable
    String provIDInstall;
    private static
    @Nullable
    String cityIDInstall;
    private static
    @Nullable
    String kecIDInstall;
    private static
    @Nullable
    String kelIDInstall;

    private static
    @Nullable
    String provIDBilling;
    private static
    @Nullable
    String cityIDBilling;
    private static
    @Nullable
    String kecIDBilling;
    private static
    @Nullable
    String kelIDBilling;

    private static
    @Nullable
    String identityList;
    private static
    @Nullable
    String gender;
    private static
    @Nullable
    String religion;

    private static
    @Nullable
    String _income;

    private static
    @Nullable
    String _homeType;

    private static
    @Nullable
    String _homeStatus;

    private static
    @Nullable
    String _job;

    private static
    @Nullable
    String relationship;

    private static
    @Nullable
    String paymentMethod;

    private static
    @Nullable
    String paymentPeriod;

    private static
    @Nullable
    String cardType;

    private static
    @Nullable
    String hwStatus1;

    private static
    @Nullable
    String proStatus;

    private TextView textView, txtZipcode;
    private List<TableZipcode> listTblZipCode;
    private String[] strings;
    private List<String> zipcodeArrays;
    private Activity context;
    private List<String> valueArray = new ArrayList<String>();
    private ArrayList<String> listID = new ArrayList<String>();
    private List<String> assignValue;
    private ArrayAdapter<String> arrayAdapter;
    private View view;
    private JSONObject obj;


    public ShowListDropDown(Activity context, View v, String title,
                            TextView textView, String[] strings) {
        this.context = context;
        this.title = title;
        this.textView = textView;
        this.strings = strings;
        this.view = v;
    }

    public ShowListDropDown(Activity context, View v, String title, TextView txt,
                            @Nullable ArrayList<String> strings1,
                            @Nullable ArrayList<String> listID,
                            @Nullable TextView txtZipcode) {

        this.context = context;
        this.title = title;
        this.textView = txt;
        this.zipcodeArrays = strings1;
        this.view = v;
        this.listID = listID;
        this.txtZipcode = txtZipcode;

    }

    public ShowListDropDown() {
    }

    public void ShowZipcode() {
        switch (view.getId()) {
            case R.id.province_install:
            case R.id.province:
            case R.id.province_billing:
                arrayAdapter = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item
                        , zipcodeArrays);
                new AlertDialog.Builder(context)
                        .setTitle(title)
                        .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                textView.setText(String.valueOf(zipcodeArrays.get(which)));
                                provID = listID.get(which);
                                if (view.getId() == R.id.province) {
                                    setProvID(provID);
                                } else if (view.getId() == R.id.province_install) {
                                    setProvIDInstall(provID);
                                } else {
                                    setProvIDBilling(provID);
                                }
                            }
                        }).create().show();
                break;
            case R.id.city_install:
            case R.id.city:
            case R.id.city_billing:
                arrayAdapter = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item
                        , zipcodeArrays);
                new AlertDialog.Builder(context)
                        .setTitle(title)
                        .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                textView.setText(String.valueOf(zipcodeArrays.get(which)));
                                cityID = listID.get(which);
                                if (view.getId() == R.id.city) {
                                    setCityID(cityID);
                                } else if (view.getId() == R.id.city_install) {
                                    setCityIDInstall(cityID);
                                } else {
                                    setCityIDBilling(cityID);
                                }


                            }
                        }).create().show();
                break;

            case R.id.kecamatan_install:
            case R.id.kecamatan:
            case R.id.kecamatan_billing:
                arrayAdapter = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item
                        , zipcodeArrays);
                new AlertDialog.Builder(context)
                        .setTitle(title)
                        .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                textView.setText(String.valueOf(zipcodeArrays.get(which)));
                                kecID = listID.get(which);
                                if (view.getId() == R.id.kecamatan) {
                                    setKecID(kecID);
                                } else if (view.getId() == R.id.kecamatan_install) {
                                    setKecIDInstall(kecID);
                                } else {
                                    setKecIDBilling(kecID);
                                }
                            }
                        }).create().show();
                break;

            case R.id.kelurahan_install:
            case R.id.kelurahan:
            case R.id.kelurahan_billing:
                arrayAdapter = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item
                        , zipcodeArrays);
                new AlertDialog.Builder(context)
                        .setTitle(title)
                        .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                textView.setText(String.valueOf(zipcodeArrays.get(which)));
                                kelID = listID.get(which);
                                if (view.getId() == R.id.kelurahan) {
                                    setKelID(kelID);
                                } else if (view.getId() == R.id.kelurahan_install) {
                                    setKelIDInstall(kelID);
                                } else {
                                    setKelIDBilling(kelID);
                                }
                                ShowZip(txtZipcode);
                            }
                        }).create().show();
                break;

        }
    }

    protected void ShowZip(TextView txt) {
        TableZipcodeAdapter zipcodeAdapter;
        zipcodeAdapter = new TableZipcodeAdapter(context);
        listTblZipCode = zipcodeAdapter.listZipcode(kelID);
        txt.setText(String.valueOf(listTblZipCode.get(0).getZIPCODE()));
    }


    public void Show() {
        assignValue = new ArrayList<>(Arrays.asList(strings));
        for (int i = 0; i < strings.length; i++) {
            try {
                String name = null;
                JSONArray ar = new JSONArray(Arrays.asList(strings).toString());
                obj = ar.getJSONObject(i);
                name = obj.getString("name");
                valueArray.add(name);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        arrayAdapter = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item
                , valueArray);
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String id = null;
                        String name = null;
                        try {
                            JSONObject obj = new JSONObject(assignValue.get(which));
                            if (obj.has("id")) {
                                id = obj.getString("id");
                            }

                            if (obj.has("name")) {
                                name = obj.getString("name");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        textView.setText(name);
                        assign = id;
                        assignName = name;
                        if (view.getId() == R.id.identity_list) {
                            setIdentityList(assign);
                        } else if (view.getId() == R.id.gender) {
                            setGender(assign);
                        } else if (view.getId() == R.id.religion) {
                            setReligion(assign);
                        } else if (view.getId() == R.id.job) {
                            set_job(assign);
                        } else if (view.getId() == R.id.home_kind) {
                            set_homeType(assign);
                        } else if (view.getId() == R.id.income) {
                            set_income(assign);
                        } else if (view.getId() == R.id.home_status) {
                            set_homeStatus(assign);
                        } else if (view.getId() == R.id.sh) {
                            if (textView.getText().toString().equalsIgnoreCase("Pinjam Pakai/ P5 (T)")) {

                                TextView form_n = (TextView) context.findViewById(R.id.form_no);
                                SharedPreferences sm = context.getSharedPreferences(
                                        "dtdPPId", Context.MODE_PRIVATE);
                                form_n.setText(sm.getString("dtdpp", null));

                            } else {
                                TextView form_n = (TextView) context.findViewById(R.id.form_no);
                                SharedPreferences sm = context.getSharedPreferences(
                                        "dtdId", Context.MODE_PRIVATE);
                                form_n.setText(sm.getString("dtd", null));
                            }
                        } else if (view.getId() == R.id.sh1) {
                            setHwStatus1(assign);
                        } else if (view.getId() == R.id.list_payment_method) {
                            setPaymentMethod(assign);
                        } else if (view.getId() == R.id.list_payment_period) {
                            setPaymentPeriod(assign);
                        } else if (view.getId() == R.id.list_payment_cc) {
                            setCardType(assign);
                        } else if (view.getId() == R.id.prosType) {
                            setProStatus(assignName);
                        } else {
                            setRelationship(assign);
                        }
                        dialog.dismiss();
                    }
                }).create().show();
    }

    @Nullable
    public static String getIdentityList() {
        return identityList;
    }

    public static void setIdentityList(@Nullable String identityList) {
        ShowListDropDown.identityList = identityList;
    }

    @Nullable
    public static String getGender() {
        return gender;
    }

    public static void setGender(@Nullable String gender) {
        ShowListDropDown.gender = gender;
    }

    @Nullable
    public static String getReligion() {
        return religion;
    }

    public static void setReligion(@Nullable String religion) {
        ShowListDropDown.religion = religion;
    }

    @Nullable
    public static String getProvID() {
        return provID;
    }

    public void setProvID(@Nullable String provID) {
        this.provID = provID;
    }

    @Nullable
    public String getAssign() {
        return provID;
    }

    public void setAssign(@Nullable String provID) {
        this.provID = provID;
    }

    @Nullable
    public static String getCityID() {
        return cityID;
    }

    public void setCityID(@Nullable String cityID) {
        this.cityID = cityID;
    }

    @Nullable
    public static String getKecID() {
        return kecID;
    }

    public void setKecID(@Nullable String kecID) {
        this.kecID = kecID;
    }

    @Nullable
    public static String getKelID() {
        return kelID;
    }

    public void setKelID(@Nullable String kelID) {
        this.kelID = kelID;
    }

    @Nullable
    public static String getProvIDInstall() {
        return provIDInstall;
    }

    public static void setProvIDInstall(@Nullable String provIDInstall) {
        ShowListDropDown.provIDInstall = provIDInstall;
    }

    @Nullable
    public static String getCityIDInstall() {
        return cityIDInstall;
    }

    public static void setCityIDInstall(@Nullable String cityIDInstall) {
        ShowListDropDown.cityIDInstall = cityIDInstall;
    }

    @Nullable
    public static String getKecIDInstall() {
        return kecIDInstall;
    }

    public static void setKecIDInstall(@Nullable String kecIDInstall) {
        ShowListDropDown.kecIDInstall = kecIDInstall;
    }

    @Nullable
    public static String getKelIDInstall() {
        return kelIDInstall;
    }

    public static void setKelIDInstall(@Nullable String kelIDInstall) {
        ShowListDropDown.kelIDInstall = kelIDInstall;
    }

    @Nullable
    public static String getProvIDBilling() {
        return provIDBilling;
    }

    public static void setProvIDBilling(@Nullable String provIDBilling) {
        ShowListDropDown.provIDBilling = provIDBilling;
    }

    @Nullable
    public static String getCityIDBilling() {
        return cityIDBilling;
    }

    public static void setCityIDBilling(@Nullable String cityIDBilling) {
        ShowListDropDown.cityIDBilling = cityIDBilling;
    }

    @Nullable
    public static String getKecIDBilling() {
        return kecIDBilling;
    }

    public static void setKecIDBilling(@Nullable String kecIDBilling) {
        ShowListDropDown.kecIDBilling = kecIDBilling;
    }

    @Nullable
    public static String getKelIDBilling() {
        return kelIDBilling;
    }

    public static void setKelIDBilling(@Nullable String kelIDBilling) {
        ShowListDropDown.kelIDBilling = kelIDBilling;
    }

    @Nullable
    public static String get_income() {
        return _income;
    }

    public static void set_income(@Nullable String _income) {
        ShowListDropDown._income = _income;
    }

    @Nullable
    public static String get_homeType() {
        return _homeType;
    }

    public static void set_homeType(@Nullable String _homeType) {
        ShowListDropDown._homeType = _homeType;
    }

    @Nullable
    public static String get_homeStatus() {
        return _homeStatus;
    }

    public static void set_homeStatus(@Nullable String _homeStatus) {
        ShowListDropDown._homeStatus = _homeStatus;
    }

    @Nullable
    public static String get_job() {
        return _job;
    }

    public static void set_job(@Nullable String _job) {
        ShowListDropDown._job = _job;
    }

    @Nullable
    public static String getRelationship() {
        return relationship;
    }

    public static void setRelationship(@Nullable String relationship) {
        ShowListDropDown.relationship = relationship;
    }

    @Nullable
    public static String getPaymentMethod() {
        return paymentMethod;
    }

    public static void setPaymentMethod(@Nullable String paymentMethod) {
        ShowListDropDown.paymentMethod = paymentMethod;
    }

    @Nullable
    public static String getPaymentPeriod() {
        return paymentPeriod;
    }

    public static void setPaymentPeriod(@Nullable String paymentPeriod) {
        ShowListDropDown.paymentPeriod = paymentPeriod;
    }

    @Nullable
    public static String getCardType() {
        return cardType;
    }

    public static void setCardType(@Nullable String cardType) {
        ShowListDropDown.cardType = cardType;
    }

    @Nullable
    public static String getHwStatus1() {
        return hwStatus1;
    }

    public static void setHwStatus1(@Nullable String hwStatus1) {
        ShowListDropDown.hwStatus1 = hwStatus1;
    }

    @Nullable
    public static String getProStatus() {
        return proStatus;
    }

    public static void setProStatus(@Nullable String proStatus) {
        ShowListDropDown.proStatus = proStatus;
    }


}
