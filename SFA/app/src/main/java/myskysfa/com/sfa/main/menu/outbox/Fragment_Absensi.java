package myskysfa.com.sfa.main.menu.outbox;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.AbsenFailedAutoSenderAdapter;
import myskysfa.com.sfa.database.TableFailedAbsen;
import myskysfa.com.sfa.database.db_adapter.TableFailedAbsenAdapter;

/**
 * Created by Eno on 11/2/2016.
 */

public class Fragment_Absensi extends Fragment {
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private List<TableFailedAbsen> listApp;
    private TableFailedAbsenAdapter mAppAdapter;
    private AbsenFailedAutoSenderAdapter adaper;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_absensi, container, false);
        mAppAdapter = new TableFailedAbsenAdapter(getActivity());
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_absensi);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        Refresh();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new LoadDataDraf().execute();
            }
        });
        return view;
    }

    private void Refresh() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                new LoadDataDraf().execute();
            }
        });
    }


    protected class LoadDataDraf extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeRefreshLayout.setRefreshing(true);
        }

        @Override
        protected String doInBackground(String... params) {
            listApp = mAppAdapter.getDatabyCondition(TableFailedAbsen.STATUS, "0");
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ShowList();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void ShowList() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutParams);
        adaper = new AbsenFailedAutoSenderAdapter(getActivity(), listApp);
        recyclerView.setAdapter(adaper);
    }
}
