package myskysfa.com.sfa.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableDataMSDTDFailed;
import myskysfa.com.sfa.database.TableFailedAbsen;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableFailedAbsenAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMSDTDFailedAdapter;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by Eno on 11/1/2016.
 */

public class AutoSender extends Service {
    private Utils utils;
    private String imei, token;
    private String entity_id, employee_id, attendance_type, latitude, longitude, attendance_time,
            id, jarak;
    private SharedPreferences sessionPref;
    private TableMSDTDFailedAdapter msdtdFailedAdapter;
    private TableFailedAbsenAdapter absenAdapter;
    private List<TableFailedAbsen> listAbsensi;
    private List<TableDataMSDTDFailed> listSales;
    private Boolean isAbsen = false, isMsdtd = false;

    @Override
    public void onCreate() {
        utils = new Utils(AutoSender.this);
        imei = utils.getIMEI();
        sessionPref = getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        token = sessionPref.getString(TableLogLogin.C_TOKEN, "");
        //msdtdFailedAdapter = new TableMSDTDFailedAdapter(AutoSender.this);
        //absenAdapter = new TableFailedAbsenAdapter(AutoSender.this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final Thread startThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(3000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (token != null) {
                            Log.d("isAbsen", String.valueOf(CheckAbsen()));
                            if (CheckAbsen()) {
                                AttempSendFailedAbsen();
                            } else {
                                Log.d("msdtdfail", "tidak ada data");
                            }

                            if (CheckSalesData()) {
                                AttempSendSalesData();
                            } else {
                                Log.d("absenfail", "tidak ada data");
                            }
                        }
                    }
                }
            }
        });
        startThread.start();
        return START_STICKY;
    }

    private boolean CheckAbsen() {
        try {
            SharedPreferences sm = getSharedPreferences(getString(R.string.fn_absen),
                    Context.MODE_PRIVATE);
            isAbsen = sm.getBoolean("fabsen", false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isAbsen;
    }

    private boolean CheckSalesData() {
        try {
            SharedPreferences sm = getSharedPreferences(getString(R.string.fn_sales),
                    Context.MODE_PRIVATE);
            isMsdtd = sm.getBoolean("fsales", false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isMsdtd;
    }

    private void AttempSendFailedAbsen() {
        try {
            String url = "", response = "", status = "";
            absenAdapter = new TableFailedAbsenAdapter(AutoSender.this);
            listAbsensi = new ArrayList<>();
            listAbsensi = absenAdapter.getAllData();
            for (int i = 0; i < listAbsensi.size(); i++) {
                entity_id = absenAdapter.getAllData().get(i).getEntity();
                employee_id = absenAdapter.getAllData().get(i).getEmployeeid();
                attendance_type = absenAdapter.getAllData().get(i).getAttendance_type();
                latitude = absenAdapter.getAllData().get(i).getLatitude();
                longitude = absenAdapter.getAllData().get(i).getLongitude();
                attendance_time = absenAdapter.getAllData().get(i).getAttendance_time();
                id = absenAdapter.getAllData().get(i).getId();
                jarak = absenAdapter.getAllData().get(i).getJarak();
                url = ConnectionManager.CM_URL_ABSEN;
                response = ConnectionManager.requestPushAbsen(url, entity_id, employee_id, attendance_type,
                        latitude, longitude, attendance_time, jarak, AutoSender.this);
                JSONObject object = new JSONObject(response);
                if (object != null) {
                    status = object.getString("status");
                    if (status != null && status.equalsIgnoreCase("200")) {
                        absenAdapter = new TableFailedAbsenAdapter(AutoSender.this);
                        absenAdapter.deleteBy(AutoSender.this, TableFailedAbsen.ATTENDANCE_TIME,
                                attendance_time);
                    }
                }
            }
            SharedPreferences preferences = getSharedPreferences(getString(R.string.fn_absen), Context.MODE_PRIVATE);
            SharedPreferences.Editor shareEditor = preferences.edit();
            shareEditor.putBoolean("fabsen", false);
            shareEditor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AttempSendSalesData() {
        try {
            String url = "", response = "", status, formNbr = "";
            url = ConnectionManager.CM_ENTRY;
            msdtdFailedAdapter = new TableMSDTDFailedAdapter(AutoSender.this);
            listSales = new ArrayList<>();
            listSales = msdtdFailedAdapter.getAllData();
            for (int i = 0; i < listSales.size(); i++) {
                formNbr = msdtdFailedAdapter.getAllData().get(i).getForm_no();
                if (msdtdFailedAdapter.getAllData().get(0).getApp_name().equalsIgnoreCase("dtd")) {
                    response = ConnectionManager.requestToServer(url, imei, token,
                            msdtdFailedAdapter.getAllData().get(0).getData(),
                            msdtdFailedAdapter.getAllData().get(0).getPlanId(),
                            Config.version, "9", "2", AutoSender.this);
                } else if (msdtdFailedAdapter.getAllData().get(0).getApp_name().equalsIgnoreCase("ms")) {
                    response = ConnectionManager.requestToServer(url, imei, token,
                            msdtdFailedAdapter.getAllData().get(0).getData(),
                            msdtdFailedAdapter.getAllData().get(0).getPlanId(),
                            Config.version, "9", "3", AutoSender.this);
                } else {
                    response = ConnectionManager.requestToServer(url, imei, token,
                            msdtdFailedAdapter.getAllData().get(0).getData(),
                            msdtdFailedAdapter.getAllData().get(0).getPlanId(),
                            Config.version, "9", "1", AutoSender.this);
                }
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject != null) {
                    status = jsonObject.getString(Config.KEY_STATUS);
                    if (status != null && status.equalsIgnoreCase("200")) {
                        Log.d("msdtdfail", "berhasil kirim");
                        msdtdFailedAdapter.deleteBy(AutoSender.this, TableDataMSDTDFailed.FORM_NO, formNbr);
                    }
                }
            }

            SharedPreferences preferences =
                    getSharedPreferences(getString(R.string.fn_sales), Context.MODE_PRIVATE);
            SharedPreferences.Editor shareEditor = preferences.edit();
            shareEditor.putBoolean("fsales", false);
            shareEditor.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceTask = new Intent(getApplicationContext(), this.getClass());
        restartServiceTask.setPackage(getPackageName());
        PendingIntent restartPendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceTask, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager myAlarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        myAlarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartPendingIntent);

        super.onTaskRemoved(rootIntent);
    }
}
