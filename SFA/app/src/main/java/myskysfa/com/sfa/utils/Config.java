package myskysfa.com.sfa.utils;

import android.os.Build;

import myskysfa.com.sfa.BuildConfig;

/**
 * Created by Three Hero on 19/08/2015.
 */
public class Config {
    public static final String URL_HTTP_STRING = "http://";
    public static final String URL_HTTPS_STRING = "https://";

    /*
   |-----------------------------------------------------------------------------------------------
   | URL server for this application to transfer data.
   |-----------------------------------------------------------------------------------------------
    */

    //public static final String url_public = "192.168.177.149";
    public static final String url_public = "192.168.177.135";
    public static final String url_private = "192.168.177.221";

    /*
   |-----------------------------------------------------------------------------------------------
   | URL server local for this application to transfer data.
   |-----------------------------------------------------------------------------------------------
    */

    public static final String url_server_proxy = "";

    /*
    |-----------------------------------------------------------------------------------------------
    | Port server for this application to transfer data.
    |-----------------------------------------------------------------------------------------------
    */
    public static final String port_Server = "";
    public static final String port_Server_proxy = "3128";
    public static final String version = BuildConfig.VERSION_NAME; // ari

    /*
   |-----------------------------------------------------------------------------------------------
   | Key for access token
   |-----------------------------------------------------------------------------------------------
   */
    public static final String KEY_ACCESS_TOKEN = "access_token";
    public static final String KEY_LOGIN_PROFILE = "LOGIN_PROFILE";

    /*
   |-----------------------------------------------------------------------------------------------
   | Key for json
   |-----------------------------------------------------------------------------------------------
   */
    public static final String KEY_JSON_PROFILE = "json";

    /*
   |-----------------------------------------------------------------------------------------------
   | Key for imei
   |-----------------------------------------------------------------------------------------------
   */
    public static final String KEY_IMEI = "imei";


    /*
   |-----------------------------------------------------------------------------------------------
   | Database name
   |-----------------------------------------------------------------------------------------------
   */
    public static final String DATABASE_NAME = "indovision_newsfa";
    public static final String DB_NAME_SERVICE = "service_sfa";

    /*
    |-----------------------------------------------------------------------------------------------
    | Database version
    |-----------------------------------------------------------------------------------------------
    */
    public static final int DATABASE_VERSION = 1;


    /*
  |-----------------------------------------------------------------------------------------------
  | Database delay time
  |-----------------------------------------------------------------------------------------------
  */
    public static final String path = "MSDTD";


    /*
  |-----------------------------------------------------------------------------------------------
  | Database delay time
  |-----------------------------------------------------------------------------------------------
  */
    public static final int DB_CLOSE_DELAY_TIME = 5000;

    /*
   |-----------------------------------------------------------------------------------------------
   | Key for json respon
   |-----------------------------------------------------------------------------------------------
   */
    public static final String KEY_STATUS = "status";
    public static final String KEY_DEVICE_ID = "device_id";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_SFLCODE = "sfl_code";
    public static final String KEY_SFLNAME = "sflName";
    public static final String KEY_STATUS_DESC = "statusDesc";
    public static final String KEY_VERSION_ID = "versionId";
    public static final String KEY_VERSION_VALUE = "versionValue";
    public static final String KEY_VERSION_TYPE = "versionType";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_STATUS_REF = "hubungan";
    public static final String KEY_APP_VERSION = "versionApp";
    public static final String KEY_APP_FORM = "versionForm";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_COUNT_DATA = "countData";
    public static final String KEY_DATA = "data";
    public static final String KEY_SLOT_TASK = "slotTask";
    public static final String KEY_INFO = "info";
    public static final String KEY_ZIP_CODE = "zip_code";
    public static final String KEY_AREA = "area";
    public static final String KEY_BRAND = "brand";
    public static final String KEY_FT_ID = "prospect_nbr";
    public static final String KEY_NAMA = "nama";
    public static final String KEY_ALAMAT = "alamat";
    public static final String KEY_HP = "hp";
    public static final String KEY_TLP = "telp";
    public static final String KEY_EMERGENCY = "emergency";
    public static final String KEY_REF = "referensi";
    public static final String KEY_VC = "vc";
    public static final String KEY_DSD = "dsd";
    public static final String KEY_NO_LNB = "lnb";
    public static final String KEY_NO_DISH = "dish";

    // For master package
    public static final String KEY_MASTER_PACKAGE_ID = "id";
    public static final String KEY_MASTER_PACKAGE_NAME = "name";
    public static final String KEY_MASTER_PACKAGE_ALACARTE = "alacarte";
    public static final String KEY_MASTER_PACKAGE_PRICE = "price";
    public static final String KEY_MASTER_PACKAGE_BRAND = "brand";
    public static final String KEY_MASTER_PACKAGE_KEY_ID = "key_id";
    public static final String KEY_MASTER_PACKAGE_BASIC_ID = "basic_id";

    // For master promo
    public static final String KEY_MASTER_PROMO_ID = "id";
    public static final String KEY_MASTER_PROMO_CODE = "code";
    public static final String KEY_MASTER_PROMO_DESC = "description";
    public static final String KEY_MASTER_PROMO_START = "start_date";
    public static final String KEY_MASTER_PROMO_END = "end_date";


    // For master material
    public static final String KEY_MASTER_MATERIAL_ID = "item_id";
    public static final String KEY_MASTER_MATERIAL_CODE = "item_code";
    public static final String KEY_MASTER_MATERIAL_DESC = "item_descr";
    public static final String KEY_MASTER_MATERIAL_RATE = "rate";
    public static final String KEY_MASTER_MATERIAL_CLASS_CODE = "item_class_code";
    public static final String KEY_MASTER_MATERIAL_IS_PACKAGE = "serialized";

    // For master modern store
    public static final String KEY_MASTER_MS_ID = "ms_id";
    public static final String KEY_MASTER_MS_CODE = "ms_code";
    public static final String KEY_MASTER_MS_ENTITY = "entity_id";
    public static final String KEY_MASTER_MS_NAME = "ms_store";
    public static final String KEY_MASTER_MS_ADDRESS = "ms_address";
    public static final String KEY_MASTER_MS_ZIPCODE = "zipcode";
    public static final String KEY_MASTER_MS_LAT = "latitude";
    public static final String KEY_MASTER_MS_LONG = "longitude";

    // For master ft Serialize
    public static final String KEY_M_SN = "serial_number";
    public static final String KEY_M_TYPE = "hw_type";
    public static final String KEY_M_ID = "hw_id";
    public static final String KEY_M_NAME = "hw_name";
    public static final String KEY_M_STATUS = "hw_status";
    public static final String KEY_M_OUT_DATE = "out_date";
    public static final String KEY_M_ISSERIAL = "serialized";
    public static final String KEY_M_PRICE = "price";
    public static final String KEY_M_PACKAGE_ID = "PACKAGE_ID";

    // For Plan DTD
    public static final String KEY_PLAN_ID = "plan_id";
    public static final String KEY_PLAN_DATE = "plan_date";
    public static final String KEY_PLAN_SFL = "sfl_code";
    public static final String KEY_PLAN_AREA = "area";
    public static final String KEY_PLAN_ZIPCODE = "zipcode";
    public static final String KEY_PLAN_TYPE = "plan_type";
    public static final String KEY_PLAN_TSCODE = "ts_code";
    public static final String KEY_PLAN_TSNAME = "ts_name";
    public static final String KEY_PLAN_TARGET = "target";

    // For Plan MS
    public static final String KEY_ASS_ID = "assignment_id";
    public static final String KEY_ASS_DATE = "assignment_date";
    public static final String KEY_ASS_SFL = "sfl_code";
    public static final String KEY_ASS_TS = "ts_name";
    public static final String KEY_ASS_ENTITY = "entity_id";
    public static final String KEY_ASS_TYPE = "plan_type";

    // Set Camera Resolution
    public static final int width = 2048;
    public static final int hight = 1152;

    /*
    |-----------------------------------------------------------------------------------------------
    | Method for append URL and URI as API
    |-----------------------------------------------------------------------------------------------
    */
    /*public static String makeUrlString(String uri) {
        StringBuilder url = new StringBuilder(URL_HTTP_STRING);
        url.append(url_Server);
        url.append("/");
        url.append(uri);
        return url.toString();
    }

    public static String makeUrlLocalString(String uri) {
        StringBuilder url = new StringBuilder(URL_HTTP_STRING);
        url.append(url_Server_local);
        url.append("/");
        url.append(uri);
        return url.toString();
    }*/

    public static String makeUrlPublic(String uri) {
        StringBuilder url = new StringBuilder(URL_HTTP_STRING);
        url.append(url_public);
        url.append("/");
        url.append(uri);
        return url.toString();
    }

    public static String makeUrlPrivate(String uri) {
        StringBuilder url = new StringBuilder(URL_HTTP_STRING);
        url.append(url_private);
        url.append("/");
        url.append(uri);
        return url.toString();
    }

}

