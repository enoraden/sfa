package myskysfa.com.sfa.utils;

import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Three Hero on 08/09/2015.
 */
public class ConnectionManager {

    //Live

    public static final String CM_URL_LOGIN = Config.makeUrlPrivate("sfapi-dev/user/login");
    public static final String CM_URL_LOGOUT = Config.makeUrlPublic("sfapi/user/logout");
    //public static final String CM_URL_CHECK             = Config.makeUrlLocalString("user/check");
    //public static final String CM_URL_PROFILE           = Config.makeUrlLocalString("user/profile");
    //public static final String CM_URL_MASTER_PACKAGE = Config.makeUrlLocalString("sfapi/master/package/list");
    //public static final String CM_URL_MASTER_PROMOTION = Config.makeUrlLocalString("sfapi/master/promotion/list");
    public static final String CM_URL_MASTER_MATERIAL = Config.makeUrlPublic("sfapi/master/material/list");
    public static final String CM_URL_MASTER_MS = Config.makeUrlPublic("sfapi/master/modernstore/list");
    //public static final String CM_URL_SAVE_PROSPECT     = Config.makeUrlLocalString("sfapi/transaksi/prospect/new");
    //public static final String CM_URL_FTDTD_LIST        = Config.makeUrlLocalString("sfapi-dev/ftdtd/list");
    public static final String CM_URL_MSDTD_LIST = Config.makeUrlPublic("sfapi-dev/msdtd/list");
    public static final String CM_URL_FTDTD_ACTIVATION = Config.makeUrlPublic("sfapi-dev/ftdtd/activate/");
    public static final String CM_URL_FTDTD_CANCEL = Config.makeUrlPublic("sfapi-dev/ftdtd/cancel/");
    //public static final String CM_URL_FTDTD_PLAN        = Config.makeUrlLocalString("sfapi-dev/ftdtd/plan/");
    public static final String CM_URL_FTDTD_PLAN_ALL = Config.makeUrlPublic("sfapi-dev/ftdtd/plan/all/");
    public static final String CM_URL_DTD_CHUNK = Config.makeUrlPublic("api/ftdtd/imageStore");
    public static final String CM_URL_FT_STS = Config.makeUrlPublic("sfapi-dev/ftdtd/entry");
    //public static final String CM_URL_FT_STSTEST        = Config.makeUrlString("sfapi-dev/ftdtd/entrytest");
    //public static final String CM_URL_FT_SOH = Config.makeUrlPublic("sfapi-dev/ftdtd/soh/");
    //public static final String CM_URL_FT_SURVAY         = Config.makeUrlLocalString("sfapi-dev/survey/entry");
    //public static final String CM_URL_FT_SURVAY_LIST    = Config.makeUrlLocalString("sfapi-dev/survey/getList/");
    public static final String CM_URL_UPLOAD_IMAGE = Config.makeUrlPublic("sfapi-dev/upload/imageft/");
    //public static final String CM_URL_ABSEN             = Config.makeUrlPublic("sfa/absen");

    /**
     * Developement
     */


    /*public static final String CM_URL_LOGOUT            = Config.makeUrlLocalString("yummy_dev/user/logout");
    public static final String CM_URL_MASTER_PACKAGE    = Config.makeUrlLocalString("yummy_dev/master/package/list");
    public static final String CM_URL_MASTER_PROMOTION  = Config.makeUrlLocalString("yummy_dev/master/promotion/list");
    public static final String CM_URL_MASTER_MATERIAL   = Config.makeUrlLocalString("yummy_dev/master/material/list");
    public static final String CM_URL_MASTER_MS         = Config.makeUrlLocalString("yummy_dev/master/modernstore/list");
    public static final String CM_URL_SAVE_PROSPECT     = Config.makeUrlLocalString("yummy_dev/transaksi/prospect/new");
    public static final String CM_URL_FTDTD_LIST        = Config.makeUrlLocalString("yummy_dev/ftdtd/list");
    public static final String CM_URL_MSDTD_LIST        = Config.makeUrlLocalString("yummy_dev/msdtd/list");
    public static final String CM_URL_FTDTD_ACTIVATION  = Config.makeUrlLocalString("yummy_dev/ftdtd/activate/");
    public static final String CM_URL_FTDTD_CANCEL      = Config.makeUrlLocalString("yummy_dev/ftdtd/cancel/");
    public static final String CM_URL_FTDTD_PLAN        = Config.makeUrlLocalString("yummy_dev/ftdtd/plan/");
    public static final String CM_URL_FTDTD_PLAN_ALL    = Config.makeUrlLocalString("yummy_dev/ftdtd/plan/all/");
    public static final String CM_URL_DTD_CHUNK         = Config.makeUrlLocalString("api/ftdtd/imageStore");
    public static final String CM_URL_FT_STS            = Config.makeUrlString("yummy_dev/ftdtd/entry");
    public static final String CM_URL_FT_STSTEST        = Config.makeUrlString("yummy_dev/ftdtd/entrytest");
    public static final String CM_URL_FT_SOH            = Config.makeUrlLocalString("yummy_dev/ftdtd/soh/");
    public static final String CM_URL_FT_SURVAY         = Config.makeUrlLocalString("yummy_dev/survey/entry");
    public static final String CM_URL_FT_SURVAY_LIST    = Config.makeUrlLocalString("yummy_dev/survey/getList/");
    public static final String CM_URL_UPLOAD_IMAGE      = Config.makeUrlLocalString("yummy_dev/upload/imageft/");*/

    //public static final String CM_URL_LOGIN = Config.makeUrlPublic("yummy-dev/user/login");
    public static final String CM_URL_ABSEN = Config.makeUrlPublic("yummy-dev/absensi/add");
    public static final String CM_URL_LIST_CHECKLIST = Config.makeUrlPublic("yummy-dev/checklist/getallData");
    public static final String CM_PLAN_ALL = Config.makeUrlPublic("yummy-dev/msdtd/plan/all/");
    public static final String CM_ENTRY = Config.makeUrlPublic("yummy-dev/msdtd/entry");
    public static final String CM_LIST_PROSPECT = Config.makeUrlPublic("yummy-dev/msdtd/list");
    public static final String CM_MASTER_MATERIAL = Config.makeUrlPublic("yummy-dev/master/material/hw");
    public static final String CM_MASTER_MS = Config.makeUrlPublic("yummy-dev/master/store");
    public static final String CM_URL_UPLOAD = Config.makeUrlPublic("yummy-dev/upload/imageft/");
    public static final String CM_URL_ACTIVATION = Config.makeUrlPublic("yummy-dev/msdtd/activate/");
    public static final String CM_URL_MASTER_PACKAGE = Config.makeUrlPublic("yummy-dev/master/package/list");
    public static final String CM_URL_MASTER_PROMOTION = Config.makeUrlPublic("yummy-dev/master/promotion/list");
    public static final String CM_URL_FT_SOH = Config.makeUrlPublic("yummy-dev/msdtd/soh/");

    private static String executeHttpClientPost(String requestURL, HashMap<String, String> params, Context context) throws IOException {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(50000);
            conn.setConnectTimeout(50000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(params));

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private static String executeHttpClientPost(String requestURL, String params, Context context) throws IOException {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(params);

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public static String connectHttpPost(String url, HashMap<String, String> params, Context context) throws IOException {
        return executeHttpClientPost(url, params, context);
    }


    public static String connectHttpPost(String url, String params, Context context) throws IOException {
        return executeHttpClientPost(url, params, context);
    }

    public static String requestLogin(String url, String username, String password, String imei, String version,
                                      Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("username", username);
        param.put("password", password);
        param.put("imei", imei);
        param.put("version", version);
        return connectHttpPost(url, param, context);
    }

    public static String requestProfile(String url, String username, String imei,
                                        String token, String version, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("username", username);
        param.put("imei", imei);
        param.put("token", token);
        param.put("version", version);
        return connectHttpPost(url, param, context);
    }

    public static String requestLogout(String url, String imei, String token, String username, String version, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", imei);
        //param.put("token", token);
        param.put("username", username);
        param.put("version", version);
        return connectHttpPost(url, param, context);
    }

    public static String requestCheckVersion(String url, String imei, String token, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", imei);
        param.put("token", token);
        return connectHttpPost(url, param, context);
    }

    public static String requestMasterPackage(String url, String version, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", "");
        param.put("version", version);
        return connectHttpPost(url, param, context);
    }

    public static String requestMasterPromo(String url, String version, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", "");
        param.put("version", version);
        return connectHttpPost(url, param, context);
    }

    public static String requestMasterMaterial(String url, String version, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", "");
        param.put("version", version);
        return connectHttpPost(url, param, context);
    }

    public static String requestMasterMs(String url, String version, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", "");
        param.put("version", version);
        return connectHttpPost(url, param, context);
    }


    public static String requestFtSoh(String url, String sfl, String token, String imei, String version, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", imei);
        param.put("token", token);
        param.put("version", version);
        return connectHttpPost(url, param, context);
    }

    public static String requestSaveProspect(String url, String imei, String token, String data,
                                             String version, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", imei);
        param.put("token", token);
        param.put("data", data);
        param.put("version", version);
        return connectHttpPost(url, param, context);
    }

    public static String requestFreeTrial(String url, String sflCode, String imei, String token, String username,
                                          int offset, String version, String signal, Context context) throws IOException {
        String page = String.valueOf(offset);
        HashMap<String, String> param = new HashMap<>();
        param.put("sflCode", sflCode);
        param.put("imei", imei);
        param.put("token", token);
        param.put("version", version);
        param.put("username", username);
        param.put("signal", signal);
        //param.put("offset", page);
        System.out.println("list: " + param.toString());
        return connectHttpPost(url, param, context);
    }

    public static String requestActivation(String url, String imei, String token, String ftId,
                                           String version, String signal, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", imei);
        param.put("token", token);
        param.put("version", version);
        //param.put("ftId", ftId);
        param.put("signal", signal);
        return connectHttpPost(url, param, context);
    }

    public static String requestDetailFreeTrial(String url, String imei, String token,
                                                String ftId, String sflCode, String offset, String version, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", imei);
        param.put("token", token);
        param.put("ftId", ftId);
        param.put("sflCode", sflCode);
        param.put("offset", offset);
        param.put("version", version);
        return connectHttpPost(url, param, context);
    }

    public static String requestPlan(String url, String imei, String token, String username, String
            version, String signal, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", imei);
        param.put("token", token);
        param.put("version", version);
        param.put("username", username);
        param.put("signal", signal);
        System.out.println("plan: " + param.toString());
        return connectHttpPost(url, param, context);
    }

    public static String requestToServer(String url, String imei, String token, String value,
                                         String planId, String version, String signal, String prefix,
                                         Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", imei);
        param.put("token", token);
        param.put("plan_id", planId);
        param.put("data", value);
        param.put("version", version);
        param.put("signal", signal);
        param.put("prefix", prefix);
        return connectHttpPost(url, param, context);
    }

    public static String postChunk(String url, String imei, String sequence, String filename, String md5File, String indexFile, String content, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", imei);
        param.put("sequence", sequence);
        param.put("filename", filename);
        param.put("md5File", md5File);
        param.put("indexFile", indexFile);
        param.put("content", content);
        return connectHttpPost(url, param, context);
    }

    public static String Survay(String url, String imei, String token, String data, String version, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", imei);
        param.put("token", token);
        param.put("data", data);
        param.put("version", version);
        return connectHttpPost(url, param, context);
    }

    public static String requestSurveyList(String url, String imei, String token, String version, Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", imei);
        param.put("token", token);
        param.put("version", version);
        return connectHttpPost(url, param, context);
    }

    public static String requestPushAbsen(String url, String entity_id, String employee_id,
                                          String attendance_type,
                                          String latitude,
                                          String longitude,
                                          String attendance_time,
                                          String jarak,
                                          Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("entity_id", entity_id);
        param.put("employee_id", employee_id);
        param.put("attendance_type", attendance_type);
        param.put("latitude", latitude);
        param.put("longitude", longitude);
        param.put("attendance_time", attendance_time);
        param.put("jarak", jarak);
        return connectHttpPost(url, param, context);
    }

    public static String requestListChecklist(String url, Context context) throws IOException {
        return connectHttpPost(url, "", context);
    }
}
