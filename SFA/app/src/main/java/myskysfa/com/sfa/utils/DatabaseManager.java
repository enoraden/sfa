package myskysfa.com.sfa.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import myskysfa.com.sfa.database.TableAbsensi;
import myskysfa.com.sfa.database.TableDataMSDTDFailed;
import myskysfa.com.sfa.database.TableFailedAbsen;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableFreeTrial;
import myskysfa.com.sfa.database.TableFreeTrialCoh;
import myskysfa.com.sfa.database.TableFreeTrialSurvay;
import myskysfa.com.sfa.database.TableFreeTrialSurvayList;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.TableMBrand;
import myskysfa.com.sfa.database.TableMHardwareStatus;
import myskysfa.com.sfa.database.TableMasterCatalog;
import myskysfa.com.sfa.database.TableMasterMS;
import myskysfa.com.sfa.database.TableMasterMaterial;
import myskysfa.com.sfa.database.TableMasterPackage;
import myskysfa.com.sfa.database.TableMasterPromo;
import myskysfa.com.sfa.database.TablePlan;
import myskysfa.com.sfa.database.TablePlanMS;
import myskysfa.com.sfa.database.TableZipcode;

/**
 * Created by admin on 6/10/2016.
 */
public class DatabaseManager extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "indovision_newsfa";
    private static final String DATABASE_PATH = "/data/data/myskysfa.com.sfa/databases/";

    // the DAO object we use to access the SimpleData table
    private Dao<TableLogLogin, Integer> tableLogLoginDAO = null;
    private Dao<TableFreeTrial, Integer> tableFreeTrialDAO = null;
    private Dao<TablePlan, Integer> tablePlanDAO = null;
    private Dao<TablePlanMS, Integer> tablePlanMSDAO = null;
    private Dao<TableFreeTrialSurvayList, Integer> tableFTSurvayDAO = null;
    private Dao<TableFreeTrialCoh, Integer> tableFTSOHDAO = null;
    private Dao<TableFreeTrialSurvay, Integer> tableFTSurvaysDAO = null;
    private Dao<TableMasterMaterial, Integer> tableMasterMaterialsDAO = null;
    private Dao<TableMasterMS, Integer> tableMasterMSDAO = null;
    private Dao<TableMasterPackage, Integer> tableMasterPackagesDAO = null;
    private Dao<TableMasterPromo, Integer> tableMasterPromoDAO = null;
    private Dao<TableMasterCatalog, Integer> tableMasterCatalogsDAO = null;
    private Dao<TableZipcode, Integer> tableZipcodeDAO = null;
    private Dao<TableMBrand, Integer> tableMBrandDAO = null;
    private Dao<TableMHardwareStatus, Integer> tableHWStatusDAO = null;
    private Dao<TableFormApp, Integer> tableFormAppsDAO = null;
    private Dao<TableAbsensi, Integer> tableAbsensiDAO = null;
    private Dao<TableFailedAbsen, Integer> failedAbsensDAO = null;
    private Dao<TableDataMSDTDFailed, Integer> failedAdaptersMsDtdDAO = null;

    public DatabaseManager(Context context) {
        super(context, DATABASE_PATH + DATABASE_NAME, null, Config.DATABASE_VERSION);

        boolean dbexist = checkdatabase();
        if (!dbexist) {

            // If database did not exist, try copying existing database from assets folder.
            try {
                File dir = new File(DATABASE_PATH);
                dir.mkdirs();
                InputStream myinput = context.getAssets().open(DATABASE_NAME);
                String outfilename = DATABASE_PATH + DATABASE_NAME;
                Log.i(DatabaseManager.class.getName(), "DB Path : " + outfilename);
                OutputStream myoutput = new FileOutputStream(outfilename);
                byte[] buffer = new byte[1024];
                int length;
                while ((length = myinput.read(buffer)) > 0) {
                    myoutput.write(buffer, 0, length);
                }
                myoutput.flush();
                myoutput.close();
                myinput.close();

                DatabaseManager db = new DatabaseManager(context);
                db.setUpgrade();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setUpgrade() {
        SQLiteDatabase db = getWritableDatabase();
        ConnectionSource cs = getConnectionSource();
        try {
            onUpgrade(db, cs, db.getVersion(), Config.DATABASE_VERSION + 1);
        } catch (SQLiteException e) {
            System.out.println("Database doesn't exist");
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, TableLogLogin.class);
            TableUtils.createTable(connectionSource, TableFreeTrial.class);
            TableUtils.createTable(connectionSource, TablePlan.class);
            TableUtils.createTable(connectionSource, TableFreeTrialSurvayList.class);
            TableUtils.createTable(connectionSource, TableFreeTrialCoh.class);
            TableUtils.createTable(connectionSource, TableFreeTrialSurvay.class);
            TableUtils.createTable(connectionSource, TableMasterMaterial.class);
            TableUtils.createTable(connectionSource, TableMasterMS.class);
            TableUtils.createTable(connectionSource, TableMasterPackage.class);
            TableUtils.createTable(connectionSource, TableMasterPromo.class);
            TableUtils.createTable(connectionSource, TableMasterCatalog.class);
            //TableUtils.createTable(connectionSource, TableMBrand.class);
            TableUtils.createTable(connectionSource, TableMHardwareStatus.class);
            TableUtils.createTable(connectionSource, TableFormApp.class);
            TableUtils.createTable(connectionSource, TablePlanMS.class);
            TableUtils.createTable(connectionSource, TableFailedAbsen.class);
            TableUtils.createTable(connectionSource, TableDataMSDTDFailed.class);
        } catch (Exception e) {
            Log.e("this", "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, TableLogLogin.class, true);
            TableUtils.dropTable(connectionSource, TableFreeTrial.class, true);
            TableUtils.dropTable(connectionSource, TablePlan.class, true);
            TableUtils.dropTable(connectionSource, TableFreeTrialSurvayList.class, true);
            TableUtils.dropTable(connectionSource, TableFreeTrialCoh.class, true);
            TableUtils.dropTable(connectionSource, TableFreeTrialSurvay.class, true);
            TableUtils.dropTable(connectionSource, TableMasterMaterial.class, true);
            TableUtils.dropTable(connectionSource, TableMasterMS.class, true);
            TableUtils.dropTable(connectionSource, TableMasterPackage.class, true);
            TableUtils.dropTable(connectionSource, TableMasterPromo.class, true);
            TableUtils.dropTable(connectionSource, TableMasterCatalog.class, true);
            //TableUtils.dropTable(connectionSource, TableMBrand.class, true);
            TableUtils.dropTable(connectionSource, TableMHardwareStatus.class, true);
            TableUtils.dropTable(connectionSource, TableFormApp.class, true);
            TableUtils.dropTable(connectionSource, TablePlanMS.class, true);
            TableUtils.dropTable(connectionSource, TableFailedAbsen.class, true);
            TableUtils.dropTable(connectionSource, TableDataMSDTDFailed.class, true);
            onCreate(sqLiteDatabase, connectionSource);
        } catch (SQLException e) {
            Log.e("this", "Can't create database", e);
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    * Check whether or not database exist
    */
    private boolean checkdatabase() {
        boolean checkdb = false;

        String myPath = DATABASE_PATH + DATABASE_NAME;
        File dbfile = new File(myPath);
        checkdb = dbfile.exists();

        Log.i(DatabaseManager.class.getName(), "DB Exist : " + checkdb);

        return checkdb;
    }

    public Dao<TableLogLogin, Integer> getTableLogLoginDAO() {
        if (null == tableLogLoginDAO) {
            try {
                tableLogLoginDAO = getDao(TableLogLogin.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableLogLoginDAO;
    }

    public Dao<TableFreeTrial, Integer> getTableFreeTrialDAO() {
        if (null == tableFreeTrialDAO) {
            try {
                tableFreeTrialDAO = getDao(TableFreeTrial.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableFreeTrialDAO;
    }

    public Dao<TablePlan, Integer> getTablePlanDAO() {
        if (null == tablePlanDAO) {
            try {
                tablePlanDAO = getDao(TablePlan.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tablePlanDAO;
    }

    public Dao<TablePlanMS, Integer> getTablePlanMSDAO() {
        if (null == tablePlanMSDAO) {
            try {
                tablePlanMSDAO = getDao(TablePlanMS.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tablePlanMSDAO;
    }

    public Dao<TableFreeTrialSurvayList, Integer> getTableFTSurvayDAO() {
        if (null == tableFTSurvayDAO) {
            try {
                tableFTSurvayDAO = getDao(TableFreeTrialSurvayList.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableFTSurvayDAO;
    }


    public Dao<TableFreeTrialCoh, Integer> getTableFTSOHDAO() {
        if (null == tableFTSOHDAO) {
            try {
                tableFTSOHDAO = getDao(TableFreeTrialCoh.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableFTSOHDAO;
    }

    public Dao<TableFreeTrialSurvay, Integer> getTableFTSurvaysDAO() {
        if (null == tableFTSurvaysDAO) {
            try {
                tableFTSurvaysDAO = getDao(TableFreeTrialSurvay.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableFTSurvaysDAO;
    }

    public Dao<TableMasterMaterial, Integer> getTableMasterMaterialsDAO() {
        if (null == tableMasterMaterialsDAO) {
            try {
                tableMasterMaterialsDAO = getDao(TableMasterMaterial.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return tableMasterMaterialsDAO;
    }

    public Dao<TableMasterMS, Integer> getTableMSDAO() {
        if (null == tableMasterMSDAO) {
            try {
                tableMasterMSDAO = getDao(TableMasterMS.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return tableMasterMSDAO;
    }

    public Dao<TableMasterPackage, Integer> getTablePackageDAO() {
        if (null == tableMasterPackagesDAO) {
            try {
                tableMasterPackagesDAO = getDao(TableMasterPackage.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return tableMasterPackagesDAO;
    }

    public Dao<TableMasterPromo, Integer> getTablePromoDAO() {
        if (null == tableMasterPromoDAO) {
            try {
                tableMasterPromoDAO = getDao(TableMasterPromo.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return tableMasterPromoDAO;
    }


    public Dao<TableMasterCatalog, Integer> getTableCatalogDAO() {
        if (null == tableMasterCatalogsDAO) {
            try {
                tableMasterCatalogsDAO = getDao(TableMasterCatalog.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return tableMasterCatalogsDAO;
    }

    public Dao<TableZipcode, Integer> getTableZipcodeDAO() {
        if (null == tableZipcodeDAO) {
            try {
                tableZipcodeDAO = getDao(TableZipcode.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableZipcodeDAO;
    }

    public Dao<TableMBrand, Integer> getTableBrandDAO() {
        if (null == tableMBrandDAO) {
            try {
                tableMBrandDAO = getDao(TableMBrand.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableMBrandDAO;
    }

    public Dao<TableMHardwareStatus, Integer> getTableHWStatusDAO() {
        if (null == tableHWStatusDAO) {
            try {
                tableHWStatusDAO = getDao(TableMHardwareStatus.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableHWStatusDAO;
    }

    public Dao<TableFormApp, Integer> getTableFormAppsDAODAO() {
        if (null == tableFormAppsDAO) {
            try {
                tableFormAppsDAO = getDao(TableFormApp.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableFormAppsDAO;
    }

    public Dao<TableAbsensi, Integer> getTableAbsensiDAODAO() {
        if (null == tableAbsensiDAO) {
            try {
                tableAbsensiDAO = getDao(TableAbsensi.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableAbsensiDAO;
    }

    public Dao<TableFailedAbsen, Integer> getFailedAbsensDAO() {
        if (null == failedAbsensDAO) {
            try {
                failedAbsensDAO = getDao(TableFailedAbsen.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return failedAbsensDAO;
    }

    public Dao<TableDataMSDTDFailed, Integer> getFailedAdaptersMsDtdDAO() {
        if (null == failedAdaptersMsDtdDAO) {
            try {
                failedAdaptersMsDtdDAO = getDao(TableDataMSDTDFailed.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return failedAdaptersMsDtdDAO;
    }


    /**
     * Close the database connections and clear any cached tableDAO.
     */
    @Override
    public void close() {
        super.close();
        tableLogLoginDAO = null;
        tableFreeTrialDAO = null;
        tablePlanDAO = null;
        tableFTSurvayDAO = null;
        tableFTSOHDAO = null;
        tableFTSurvaysDAO = null;
        tableMasterMaterialsDAO = null;
        tableMasterMSDAO = null;
        tableMasterPackagesDAO = null;
        tableMasterPromoDAO = null;
        tableMasterCatalogsDAO = null;
        tableZipcodeDAO = null;
        tableMBrandDAO = null;
        tableHWStatusDAO = null;
        tableFormAppsDAO = null;
        tablePlanMSDAO = null;
        failedAbsensDAO = null;
        failedAdaptersMsDtdDAO = null;

    }
}

