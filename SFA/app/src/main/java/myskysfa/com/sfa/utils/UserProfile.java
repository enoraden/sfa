package myskysfa.com.sfa.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;

import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableLogLoginAdapter;

/**
 * Created by admin on 1/6/2016.
 */
public class UserProfile {
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private List<TableLogLogin> listLogin;
    private String isLogin = "1";


    public void UserProfile(Context context) {

    }

    public void setActiveLogin(Context context) {
        TableLogLoginAdapter db = new TableLogLoginAdapter(context);
        listLogin = db.getDatabyCondition(TableLogLogin.C_IS_LOGIN, isLogin);

        mPreferences = context.getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();
        mEditor.putString(TableLogLogin.C_UID, listLogin.get(0).getcUid());
        mEditor.putString(TableLogLogin.C_USER_NAME, listLogin.get(0).getcUserName());
        mEditor.putString(TableLogLogin.C_EMPLOYEE_ID, listLogin.get(0).getcEmployeeId());
        mEditor.putString(TableLogLogin.C_FULL_NAME, listLogin.get(0).getcFullName());
        mEditor.putString(TableLogLogin.C_ROLE_CODE, listLogin.get(0).getcRoleCode());
        mEditor.putString(TableLogLogin.C_ROLE_NAME, listLogin.get(0).getcRoleName());
        mEditor.putString(TableLogLogin.C_IS_SUPERVISOR, listLogin.get(0).getcIsSupervisor());
        mEditor.putString(TableLogLogin.C_TOKEN, listLogin.get(0).getcToken());
        mEditor.putString(TableLogLogin.C_SFL_CODE, listLogin.get(0).getcSflCode());
        mEditor.putString(TableLogLogin.C_SFL_NAME, listLogin.get(0).getcSflName());
        mEditor.putString(TableLogLogin.C_LOG_DATE, listLogin.get(0).getcLogDate());
        mEditor.putString(TableLogLogin.C_IS_LOGIN, String.valueOf(listLogin.get(0).getcIsLogin()));
        mEditor.putString(TableLogLogin.C_PASSWORD, listLogin.get(0).getcPassword());
        mEditor.putString(TableLogLogin.C_BRAND, listLogin.get(0).getcBrand());
        mEditor.putString(TableLogLogin.C_BRANCH, listLogin.get(0).getcBranch());
        mEditor.putString(TableLogLogin.C_BRANCH_ID, listLogin.get(0).getcBranchId());
        mEditor.putString(TableLogLogin.C_USERTYPE, listLogin.get(0).getcUsertype());
        mEditor.putString(TableLogLogin.C_REGIONCODE, listLogin.get(0).getcRegioncode());
        mEditor.putString(TableLogLogin.C_REGIONNAME, listLogin.get(0).getcRegionname());
        mEditor.putString(TableLogLogin.C_NIK, listLogin.get(0).getNik());
        mEditor.commit();

    }

    public void clearActiveLogin(Context context) {
        mPreferences = context.getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();
        mEditor.clear();
        mEditor.commit();
        mPreferences = context.getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
    }

    }
