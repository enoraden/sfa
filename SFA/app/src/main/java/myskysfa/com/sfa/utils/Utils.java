package myskysfa.com.sfa.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.File;
import java.io.FileInputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.msebera.android.httpclient.conn.util.InetAddressUtils;
import myskysfa.com.sfa.R;
import myskysfa.com.sfa.main.SplashActivity;

import static android.content.Context.WIFI_SERVICE;

/**
 * Created by Hari Hendryan on 08/09/2015.
 */
public class Utils {
    private Context _context;
    private String longitude, latitude, addressLine = "", deviceID = "";
    private MyPhoneStateListener listenerSignal;

    public Utils(Context context) {
        _context = context;
    }

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private TimePickerDialog timePickerDialog;

    /*
   |-----------------------------------------------------------------------------------------------
   | Dialog for alert no internet connection
   |-----------------------------------------------------------------------------------------------
   */
    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                new ContextThemeWrapper(context,
                        android.R.style.Theme_Dialog));
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("Close",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.setIcon(android.R.drawable.stat_sys_warning);
        alert.show();
    }

    /*
   |-----------------------------------------------------------------------------------------------
   | Dialog for alert request failed
   |-----------------------------------------------------------------------------------------------
   */
    public void showErrorDlg(Handler handler, String message, Context context) {
        final String strMessage = message;
        final Context mContext = context;
        handler.post(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        new ContextThemeWrapper(mContext,
                                android.R.style.Theme_Dialog));
                builder.setMessage(strMessage)
                        .setCancelable(false)
                        .setNegativeButton("close", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.setIcon(android.R.drawable.stat_sys_warning);
                alertDialog.show();
            }
        });
    }

    public void showEditOwnDate(Activity activity, String title, final TextView textView,
                                Boolean activeCheckBox) {
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setCancelable(true);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_date_editor, null);
        final EditText day, month, year;
        final TextView titleDate;
        final CheckBox foreverID;

        day = (EditText) dialogView.findViewById(R.id.day);
        month = (EditText) dialogView.findViewById(R.id.month);
        year = (EditText) dialogView.findViewById(R.id.year);
        titleDate = (TextView) dialogView.findViewById(R.id.titleDate);
        foreverID = (CheckBox) dialogView.findViewById(R.id.foreverID);

        if (activeCheckBox) {
            foreverID.setVisibility(View.VISIBLE);
        } else {
            foreverID.setVisibility(View.GONE);
        }
        foreverID.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    day.setText("30");
                    month.setText("12");
                    year.setText("9999");
                } else {
                    day.setText("");
                    month.setText("");
                    year.setText("");
                }

            }
        });
        titleDate.setText(title);
        builder.setView(dialogView)
                .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (day.getText().toString().equalsIgnoreCase("")
                                || month.getText().toString().equalsIgnoreCase("")
                                || year.getText().toString().equalsIgnoreCase("")) {
                            textView.setText("");
                        } else {
                            if (day.getText().toString().trim().length() < 2
                                    || month.getText().toString().trim().length() < 2
                                    || year.getText().toString().trim().length() < 4
                                    || Integer.parseInt(day.getText().toString()) > 31
                                    || Integer.parseInt(month.getText().toString()) > 12
                                    || Integer.parseInt(year.getText().toString()) < 1900) {

                                textView.setText("");
                                Toast.makeText(_context, "Format salah, periksa kembali!", Toast.LENGTH_SHORT).show();

                            } else {
                                textView.setText(day.getText().toString() + "/" + month.getText().toString()
                                        + "/" + year.getText().toString());
                            }
                        }
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /*
   |-----------------------------------------------------------------------------------------------
   | Method for Location
   |-----------------------------------------------------------------------------------------------
   */
    public void setGeoLocation() {
        try {
            GPSTracker gpsTracker = new GPSTracker(_context);
            if (gpsTracker.getIsGPSTrackingEnabled()) {
                latitude = String.valueOf(gpsTracker.latitude);
                longitude = String.valueOf(gpsTracker.longitude);
                addressLine = gpsTracker.getAddressLine(_context);
            } else {
                gpsTracker.showSettingsAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getLongitude() {
        if (this.longitude != null) {
            return this.longitude;
        }
        return "";
    }

    public String getLatitude() {
        if (this.latitude != null) {
            return this.latitude;
        }
        return "";
    }

    public String getAddressLine() {
        if (this.addressLine != null) {
            return this.addressLine;
        }
        return "";
    }

    /**
     * Method to get current date
     */
    public String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String getCurrentDateandTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String getCurrentDateandTimeSecond() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String getCurrentDateandTimeMerge() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmm");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String getCurrentDateMerge() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String getCurrentDateandTimeMergeReverse(String dateParam) {
        DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        Date date = null;
        String finalString = null;
        try {
            date = formatter.parse(dateParam);
            SimpleDateFormat newFormat = new SimpleDateFormat("yyyyMMddHHmm");
            finalString = newFormat.format(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return finalString;
    }

    public String getCurrentDateMergeReverse(String dateParam) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = null;
        String finalString = null;
        try {
            date = formatter.parse(dateParam);
            SimpleDateFormat newFormat = new SimpleDateFormat("yyyyMMdd");
            finalString = newFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return finalString;
    }

    public String changeFormatDate(String dateParam) {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        String finalString = null;
        try {
            date = formatter.parse(dateParam);
            SimpleDateFormat newFormat = new SimpleDateFormat("yyyyMMdd");
            finalString = newFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return finalString;
    }

    public Calendar getAfterCurrentOneDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, +0);

        return null;
    }

    public static Date formatStringToDate(String timeStamp) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = df.parse(timeStamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    /**
     * Method to convert format date from yyyy-MM-dd to dd/MM/yyyy
     */
    public static String formatDateReverse(String date) throws ParseException {
        String initDateFormat;
        String endDateFormat;
        initDateFormat = "yyyy-MM-dd";
        endDateFormat = "dd/MM/yyyy";
        Date initDate = new SimpleDateFormat(initDateFormat).parse(date);
        SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
        String parsedDate = formatter.format(initDate);

        return parsedDate;
    }

    public static String formatDateTimeReverse(String date) throws ParseException {
        String initDateFormat;
        String endDateFormat;
        initDateFormat = "yyyy-MM-dd HH:mm";
        endDateFormat = "dd-MM-yyyy HH:mm";
        Date initDate = new SimpleDateFormat(initDateFormat).parse(date);
        SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
        String parsedDate = formatter.format(initDate);

        return parsedDate;
    }

    /**
     * Method to convert format date from dd/MM/yyyy to yyyy-MM-dd
     */
    public static String formatDate(String date) throws ParseException {
        String initDateFormat;
        String endDateFormat;
        initDateFormat = "yyyy-MM-dd HH:mm";
        endDateFormat = "dd/MM/yyyy HH:mm:ss";
        Date initDate = new SimpleDateFormat(initDateFormat).parse(date);
        SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
        String parsedDate = formatter.format(initDate);

        return parsedDate;
    }

    /**
     * Method for set time field
     */
    public void setTimeField(Context context, TextView textView) {
        final TextView time = textView;
        Calendar newCalendar = Calendar.getInstance();
        timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minuteOfDay) {
                time.setText(hourOfDay + ":" + minuteOfDay);
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }

    /**
     * Method for set date field
     */
    public void setDateField(Context context, TextView textView) {
        final TextView date = textView;
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        final SimpleDateFormat simpleDateFormat = dateFormatter;

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                date.setText(simpleDateFormat.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }


    /**
     * Method for set date field
     */
    public void setDateTimeField(Context context, TextView textView) {
        final TextView date = textView;
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);
        final SimpleDateFormat simpleDateFormat = dateFormatter;

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                date.setText(simpleDateFormat.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    /**
     * Method for set time field
     */
    public void setTimeField(Context context, TimePickerDialog timePickerDialog, TextView textView) {
        final TextView time = textView;
        Calendar newCalendar = Calendar.getInstance();
        timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minuteOfDay) {
                time.setText(hourOfDay + ":" + minuteOfDay);
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }

    /**
     * Method for set date field
     */
    public void setDateField(Context context, DatePickerDialog datePickerDialog, SimpleDateFormat dateFormatter,
                             TextView textView) {
        final TextView date = textView;
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        final SimpleDateFormat simpleDateFormat = dateFormatter;

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                date.setText(simpleDateFormat.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    /**
     * Part of Floating Button library
     */
    public static int dpToPx(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return Math.round(dp * scale);
    }

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Email Validator
     */

    public class EmailValidator {
        private Pattern pattern;
        private Matcher matcher;

        private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        public EmailValidator() {
            pattern = Pattern.compile(EMAIL_PATTERN);
        }

        public boolean validate(final String hex) {
            matcher = pattern.matcher(hex);
            return matcher.matches();
        }
    }

    public boolean isMyServiceRunning(Class<?> serviceClass1) {
        ActivityManager manager = (ActivityManager) _context.getSystemService(_context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass1.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Count distance between two coordinate
     */
    public static int distance(LatLng StartP, LatLng EndP) {
        double earthRadius = 6371000; //meters
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lng1 = StartP.longitude;
        double lng2 = EndP.longitude;

        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        int dist = (int) (earthRadius * c);
        return dist;
    }

    public static void midPoint(LatLng StartP, LatLng EndP) {

        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;

        double dLon = Math.toRadians(lon2 - lon1);

        //convert to radians
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);
        lon1 = Math.toRadians(lon1);

        double Bx = Math.cos(lat2) * Math.cos(dLon);
        double By = Math.cos(lat2) * Math.sin(dLon);
        double lat3 = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By * By));
        double lon3 = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);
        //print out in degrees
        System.out.println(Math.toDegrees(lat3) + " " + Math.toDegrees(lon3));
        Log.d("Utils", "lat= " + lat3 + " lon= " + lon3 + " lat2= " + Math.toDegrees(lat3) + " lon2= " + Math.toDegrees(lon3));
    }

    /**
     * Method for scale bitmap
     */
    public static Bitmap decodeBitmapFromFile(String filePath,
                                              int reqWidth, int reqHeight) {

        Bitmap bm = null;
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(filePath, options);
        return bm;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeFile(File f, int WIDTH, int HIGHT) {
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH = WIDTH;
            final int REQUIRED_HIGHT = HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Method for generate file name for image
     */
    public String setImageName(String formNumber, String type, String imageName) {
        String name = "";
        name = formNumber + "_" + type + "_" + imageName;
        return name;
    }

    public void setIMEI() {
        final TelephonyManager tm = (TelephonyManager) _context.getSystemService(Context.TELEPHONY_SERVICE);
        deviceID = tm.getDeviceId();
        SharedPreferences imeiPref = _context.getSharedPreferences(Config.KEY_IMEI, Context.MODE_PRIVATE);
        SharedPreferences.Editor shareEditor = imeiPref.edit();
        shareEditor.putString(Config.KEY_DEVICE_ID, deviceID);
        shareEditor.commit();
    }

    public String getIMEI() {
        SharedPreferences sessionpref = _context.getSharedPreferences(Config.KEY_IMEI, Context.MODE_PRIVATE);
        deviceID = sessionpref.getString(Config.KEY_DEVICE_ID, "");
        return deviceID;
    }

    public int checkSignal(Context context) {
        listenerSignal = new MyPhoneStateListener();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(listenerSignal, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        return listenerSignal.setSignal();
    }

    public boolean findStringImage(String source, String keyword) {
        Boolean found = Arrays.asList(source.split("_")).contains(keyword);
        if (found) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void setHideKeyboard(Context _context, View view) {
        if (view != null) {
            //hide Keyboard
            InputMethodManager imm = (InputMethodManager) _context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String getDeviceIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : networkInterfaces) {
                List<InetAddress> inetAddresses = Collections.list(networkInterface.getInetAddresses());
                for (InetAddress inetAddress : inetAddresses) {
                    if (!inetAddress.isLoopbackAddress()) {
                        String sAddr = inetAddress.getHostAddress().toUpperCase();
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                // drop ip6 port suffix
                                int delim = sAddr.indexOf('%');
                                return delim < 0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }
}
